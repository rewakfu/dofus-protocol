package tofus.protocol

import scala.annotation.tailrec
import scodec.bits.ByteVector

object Varint {

  val MSB              = 0x80
  val LOW_7_BITS       = 0x7F
  val SHORT_REST_BITES = 0xFF80
  val INT_REST_BITES   = 0xFFFFFF80
  val LONG_REST_BITES  = 0xFFFFFFFFFFFFFF80L

  def encodeShort(num: Short): ByteVector = {
    @tailrec
    def rec(value: Short, acc: ByteVector): ByteVector =
      if ((value & SHORT_REST_BITES) == 0) acc :+ (value & LOW_7_BITS).toByte
      else rec((value >>> 7).toShort, acc :+ ((value & LOW_7_BITS) | MSB).toByte)

    rec(num, ByteVector.empty)
  }

  def encodeInt(num: Int): ByteVector = {
    @tailrec
    def rec(value: Int, acc: ByteVector): ByteVector =
      if ((value & INT_REST_BITES) == 0) acc :+ (value & LOW_7_BITS).toByte
      else rec(value >>> 7, acc :+ ((value & LOW_7_BITS) | MSB).toByte)

    rec(num, ByteVector.empty)
  }

  def encodeLong(num: Long): ByteVector = {
    @tailrec
    def rec(value: Long, acc: ByteVector): ByteVector =
      if ((value & LONG_REST_BITES) == 0) acc :+ (value & LOW_7_BITS).toByte
      else rec(value >>> 7, acc :+ ((value & LOW_7_BITS) | MSB).toByte)

    rec(num, ByteVector.empty)
  }

  def decodeToShort(bytes: ByteVector): (Short, ByteVector) = {
    @tailrec
    def rec(slice: ByteVector, shift: Short, acc: Short): (Short, ByteVector) =
      if (slice.isEmpty) throw new IllegalArgumentException("Cannot find the ending Byte.")
      else if ((slice.head & MSB) == 0) ((acc | (slice.head << shift)).toShort, slice.tail)
      else rec(slice.tail, (shift + 7).toShort, (acc | ((slice.head & LOW_7_BITS) << shift)).toShort)

    rec(bytes, 0, 0)
  }

  def decodeToInt(bytes: ByteVector): (Int, ByteVector) = {
    @tailrec
    def rec(slice: ByteVector, shift: Int, acc: Int): (Int, ByteVector) =
      if (slice.isEmpty) throw new IllegalArgumentException("Cannot find the ending Byte.")
      else if ((slice.head & MSB) == 0) (acc | (slice.head << shift), slice.tail)
      else rec(slice.tail, shift + 7, acc | ((slice.head & LOW_7_BITS) << shift))

    rec(bytes, 0, 0)
  }

  def decodeToLong(bytes: ByteVector): (Long, ByteVector) = {
    @tailrec
    def rec(slice: ByteVector, shift: Long, acc: Long): (Long, ByteVector) =
      if (slice.isEmpty) throw new IllegalArgumentException("Cannot find the ending Byte.")
      else if ((slice.head & MSB) == 0) (acc | (slice.head.toLong << shift), slice.tail)
      else rec(slice.tail, shift + 7, acc | ((slice.head.toLong & LOW_7_BITS) << shift))

    rec(bytes, 0L, 0L)
  }

  def extractLength(bytes: ByteVector, offset: Int): Int = {
    @tailrec
    def rec(index: Int): Int =
      if (index >= bytes.length) 0
      else if ((bytes(index) & MSB) == 0) index + 1 - offset
      else rec(index + 1)

    rec(offset)
  }

}
