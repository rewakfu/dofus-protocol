package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetSavedMessage(
  presetId: Short,
  preset: Preset
) extends Message(6763)

object PresetSavedMessage {
  implicit val codec: Codec[PresetSavedMessage] =
    new Codec[PresetSavedMessage] {
      def decode: Get[PresetSavedMessage] =
        for {
          presetId <- short.decode
          preset <- Codec[Preset].decode
        } yield PresetSavedMessage(presetId, preset)

      def encode(value: PresetSavedMessage): ByteVector =
        short.encode(value.presetId) ++
        Codec[Preset].encode(value.preset)
    }
}
