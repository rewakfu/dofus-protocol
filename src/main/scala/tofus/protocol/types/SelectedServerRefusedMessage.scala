package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SelectedServerRefusedMessage(
  serverId: Short,
  error: Byte,
  serverStatus: Byte
) extends Message(41)

object SelectedServerRefusedMessage {
  implicit val codec: Codec[SelectedServerRefusedMessage] =
    new Codec[SelectedServerRefusedMessage] {
      def decode: Get[SelectedServerRefusedMessage] =
        for {
          serverId <- varShort.decode
          error <- byte.decode
          serverStatus <- byte.decode
        } yield SelectedServerRefusedMessage(serverId, error, serverStatus)

      def encode(value: SelectedServerRefusedMessage): ByteVector =
        varShort.encode(value.serverId) ++
        byte.encode(value.error) ++
        byte.encode(value.serverStatus)
    }
}
