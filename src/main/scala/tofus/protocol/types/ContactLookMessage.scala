package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ContactLookMessage(
  requestId: Int,
  playerName: String,
  playerId: Long,
  look: EntityLook
) extends Message(5934)

object ContactLookMessage {
  implicit val codec: Codec[ContactLookMessage] =
    new Codec[ContactLookMessage] {
      def decode: Get[ContactLookMessage] =
        for {
          requestId <- varInt.decode
          playerName <- utf8(ushort).decode
          playerId <- varLong.decode
          look <- Codec[EntityLook].decode
        } yield ContactLookMessage(requestId, playerName, playerId, look)

      def encode(value: ContactLookMessage): ByteVector =
        varInt.encode(value.requestId) ++
        utf8(ushort).encode(value.playerName) ++
        varLong.encode(value.playerId) ++
        Codec[EntityLook].encode(value.look)
    }
}
