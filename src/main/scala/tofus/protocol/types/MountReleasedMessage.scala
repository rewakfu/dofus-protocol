package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountReleasedMessage(
  mountId: Int
) extends Message(6308)

object MountReleasedMessage {
  implicit val codec: Codec[MountReleasedMessage] =
    new Codec[MountReleasedMessage] {
      def decode: Get[MountReleasedMessage] =
        for {
          mountId <- varInt.decode
        } yield MountReleasedMessage(mountId)

      def encode(value: MountReleasedMessage): ByteVector =
        varInt.encode(value.mountId)
    }
}
