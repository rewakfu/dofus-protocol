package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ReloginTokenRequestMessage(

) extends Message(6540)

object ReloginTokenRequestMessage {
  implicit val codec: Codec[ReloginTokenRequestMessage] =
    Codec.const(ReloginTokenRequestMessage())
}
