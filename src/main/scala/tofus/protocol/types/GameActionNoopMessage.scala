package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionNoopMessage(

) extends Message(1002)

object GameActionNoopMessage {
  implicit val codec: Codec[GameActionNoopMessage] =
    Codec.const(GameActionNoopMessage())
}
