package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyLocateMembersMessage(
  partyId: Int,
  geopositions: List[PartyMemberGeoPosition]
) extends Message(5595)

object PartyLocateMembersMessage {
  implicit val codec: Codec[PartyLocateMembersMessage] =
    new Codec[PartyLocateMembersMessage] {
      def decode: Get[PartyLocateMembersMessage] =
        for {
          partyId <- varInt.decode
          geopositions <- list(ushort, Codec[PartyMemberGeoPosition]).decode
        } yield PartyLocateMembersMessage(partyId, geopositions)

      def encode(value: PartyLocateMembersMessage): ByteVector =
        varInt.encode(value.partyId) ++
        list(ushort, Codec[PartyMemberGeoPosition]).encode(value.geopositions)
    }
}
