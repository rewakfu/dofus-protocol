package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInvitedMessage(
  recruterId: Long,
  recruterName: String,
  guildInfo: ConcreteBasicGuildInformations
) extends Message(5552)

object GuildInvitedMessage {
  implicit val codec: Codec[GuildInvitedMessage] =
    new Codec[GuildInvitedMessage] {
      def decode: Get[GuildInvitedMessage] =
        for {
          recruterId <- varLong.decode
          recruterName <- utf8(ushort).decode
          guildInfo <- Codec[ConcreteBasicGuildInformations].decode
        } yield GuildInvitedMessage(recruterId, recruterName, guildInfo)

      def encode(value: GuildInvitedMessage): ByteVector =
        varLong.encode(value.recruterId) ++
        utf8(ushort).encode(value.recruterName) ++
        Codec[ConcreteBasicGuildInformations].encode(value.guildInfo)
    }
}
