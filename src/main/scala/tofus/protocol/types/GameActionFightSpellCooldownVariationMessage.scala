package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightSpellCooldownVariationMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  spellId: Short,
  value: Short
) extends Message(6219)

object GameActionFightSpellCooldownVariationMessage {
  implicit val codec: Codec[GameActionFightSpellCooldownVariationMessage] =
    new Codec[GameActionFightSpellCooldownVariationMessage] {
      def decode: Get[GameActionFightSpellCooldownVariationMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          spellId <- varShort.decode
          value <- varShort.decode
        } yield GameActionFightSpellCooldownVariationMessage(actionId, sourceId, targetId, spellId, value)

      def encode(value: GameActionFightSpellCooldownVariationMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varShort.encode(value.spellId) ++
        varShort.encode(value.value)
    }
}
