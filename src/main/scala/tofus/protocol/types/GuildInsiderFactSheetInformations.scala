package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInsiderFactSheetInformations(
  guildId: Int,
  guildName: String,
  guildLevel: Short,
  guildEmblem: GuildEmblem,
  leaderId: Long,
  nbMembers: Short,
  leaderName: String,
  nbConnectedMembers: Short,
  nbTaxCollectors: Byte,
  lastActivity: Int
) extends GuildFactSheetInformations {
  override val protocolId = 423
}

object GuildInsiderFactSheetInformations {
  implicit val codec: Codec[GuildInsiderFactSheetInformations] =
    new Codec[GuildInsiderFactSheetInformations] {
      def decode: Get[GuildInsiderFactSheetInformations] =
        for {
          guildId <- varInt.decode
          guildName <- utf8(ushort).decode
          guildLevel <- ubyte.decode
          guildEmblem <- Codec[GuildEmblem].decode
          leaderId <- varLong.decode
          nbMembers <- varShort.decode
          leaderName <- utf8(ushort).decode
          nbConnectedMembers <- varShort.decode
          nbTaxCollectors <- byte.decode
          lastActivity <- int.decode
        } yield GuildInsiderFactSheetInformations(guildId, guildName, guildLevel, guildEmblem, leaderId, nbMembers, leaderName, nbConnectedMembers, nbTaxCollectors, lastActivity)

      def encode(value: GuildInsiderFactSheetInformations): ByteVector =
        varInt.encode(value.guildId) ++
        utf8(ushort).encode(value.guildName) ++
        ubyte.encode(value.guildLevel) ++
        Codec[GuildEmblem].encode(value.guildEmblem) ++
        varLong.encode(value.leaderId) ++
        varShort.encode(value.nbMembers) ++
        utf8(ushort).encode(value.leaderName) ++
        varShort.encode(value.nbConnectedMembers) ++
        byte.encode(value.nbTaxCollectors) ++
        int.encode(value.lastActivity)
    }
}
