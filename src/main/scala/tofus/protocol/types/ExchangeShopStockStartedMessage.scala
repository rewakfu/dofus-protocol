package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeShopStockStartedMessage(
  objectsInfos: List[ConcreteObjectItemToSell]
) extends Message(5910)

object ExchangeShopStockStartedMessage {
  implicit val codec: Codec[ExchangeShopStockStartedMessage] =
    new Codec[ExchangeShopStockStartedMessage] {
      def decode: Get[ExchangeShopStockStartedMessage] =
        for {
          objectsInfos <- list(ushort, Codec[ConcreteObjectItemToSell]).decode
        } yield ExchangeShopStockStartedMessage(objectsInfos)

      def encode(value: ExchangeShopStockStartedMessage): ByteVector =
        list(ushort, Codec[ConcreteObjectItemToSell]).encode(value.objectsInfos)
    }
}
