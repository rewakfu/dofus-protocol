package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SubscriptionLimitationMessage(
  reason: Byte
) extends Message(5542)

object SubscriptionLimitationMessage {
  implicit val codec: Codec[SubscriptionLimitationMessage] =
    new Codec[SubscriptionLimitationMessage] {
      def decode: Get[SubscriptionLimitationMessage] =
        for {
          reason <- byte.decode
        } yield SubscriptionLimitationMessage(reason)

      def encode(value: SubscriptionLimitationMessage): ByteVector =
        byte.encode(value.reason)
    }
}
