package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectMount(
  actionId: Short,
  flags0: Byte,
  id: Long,
  expirationDate: Long,
  model: Int,
  name: String,
  owner: String,
  level: Byte,
  reproductionCount: Int,
  reproductionCountMax: Int,
  effects: List[ObjectEffectInteger],
  capacities: List[Int]
) extends ObjectEffect {
  override val protocolId = 179
}

object ObjectEffectMount {
  implicit val codec: Codec[ObjectEffectMount] =
    new Codec[ObjectEffectMount] {
      def decode: Get[ObjectEffectMount] =
        for {
          actionId <- varShort.decode
          flags0 <- byte.decode
          id <- varLong.decode
          expirationDate <- varLong.decode
          model <- varInt.decode
          name <- utf8(ushort).decode
          owner <- utf8(ushort).decode
          level <- byte.decode
          reproductionCount <- varInt.decode
          reproductionCountMax <- varInt.decode
          effects <- list(ushort, Codec[ObjectEffectInteger]).decode
          capacities <- list(ushort, varInt).decode
        } yield ObjectEffectMount(actionId, flags0, id, expirationDate, model, name, owner, level, reproductionCount, reproductionCountMax, effects, capacities)

      def encode(value: ObjectEffectMount): ByteVector =
        varShort.encode(value.actionId) ++
        byte.encode(value.flags0) ++
        varLong.encode(value.id) ++
        varLong.encode(value.expirationDate) ++
        varInt.encode(value.model) ++
        utf8(ushort).encode(value.name) ++
        utf8(ushort).encode(value.owner) ++
        byte.encode(value.level) ++
        varInt.encode(value.reproductionCount) ++
        varInt.encode(value.reproductionCountMax) ++
        list(ushort, Codec[ObjectEffectInteger]).encode(value.effects) ++
        list(ushort, varInt).encode(value.capacities)
    }
}
