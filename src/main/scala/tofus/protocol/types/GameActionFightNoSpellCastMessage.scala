package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightNoSpellCastMessage(
  spellLevelId: Int
) extends Message(6132)

object GameActionFightNoSpellCastMessage {
  implicit val codec: Codec[GameActionFightNoSpellCastMessage] =
    new Codec[GameActionFightNoSpellCastMessage] {
      def decode: Get[GameActionFightNoSpellCastMessage] =
        for {
          spellLevelId <- varInt.decode
        } yield GameActionFightNoSpellCastMessage(spellLevelId)

      def encode(value: GameActionFightNoSpellCastMessage): ByteVector =
        varInt.encode(value.spellLevelId)
    }
}
