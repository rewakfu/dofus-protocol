package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInAllianceInformations(
  guildId: Int,
  guildName: String,
  guildLevel: Short,
  guildEmblem: GuildEmblem,
  nbMembers: Short,
  joinDate: Int
) extends GuildInformations {
  override val protocolId = 420
}

object GuildInAllianceInformations {
  implicit val codec: Codec[GuildInAllianceInformations] =
    new Codec[GuildInAllianceInformations] {
      def decode: Get[GuildInAllianceInformations] =
        for {
          guildId <- varInt.decode
          guildName <- utf8(ushort).decode
          guildLevel <- ubyte.decode
          guildEmblem <- Codec[GuildEmblem].decode
          nbMembers <- ubyte.decode
          joinDate <- int.decode
        } yield GuildInAllianceInformations(guildId, guildName, guildLevel, guildEmblem, nbMembers, joinDate)

      def encode(value: GuildInAllianceInformations): ByteVector =
        varInt.encode(value.guildId) ++
        utf8(ushort).encode(value.guildName) ++
        ubyte.encode(value.guildLevel) ++
        Codec[GuildEmblem].encode(value.guildEmblem) ++
        ubyte.encode(value.nbMembers) ++
        int.encode(value.joinDate)
    }
}
