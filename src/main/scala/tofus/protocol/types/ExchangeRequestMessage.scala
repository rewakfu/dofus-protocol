package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeRequestMessage(
  exchangeType: Byte
) extends Message(5505)

object ExchangeRequestMessage {
  implicit val codec: Codec[ExchangeRequestMessage] =
    new Codec[ExchangeRequestMessage] {
      def decode: Get[ExchangeRequestMessage] =
        for {
          exchangeType <- byte.decode
        } yield ExchangeRequestMessage(exchangeType)

      def encode(value: ExchangeRequestMessage): ByteVector =
        byte.encode(value.exchangeType)
    }
}
