package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LifePointsRegenEndMessage(
  lifePoints: Int,
  maxLifePoints: Int,
  lifePointsGained: Int
) extends Message(5686)

object LifePointsRegenEndMessage {
  implicit val codec: Codec[LifePointsRegenEndMessage] =
    new Codec[LifePointsRegenEndMessage] {
      def decode: Get[LifePointsRegenEndMessage] =
        for {
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
          lifePointsGained <- varInt.decode
        } yield LifePointsRegenEndMessage(lifePoints, maxLifePoints, lifePointsGained)

      def encode(value: LifePointsRegenEndMessage): ByteVector =
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints) ++
        varInt.encode(value.lifePointsGained)
    }
}
