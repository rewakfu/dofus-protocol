package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyCannotJoinErrorMessage(
  partyId: Int,
  reason: Byte
) extends Message(5583)

object PartyCannotJoinErrorMessage {
  implicit val codec: Codec[PartyCannotJoinErrorMessage] =
    new Codec[PartyCannotJoinErrorMessage] {
      def decode: Get[PartyCannotJoinErrorMessage] =
        for {
          partyId <- varInt.decode
          reason <- byte.decode
        } yield PartyCannotJoinErrorMessage(partyId, reason)

      def encode(value: PartyCannotJoinErrorMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.reason)
    }
}
