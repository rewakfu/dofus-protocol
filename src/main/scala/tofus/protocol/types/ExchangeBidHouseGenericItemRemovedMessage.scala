package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseGenericItemRemovedMessage(
  objGenericId: Short
) extends Message(5948)

object ExchangeBidHouseGenericItemRemovedMessage {
  implicit val codec: Codec[ExchangeBidHouseGenericItemRemovedMessage] =
    new Codec[ExchangeBidHouseGenericItemRemovedMessage] {
      def decode: Get[ExchangeBidHouseGenericItemRemovedMessage] =
        for {
          objGenericId <- varShort.decode
        } yield ExchangeBidHouseGenericItemRemovedMessage(objGenericId)

      def encode(value: ExchangeBidHouseGenericItemRemovedMessage): ByteVector =
        varShort.encode(value.objGenericId)
    }
}
