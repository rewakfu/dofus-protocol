package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightReflectDamagesMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double
) extends Message(5530)

object GameActionFightReflectDamagesMessage {
  implicit val codec: Codec[GameActionFightReflectDamagesMessage] =
    new Codec[GameActionFightReflectDamagesMessage] {
      def decode: Get[GameActionFightReflectDamagesMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
        } yield GameActionFightReflectDamagesMessage(actionId, sourceId, targetId)

      def encode(value: GameActionFightReflectDamagesMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId)
    }
}
