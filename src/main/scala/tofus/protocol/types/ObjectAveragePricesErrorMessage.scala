package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectAveragePricesErrorMessage(

) extends Message(6336)

object ObjectAveragePricesErrorMessage {
  implicit val codec: Codec[ObjectAveragePricesErrorMessage] =
    Codec.const(ObjectAveragePricesErrorMessage())
}
