package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseBuyResultMessage(
  flags0: Byte,
  houseId: Int,
  instanceId: Int,
  realPrice: Long
) extends Message(5735)

object HouseBuyResultMessage {
  implicit val codec: Codec[HouseBuyResultMessage] =
    new Codec[HouseBuyResultMessage] {
      def decode: Get[HouseBuyResultMessage] =
        for {
          flags0 <- byte.decode
          houseId <- varInt.decode
          instanceId <- int.decode
          realPrice <- varLong.decode
        } yield HouseBuyResultMessage(flags0, houseId, instanceId, realPrice)

      def encode(value: HouseBuyResultMessage): ByteVector =
        byte.encode(value.flags0) ++
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId) ++
        varLong.encode(value.realPrice)
    }
}
