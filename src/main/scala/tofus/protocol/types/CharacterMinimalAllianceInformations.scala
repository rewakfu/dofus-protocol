package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterMinimalAllianceInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte,
  guild: ConcreteBasicGuildInformations,
  alliance: ConcreteBasicAllianceInformations
) extends CharacterMinimalGuildInformations {
  override val protocolId = 444
}

object CharacterMinimalAllianceInformations {
  implicit val codec: Codec[CharacterMinimalAllianceInformations] =
    new Codec[CharacterMinimalAllianceInformations] {
      def decode: Get[CharacterMinimalAllianceInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
          guild <- Codec[ConcreteBasicGuildInformations].decode
          alliance <- Codec[ConcreteBasicAllianceInformations].decode
        } yield CharacterMinimalAllianceInformations(id, name, level, entityLook, breed, guild, alliance)

      def encode(value: CharacterMinimalAllianceInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed) ++
        Codec[ConcreteBasicGuildInformations].encode(value.guild) ++
        Codec[ConcreteBasicAllianceInformations].encode(value.alliance)
    }
}
