package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightModifyEffectsDurationMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  delta: Short
) extends Message(6304)

object GameActionFightModifyEffectsDurationMessage {
  implicit val codec: Codec[GameActionFightModifyEffectsDurationMessage] =
    new Codec[GameActionFightModifyEffectsDurationMessage] {
      def decode: Get[GameActionFightModifyEffectsDurationMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          delta <- short.decode
        } yield GameActionFightModifyEffectsDurationMessage(actionId, sourceId, targetId, delta)

      def encode(value: GameActionFightModifyEffectsDurationMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.delta)
    }
}
