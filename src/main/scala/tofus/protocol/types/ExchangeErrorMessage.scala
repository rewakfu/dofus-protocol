package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeErrorMessage(
  errorType: Byte
) extends Message(5513)

object ExchangeErrorMessage {
  implicit val codec: Codec[ExchangeErrorMessage] =
    new Codec[ExchangeErrorMessage] {
      def decode: Get[ExchangeErrorMessage] =
        for {
          errorType <- byte.decode
        } yield ExchangeErrorMessage(errorType)

      def encode(value: ExchangeErrorMessage): ByteVector =
        byte.encode(value.errorType)
    }
}
