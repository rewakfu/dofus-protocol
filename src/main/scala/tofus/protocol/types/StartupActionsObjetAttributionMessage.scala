package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StartupActionsObjetAttributionMessage(
  actionId: Int,
  characterId: Long
) extends Message(1303)

object StartupActionsObjetAttributionMessage {
  implicit val codec: Codec[StartupActionsObjetAttributionMessage] =
    new Codec[StartupActionsObjetAttributionMessage] {
      def decode: Get[StartupActionsObjetAttributionMessage] =
        for {
          actionId <- int.decode
          characterId <- varLong.decode
        } yield StartupActionsObjetAttributionMessage(actionId, characterId)

      def encode(value: StartupActionsObjetAttributionMessage): ByteVector =
        int.encode(value.actionId) ++
        varLong.encode(value.characterId)
    }
}
