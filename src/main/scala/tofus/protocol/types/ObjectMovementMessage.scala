package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectMovementMessage(
  objectUID: Int,
  position: Short
) extends Message(3010)

object ObjectMovementMessage {
  implicit val codec: Codec[ObjectMovementMessage] =
    new Codec[ObjectMovementMessage] {
      def decode: Get[ObjectMovementMessage] =
        for {
          objectUID <- varInt.decode
          position <- short.decode
        } yield ObjectMovementMessage(objectUID, position)

      def encode(value: ObjectMovementMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        short.encode(value.position)
    }
}
