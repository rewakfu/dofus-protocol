package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlignmentWarEffortProgressionMessage(
  effortProgressions: List[AlignmentWarEffortInformation]
) extends Message(6852)

object AlignmentWarEffortProgressionMessage {
  implicit val codec: Codec[AlignmentWarEffortProgressionMessage] =
    new Codec[AlignmentWarEffortProgressionMessage] {
      def decode: Get[AlignmentWarEffortProgressionMessage] =
        for {
          effortProgressions <- list(ushort, Codec[AlignmentWarEffortInformation]).decode
        } yield AlignmentWarEffortProgressionMessage(effortProgressions)

      def encode(value: AlignmentWarEffortProgressionMessage): ByteVector =
        list(ushort, Codec[AlignmentWarEffortInformation]).encode(value.effortProgressions)
    }
}
