package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartAsVendorMessage(

) extends Message(5775)

object ExchangeStartAsVendorMessage {
  implicit val codec: Codec[ExchangeStartAsVendorMessage] =
    Codec.const(ExchangeStartAsVendorMessage())
}
