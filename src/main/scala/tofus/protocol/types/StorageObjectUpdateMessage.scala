package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StorageObjectUpdateMessage(
  `object`: ObjectItem
) extends Message(5647)

object StorageObjectUpdateMessage {
  implicit val codec: Codec[StorageObjectUpdateMessage] =
    new Codec[StorageObjectUpdateMessage] {
      def decode: Get[StorageObjectUpdateMessage] =
        for {
          `object` <- Codec[ObjectItem].decode
        } yield StorageObjectUpdateMessage(`object`)

      def encode(value: StorageObjectUpdateMessage): ByteVector =
        Codec[ObjectItem].encode(value.`object`)
    }
}
