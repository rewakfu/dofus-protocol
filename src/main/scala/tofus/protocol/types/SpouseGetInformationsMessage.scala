package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpouseGetInformationsMessage(

) extends Message(6355)

object SpouseGetInformationsMessage {
  implicit val codec: Codec[SpouseGetInformationsMessage] =
    Codec.const(SpouseGetInformationsMessage())
}
