package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectorySettingsMessage(
  craftersSettings: List[JobCrafterDirectorySettings]
) extends Message(5652)

object JobCrafterDirectorySettingsMessage {
  implicit val codec: Codec[JobCrafterDirectorySettingsMessage] =
    new Codec[JobCrafterDirectorySettingsMessage] {
      def decode: Get[JobCrafterDirectorySettingsMessage] =
        for {
          craftersSettings <- list(ushort, Codec[JobCrafterDirectorySettings]).decode
        } yield JobCrafterDirectorySettingsMessage(craftersSettings)

      def encode(value: JobCrafterDirectorySettingsMessage): ByteVector =
        list(ushort, Codec[JobCrafterDirectorySettings]).encode(value.craftersSettings)
    }
}
