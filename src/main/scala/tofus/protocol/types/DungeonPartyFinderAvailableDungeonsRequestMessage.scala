package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderAvailableDungeonsRequestMessage(

) extends Message(6240)

object DungeonPartyFinderAvailableDungeonsRequestMessage {
  implicit val codec: Codec[DungeonPartyFinderAvailableDungeonsRequestMessage] =
    Codec.const(DungeonPartyFinderAvailableDungeonsRequestMessage())
}
