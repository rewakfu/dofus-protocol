package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnReadyRequestMessage(
  id: Double
) extends Message(715)

object GameFightTurnReadyRequestMessage {
  implicit val codec: Codec[GameFightTurnReadyRequestMessage] =
    new Codec[GameFightTurnReadyRequestMessage] {
      def decode: Get[GameFightTurnReadyRequestMessage] =
        for {
          id <- double.decode
        } yield GameFightTurnReadyRequestMessage(id)

      def encode(value: GameFightTurnReadyRequestMessage): ByteVector =
        double.encode(value.id)
    }
}
