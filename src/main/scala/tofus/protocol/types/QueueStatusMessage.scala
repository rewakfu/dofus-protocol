package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QueueStatusMessage(
  position: Int,
  total: Int
) extends Message(6100)

object QueueStatusMessage {
  implicit val codec: Codec[QueueStatusMessage] =
    new Codec[QueueStatusMessage] {
      def decode: Get[QueueStatusMessage] =
        for {
          position <- ushort.decode
          total <- ushort.decode
        } yield QueueStatusMessage(position, total)

      def encode(value: QueueStatusMessage): ByteVector =
        ushort.encode(value.position) ++
        ushort.encode(value.total)
    }
}
