package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapRunningFightListRequestMessage(

) extends Message(5742)

object MapRunningFightListRequestMessage {
  implicit val codec: Codec[MapRunningFightListRequestMessage] =
    Codec.const(MapRunningFightListRequestMessage())
}
