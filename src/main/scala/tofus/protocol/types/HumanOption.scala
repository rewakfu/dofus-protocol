package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait HumanOption extends ProtocolType

final case class ConcreteHumanOption(

) extends HumanOption {
  override val protocolId = 406
}

object ConcreteHumanOption {
  implicit val codec: Codec[ConcreteHumanOption] =  
    Codec.const(ConcreteHumanOption())
}

object HumanOption {
  implicit val codec: Codec[HumanOption] =
    new Codec[HumanOption] {
      def decode: Get[HumanOption] =
        ushort.decode.flatMap {
          case 406 => Codec[ConcreteHumanOption].decode
          case 425 => Codec[HumanOptionAlliance].decode
          case 495 => Codec[HumanOptionSkillUse].decode
          case 407 => Codec[HumanOptionEmote].decode
          case 410 => Codec[HumanOptionFollowers].decode
          case 449 => Codec[HumanOptionObjectUse].decode
          case 408 => Codec[HumanOptionTitle].decode
          case 409 => Codec[HumanOptionGuild].decode
          case 411 => Codec[HumanOptionOrnament].decode
        }

      def encode(value: HumanOption): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteHumanOption => Codec[ConcreteHumanOption].encode(i)
          case i: HumanOptionAlliance => Codec[HumanOptionAlliance].encode(i)
          case i: HumanOptionSkillUse => Codec[HumanOptionSkillUse].encode(i)
          case i: HumanOptionEmote => Codec[HumanOptionEmote].encode(i)
          case i: HumanOptionFollowers => Codec[HumanOptionFollowers].encode(i)
          case i: HumanOptionObjectUse => Codec[HumanOptionObjectUse].encode(i)
          case i: HumanOptionTitle => Codec[HumanOptionTitle].encode(i)
          case i: HumanOptionGuild => Codec[HumanOptionGuild].encode(i)
          case i: HumanOptionOrnament => Codec[HumanOptionOrnament].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
