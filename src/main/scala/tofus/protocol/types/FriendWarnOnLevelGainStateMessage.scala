package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendWarnOnLevelGainStateMessage(
  enable: Boolean
) extends Message(6078)

object FriendWarnOnLevelGainStateMessage {
  implicit val codec: Codec[FriendWarnOnLevelGainStateMessage] =
    new Codec[FriendWarnOnLevelGainStateMessage] {
      def decode: Get[FriendWarnOnLevelGainStateMessage] =
        for {
          enable <- bool.decode
        } yield FriendWarnOnLevelGainStateMessage(enable)

      def encode(value: FriendWarnOnLevelGainStateMessage): ByteVector =
        bool.encode(value.enable)
    }
}
