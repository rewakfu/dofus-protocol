package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseToSellListRequestMessage(
  pageIndex: Short
) extends Message(6139)

object HouseToSellListRequestMessage {
  implicit val codec: Codec[HouseToSellListRequestMessage] =
    new Codec[HouseToSellListRequestMessage] {
      def decode: Get[HouseToSellListRequestMessage] =
        for {
          pageIndex <- varShort.decode
        } yield HouseToSellListRequestMessage(pageIndex)

      def encode(value: HouseToSellListRequestMessage): ByteVector =
        varShort.encode(value.pageIndex)
    }
}
