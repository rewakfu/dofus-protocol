package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectString(
  actionId: Short,
  value: String
) extends ObjectEffect {
  override val protocolId = 74
}

object ObjectEffectString {
  implicit val codec: Codec[ObjectEffectString] =
    new Codec[ObjectEffectString] {
      def decode: Get[ObjectEffectString] =
        for {
          actionId <- varShort.decode
          value <- utf8(ushort).decode
        } yield ObjectEffectString(actionId, value)

      def encode(value: ObjectEffectString): ByteVector =
        varShort.encode(value.actionId) ++
        utf8(ushort).encode(value.value)
    }
}
