package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeRequestedMessage(
  exchangeType: Byte
) extends Message(5522)

object ExchangeRequestedMessage {
  implicit val codec: Codec[ExchangeRequestedMessage] =
    new Codec[ExchangeRequestedMessage] {
      def decode: Get[ExchangeRequestedMessage] =
        for {
          exchangeType <- byte.decode
        } yield ExchangeRequestedMessage(exchangeType)

      def encode(value: ExchangeRequestedMessage): ByteVector =
        byte.encode(value.exchangeType)
    }
}
