package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExtendedLockedBreachBranch(
  room: Byte,
  element: Int,
  bosses: List[ConcreteMonsterInGroupLightInformations],
  map: Double,
  score: Short,
  relativeScore: Short,
  monsters: List[ConcreteMonsterInGroupLightInformations],
  rewards: List[BreachReward],
  modifier: Int,
  prize: Int,
  unlockPrice: Int
) extends ExtendedBreachBranch {
  override val protocolId = 578
}

object ExtendedLockedBreachBranch {
  implicit val codec: Codec[ExtendedLockedBreachBranch] =
    new Codec[ExtendedLockedBreachBranch] {
      def decode: Get[ExtendedLockedBreachBranch] =
        for {
          room <- byte.decode
          element <- int.decode
          bosses <- list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).decode
          map <- double.decode
          score <- short.decode
          relativeScore <- short.decode
          monsters <- list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).decode
          rewards <- list(ushort, Codec[BreachReward]).decode
          modifier <- varInt.decode
          prize <- varInt.decode
          unlockPrice <- varInt.decode
        } yield ExtendedLockedBreachBranch(room, element, bosses, map, score, relativeScore, monsters, rewards, modifier, prize, unlockPrice)

      def encode(value: ExtendedLockedBreachBranch): ByteVector =
        byte.encode(value.room) ++
        int.encode(value.element) ++
        list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).encode(value.bosses) ++
        double.encode(value.map) ++
        short.encode(value.score) ++
        short.encode(value.relativeScore) ++
        list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).encode(value.monsters) ++
        list(ushort, Codec[BreachReward]).encode(value.rewards) ++
        varInt.encode(value.modifier) ++
        varInt.encode(value.prize) ++
        varInt.encode(value.unlockPrice)
    }
}
