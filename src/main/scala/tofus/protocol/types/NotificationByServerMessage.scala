package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NotificationByServerMessage(
  id: Short,
  parameters: List[String],
  forceOpen: Boolean
) extends Message(6103)

object NotificationByServerMessage {
  implicit val codec: Codec[NotificationByServerMessage] =
    new Codec[NotificationByServerMessage] {
      def decode: Get[NotificationByServerMessage] =
        for {
          id <- varShort.decode
          parameters <- list(ushort, utf8(ushort)).decode
          forceOpen <- bool.decode
        } yield NotificationByServerMessage(id, parameters, forceOpen)

      def encode(value: NotificationByServerMessage): ByteVector =
        varShort.encode(value.id) ++
        list(ushort, utf8(ushort)).encode(value.parameters) ++
        bool.encode(value.forceOpen)
    }
}
