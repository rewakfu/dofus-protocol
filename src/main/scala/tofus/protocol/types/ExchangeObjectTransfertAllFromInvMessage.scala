package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectTransfertAllFromInvMessage(

) extends Message(6184)

object ExchangeObjectTransfertAllFromInvMessage {
  implicit val codec: Codec[ExchangeObjectTransfertAllFromInvMessage] =
    Codec.const(ExchangeObjectTransfertAllFromInvMessage())
}
