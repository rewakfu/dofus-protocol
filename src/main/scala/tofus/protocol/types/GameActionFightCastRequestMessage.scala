package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightCastRequestMessage(
  spellId: Short,
  cellId: Short
) extends Message(1005)

object GameActionFightCastRequestMessage {
  implicit val codec: Codec[GameActionFightCastRequestMessage] =
    new Codec[GameActionFightCastRequestMessage] {
      def decode: Get[GameActionFightCastRequestMessage] =
        for {
          spellId <- varShort.decode
          cellId <- short.decode
        } yield GameActionFightCastRequestMessage(spellId, cellId)

      def encode(value: GameActionFightCastRequestMessage): ByteVector =
        varShort.encode(value.spellId) ++
        short.encode(value.cellId)
    }
}
