package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightResultPlayerListEntry(
  outcome: Short,
  wave: Byte,
  rewards: FightLoot,
  id: Double,
  alive: Boolean,
  level: Short,
  additional: List[FightResultAdditionalData]
) extends FightResultFighterListEntry {
  override val protocolId = 24
}

object FightResultPlayerListEntry {
  implicit val codec: Codec[FightResultPlayerListEntry] =
    new Codec[FightResultPlayerListEntry] {
      def decode: Get[FightResultPlayerListEntry] =
        for {
          outcome <- varShort.decode
          wave <- byte.decode
          rewards <- Codec[FightLoot].decode
          id <- double.decode
          alive <- bool.decode
          level <- varShort.decode
          additional <- list(ushort, Codec[FightResultAdditionalData]).decode
        } yield FightResultPlayerListEntry(outcome, wave, rewards, id, alive, level, additional)

      def encode(value: FightResultPlayerListEntry): ByteVector =
        varShort.encode(value.outcome) ++
        byte.encode(value.wave) ++
        Codec[FightLoot].encode(value.rewards) ++
        double.encode(value.id) ++
        bool.encode(value.alive) ++
        varShort.encode(value.level) ++
        list(ushort, Codec[FightResultAdditionalData]).encode(value.additional)
    }
}
