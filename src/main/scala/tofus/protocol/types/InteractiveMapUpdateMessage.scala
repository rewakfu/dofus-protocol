package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveMapUpdateMessage(
  interactiveElements: List[InteractiveElement]
) extends Message(5002)

object InteractiveMapUpdateMessage {
  implicit val codec: Codec[InteractiveMapUpdateMessage] =
    new Codec[InteractiveMapUpdateMessage] {
      def decode: Get[InteractiveMapUpdateMessage] =
        for {
          interactiveElements <- list(ushort, Codec[InteractiveElement]).decode
        } yield InteractiveMapUpdateMessage(interactiveElements)

      def encode(value: InteractiveMapUpdateMessage): ByteVector =
        list(ushort, Codec[InteractiveElement]).encode(value.interactiveElements)
    }
}
