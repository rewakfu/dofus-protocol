package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class RecycledItem(
  id: Short,
  qty: Long
) extends ProtocolType {
  override val protocolId = 547
}

object RecycledItem {
  implicit val codec: Codec[RecycledItem] =
    new Codec[RecycledItem] {
      def decode: Get[RecycledItem] =
        for {
          id <- varShort.decode
          qty <- uint.decode
        } yield RecycledItem(id, qty)

      def encode(value: RecycledItem): ByteVector =
        varShort.encode(value.id) ++
        uint.encode(value.qty)
    }
}
