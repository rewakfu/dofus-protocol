package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightJoinRequestMessage(
  fighterId: Double,
  fightId: Short
) extends Message(701)

object GameFightJoinRequestMessage {
  implicit val codec: Codec[GameFightJoinRequestMessage] =
    new Codec[GameFightJoinRequestMessage] {
      def decode: Get[GameFightJoinRequestMessage] =
        for {
          fighterId <- double.decode
          fightId <- varShort.decode
        } yield GameFightJoinRequestMessage(fighterId, fightId)

      def encode(value: GameFightJoinRequestMessage): ByteVector =
        double.encode(value.fighterId) ++
        varShort.encode(value.fightId)
    }
}
