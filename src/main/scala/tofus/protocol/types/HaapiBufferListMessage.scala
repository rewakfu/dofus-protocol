package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiBufferListMessage(
  buffers: List[BufferInformation]
) extends Message(6845)

object HaapiBufferListMessage {
  implicit val codec: Codec[HaapiBufferListMessage] =
    new Codec[HaapiBufferListMessage] {
      def decode: Get[HaapiBufferListMessage] =
        for {
          buffers <- list(ushort, Codec[BufferInformation]).decode
        } yield HaapiBufferListMessage(buffers)

      def encode(value: HaapiBufferListMessage): ByteVector =
        list(ushort, Codec[BufferInformation]).encode(value.buffers)
    }
}
