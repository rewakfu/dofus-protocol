package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IndexedEntityLook(
  look: EntityLook,
  index: Byte
) extends ProtocolType {
  override val protocolId = 405
}

object IndexedEntityLook {
  implicit val codec: Codec[IndexedEntityLook] =
    new Codec[IndexedEntityLook] {
      def decode: Get[IndexedEntityLook] =
        for {
          look <- Codec[EntityLook].decode
          index <- byte.decode
        } yield IndexedEntityLook(look, index)

      def encode(value: IndexedEntityLook): ByteVector =
        Codec[EntityLook].encode(value.look) ++
        byte.encode(value.index)
    }
}
