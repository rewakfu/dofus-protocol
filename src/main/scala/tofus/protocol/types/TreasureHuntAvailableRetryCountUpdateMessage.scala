package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntAvailableRetryCountUpdateMessage(
  questType: Byte,
  availableRetryCount: Int
) extends Message(6491)

object TreasureHuntAvailableRetryCountUpdateMessage {
  implicit val codec: Codec[TreasureHuntAvailableRetryCountUpdateMessage] =
    new Codec[TreasureHuntAvailableRetryCountUpdateMessage] {
      def decode: Get[TreasureHuntAvailableRetryCountUpdateMessage] =
        for {
          questType <- byte.decode
          availableRetryCount <- int.decode
        } yield TreasureHuntAvailableRetryCountUpdateMessage(questType, availableRetryCount)

      def encode(value: TreasureHuntAvailableRetryCountUpdateMessage): ByteVector =
        byte.encode(value.questType) ++
        int.encode(value.availableRetryCount)
    }
}
