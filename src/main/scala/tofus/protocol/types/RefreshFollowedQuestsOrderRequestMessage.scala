package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class RefreshFollowedQuestsOrderRequestMessage(
  quests: List[Short]
) extends Message(6722)

object RefreshFollowedQuestsOrderRequestMessage {
  implicit val codec: Codec[RefreshFollowedQuestsOrderRequestMessage] =
    new Codec[RefreshFollowedQuestsOrderRequestMessage] {
      def decode: Get[RefreshFollowedQuestsOrderRequestMessage] =
        for {
          quests <- list(ushort, varShort).decode
        } yield RefreshFollowedQuestsOrderRequestMessage(quests)

      def encode(value: RefreshFollowedQuestsOrderRequestMessage): ByteVector =
        list(ushort, varShort).encode(value.quests)
    }
}
