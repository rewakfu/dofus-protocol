package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountsStableBornAddMessage(
  mountDescription: List[MountClientData]
) extends Message(6557)

object ExchangeMountsStableBornAddMessage {
  implicit val codec: Codec[ExchangeMountsStableBornAddMessage] =
    new Codec[ExchangeMountsStableBornAddMessage] {
      def decode: Get[ExchangeMountsStableBornAddMessage] =
        for {
          mountDescription <- list(ushort, Codec[MountClientData]).decode
        } yield ExchangeMountsStableBornAddMessage(mountDescription)

      def encode(value: ExchangeMountsStableBornAddMessage): ByteVector =
        list(ushort, Codec[MountClientData]).encode(value.mountDescription)
    }
}
