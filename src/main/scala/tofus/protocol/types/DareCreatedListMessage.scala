package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareCreatedListMessage(
  daresFixedInfos: List[DareInformations],
  daresVersatilesInfos: List[DareVersatileInformations]
) extends Message(6663)

object DareCreatedListMessage {
  implicit val codec: Codec[DareCreatedListMessage] =
    new Codec[DareCreatedListMessage] {
      def decode: Get[DareCreatedListMessage] =
        for {
          daresFixedInfos <- list(ushort, Codec[DareInformations]).decode
          daresVersatilesInfos <- list(ushort, Codec[DareVersatileInformations]).decode
        } yield DareCreatedListMessage(daresFixedInfos, daresVersatilesInfos)

      def encode(value: DareCreatedListMessage): ByteVector =
        list(ushort, Codec[DareInformations]).encode(value.daresFixedInfos) ++
        list(ushort, Codec[DareVersatileInformations]).encode(value.daresVersatilesInfos)
    }
}
