package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInsiderInfoMessage(
  allianceInfos: AllianceFactSheetInformations,
  guilds: List[GuildInsiderFactSheetInformations],
  prisms: List[PrismSubareaEmptyInfo]
) extends Message(6403)

object AllianceInsiderInfoMessage {
  implicit val codec: Codec[AllianceInsiderInfoMessage] =
    new Codec[AllianceInsiderInfoMessage] {
      def decode: Get[AllianceInsiderInfoMessage] =
        for {
          allianceInfos <- Codec[AllianceFactSheetInformations].decode
          guilds <- list(ushort, Codec[GuildInsiderFactSheetInformations]).decode
          prisms <- list(ushort, Codec[PrismSubareaEmptyInfo]).decode
        } yield AllianceInsiderInfoMessage(allianceInfos, guilds, prisms)

      def encode(value: AllianceInsiderInfoMessage): ByteVector =
        Codec[AllianceFactSheetInformations].encode(value.allianceInfos) ++
        list(ushort, Codec[GuildInsiderFactSheetInformations]).encode(value.guilds) ++
        list(ushort, Codec[PrismSubareaEmptyInfo]).encode(value.prisms)
    }
}
