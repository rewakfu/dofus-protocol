package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationDetailsMessage(
  partyId: Int,
  partyType: Byte,
  partyName: String,
  fromId: Long,
  fromName: String,
  leaderId: Long,
  members: List[PartyInvitationMemberInformations],
  guests: List[PartyGuestInformations]
) extends Message(6263)

object PartyInvitationDetailsMessage {
  implicit val codec: Codec[PartyInvitationDetailsMessage] =
    new Codec[PartyInvitationDetailsMessage] {
      def decode: Get[PartyInvitationDetailsMessage] =
        for {
          partyId <- varInt.decode
          partyType <- byte.decode
          partyName <- utf8(ushort).decode
          fromId <- varLong.decode
          fromName <- utf8(ushort).decode
          leaderId <- varLong.decode
          members <- list(ushort, Codec[PartyInvitationMemberInformations]).decode
          guests <- list(ushort, Codec[PartyGuestInformations]).decode
        } yield PartyInvitationDetailsMessage(partyId, partyType, partyName, fromId, fromName, leaderId, members, guests)

      def encode(value: PartyInvitationDetailsMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.partyType) ++
        utf8(ushort).encode(value.partyName) ++
        varLong.encode(value.fromId) ++
        utf8(ushort).encode(value.fromName) ++
        varLong.encode(value.leaderId) ++
        list(ushort, Codec[PartyInvitationMemberInformations]).encode(value.members) ++
        list(ushort, Codec[PartyGuestInformations]).encode(value.guests)
    }
}
