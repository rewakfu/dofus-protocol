package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlignmentWarEffortDonateRequestMessage(
  donation: Long
) extends Message(6857)

object AlignmentWarEffortDonateRequestMessage {
  implicit val codec: Codec[AlignmentWarEffortDonateRequestMessage] =
    new Codec[AlignmentWarEffortDonateRequestMessage] {
      def decode: Get[AlignmentWarEffortDonateRequestMessage] =
        for {
          donation <- varLong.decode
        } yield AlignmentWarEffortDonateRequestMessage(donation)

      def encode(value: AlignmentWarEffortDonateRequestMessage): ByteVector =
        varLong.encode(value.donation)
    }
}
