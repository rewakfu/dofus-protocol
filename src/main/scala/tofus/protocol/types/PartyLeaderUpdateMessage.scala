package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyLeaderUpdateMessage(
  partyId: Int,
  partyLeaderId: Long
) extends Message(5578)

object PartyLeaderUpdateMessage {
  implicit val codec: Codec[PartyLeaderUpdateMessage] =
    new Codec[PartyLeaderUpdateMessage] {
      def decode: Get[PartyLeaderUpdateMessage] =
        for {
          partyId <- varInt.decode
          partyLeaderId <- varLong.decode
        } yield PartyLeaderUpdateMessage(partyId, partyLeaderId)

      def encode(value: PartyLeaderUpdateMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.partyLeaderId)
    }
}
