package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FightTeamMemberInformations extends ProtocolType

final case class ConcreteFightTeamMemberInformations(
  id: Double
) extends FightTeamMemberInformations {
  override val protocolId = 44
}

object ConcreteFightTeamMemberInformations {
  implicit val codec: Codec[ConcreteFightTeamMemberInformations] =  
    new Codec[ConcreteFightTeamMemberInformations] {
      def decode: Get[ConcreteFightTeamMemberInformations] =
        for {
          id <- double.decode
        } yield ConcreteFightTeamMemberInformations(id)

      def encode(value: ConcreteFightTeamMemberInformations): ByteVector =
        double.encode(value.id)
    }
}

object FightTeamMemberInformations {
  implicit val codec: Codec[FightTeamMemberInformations] =
    new Codec[FightTeamMemberInformations] {
      def decode: Get[FightTeamMemberInformations] =
        ushort.decode.flatMap {
          case 44 => Codec[ConcreteFightTeamMemberInformations].decode
          case 426 => Codec[FightTeamMemberWithAllianceCharacterInformations].decode
          case 13 => Codec[ConcreteFightTeamMemberCharacterInformations].decode
          case 6 => Codec[FightTeamMemberMonsterInformations].decode
          case 177 => Codec[FightTeamMemberTaxCollectorInformations].decode
          case 549 => Codec[FightTeamMemberEntityInformation].decode
        }

      def encode(value: FightTeamMemberInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFightTeamMemberInformations => Codec[ConcreteFightTeamMemberInformations].encode(i)
          case i: FightTeamMemberWithAllianceCharacterInformations => Codec[FightTeamMemberWithAllianceCharacterInformations].encode(i)
          case i: ConcreteFightTeamMemberCharacterInformations => Codec[ConcreteFightTeamMemberCharacterInformations].encode(i)
          case i: FightTeamMemberMonsterInformations => Codec[FightTeamMemberMonsterInformations].encode(i)
          case i: FightTeamMemberTaxCollectorInformations => Codec[FightTeamMemberTaxCollectorInformations].encode(i)
          case i: FightTeamMemberEntityInformation => Codec[FightTeamMemberEntityInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
