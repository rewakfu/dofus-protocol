package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildPaddockBoughtMessage(
  paddockInfo: PaddockContentInformations
) extends Message(5952)

object GuildPaddockBoughtMessage {
  implicit val codec: Codec[GuildPaddockBoughtMessage] =
    new Codec[GuildPaddockBoughtMessage] {
      def decode: Get[GuildPaddockBoughtMessage] =
        for {
          paddockInfo <- Codec[PaddockContentInformations].decode
        } yield GuildPaddockBoughtMessage(paddockInfo)

      def encode(value: GuildPaddockBoughtMessage): ByteVector =
        Codec[PaddockContentInformations].encode(value.paddockInfo)
    }
}
