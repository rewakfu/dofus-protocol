package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutEntitiesPreset(
  slot: Byte,
  presetId: Short
) extends Shortcut {
  override val protocolId = 544
}

object ShortcutEntitiesPreset {
  implicit val codec: Codec[ShortcutEntitiesPreset] =
    new Codec[ShortcutEntitiesPreset] {
      def decode: Get[ShortcutEntitiesPreset] =
        for {
          slot <- byte.decode
          presetId <- short.decode
        } yield ShortcutEntitiesPreset(slot, presetId)

      def encode(value: ShortcutEntitiesPreset): ByteVector =
        byte.encode(value.slot) ++
        short.encode(value.presetId)
    }
}
