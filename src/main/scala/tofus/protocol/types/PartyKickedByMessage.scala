package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyKickedByMessage(
  partyId: Int,
  kickerId: Long
) extends Message(5590)

object PartyKickedByMessage {
  implicit val codec: Codec[PartyKickedByMessage] =
    new Codec[PartyKickedByMessage] {
      def decode: Get[PartyKickedByMessage] =
        for {
          partyId <- varInt.decode
          kickerId <- varLong.decode
        } yield PartyKickedByMessage(partyId, kickerId)

      def encode(value: PartyKickedByMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.kickerId)
    }
}
