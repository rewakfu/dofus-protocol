package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterSelectedForceReadyMessage(

) extends Message(6072)

object CharacterSelectedForceReadyMessage {
  implicit val codec: Codec[CharacterSelectedForceReadyMessage] =
    Codec.const(CharacterSelectedForceReadyMessage())
}
