package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFightPlayersHelpersLeaveMessage(
  fightId: Double,
  playerId: Long
) extends Message(5719)

object GuildFightPlayersHelpersLeaveMessage {
  implicit val codec: Codec[GuildFightPlayersHelpersLeaveMessage] =
    new Codec[GuildFightPlayersHelpersLeaveMessage] {
      def decode: Get[GuildFightPlayersHelpersLeaveMessage] =
        for {
          fightId <- double.decode
          playerId <- varLong.decode
        } yield GuildFightPlayersHelpersLeaveMessage(fightId, playerId)

      def encode(value: GuildFightPlayersHelpersLeaveMessage): ByteVector =
        double.encode(value.fightId) ++
        varLong.encode(value.playerId)
    }
}
