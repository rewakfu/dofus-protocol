package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightLifeAndShieldPointsLostMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  loss: Int,
  permanentDamages: Int,
  elementId: Int,
  shieldLoss: Short
) extends Message(6310)

object GameActionFightLifeAndShieldPointsLostMessage {
  implicit val codec: Codec[GameActionFightLifeAndShieldPointsLostMessage] =
    new Codec[GameActionFightLifeAndShieldPointsLostMessage] {
      def decode: Get[GameActionFightLifeAndShieldPointsLostMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          loss <- varInt.decode
          permanentDamages <- varInt.decode
          elementId <- varInt.decode
          shieldLoss <- varShort.decode
        } yield GameActionFightLifeAndShieldPointsLostMessage(actionId, sourceId, targetId, loss, permanentDamages, elementId, shieldLoss)

      def encode(value: GameActionFightLifeAndShieldPointsLostMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varInt.encode(value.loss) ++
        varInt.encode(value.permanentDamages) ++
        varInt.encode(value.elementId) ++
        varShort.encode(value.shieldLoss)
    }
}
