package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderListenErrorMessage(
  dungeonId: Short
) extends Message(6248)

object DungeonPartyFinderListenErrorMessage {
  implicit val codec: Codec[DungeonPartyFinderListenErrorMessage] =
    new Codec[DungeonPartyFinderListenErrorMessage] {
      def decode: Get[DungeonPartyFinderListenErrorMessage] =
        for {
          dungeonId <- varShort.decode
        } yield DungeonPartyFinderListenErrorMessage(dungeonId)

      def encode(value: DungeonPartyFinderListenErrorMessage): ByteVector =
        varShort.encode(value.dungeonId)
    }
}
