package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EntityInformation(
  id: Short,
  experience: Int,
  status: Boolean
) extends ProtocolType {
  override val protocolId = 546
}

object EntityInformation {
  implicit val codec: Codec[EntityInformation] =
    new Codec[EntityInformation] {
      def decode: Get[EntityInformation] =
        for {
          id <- varShort.decode
          experience <- varInt.decode
          status <- bool.decode
        } yield EntityInformation(id, experience, status)

      def encode(value: EntityInformation): ByteVector =
        varShort.encode(value.id) ++
        varInt.encode(value.experience) ++
        bool.encode(value.status)
    }
}
