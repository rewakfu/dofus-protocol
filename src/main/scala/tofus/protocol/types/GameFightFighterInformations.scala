package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameFightFighterInformations extends GameContextActorInformations

final case class ConcreteGameFightFighterInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short]
) extends GameFightFighterInformations {
  override val protocolId = 143
}

object ConcreteGameFightFighterInformations {
  implicit val codec: Codec[ConcreteGameFightFighterInformations] =  
    new Codec[ConcreteGameFightFighterInformations] {
      def decode: Get[ConcreteGameFightFighterInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
        } yield ConcreteGameFightFighterInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions)

      def encode(value: ConcreteGameFightFighterInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions)
    }
}

object GameFightFighterInformations {
  implicit val codec: Codec[GameFightFighterInformations] =
    new Codec[GameFightFighterInformations] {
      def decode: Get[GameFightFighterInformations] =
        ushort.decode.flatMap {
          case 143 => Codec[ConcreteGameFightFighterInformations].decode
          case 46 => Codec[GameFightCharacterInformations].decode
          case 50 => Codec[GameFightMutantInformations].decode
          case 158 => Codec[ConcreteGameFightFighterNamedInformations].decode
          case 203 => Codec[GameFightMonsterWithAlignmentInformations].decode
          case 29 => Codec[ConcreteGameFightMonsterInformations].decode
          case 48 => Codec[GameFightTaxCollectorInformations].decode
          case 151 => Codec[ConcreteGameFightAIInformations].decode
          case 551 => Codec[GameFightEntityInformation].decode
        }

      def encode(value: GameFightFighterInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameFightFighterInformations => Codec[ConcreteGameFightFighterInformations].encode(i)
          case i: GameFightCharacterInformations => Codec[GameFightCharacterInformations].encode(i)
          case i: GameFightMutantInformations => Codec[GameFightMutantInformations].encode(i)
          case i: ConcreteGameFightFighterNamedInformations => Codec[ConcreteGameFightFighterNamedInformations].encode(i)
          case i: GameFightMonsterWithAlignmentInformations => Codec[GameFightMonsterWithAlignmentInformations].encode(i)
          case i: ConcreteGameFightMonsterInformations => Codec[ConcreteGameFightMonsterInformations].encode(i)
          case i: GameFightTaxCollectorInformations => Codec[GameFightTaxCollectorInformations].encode(i)
          case i: ConcreteGameFightAIInformations => Codec[ConcreteGameFightAIInformations].encode(i)
          case i: GameFightEntityInformation => Codec[GameFightEntityInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
