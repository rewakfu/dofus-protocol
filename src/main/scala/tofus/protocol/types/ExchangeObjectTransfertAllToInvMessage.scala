package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectTransfertAllToInvMessage(

) extends Message(6032)

object ExchangeObjectTransfertAllToInvMessage {
  implicit val codec: Codec[ExchangeObjectTransfertAllToInvMessage] =
    Codec.const(ExchangeObjectTransfertAllToInvMessage())
}
