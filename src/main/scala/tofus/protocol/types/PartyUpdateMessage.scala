package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyUpdateMessage(
  partyId: Int,
  memberInformations: PartyMemberInformations
) extends Message(5575)

object PartyUpdateMessage {
  implicit val codec: Codec[PartyUpdateMessage] =
    new Codec[PartyUpdateMessage] {
      def decode: Get[PartyUpdateMessage] =
        for {
          partyId <- varInt.decode
          memberInformations <- Codec[PartyMemberInformations].decode
        } yield PartyUpdateMessage(partyId, memberInformations)

      def encode(value: PartyUpdateMessage): ByteVector =
        varInt.encode(value.partyId) ++
        Codec[PartyMemberInformations].encode(value.memberInformations)
    }
}
