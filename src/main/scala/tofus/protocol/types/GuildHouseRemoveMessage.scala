package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildHouseRemoveMessage(
  houseId: Int,
  instanceId: Int,
  secondHand: Boolean
) extends Message(6180)

object GuildHouseRemoveMessage {
  implicit val codec: Codec[GuildHouseRemoveMessage] =
    new Codec[GuildHouseRemoveMessage] {
      def decode: Get[GuildHouseRemoveMessage] =
        for {
          houseId <- varInt.decode
          instanceId <- int.decode
          secondHand <- bool.decode
        } yield GuildHouseRemoveMessage(houseId, instanceId, secondHand)

      def encode(value: GuildHouseRemoveMessage): ByteVector =
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId) ++
        bool.encode(value.secondHand)
    }
}
