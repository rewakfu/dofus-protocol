package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiAuthErrorMessage(
  `type`: Byte
) extends Message(6768)

object HaapiAuthErrorMessage {
  implicit val codec: Codec[HaapiAuthErrorMessage] =
    new Codec[HaapiAuthErrorMessage] {
      def decode: Get[HaapiAuthErrorMessage] =
        for {
          `type` <- byte.decode
        } yield HaapiAuthErrorMessage(`type`)

      def encode(value: HaapiAuthErrorMessage): ByteVector =
        byte.encode(value.`type`)
    }
}
