package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestStepInfoRequestMessage(
  questId: Short
) extends Message(5622)

object QuestStepInfoRequestMessage {
  implicit val codec: Codec[QuestStepInfoRequestMessage] =
    new Codec[QuestStepInfoRequestMessage] {
      def decode: Get[QuestStepInfoRequestMessage] =
        for {
          questId <- varShort.decode
        } yield QuestStepInfoRequestMessage(questId)

      def encode(value: QuestStepInfoRequestMessage): ByteVector =
        varShort.encode(value.questId)
    }
}
