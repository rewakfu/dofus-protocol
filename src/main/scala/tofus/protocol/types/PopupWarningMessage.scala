package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PopupWarningMessage(
  lockDuration: Short,
  author: String,
  content: String
) extends Message(6134)

object PopupWarningMessage {
  implicit val codec: Codec[PopupWarningMessage] =
    new Codec[PopupWarningMessage] {
      def decode: Get[PopupWarningMessage] =
        for {
          lockDuration <- ubyte.decode
          author <- utf8(ushort).decode
          content <- utf8(ushort).decode
        } yield PopupWarningMessage(lockDuration, author, content)

      def encode(value: PopupWarningMessage): ByteVector =
        ubyte.encode(value.lockDuration) ++
        utf8(ushort).encode(value.author) ++
        utf8(ushort).encode(value.content)
    }
}
