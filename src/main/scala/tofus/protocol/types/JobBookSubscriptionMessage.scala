package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobBookSubscriptionMessage(
  subscriptions: List[JobBookSubscription]
) extends Message(6593)

object JobBookSubscriptionMessage {
  implicit val codec: Codec[JobBookSubscriptionMessage] =
    new Codec[JobBookSubscriptionMessage] {
      def decode: Get[JobBookSubscriptionMessage] =
        for {
          subscriptions <- list(ushort, Codec[JobBookSubscription]).decode
        } yield JobBookSubscriptionMessage(subscriptions)

      def encode(value: JobBookSubscriptionMessage): ByteVector =
        list(ushort, Codec[JobBookSubscription]).encode(value.subscriptions)
    }
}
