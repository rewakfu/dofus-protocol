package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayPortalInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  portal: PortalInformation
) extends GameRolePlayActorInformations {
  override val protocolId = 467
}

object GameRolePlayPortalInformations {
  implicit val codec: Codec[GameRolePlayPortalInformations] =
    new Codec[GameRolePlayPortalInformations] {
      def decode: Get[GameRolePlayPortalInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          portal <- Codec[PortalInformation].decode
        } yield GameRolePlayPortalInformations(contextualId, disposition, look, portal)

      def encode(value: GameRolePlayPortalInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[PortalInformation].encode(value.portal)
    }
}
