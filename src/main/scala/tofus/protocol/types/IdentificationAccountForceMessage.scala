package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdentificationAccountForceMessage(
  flags0: Byte,
  version: Version,
  lang: String,
  credentials: ByteVector,
  serverId: Short,
  sessionOptionalSalt: Long,
  failedAttempts: List[Short],
  forcedAccountLogin: String
) extends Message(6119)

object IdentificationAccountForceMessage {
  implicit val codec: Codec[IdentificationAccountForceMessage] =
    new Codec[IdentificationAccountForceMessage] {
      def decode: Get[IdentificationAccountForceMessage] =
        for {
          flags0 <- byte.decode
          version <- Codec[Version].decode
          lang <- utf8(ushort).decode
          credentials <- bytes(varInt).decode
          serverId <- short.decode
          sessionOptionalSalt <- varLong.decode
          failedAttempts <- list(ushort, varShort).decode
          forcedAccountLogin <- utf8(ushort).decode
        } yield IdentificationAccountForceMessage(flags0, version, lang, credentials, serverId, sessionOptionalSalt, failedAttempts, forcedAccountLogin)

      def encode(value: IdentificationAccountForceMessage): ByteVector =
        byte.encode(value.flags0) ++
        Codec[Version].encode(value.version) ++
        utf8(ushort).encode(value.lang) ++
        bytes(varInt).encode(value.credentials) ++
        short.encode(value.serverId) ++
        varLong.encode(value.sessionOptionalSalt) ++
        list(ushort, varShort).encode(value.failedAttempts) ++
        utf8(ushort).encode(value.forcedAccountLogin)
    }
}
