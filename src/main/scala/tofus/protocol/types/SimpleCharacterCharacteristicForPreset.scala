package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait SimpleCharacterCharacteristicForPreset extends ProtocolType

final case class ConcreteSimpleCharacterCharacteristicForPreset(
  keyword: String,
  base: Short,
  additionnal: Short
) extends SimpleCharacterCharacteristicForPreset {
  override val protocolId = 541
}

object ConcreteSimpleCharacterCharacteristicForPreset {
  implicit val codec: Codec[ConcreteSimpleCharacterCharacteristicForPreset] =  
    new Codec[ConcreteSimpleCharacterCharacteristicForPreset] {
      def decode: Get[ConcreteSimpleCharacterCharacteristicForPreset] =
        for {
          keyword <- utf8(ushort).decode
          base <- varShort.decode
          additionnal <- varShort.decode
        } yield ConcreteSimpleCharacterCharacteristicForPreset(keyword, base, additionnal)

      def encode(value: ConcreteSimpleCharacterCharacteristicForPreset): ByteVector =
        utf8(ushort).encode(value.keyword) ++
        varShort.encode(value.base) ++
        varShort.encode(value.additionnal)
    }
}

object SimpleCharacterCharacteristicForPreset {
  implicit val codec: Codec[SimpleCharacterCharacteristicForPreset] =
    new Codec[SimpleCharacterCharacteristicForPreset] {
      def decode: Get[SimpleCharacterCharacteristicForPreset] =
        ushort.decode.flatMap {
          case 541 => Codec[ConcreteSimpleCharacterCharacteristicForPreset].decode
          case 539 => Codec[CharacterCharacteristicForPreset].decode
        }

      def encode(value: SimpleCharacterCharacteristicForPreset): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteSimpleCharacterCharacteristicForPreset => Codec[ConcreteSimpleCharacterCharacteristicForPreset].encode(i)
          case i: CharacterCharacteristicForPreset => Codec[CharacterCharacteristicForPreset].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
