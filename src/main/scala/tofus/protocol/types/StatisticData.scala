package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait StatisticData extends ProtocolType

final case class ConcreteStatisticData(

) extends StatisticData {
  override val protocolId = 484
}

object ConcreteStatisticData {
  implicit val codec: Codec[ConcreteStatisticData] =  
    Codec.const(ConcreteStatisticData())
}

object StatisticData {
  implicit val codec: Codec[StatisticData] =
    new Codec[StatisticData] {
      def decode: Get[StatisticData] =
        ushort.decode.flatMap {
          case 484 => Codec[ConcreteStatisticData].decode
          case 482 => Codec[StatisticDataBoolean].decode
          case 485 => Codec[StatisticDataInt].decode
          case 488 => Codec[StatisticDataShort].decode
          case 486 => Codec[StatisticDataByte].decode
          case 487 => Codec[StatisticDataString].decode
        }

      def encode(value: StatisticData): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteStatisticData => Codec[ConcreteStatisticData].encode(i)
          case i: StatisticDataBoolean => Codec[StatisticDataBoolean].encode(i)
          case i: StatisticDataInt => Codec[StatisticDataInt].encode(i)
          case i: StatisticDataShort => Codec[StatisticDataShort].encode(i)
          case i: StatisticDataByte => Codec[StatisticDataByte].encode(i)
          case i: StatisticDataString => Codec[StatisticDataString].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
