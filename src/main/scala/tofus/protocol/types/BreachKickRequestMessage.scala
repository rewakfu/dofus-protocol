package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachKickRequestMessage(
  target: Long
) extends Message(6804)

object BreachKickRequestMessage {
  implicit val codec: Codec[BreachKickRequestMessage] =
    new Codec[BreachKickRequestMessage] {
      def decode: Get[BreachKickRequestMessage] =
        for {
          target <- varLong.decode
        } yield BreachKickRequestMessage(target)

      def encode(value: BreachKickRequestMessage): ByteVector =
        varLong.encode(value.target)
    }
}
