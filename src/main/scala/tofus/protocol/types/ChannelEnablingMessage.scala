package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChannelEnablingMessage(
  channel: Byte,
  enable: Boolean
) extends Message(890)

object ChannelEnablingMessage {
  implicit val codec: Codec[ChannelEnablingMessage] =
    new Codec[ChannelEnablingMessage] {
      def decode: Get[ChannelEnablingMessage] =
        for {
          channel <- byte.decode
          enable <- bool.decode
        } yield ChannelEnablingMessage(channel, enable)

      def encode(value: ChannelEnablingMessage): ByteVector =
        byte.encode(value.channel) ++
        bool.encode(value.enable)
    }
}
