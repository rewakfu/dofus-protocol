package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightTriggerEffectMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  verboseCast: Boolean,
  boostUID: Int
) extends Message(6147)

object GameActionFightTriggerEffectMessage {
  implicit val codec: Codec[GameActionFightTriggerEffectMessage] =
    new Codec[GameActionFightTriggerEffectMessage] {
      def decode: Get[GameActionFightTriggerEffectMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          verboseCast <- bool.decode
          boostUID <- int.decode
        } yield GameActionFightTriggerEffectMessage(actionId, sourceId, targetId, verboseCast, boostUID)

      def encode(value: GameActionFightTriggerEffectMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        bool.encode(value.verboseCast) ++
        int.encode(value.boostUID)
    }
}
