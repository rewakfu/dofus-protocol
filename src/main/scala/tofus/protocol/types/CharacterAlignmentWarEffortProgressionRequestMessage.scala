package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterAlignmentWarEffortProgressionRequestMessage(

) extends Message(6855)

object CharacterAlignmentWarEffortProgressionRequestMessage {
  implicit val codec: Codec[CharacterAlignmentWarEffortProgressionRequestMessage] =
    Codec.const(CharacterAlignmentWarEffortProgressionRequestMessage())
}
