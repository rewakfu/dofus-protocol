package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFactsErrorMessage(
  guildId: Int
) extends Message(6424)

object GuildFactsErrorMessage {
  implicit val codec: Codec[GuildFactsErrorMessage] =
    new Codec[GuildFactsErrorMessage] {
      def decode: Get[GuildFactsErrorMessage] =
        for {
          guildId <- varInt.decode
        } yield GuildFactsErrorMessage(guildId)

      def encode(value: GuildFactsErrorMessage): ByteVector =
        varInt.encode(value.guildId)
    }
}
