package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class OrnamentLostMessage(
  ornamentId: Short
) extends Message(6770)

object OrnamentLostMessage {
  implicit val codec: Codec[OrnamentLostMessage] =
    new Codec[OrnamentLostMessage] {
      def decode: Get[OrnamentLostMessage] =
        for {
          ornamentId <- short.decode
        } yield OrnamentLostMessage(ornamentId)

      def encode(value: OrnamentLostMessage): ByteVector =
        short.encode(value.ornamentId)
    }
}
