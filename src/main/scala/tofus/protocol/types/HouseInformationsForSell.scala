package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseInformationsForSell(
  instanceId: Int,
  secondHand: Boolean,
  modelId: Int,
  ownerName: String,
  ownerConnected: Boolean,
  worldX: Short,
  worldY: Short,
  subAreaId: Short,
  nbRoom: Byte,
  nbChest: Byte,
  skillListIds: List[Int],
  isLocked: Boolean,
  price: Long
) extends ProtocolType {
  override val protocolId = 221
}

object HouseInformationsForSell {
  implicit val codec: Codec[HouseInformationsForSell] =
    new Codec[HouseInformationsForSell] {
      def decode: Get[HouseInformationsForSell] =
        for {
          instanceId <- int.decode
          secondHand <- bool.decode
          modelId <- varInt.decode
          ownerName <- utf8(ushort).decode
          ownerConnected <- bool.decode
          worldX <- short.decode
          worldY <- short.decode
          subAreaId <- varShort.decode
          nbRoom <- byte.decode
          nbChest <- byte.decode
          skillListIds <- list(ushort, int).decode
          isLocked <- bool.decode
          price <- varLong.decode
        } yield HouseInformationsForSell(instanceId, secondHand, modelId, ownerName, ownerConnected, worldX, worldY, subAreaId, nbRoom, nbChest, skillListIds, isLocked, price)

      def encode(value: HouseInformationsForSell): ByteVector =
        int.encode(value.instanceId) ++
        bool.encode(value.secondHand) ++
        varInt.encode(value.modelId) ++
        utf8(ushort).encode(value.ownerName) ++
        bool.encode(value.ownerConnected) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        varShort.encode(value.subAreaId) ++
        byte.encode(value.nbRoom) ++
        byte.encode(value.nbChest) ++
        list(ushort, int).encode(value.skillListIds) ++
        bool.encode(value.isLocked) ++
        varLong.encode(value.price)
    }
}
