package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountInformationRequestMessage(
  id: Double,
  time: Double
) extends Message(5972)

object MountInformationRequestMessage {
  implicit val codec: Codec[MountInformationRequestMessage] =
    new Codec[MountInformationRequestMessage] {
      def decode: Get[MountInformationRequestMessage] =
        for {
          id <- double.decode
          time <- double.decode
        } yield MountInformationRequestMessage(id, time)

      def encode(value: MountInformationRequestMessage): ByteVector =
        double.encode(value.id) ++
        double.encode(value.time)
    }
}
