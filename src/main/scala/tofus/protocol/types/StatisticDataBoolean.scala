package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatisticDataBoolean(
  value: Boolean
) extends StatisticData {
  override val protocolId = 482
}

object StatisticDataBoolean {
  implicit val codec: Codec[StatisticDataBoolean] =
    new Codec[StatisticDataBoolean] {
      def decode: Get[StatisticDataBoolean] =
        for {
          value <- bool.decode
        } yield StatisticDataBoolean(value)

      def encode(value: StatisticDataBoolean): ByteVector =
        bool.encode(value.value)
    }
}
