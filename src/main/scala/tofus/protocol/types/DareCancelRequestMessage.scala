package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareCancelRequestMessage(
  dareId: Double
) extends Message(6680)

object DareCancelRequestMessage {
  implicit val codec: Codec[DareCancelRequestMessage] =
    new Codec[DareCancelRequestMessage] {
      def decode: Get[DareCancelRequestMessage] =
        for {
          dareId <- double.decode
        } yield DareCancelRequestMessage(dareId)

      def encode(value: DareCancelRequestMessage): ByteVector =
        double.encode(value.dareId)
    }
}
