package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeSellOkMessage(

) extends Message(5792)

object ExchangeSellOkMessage {
  implicit val codec: Codec[ExchangeSellOkMessage] =
    Codec.const(ExchangeSellOkMessage())
}
