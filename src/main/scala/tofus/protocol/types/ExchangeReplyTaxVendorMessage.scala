package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeReplyTaxVendorMessage(
  objectValue: Long,
  totalTaxValue: Long
) extends Message(5787)

object ExchangeReplyTaxVendorMessage {
  implicit val codec: Codec[ExchangeReplyTaxVendorMessage] =
    new Codec[ExchangeReplyTaxVendorMessage] {
      def decode: Get[ExchangeReplyTaxVendorMessage] =
        for {
          objectValue <- varLong.decode
          totalTaxValue <- varLong.decode
        } yield ExchangeReplyTaxVendorMessage(objectValue, totalTaxValue)

      def encode(value: ExchangeReplyTaxVendorMessage): ByteVector =
        varLong.encode(value.objectValue) ++
        varLong.encode(value.totalTaxValue)
    }
}
