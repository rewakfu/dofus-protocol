package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccountLinkRequiredMessage(

) extends Message(6607)

object AccountLinkRequiredMessage {
  implicit val codec: Codec[AccountLinkRequiredMessage] =
    Codec.const(AccountLinkRequiredMessage())
}
