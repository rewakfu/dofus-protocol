package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectGroundListAddedMessage(
  cells: List[Short],
  referenceIds: List[Short]
) extends Message(5925)

object ObjectGroundListAddedMessage {
  implicit val codec: Codec[ObjectGroundListAddedMessage] =
    new Codec[ObjectGroundListAddedMessage] {
      def decode: Get[ObjectGroundListAddedMessage] =
        for {
          cells <- list(ushort, varShort).decode
          referenceIds <- list(ushort, varShort).decode
        } yield ObjectGroundListAddedMessage(cells, referenceIds)

      def encode(value: ObjectGroundListAddedMessage): ByteVector =
        list(ushort, varShort).encode(value.cells) ++
        list(ushort, varShort).encode(value.referenceIds)
    }
}
