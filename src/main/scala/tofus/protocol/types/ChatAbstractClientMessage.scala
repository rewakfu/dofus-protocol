package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatAbstractClientMessage(
  content: String
) extends Message(850)

object ChatAbstractClientMessage {
  implicit val codec: Codec[ChatAbstractClientMessage] =
    new Codec[ChatAbstractClientMessage] {
      def decode: Get[ChatAbstractClientMessage] =
        for {
          content <- utf8(ushort).decode
        } yield ChatAbstractClientMessage(content)

      def encode(value: ChatAbstractClientMessage): ByteVector =
        utf8(ushort).encode(value.content)
    }
}
