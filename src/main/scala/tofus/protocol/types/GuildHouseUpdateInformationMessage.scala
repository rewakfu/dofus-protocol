package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildHouseUpdateInformationMessage(
  housesInformations: HouseInformationsForGuild
) extends Message(6181)

object GuildHouseUpdateInformationMessage {
  implicit val codec: Codec[GuildHouseUpdateInformationMessage] =
    new Codec[GuildHouseUpdateInformationMessage] {
      def decode: Get[GuildHouseUpdateInformationMessage] =
        for {
          housesInformations <- Codec[HouseInformationsForGuild].decode
        } yield GuildHouseUpdateInformationMessage(housesInformations)

      def encode(value: GuildHouseUpdateInformationMessage): ByteVector =
        Codec[HouseInformationsForGuild].encode(value.housesInformations)
    }
}
