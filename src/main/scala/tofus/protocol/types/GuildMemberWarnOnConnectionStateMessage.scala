package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMemberWarnOnConnectionStateMessage(
  enable: Boolean
) extends Message(6160)

object GuildMemberWarnOnConnectionStateMessage {
  implicit val codec: Codec[GuildMemberWarnOnConnectionStateMessage] =
    new Codec[GuildMemberWarnOnConnectionStateMessage] {
      def decode: Get[GuildMemberWarnOnConnectionStateMessage] =
        for {
          enable <- bool.decode
        } yield GuildMemberWarnOnConnectionStateMessage(enable)

      def encode(value: GuildMemberWarnOnConnectionStateMessage): ByteVector =
        bool.encode(value.enable)
    }
}
