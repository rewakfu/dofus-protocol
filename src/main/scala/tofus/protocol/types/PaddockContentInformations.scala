package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockContentInformations(
  maxOutdoorMount: Short,
  maxItems: Short,
  paddockId: Double,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short,
  abandonned: Boolean,
  mountsInformations: List[MountInformationsForPaddock]
) extends PaddockInformations {
  override val protocolId = 183
}

object PaddockContentInformations {
  implicit val codec: Codec[PaddockContentInformations] =
    new Codec[PaddockContentInformations] {
      def decode: Get[PaddockContentInformations] =
        for {
          maxOutdoorMount <- varShort.decode
          maxItems <- varShort.decode
          paddockId <- double.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          abandonned <- bool.decode
          mountsInformations <- list(ushort, Codec[MountInformationsForPaddock]).decode
        } yield PaddockContentInformations(maxOutdoorMount, maxItems, paddockId, worldX, worldY, mapId, subAreaId, abandonned, mountsInformations)

      def encode(value: PaddockContentInformations): ByteVector =
        varShort.encode(value.maxOutdoorMount) ++
        varShort.encode(value.maxItems) ++
        double.encode(value.paddockId) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        bool.encode(value.abandonned) ++
        list(ushort, Codec[MountInformationsForPaddock]).encode(value.mountsInformations)
    }
}
