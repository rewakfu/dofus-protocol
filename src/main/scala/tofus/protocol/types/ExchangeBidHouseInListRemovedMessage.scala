package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseInListRemovedMessage(
  itemUID: Int,
  objectGID: Short,
  objectType: Int
) extends Message(5950)

object ExchangeBidHouseInListRemovedMessage {
  implicit val codec: Codec[ExchangeBidHouseInListRemovedMessage] =
    new Codec[ExchangeBidHouseInListRemovedMessage] {
      def decode: Get[ExchangeBidHouseInListRemovedMessage] =
        for {
          itemUID <- int.decode
          objectGID <- varShort.decode
          objectType <- int.decode
        } yield ExchangeBidHouseInListRemovedMessage(itemUID, objectGID, objectType)

      def encode(value: ExchangeBidHouseInListRemovedMessage): ByteVector =
        int.encode(value.itemUID) ++
        varShort.encode(value.objectGID) ++
        int.encode(value.objectType)
    }
}
