package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CloseHavenBagFurnitureSequenceRequestMessage(

) extends Message(6621)

object CloseHavenBagFurnitureSequenceRequestMessage {
  implicit val codec: Codec[CloseHavenBagFurnitureSequenceRequestMessage] =
    Codec.const(CloseHavenBagFurnitureSequenceRequestMessage())
}
