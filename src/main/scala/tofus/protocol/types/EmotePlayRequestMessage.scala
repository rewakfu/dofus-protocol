package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmotePlayRequestMessage(
  emoteId: Short
) extends Message(5685)

object EmotePlayRequestMessage {
  implicit val codec: Codec[EmotePlayRequestMessage] =
    new Codec[EmotePlayRequestMessage] {
      def decode: Get[EmotePlayRequestMessage] =
        for {
          emoteId <- ubyte.decode
        } yield EmotePlayRequestMessage(emoteId)

      def encode(value: EmotePlayRequestMessage): ByteVector =
        ubyte.encode(value.emoteId)
    }
}
