package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChallengeTargetUpdateMessage(
  challengeId: Short,
  targetId: Double
) extends Message(6123)

object ChallengeTargetUpdateMessage {
  implicit val codec: Codec[ChallengeTargetUpdateMessage] =
    new Codec[ChallengeTargetUpdateMessage] {
      def decode: Get[ChallengeTargetUpdateMessage] =
        for {
          challengeId <- varShort.decode
          targetId <- double.decode
        } yield ChallengeTargetUpdateMessage(challengeId, targetId)

      def encode(value: ChallengeTargetUpdateMessage): ByteVector =
        varShort.encode(value.challengeId) ++
        double.encode(value.targetId)
    }
}
