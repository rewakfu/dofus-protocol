package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterSelectionWithRemodelMessage(
  id: Long,
  remodel: RemodelingInformation
) extends Message(6549)

object CharacterSelectionWithRemodelMessage {
  implicit val codec: Codec[CharacterSelectionWithRemodelMessage] =
    new Codec[CharacterSelectionWithRemodelMessage] {
      def decode: Get[CharacterSelectionWithRemodelMessage] =
        for {
          id <- varLong.decode
          remodel <- Codec[RemodelingInformation].decode
        } yield CharacterSelectionWithRemodelMessage(id, remodel)

      def encode(value: CharacterSelectionWithRemodelMessage): ByteVector =
        varLong.encode(value.id) ++
        Codec[RemodelingInformation].encode(value.remodel)
    }
}
