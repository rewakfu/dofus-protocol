package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorGuildInformations(
  guild: ConcreteBasicGuildInformations
) extends TaxCollectorComplementaryInformations {
  override val protocolId = 446
}

object TaxCollectorGuildInformations {
  implicit val codec: Codec[TaxCollectorGuildInformations] =
    new Codec[TaxCollectorGuildInformations] {
      def decode: Get[TaxCollectorGuildInformations] =
        for {
          guild <- Codec[ConcreteBasicGuildInformations].decode
        } yield TaxCollectorGuildInformations(guild)

      def encode(value: TaxCollectorGuildInformations): ByteVector =
        Codec[ConcreteBasicGuildInformations].encode(value.guild)
    }
}
