package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareReward(
  `type`: Byte,
  monsterId: Short,
  kamas: Long,
  dareId: Double
) extends ProtocolType {
  override val protocolId = 505
}

object DareReward {
  implicit val codec: Codec[DareReward] =
    new Codec[DareReward] {
      def decode: Get[DareReward] =
        for {
          `type` <- byte.decode
          monsterId <- varShort.decode
          kamas <- varLong.decode
          dareId <- double.decode
        } yield DareReward(`type`, monsterId, kamas, dareId)

      def encode(value: DareReward): ByteVector =
        byte.encode(value.`type`) ++
        varShort.encode(value.monsterId) ++
        varLong.encode(value.kamas) ++
        double.encode(value.dareId)
    }
}
