package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterReplayRequestMessage(
  characterId: Long
) extends Message(167)

object CharacterReplayRequestMessage {
  implicit val codec: Codec[CharacterReplayRequestMessage] =
    new Codec[CharacterReplayRequestMessage] {
      def decode: Get[CharacterReplayRequestMessage] =
        for {
          characterId <- varLong.decode
        } yield CharacterReplayRequestMessage(characterId)

      def encode(value: CharacterReplayRequestMessage): ByteVector =
        varLong.encode(value.characterId)
    }
}
