package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class KamaDebtInformation(
  id: Double,
  timestamp: Double,
  kamas: Long
) extends DebtInformation {
  override val protocolId = 580
}

object KamaDebtInformation {
  implicit val codec: Codec[KamaDebtInformation] =
    new Codec[KamaDebtInformation] {
      def decode: Get[KamaDebtInformation] =
        for {
          id <- double.decode
          timestamp <- double.decode
          kamas <- varLong.decode
        } yield KamaDebtInformation(id, timestamp, kamas)

      def encode(value: KamaDebtInformation): ByteVector =
        double.encode(value.id) ++
        double.encode(value.timestamp) ++
        varLong.encode(value.kamas)
    }
}
