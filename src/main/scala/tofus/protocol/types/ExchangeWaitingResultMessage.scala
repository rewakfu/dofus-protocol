package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeWaitingResultMessage(
  bwait: Boolean
) extends Message(5786)

object ExchangeWaitingResultMessage {
  implicit val codec: Codec[ExchangeWaitingResultMessage] =
    new Codec[ExchangeWaitingResultMessage] {
      def decode: Get[ExchangeWaitingResultMessage] =
        for {
          bwait <- bool.decode
        } yield ExchangeWaitingResultMessage(bwait)

      def encode(value: ExchangeWaitingResultMessage): ByteVector =
        bool.encode(value.bwait)
    }
}
