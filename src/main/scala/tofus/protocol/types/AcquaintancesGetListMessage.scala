package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AcquaintancesGetListMessage(

) extends Message(6819)

object AcquaintancesGetListMessage {
  implicit val codec: Codec[AcquaintancesGetListMessage] =
    Codec.const(AcquaintancesGetListMessage())
}
