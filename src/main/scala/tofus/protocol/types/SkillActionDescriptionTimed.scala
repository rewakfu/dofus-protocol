package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait SkillActionDescriptionTimed extends SkillActionDescription

final case class ConcreteSkillActionDescriptionTimed(
  skillId: Short,
  time: Short
) extends SkillActionDescriptionTimed {
  override val protocolId = 103
}

object ConcreteSkillActionDescriptionTimed {
  implicit val codec: Codec[ConcreteSkillActionDescriptionTimed] =  
    new Codec[ConcreteSkillActionDescriptionTimed] {
      def decode: Get[ConcreteSkillActionDescriptionTimed] =
        for {
          skillId <- varShort.decode
          time <- ubyte.decode
        } yield ConcreteSkillActionDescriptionTimed(skillId, time)

      def encode(value: ConcreteSkillActionDescriptionTimed): ByteVector =
        varShort.encode(value.skillId) ++
        ubyte.encode(value.time)
    }
}

object SkillActionDescriptionTimed {
  implicit val codec: Codec[SkillActionDescriptionTimed] =
    new Codec[SkillActionDescriptionTimed] {
      def decode: Get[SkillActionDescriptionTimed] =
        ushort.decode.flatMap {
          case 103 => Codec[ConcreteSkillActionDescriptionTimed].decode
          case 99 => Codec[SkillActionDescriptionCollect].decode
        }

      def encode(value: SkillActionDescriptionTimed): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteSkillActionDescriptionTimed => Codec[ConcreteSkillActionDescriptionTimed].encode(i)
          case i: SkillActionDescriptionCollect => Codec[SkillActionDescriptionCollect].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
