package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementSwapPositionsMessage(
  dispositions: List[IdentifiedEntityDispositionInformations]
) extends Message(6544)

object GameFightPlacementSwapPositionsMessage {
  implicit val codec: Codec[GameFightPlacementSwapPositionsMessage] =
    new Codec[GameFightPlacementSwapPositionsMessage] {
      def decode: Get[GameFightPlacementSwapPositionsMessage] =
        for {
          dispositions <- listC(2, Codec[IdentifiedEntityDispositionInformations]).decode
        } yield GameFightPlacementSwapPositionsMessage(dispositions)

      def encode(value: GameFightPlacementSwapPositionsMessage): ByteVector =
        listC(2, Codec[IdentifiedEntityDispositionInformations]).encode(value.dispositions)
    }
}
