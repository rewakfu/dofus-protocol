package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait MonsterInGroupLightInformations extends ProtocolType

final case class ConcreteMonsterInGroupLightInformations(
  genericId: Int,
  grade: Byte,
  level: Short
) extends MonsterInGroupLightInformations {
  override val protocolId = 395
}

object ConcreteMonsterInGroupLightInformations {
  implicit val codec: Codec[ConcreteMonsterInGroupLightInformations] =  
    new Codec[ConcreteMonsterInGroupLightInformations] {
      def decode: Get[ConcreteMonsterInGroupLightInformations] =
        for {
          genericId <- int.decode
          grade <- byte.decode
          level <- short.decode
        } yield ConcreteMonsterInGroupLightInformations(genericId, grade, level)

      def encode(value: ConcreteMonsterInGroupLightInformations): ByteVector =
        int.encode(value.genericId) ++
        byte.encode(value.grade) ++
        short.encode(value.level)
    }
}

object MonsterInGroupLightInformations {
  implicit val codec: Codec[MonsterInGroupLightInformations] =
    new Codec[MonsterInGroupLightInformations] {
      def decode: Get[MonsterInGroupLightInformations] =
        ushort.decode.flatMap {
          case 395 => Codec[ConcreteMonsterInGroupLightInformations].decode
          case 144 => Codec[MonsterInGroupInformations].decode
        }

      def encode(value: MonsterInGroupLightInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteMonsterInGroupLightInformations => Codec[ConcreteMonsterInGroupLightInformations].encode(i)
          case i: MonsterInGroupInformations => Codec[MonsterInGroupInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
