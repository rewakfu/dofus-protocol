package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class URLOpenMessage(
  urlId: Byte
) extends Message(6266)

object URLOpenMessage {
  implicit val codec: Codec[URLOpenMessage] =
    new Codec[URLOpenMessage] {
      def decode: Get[URLOpenMessage] =
        for {
          urlId <- byte.decode
        } yield URLOpenMessage(urlId)

      def encode(value: URLOpenMessage): ByteVector =
        byte.encode(value.urlId)
    }
}
