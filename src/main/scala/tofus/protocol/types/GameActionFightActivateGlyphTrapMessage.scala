package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightActivateGlyphTrapMessage(
  actionId: Short,
  sourceId: Double,
  markId: Short,
  active: Boolean
) extends Message(6545)

object GameActionFightActivateGlyphTrapMessage {
  implicit val codec: Codec[GameActionFightActivateGlyphTrapMessage] =
    new Codec[GameActionFightActivateGlyphTrapMessage] {
      def decode: Get[GameActionFightActivateGlyphTrapMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          markId <- short.decode
          active <- bool.decode
        } yield GameActionFightActivateGlyphTrapMessage(actionId, sourceId, markId, active)

      def encode(value: GameActionFightActivateGlyphTrapMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        short.encode(value.markId) ++
        bool.encode(value.active)
    }
}
