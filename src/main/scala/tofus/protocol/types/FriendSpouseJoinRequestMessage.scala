package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendSpouseJoinRequestMessage(

) extends Message(5604)

object FriendSpouseJoinRequestMessage {
  implicit val codec: Codec[FriendSpouseJoinRequestMessage] =
    Codec.const(FriendSpouseJoinRequestMessage())
}
