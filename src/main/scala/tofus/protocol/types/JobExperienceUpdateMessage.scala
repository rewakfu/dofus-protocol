package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobExperienceUpdateMessage(
  experiencesUpdate: JobExperience
) extends Message(5654)

object JobExperienceUpdateMessage {
  implicit val codec: Codec[JobExperienceUpdateMessage] =
    new Codec[JobExperienceUpdateMessage] {
      def decode: Get[JobExperienceUpdateMessage] =
        for {
          experiencesUpdate <- Codec[JobExperience].decode
        } yield JobExperienceUpdateMessage(experiencesUpdate)

      def encode(value: JobExperienceUpdateMessage): ByteVector =
        Codec[JobExperience].encode(value.experiencesUpdate)
    }
}
