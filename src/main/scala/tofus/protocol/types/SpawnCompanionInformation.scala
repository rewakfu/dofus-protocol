package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpawnCompanionInformation(
  modelId: Byte,
  level: Short,
  summonerId: Double,
  ownerId: Double
) extends SpawnInformation {
  override val protocolId = 573
}

object SpawnCompanionInformation {
  implicit val codec: Codec[SpawnCompanionInformation] =
    new Codec[SpawnCompanionInformation] {
      def decode: Get[SpawnCompanionInformation] =
        for {
          modelId <- byte.decode
          level <- varShort.decode
          summonerId <- double.decode
          ownerId <- double.decode
        } yield SpawnCompanionInformation(modelId, level, summonerId, ownerId)

      def encode(value: SpawnCompanionInformation): ByteVector =
        byte.encode(value.modelId) ++
        varShort.encode(value.level) ++
        double.encode(value.summonerId) ++
        double.encode(value.ownerId)
    }
}
