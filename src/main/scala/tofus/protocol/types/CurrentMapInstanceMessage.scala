package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CurrentMapInstanceMessage(
  mapId: Double,
  mapKey: String,
  instantiatedMapId: Double
) extends Message(6738)

object CurrentMapInstanceMessage {
  implicit val codec: Codec[CurrentMapInstanceMessage] =
    new Codec[CurrentMapInstanceMessage] {
      def decode: Get[CurrentMapInstanceMessage] =
        for {
          mapId <- double.decode
          mapKey <- utf8(ushort).decode
          instantiatedMapId <- double.decode
        } yield CurrentMapInstanceMessage(mapId, mapKey, instantiatedMapId)

      def encode(value: CurrentMapInstanceMessage): ByteVector =
        double.encode(value.mapId) ++
        utf8(ushort).encode(value.mapKey) ++
        double.encode(value.instantiatedMapId)
    }
}
