package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismGeolocalizedInformation(
  subAreaId: Short,
  allianceId: Int,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  prism: PrismInformation
) extends PrismSubareaEmptyInfo {
  override val protocolId = 434
}

object PrismGeolocalizedInformation {
  implicit val codec: Codec[PrismGeolocalizedInformation] =
    new Codec[PrismGeolocalizedInformation] {
      def decode: Get[PrismGeolocalizedInformation] =
        for {
          subAreaId <- varShort.decode
          allianceId <- varInt.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          prism <- Codec[PrismInformation].decode
        } yield PrismGeolocalizedInformation(subAreaId, allianceId, worldX, worldY, mapId, prism)

      def encode(value: PrismGeolocalizedInformation): ByteVector =
        varShort.encode(value.subAreaId) ++
        varInt.encode(value.allianceId) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        Codec[PrismInformation].encode(value.prism)
    }
}
