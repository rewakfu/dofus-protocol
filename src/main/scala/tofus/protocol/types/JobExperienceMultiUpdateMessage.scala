package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobExperienceMultiUpdateMessage(
  experiencesUpdate: List[JobExperience]
) extends Message(5809)

object JobExperienceMultiUpdateMessage {
  implicit val codec: Codec[JobExperienceMultiUpdateMessage] =
    new Codec[JobExperienceMultiUpdateMessage] {
      def decode: Get[JobExperienceMultiUpdateMessage] =
        for {
          experiencesUpdate <- list(ushort, Codec[JobExperience]).decode
        } yield JobExperienceMultiUpdateMessage(experiencesUpdate)

      def encode(value: JobExperienceMultiUpdateMessage): ByteVector =
        list(ushort, Codec[JobExperience]).encode(value.experiencesUpdate)
    }
}
