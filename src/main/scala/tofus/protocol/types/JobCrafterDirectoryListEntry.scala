package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryListEntry(
  playerInfo: JobCrafterDirectoryEntryPlayerInfo,
  jobInfo: JobCrafterDirectoryEntryJobInfo
) extends ProtocolType {
  override val protocolId = 196
}

object JobCrafterDirectoryListEntry {
  implicit val codec: Codec[JobCrafterDirectoryListEntry] =
    new Codec[JobCrafterDirectoryListEntry] {
      def decode: Get[JobCrafterDirectoryListEntry] =
        for {
          playerInfo <- Codec[JobCrafterDirectoryEntryPlayerInfo].decode
          jobInfo <- Codec[JobCrafterDirectoryEntryJobInfo].decode
        } yield JobCrafterDirectoryListEntry(playerInfo, jobInfo)

      def encode(value: JobCrafterDirectoryListEntry): ByteVector =
        Codec[JobCrafterDirectoryEntryPlayerInfo].encode(value.playerInfo) ++
        Codec[JobCrafterDirectoryEntryJobInfo].encode(value.jobInfo)
    }
}
