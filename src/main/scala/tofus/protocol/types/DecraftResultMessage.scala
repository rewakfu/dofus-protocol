package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DecraftResultMessage(
  results: List[DecraftedItemStackInfo]
) extends Message(6569)

object DecraftResultMessage {
  implicit val codec: Codec[DecraftResultMessage] =
    new Codec[DecraftResultMessage] {
      def decode: Get[DecraftResultMessage] =
        for {
          results <- list(ushort, Codec[DecraftedItemStackInfo]).decode
        } yield DecraftResultMessage(results)

      def encode(value: DecraftResultMessage): ByteVector =
        list(ushort, Codec[DecraftedItemStackInfo]).encode(value.results)
    }
}
