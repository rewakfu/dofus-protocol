package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectUseInWorkshopMessage(
  objectUID: Int,
  quantity: Int
) extends Message(6004)

object ExchangeObjectUseInWorkshopMessage {
  implicit val codec: Codec[ExchangeObjectUseInWorkshopMessage] =
    new Codec[ExchangeObjectUseInWorkshopMessage] {
      def decode: Get[ExchangeObjectUseInWorkshopMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ExchangeObjectUseInWorkshopMessage(objectUID, quantity)

      def encode(value: ExchangeObjectUseInWorkshopMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
