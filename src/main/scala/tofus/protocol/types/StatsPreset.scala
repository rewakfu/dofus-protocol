package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatsPreset(
  id: Short,
  stats: List[ConcreteSimpleCharacterCharacteristicForPreset]
) extends Preset {
  override val protocolId = 521
}

object StatsPreset {
  implicit val codec: Codec[StatsPreset] =
    new Codec[StatsPreset] {
      def decode: Get[StatsPreset] =
        for {
          id <- short.decode
          stats <- list(ushort, Codec[ConcreteSimpleCharacterCharacteristicForPreset]).decode
        } yield StatsPreset(id, stats)

      def encode(value: StatsPreset): ByteVector =
        short.encode(value.id) ++
        list(ushort, Codec[ConcreteSimpleCharacterCharacteristicForPreset]).encode(value.stats)
    }
}
