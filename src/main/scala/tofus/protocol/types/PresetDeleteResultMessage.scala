package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetDeleteResultMessage(
  presetId: Short,
  code: Byte
) extends Message(6748)

object PresetDeleteResultMessage {
  implicit val codec: Codec[PresetDeleteResultMessage] =
    new Codec[PresetDeleteResultMessage] {
      def decode: Get[PresetDeleteResultMessage] =
        for {
          presetId <- short.decode
          code <- byte.decode
        } yield PresetDeleteResultMessage(presetId, code)

      def encode(value: PresetDeleteResultMessage): ByteVector =
        short.encode(value.presetId) ++
        byte.encode(value.code)
    }
}
