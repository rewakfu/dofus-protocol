package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PurchasableDialogMessage(
  flags0: Byte,
  purchasableId: Double,
  purchasableInstanceId: Int,
  price: Long
) extends Message(5739)

object PurchasableDialogMessage {
  implicit val codec: Codec[PurchasableDialogMessage] =
    new Codec[PurchasableDialogMessage] {
      def decode: Get[PurchasableDialogMessage] =
        for {
          flags0 <- byte.decode
          purchasableId <- double.decode
          purchasableInstanceId <- int.decode
          price <- varLong.decode
        } yield PurchasableDialogMessage(flags0, purchasableId, purchasableInstanceId, price)

      def encode(value: PurchasableDialogMessage): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.purchasableId) ++
        int.encode(value.purchasableInstanceId) ++
        varLong.encode(value.price)
    }
}
