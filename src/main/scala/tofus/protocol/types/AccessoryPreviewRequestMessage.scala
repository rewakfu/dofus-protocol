package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccessoryPreviewRequestMessage(
  genericId: List[Short]
) extends Message(6518)

object AccessoryPreviewRequestMessage {
  implicit val codec: Codec[AccessoryPreviewRequestMessage] =
    new Codec[AccessoryPreviewRequestMessage] {
      def decode: Get[AccessoryPreviewRequestMessage] =
        for {
          genericId <- list(ushort, varShort).decode
        } yield AccessoryPreviewRequestMessage(genericId)

      def encode(value: AccessoryPreviewRequestMessage): ByteVector =
        list(ushort, varShort).encode(value.genericId)
    }
}
