package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectsRemovedMessage(
  remote: Boolean,
  objectUID: List[Int]
) extends Message(6532)

object ExchangeObjectsRemovedMessage {
  implicit val codec: Codec[ExchangeObjectsRemovedMessage] =
    new Codec[ExchangeObjectsRemovedMessage] {
      def decode: Get[ExchangeObjectsRemovedMessage] =
        for {
          remote <- bool.decode
          objectUID <- list(ushort, varInt).decode
        } yield ExchangeObjectsRemovedMessage(remote, objectUID)

      def encode(value: ExchangeObjectsRemovedMessage): ByteVector =
        bool.encode(value.remote) ++
        list(ushort, varInt).encode(value.objectUID)
    }
}
