package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterBuildPreset(
  id: Short,
  presets: List[Preset],
  iconId: Short,
  name: String
) extends PresetsContainerPreset {
  override val protocolId = 534
}

object CharacterBuildPreset {
  implicit val codec: Codec[CharacterBuildPreset] =
    new Codec[CharacterBuildPreset] {
      def decode: Get[CharacterBuildPreset] =
        for {
          id <- short.decode
          presets <- list(ushort, Codec[Preset]).decode
          iconId <- short.decode
          name <- utf8(ushort).decode
        } yield CharacterBuildPreset(id, presets, iconId, name)

      def encode(value: CharacterBuildPreset): ByteVector =
        short.encode(value.id) ++
        list(ushort, Codec[Preset]).encode(value.presets) ++
        short.encode(value.iconId) ++
        utf8(ushort).encode(value.name)
    }
}
