package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItemInformationWithQuantity(
  objectGID: Short,
  effects: List[ObjectEffect],
  quantity: Int
) extends ObjectItemMinimalInformation {
  override val protocolId = 387
}

object ObjectItemInformationWithQuantity {
  implicit val codec: Codec[ObjectItemInformationWithQuantity] =
    new Codec[ObjectItemInformationWithQuantity] {
      def decode: Get[ObjectItemInformationWithQuantity] =
        for {
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          quantity <- varInt.decode
        } yield ObjectItemInformationWithQuantity(objectGID, effects, quantity)

      def encode(value: ObjectItemInformationWithQuantity): ByteVector =
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        varInt.encode(value.quantity)
    }
}
