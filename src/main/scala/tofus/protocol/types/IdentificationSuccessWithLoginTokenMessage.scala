package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdentificationSuccessWithLoginTokenMessage(
  flags0: Byte,
  login: String,
  nickname: String,
  accountId: Int,
  communityId: Byte,
  secretQuestion: String,
  accountCreation: Double,
  subscriptionElapsedDuration: Double,
  subscriptionEndDate: Double,
  havenbagAvailableRoom: Short,
  loginToken: String
) extends Message(6209)

object IdentificationSuccessWithLoginTokenMessage {
  implicit val codec: Codec[IdentificationSuccessWithLoginTokenMessage] =
    new Codec[IdentificationSuccessWithLoginTokenMessage] {
      def decode: Get[IdentificationSuccessWithLoginTokenMessage] =
        for {
          flags0 <- byte.decode
          login <- utf8(ushort).decode
          nickname <- utf8(ushort).decode
          accountId <- int.decode
          communityId <- byte.decode
          secretQuestion <- utf8(ushort).decode
          accountCreation <- double.decode
          subscriptionElapsedDuration <- double.decode
          subscriptionEndDate <- double.decode
          havenbagAvailableRoom <- ubyte.decode
          loginToken <- utf8(ushort).decode
        } yield IdentificationSuccessWithLoginTokenMessage(flags0, login, nickname, accountId, communityId, secretQuestion, accountCreation, subscriptionElapsedDuration, subscriptionEndDate, havenbagAvailableRoom, loginToken)

      def encode(value: IdentificationSuccessWithLoginTokenMessage): ByteVector =
        byte.encode(value.flags0) ++
        utf8(ushort).encode(value.login) ++
        utf8(ushort).encode(value.nickname) ++
        int.encode(value.accountId) ++
        byte.encode(value.communityId) ++
        utf8(ushort).encode(value.secretQuestion) ++
        double.encode(value.accountCreation) ++
        double.encode(value.subscriptionElapsedDuration) ++
        double.encode(value.subscriptionEndDate) ++
        ubyte.encode(value.havenbagAvailableRoom) ++
        utf8(ushort).encode(value.loginToken)
    }
}
