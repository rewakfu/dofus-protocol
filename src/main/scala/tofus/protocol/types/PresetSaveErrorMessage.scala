package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetSaveErrorMessage(
  presetId: Short,
  code: Byte
) extends Message(6762)

object PresetSaveErrorMessage {
  implicit val codec: Codec[PresetSaveErrorMessage] =
    new Codec[PresetSaveErrorMessage] {
      def decode: Get[PresetSaveErrorMessage] =
        for {
          presetId <- short.decode
          code <- byte.decode
        } yield PresetSaveErrorMessage(presetId, code)

      def encode(value: PresetSaveErrorMessage): ByteVector =
        short.encode(value.presetId) ++
        byte.encode(value.code)
    }
}
