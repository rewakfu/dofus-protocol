package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightSpellCooldown(
  spellId: Int,
  cooldown: Byte
) extends ProtocolType {
  override val protocolId = 205
}

object GameFightSpellCooldown {
  implicit val codec: Codec[GameFightSpellCooldown] =
    new Codec[GameFightSpellCooldown] {
      def decode: Get[GameFightSpellCooldown] =
        for {
          spellId <- int.decode
          cooldown <- byte.decode
        } yield GameFightSpellCooldown(spellId, cooldown)

      def encode(value: GameFightSpellCooldown): ByteVector =
        int.encode(value.spellId) ++
        byte.encode(value.cooldown)
    }
}
