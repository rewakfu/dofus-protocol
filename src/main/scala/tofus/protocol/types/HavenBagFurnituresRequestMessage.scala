package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagFurnituresRequestMessage(
  cellIds: List[Short],
  funitureIds: List[Int],
  orientations: ByteVector
) extends Message(6637)

object HavenBagFurnituresRequestMessage {
  implicit val codec: Codec[HavenBagFurnituresRequestMessage] =
    new Codec[HavenBagFurnituresRequestMessage] {
      def decode: Get[HavenBagFurnituresRequestMessage] =
        for {
          cellIds <- list(ushort, varShort).decode
          funitureIds <- list(ushort, int).decode
          orientations <- bytes(ushort).decode
        } yield HavenBagFurnituresRequestMessage(cellIds, funitureIds, orientations)

      def encode(value: HavenBagFurnituresRequestMessage): ByteVector =
        list(ushort, varShort).encode(value.cellIds) ++
        list(ushort, int).encode(value.funitureIds) ++
        bytes(ushort).encode(value.orientations)
    }
}
