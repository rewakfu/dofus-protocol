package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ArenaRanking(
  rank: Short,
  bestRank: Short
) extends ProtocolType {
  override val protocolId = 554
}

object ArenaRanking {
  implicit val codec: Codec[ArenaRanking] =
    new Codec[ArenaRanking] {
      def decode: Get[ArenaRanking] =
        for {
          rank <- varShort.decode
          bestRank <- varShort.decode
        } yield ArenaRanking(rank, bestRank)

      def encode(value: ArenaRanking): ByteVector =
        varShort.encode(value.rank) ++
        varShort.encode(value.bestRank)
    }
}
