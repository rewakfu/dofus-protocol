package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AcquaintanceServerListMessage(
  servers: List[Short]
) extends Message(6142)

object AcquaintanceServerListMessage {
  implicit val codec: Codec[AcquaintanceServerListMessage] =
    new Codec[AcquaintanceServerListMessage] {
      def decode: Get[AcquaintanceServerListMessage] =
        for {
          servers <- list(ushort, varShort).decode
        } yield AcquaintanceServerListMessage(servers)

      def encode(value: AcquaintanceServerListMessage): ByteVector =
        list(ushort, varShort).encode(value.servers)
    }
}
