package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ItemNoMoreAvailableMessage(

) extends Message(5769)

object ItemNoMoreAvailableMessage {
  implicit val codec: Codec[ItemNoMoreAvailableMessage] =
    Codec.const(ItemNoMoreAvailableMessage())
}
