package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NpcDialogReplyMessage(
  replyId: Int
) extends Message(5616)

object NpcDialogReplyMessage {
  implicit val codec: Codec[NpcDialogReplyMessage] =
    new Codec[NpcDialogReplyMessage] {
      def decode: Get[NpcDialogReplyMessage] =
        for {
          replyId <- varInt.decode
        } yield NpcDialogReplyMessage(replyId)

      def encode(value: NpcDialogReplyMessage): ByteVector =
        varInt.encode(value.replyId)
    }
}
