package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendSetWarnOnLevelGainMessage(
  enable: Boolean
) extends Message(6077)

object FriendSetWarnOnLevelGainMessage {
  implicit val codec: Codec[FriendSetWarnOnLevelGainMessage] =
    new Codec[FriendSetWarnOnLevelGainMessage] {
      def decode: Get[FriendSetWarnOnLevelGainMessage] =
        for {
          enable <- bool.decode
        } yield FriendSetWarnOnLevelGainMessage(enable)

      def encode(value: FriendSetWarnOnLevelGainMessage): ByteVector =
        bool.encode(value.enable)
    }
}
