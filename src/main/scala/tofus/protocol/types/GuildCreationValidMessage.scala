package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildCreationValidMessage(
  guildName: String,
  guildEmblem: GuildEmblem
) extends Message(5546)

object GuildCreationValidMessage {
  implicit val codec: Codec[GuildCreationValidMessage] =
    new Codec[GuildCreationValidMessage] {
      def decode: Get[GuildCreationValidMessage] =
        for {
          guildName <- utf8(ushort).decode
          guildEmblem <- Codec[GuildEmblem].decode
        } yield GuildCreationValidMessage(guildName, guildEmblem)

      def encode(value: GuildCreationValidMessage): ByteVector =
        utf8(ushort).encode(value.guildName) ++
        Codec[GuildEmblem].encode(value.guildEmblem)
    }
}
