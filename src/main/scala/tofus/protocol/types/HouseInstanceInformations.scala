package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait HouseInstanceInformations extends ProtocolType

final case class ConcreteHouseInstanceInformations(
  flags0: Byte,
  instanceId: Int,
  ownerName: String,
  price: Long
) extends HouseInstanceInformations {
  override val protocolId = 511
}

object ConcreteHouseInstanceInformations {
  implicit val codec: Codec[ConcreteHouseInstanceInformations] =  
    new Codec[ConcreteHouseInstanceInformations] {
      def decode: Get[ConcreteHouseInstanceInformations] =
        for {
          flags0 <- byte.decode
          instanceId <- int.decode
          ownerName <- utf8(ushort).decode
          price <- varLong.decode
        } yield ConcreteHouseInstanceInformations(flags0, instanceId, ownerName, price)

      def encode(value: ConcreteHouseInstanceInformations): ByteVector =
        byte.encode(value.flags0) ++
        int.encode(value.instanceId) ++
        utf8(ushort).encode(value.ownerName) ++
        varLong.encode(value.price)
    }
}

object HouseInstanceInformations {
  implicit val codec: Codec[HouseInstanceInformations] =
    new Codec[HouseInstanceInformations] {
      def decode: Get[HouseInstanceInformations] =
        ushort.decode.flatMap {
          case 511 => Codec[ConcreteHouseInstanceInformations].decode
          case 512 => Codec[HouseGuildedInformations].decode
        }

      def encode(value: HouseInstanceInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteHouseInstanceInformations => Codec[ConcreteHouseInstanceInformations].encode(i)
          case i: HouseGuildedInformations => Codec[HouseGuildedInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
