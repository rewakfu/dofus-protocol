package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseInListAddedMessage(
  itemUID: Int,
  objectGID: Short,
  objectType: Int,
  effects: List[ObjectEffect],
  prices: List[Long]
) extends Message(5949)

object ExchangeBidHouseInListAddedMessage {
  implicit val codec: Codec[ExchangeBidHouseInListAddedMessage] =
    new Codec[ExchangeBidHouseInListAddedMessage] {
      def decode: Get[ExchangeBidHouseInListAddedMessage] =
        for {
          itemUID <- int.decode
          objectGID <- varShort.decode
          objectType <- int.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          prices <- list(ushort, varLong).decode
        } yield ExchangeBidHouseInListAddedMessage(itemUID, objectGID, objectType, effects, prices)

      def encode(value: ExchangeBidHouseInListAddedMessage): ByteVector =
        int.encode(value.itemUID) ++
        varShort.encode(value.objectGID) ++
        int.encode(value.objectType) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        list(ushort, varLong).encode(value.prices)
    }
}
