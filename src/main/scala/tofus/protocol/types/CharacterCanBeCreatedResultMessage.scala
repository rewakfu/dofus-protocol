package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterCanBeCreatedResultMessage(
  yesYouCan: Boolean
) extends Message(6733)

object CharacterCanBeCreatedResultMessage {
  implicit val codec: Codec[CharacterCanBeCreatedResultMessage] =
    new Codec[CharacterCanBeCreatedResultMessage] {
      def decode: Get[CharacterCanBeCreatedResultMessage] =
        for {
          yesYouCan <- bool.decode
        } yield CharacterCanBeCreatedResultMessage(yesYouCan)

      def encode(value: CharacterCanBeCreatedResultMessage): ByteVector =
        bool.encode(value.yesYouCan)
    }
}
