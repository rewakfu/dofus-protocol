package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorStateUpdateMessage(
  uniqueId: Double,
  state: Byte
) extends Message(6455)

object TaxCollectorStateUpdateMessage {
  implicit val codec: Codec[TaxCollectorStateUpdateMessage] =
    new Codec[TaxCollectorStateUpdateMessage] {
      def decode: Get[TaxCollectorStateUpdateMessage] =
        for {
          uniqueId <- double.decode
          state <- byte.decode
        } yield TaxCollectorStateUpdateMessage(uniqueId, state)

      def encode(value: TaxCollectorStateUpdateMessage): ByteVector =
        double.encode(value.uniqueId) ++
        byte.encode(value.state)
    }
}
