package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightFighterTaxCollectorLightInformations(
  flags0: Byte,
  id: Double,
  wave: Byte,
  level: Short,
  breed: Byte,
  firstNameId: Short,
  lastNameId: Short
) extends GameFightFighterLightInformations {
  override val protocolId = 457
}

object GameFightFighterTaxCollectorLightInformations {
  implicit val codec: Codec[GameFightFighterTaxCollectorLightInformations] =
    new Codec[GameFightFighterTaxCollectorLightInformations] {
      def decode: Get[GameFightFighterTaxCollectorLightInformations] =
        for {
          flags0 <- byte.decode
          id <- double.decode
          wave <- byte.decode
          level <- varShort.decode
          breed <- byte.decode
          firstNameId <- varShort.decode
          lastNameId <- varShort.decode
        } yield GameFightFighterTaxCollectorLightInformations(flags0, id, wave, level, breed, firstNameId, lastNameId)

      def encode(value: GameFightFighterTaxCollectorLightInformations): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.id) ++
        byte.encode(value.wave) ++
        varShort.encode(value.level) ++
        byte.encode(value.breed) ++
        varShort.encode(value.firstNameId) ++
        varShort.encode(value.lastNameId)
    }
}
