package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AnomalySubareaInformation(
  subAreaId: Short,
  rewardRate: Short,
  hasAnomaly: Boolean,
  anomalyClosingTime: Long
) extends ProtocolType {
  override val protocolId = 565
}

object AnomalySubareaInformation {
  implicit val codec: Codec[AnomalySubareaInformation] =
    new Codec[AnomalySubareaInformation] {
      def decode: Get[AnomalySubareaInformation] =
        for {
          subAreaId <- varShort.decode
          rewardRate <- varShort.decode
          hasAnomaly <- bool.decode
          anomalyClosingTime <- varLong.decode
        } yield AnomalySubareaInformation(subAreaId, rewardRate, hasAnomaly, anomalyClosingTime)

      def encode(value: AnomalySubareaInformation): ByteVector =
        varShort.encode(value.subAreaId) ++
        varShort.encode(value.rewardRate) ++
        bool.encode(value.hasAnomaly) ++
        varLong.encode(value.anomalyClosingTime)
    }
}
