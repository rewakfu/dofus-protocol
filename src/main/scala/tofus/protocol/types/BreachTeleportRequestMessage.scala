package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachTeleportRequestMessage(

) extends Message(6817)

object BreachTeleportRequestMessage {
  implicit val codec: Codec[BreachTeleportRequestMessage] =
    Codec.const(BreachTeleportRequestMessage())
}
