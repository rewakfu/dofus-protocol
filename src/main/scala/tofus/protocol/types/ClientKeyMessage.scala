package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ClientKeyMessage(
  key: String
) extends Message(5607)

object ClientKeyMessage {
  implicit val codec: Codec[ClientKeyMessage] =
    new Codec[ClientKeyMessage] {
      def decode: Get[ClientKeyMessage] =
        for {
          key <- utf8(ushort).decode
        } yield ClientKeyMessage(key)

      def encode(value: ClientKeyMessage): ByteVector =
        utf8(ushort).encode(value.key)
    }
}
