package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LeagueFriendInformations(
  accountId: Int,
  accountName: String,
  playerId: Long,
  playerName: String,
  breed: Byte,
  sex: Boolean,
  level: Short,
  leagueId: Short,
  totalLeaguePoints: Short,
  ladderPosition: Int
) extends AbstractContactInformations {
  override val protocolId = 555
}

object LeagueFriendInformations {
  implicit val codec: Codec[LeagueFriendInformations] =
    new Codec[LeagueFriendInformations] {
      def decode: Get[LeagueFriendInformations] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
          breed <- byte.decode
          sex <- bool.decode
          level <- varShort.decode
          leagueId <- varShort.decode
          totalLeaguePoints <- varShort.decode
          ladderPosition <- int.decode
        } yield LeagueFriendInformations(accountId, accountName, playerId, playerName, breed, sex, level, leagueId, totalLeaguePoints, ladderPosition)

      def encode(value: LeagueFriendInformations): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName) ++
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        varShort.encode(value.level) ++
        varShort.encode(value.leagueId) ++
        varShort.encode(value.totalLeaguePoints) ++
        int.encode(value.ladderPosition)
    }
}
