package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait DebtInformation extends ProtocolType

final case class ConcreteDebtInformation(
  id: Double,
  timestamp: Double
) extends DebtInformation {
  override val protocolId = 579
}

object ConcreteDebtInformation {
  implicit val codec: Codec[ConcreteDebtInformation] =  
    new Codec[ConcreteDebtInformation] {
      def decode: Get[ConcreteDebtInformation] =
        for {
          id <- double.decode
          timestamp <- double.decode
        } yield ConcreteDebtInformation(id, timestamp)

      def encode(value: ConcreteDebtInformation): ByteVector =
        double.encode(value.id) ++
        double.encode(value.timestamp)
    }
}

object DebtInformation {
  implicit val codec: Codec[DebtInformation] =
    new Codec[DebtInformation] {
      def decode: Get[DebtInformation] =
        ushort.decode.flatMap {
          case 579 => Codec[ConcreteDebtInformation].decode
          case 580 => Codec[KamaDebtInformation].decode
        }

      def encode(value: DebtInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteDebtInformation => Codec[ConcreteDebtInformation].encode(i)
          case i: KamaDebtInformation => Codec[KamaDebtInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
