package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseSearchMessage(
  genId: Short,
  follow: Boolean
) extends Message(5806)

object ExchangeBidHouseSearchMessage {
  implicit val codec: Codec[ExchangeBidHouseSearchMessage] =
    new Codec[ExchangeBidHouseSearchMessage] {
      def decode: Get[ExchangeBidHouseSearchMessage] =
        for {
          genId <- varShort.decode
          follow <- bool.decode
        } yield ExchangeBidHouseSearchMessage(genId, follow)

      def encode(value: ExchangeBidHouseSearchMessage): ByteVector =
        varShort.encode(value.genId) ++
        bool.encode(value.follow)
    }
}
