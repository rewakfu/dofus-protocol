package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectJobAddedMessage(
  jobId: Byte
) extends Message(6014)

object ObjectJobAddedMessage {
  implicit val codec: Codec[ObjectJobAddedMessage] =
    new Codec[ObjectJobAddedMessage] {
      def decode: Get[ObjectJobAddedMessage] =
        for {
          jobId <- byte.decode
        } yield ObjectJobAddedMessage(jobId)

      def encode(value: ObjectJobAddedMessage): ByteVector =
        byte.encode(value.jobId)
    }
}
