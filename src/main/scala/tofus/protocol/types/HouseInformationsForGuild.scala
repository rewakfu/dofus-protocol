package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseInformationsForGuild(
  houseId: Int,
  modelId: Short,
  instanceId: Int,
  secondHand: Boolean,
  ownerName: String,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short,
  skillListIds: List[Int],
  guildshareParams: Int
) extends HouseInformations {
  override val protocolId = 170
}

object HouseInformationsForGuild {
  implicit val codec: Codec[HouseInformationsForGuild] =
    new Codec[HouseInformationsForGuild] {
      def decode: Get[HouseInformationsForGuild] =
        for {
          houseId <- varInt.decode
          modelId <- varShort.decode
          instanceId <- int.decode
          secondHand <- bool.decode
          ownerName <- utf8(ushort).decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          skillListIds <- list(ushort, int).decode
          guildshareParams <- varInt.decode
        } yield HouseInformationsForGuild(houseId, modelId, instanceId, secondHand, ownerName, worldX, worldY, mapId, subAreaId, skillListIds, guildshareParams)

      def encode(value: HouseInformationsForGuild): ByteVector =
        varInt.encode(value.houseId) ++
        varShort.encode(value.modelId) ++
        int.encode(value.instanceId) ++
        bool.encode(value.secondHand) ++
        utf8(ushort).encode(value.ownerName) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        list(ushort, int).encode(value.skillListIds) ++
        varInt.encode(value.guildshareParams)
    }
}
