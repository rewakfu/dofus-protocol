package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayShowActorWithEventMessage(
  informations: GameRolePlayActorInformations,
  actorEventId: Byte
) extends Message(6407)

object GameRolePlayShowActorWithEventMessage {
  implicit val codec: Codec[GameRolePlayShowActorWithEventMessage] =
    new Codec[GameRolePlayShowActorWithEventMessage] {
      def decode: Get[GameRolePlayShowActorWithEventMessage] =
        for {
          informations <- Codec[GameRolePlayActorInformations].decode
          actorEventId <- byte.decode
        } yield GameRolePlayShowActorWithEventMessage(informations, actorEventId)

      def encode(value: GameRolePlayShowActorWithEventMessage): ByteVector =
        Codec[GameRolePlayActorInformations].encode(value.informations) ++
        byte.encode(value.actorEventId)
    }
}
