package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayAggressionMessage(
  attackerId: Long,
  defenderId: Long
) extends Message(6073)

object GameRolePlayAggressionMessage {
  implicit val codec: Codec[GameRolePlayAggressionMessage] =
    new Codec[GameRolePlayAggressionMessage] {
      def decode: Get[GameRolePlayAggressionMessage] =
        for {
          attackerId <- varLong.decode
          defenderId <- varLong.decode
        } yield GameRolePlayAggressionMessage(attackerId, defenderId)

      def encode(value: GameRolePlayAggressionMessage): ByteVector =
        varLong.encode(value.attackerId) ++
        varLong.encode(value.defenderId)
    }
}
