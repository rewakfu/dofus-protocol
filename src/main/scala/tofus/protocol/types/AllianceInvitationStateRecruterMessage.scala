package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInvitationStateRecruterMessage(
  recrutedName: String,
  invitationState: Byte
) extends Message(6396)

object AllianceInvitationStateRecruterMessage {
  implicit val codec: Codec[AllianceInvitationStateRecruterMessage] =
    new Codec[AllianceInvitationStateRecruterMessage] {
      def decode: Get[AllianceInvitationStateRecruterMessage] =
        for {
          recrutedName <- utf8(ushort).decode
          invitationState <- byte.decode
        } yield AllianceInvitationStateRecruterMessage(recrutedName, invitationState)

      def encode(value: AllianceInvitationStateRecruterMessage): ByteVector =
        utf8(ushort).encode(value.recrutedName) ++
        byte.encode(value.invitationState)
    }
}
