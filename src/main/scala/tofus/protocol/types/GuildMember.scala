package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMember(
  id: Long,
  name: String,
  level: Short,
  flags0: Byte,
  breed: Byte,
  rank: Short,
  givenExperience: Long,
  experienceGivenPercent: Byte,
  rights: Int,
  connected: Byte,
  alignmentSide: Byte,
  hoursSinceLastConnection: Int,
  moodSmileyId: Short,
  accountId: Int,
  achievementPoints: Int,
  status: PlayerStatus
) extends CharacterMinimalInformations {
  override val protocolId = 88
}

object GuildMember {
  implicit val codec: Codec[GuildMember] =
    new Codec[GuildMember] {
      def decode: Get[GuildMember] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          flags0 <- byte.decode
          breed <- byte.decode
          rank <- varShort.decode
          givenExperience <- varLong.decode
          experienceGivenPercent <- byte.decode
          rights <- varInt.decode
          connected <- byte.decode
          alignmentSide <- byte.decode
          hoursSinceLastConnection <- ushort.decode
          moodSmileyId <- varShort.decode
          accountId <- int.decode
          achievementPoints <- int.decode
          status <- Codec[PlayerStatus].decode
        } yield GuildMember(id, name, level, flags0, breed, rank, givenExperience, experienceGivenPercent, rights, connected, alignmentSide, hoursSinceLastConnection, moodSmileyId, accountId, achievementPoints, status)

      def encode(value: GuildMember): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        byte.encode(value.flags0) ++
        byte.encode(value.breed) ++
        varShort.encode(value.rank) ++
        varLong.encode(value.givenExperience) ++
        byte.encode(value.experienceGivenPercent) ++
        varInt.encode(value.rights) ++
        byte.encode(value.connected) ++
        byte.encode(value.alignmentSide) ++
        ushort.encode(value.hoursSinceLastConnection) ++
        varShort.encode(value.moodSmileyId) ++
        int.encode(value.accountId) ++
        int.encode(value.achievementPoints) ++
        Codec[PlayerStatus].encode(value.status)
    }
}
