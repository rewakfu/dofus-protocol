package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectDropMessage(
  objectUID: Int,
  quantity: Int
) extends Message(3005)

object ObjectDropMessage {
  implicit val codec: Codec[ObjectDropMessage] =
    new Codec[ObjectDropMessage] {
      def decode: Get[ObjectDropMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ObjectDropMessage(objectUID, quantity)

      def encode(value: ObjectDropMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
