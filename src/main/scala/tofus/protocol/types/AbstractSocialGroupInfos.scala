package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AbstractSocialGroupInfos extends ProtocolType

final case class ConcreteAbstractSocialGroupInfos(

) extends AbstractSocialGroupInfos {
  override val protocolId = 416
}

object ConcreteAbstractSocialGroupInfos {
  implicit val codec: Codec[ConcreteAbstractSocialGroupInfos] =  
    Codec.const(ConcreteAbstractSocialGroupInfos())
}

object AbstractSocialGroupInfos {
  implicit val codec: Codec[AbstractSocialGroupInfos] =
    new Codec[AbstractSocialGroupInfos] {
      def decode: Get[AbstractSocialGroupInfos] =
        ushort.decode.flatMap {
          case 416 => Codec[ConcreteAbstractSocialGroupInfos].decode
          case 421 => Codec[AllianceFactSheetInformations].decode
          case 417 => Codec[ConcreteAllianceInformations].decode
          case 418 => Codec[ConcreteBasicNamedAllianceInformations].decode
          case 419 => Codec[ConcreteBasicAllianceInformations].decode
          case 423 => Codec[GuildInsiderFactSheetInformations].decode
          case 424 => Codec[ConcreteGuildFactSheetInformations].decode
          case 420 => Codec[GuildInAllianceInformations].decode
          case 422 => Codec[AlliancedGuildFactSheetInformations].decode
          case 127 => Codec[ConcreteGuildInformations].decode
          case 365 => Codec[ConcreteBasicGuildInformations].decode
        }

      def encode(value: AbstractSocialGroupInfos): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAbstractSocialGroupInfos => Codec[ConcreteAbstractSocialGroupInfos].encode(i)
          case i: AllianceFactSheetInformations => Codec[AllianceFactSheetInformations].encode(i)
          case i: ConcreteAllianceInformations => Codec[ConcreteAllianceInformations].encode(i)
          case i: ConcreteBasicNamedAllianceInformations => Codec[ConcreteBasicNamedAllianceInformations].encode(i)
          case i: ConcreteBasicAllianceInformations => Codec[ConcreteBasicAllianceInformations].encode(i)
          case i: GuildInsiderFactSheetInformations => Codec[GuildInsiderFactSheetInformations].encode(i)
          case i: ConcreteGuildFactSheetInformations => Codec[ConcreteGuildFactSheetInformations].encode(i)
          case i: GuildInAllianceInformations => Codec[GuildInAllianceInformations].encode(i)
          case i: AlliancedGuildFactSheetInformations => Codec[AlliancedGuildFactSheetInformations].encode(i)
          case i: ConcreteGuildInformations => Codec[ConcreteGuildInformations].encode(i)
          case i: ConcreteBasicGuildInformations => Codec[ConcreteBasicGuildInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
