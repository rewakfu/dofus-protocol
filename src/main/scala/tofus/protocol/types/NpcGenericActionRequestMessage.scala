package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NpcGenericActionRequestMessage(
  npcId: Int,
  npcActionId: Byte,
  npcMapId: Double
) extends Message(5898)

object NpcGenericActionRequestMessage {
  implicit val codec: Codec[NpcGenericActionRequestMessage] =
    new Codec[NpcGenericActionRequestMessage] {
      def decode: Get[NpcGenericActionRequestMessage] =
        for {
          npcId <- int.decode
          npcActionId <- byte.decode
          npcMapId <- double.decode
        } yield NpcGenericActionRequestMessage(npcId, npcActionId, npcMapId)

      def encode(value: NpcGenericActionRequestMessage): ByteVector =
        int.encode(value.npcId) ++
        byte.encode(value.npcActionId) ++
        double.encode(value.npcMapId)
    }
}
