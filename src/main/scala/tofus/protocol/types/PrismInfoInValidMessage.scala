package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismInfoInValidMessage(
  reason: Byte
) extends Message(5859)

object PrismInfoInValidMessage {
  implicit val codec: Codec[PrismInfoInValidMessage] =
    new Codec[PrismInfoInValidMessage] {
      def decode: Get[PrismInfoInValidMessage] =
        for {
          reason <- byte.decode
        } yield PrismInfoInValidMessage(reason)

      def encode(value: PrismInfoInValidMessage): ByteVector =
        byte.encode(value.reason)
    }
}
