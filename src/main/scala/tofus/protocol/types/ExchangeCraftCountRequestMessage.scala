package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftCountRequestMessage(
  count: Int
) extends Message(6597)

object ExchangeCraftCountRequestMessage {
  implicit val codec: Codec[ExchangeCraftCountRequestMessage] =
    new Codec[ExchangeCraftCountRequestMessage] {
      def decode: Get[ExchangeCraftCountRequestMessage] =
        for {
          count <- varInt.decode
        } yield ExchangeCraftCountRequestMessage(count)

      def encode(value: ExchangeCraftCountRequestMessage): ByteVector =
        varInt.encode(value.count)
    }
}
