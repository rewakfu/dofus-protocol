package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AnomalyStateMessage(
  subAreaId: Short,
  open: Boolean,
  closingTime: Long
) extends Message(6831)

object AnomalyStateMessage {
  implicit val codec: Codec[AnomalyStateMessage] =
    new Codec[AnomalyStateMessage] {
      def decode: Get[AnomalyStateMessage] =
        for {
          subAreaId <- varShort.decode
          open <- bool.decode
          closingTime <- varLong.decode
        } yield AnomalyStateMessage(subAreaId, open, closingTime)

      def encode(value: AnomalyStateMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        bool.encode(value.open) ++
        varLong.encode(value.closingTime)
    }
}
