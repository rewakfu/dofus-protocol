package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareSubscribedListMessage(
  daresFixedInfos: List[DareInformations],
  daresVersatilesInfos: List[DareVersatileInformations]
) extends Message(6658)

object DareSubscribedListMessage {
  implicit val codec: Codec[DareSubscribedListMessage] =
    new Codec[DareSubscribedListMessage] {
      def decode: Get[DareSubscribedListMessage] =
        for {
          daresFixedInfos <- list(ushort, Codec[DareInformations]).decode
          daresVersatilesInfos <- list(ushort, Codec[DareVersatileInformations]).decode
        } yield DareSubscribedListMessage(daresFixedInfos, daresVersatilesInfos)

      def encode(value: DareSubscribedListMessage): ByteVector =
        list(ushort, Codec[DareInformations]).encode(value.daresFixedInfos) ++
        list(ushort, Codec[DareVersatileInformations]).encode(value.daresVersatilesInfos)
    }
}
