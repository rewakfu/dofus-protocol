package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildModificationNameValidMessage(
  guildName: String
) extends Message(6327)

object GuildModificationNameValidMessage {
  implicit val codec: Codec[GuildModificationNameValidMessage] =
    new Codec[GuildModificationNameValidMessage] {
      def decode: Get[GuildModificationNameValidMessage] =
        for {
          guildName <- utf8(ushort).decode
        } yield GuildModificationNameValidMessage(guildName)

      def encode(value: GuildModificationNameValidMessage): ByteVector =
        utf8(ushort).encode(value.guildName)
    }
}
