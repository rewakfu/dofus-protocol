package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobAllowMultiCraftRequestMessage(
  enabled: Boolean
) extends Message(5748)

object JobAllowMultiCraftRequestMessage {
  implicit val codec: Codec[JobAllowMultiCraftRequestMessage] =
    new Codec[JobAllowMultiCraftRequestMessage] {
      def decode: Get[JobAllowMultiCraftRequestMessage] =
        for {
          enabled <- bool.decode
        } yield JobAllowMultiCraftRequestMessage(enabled)

      def encode(value: JobAllowMultiCraftRequestMessage): ByteVector =
        bool.encode(value.enabled)
    }
}
