package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightStartingMessage(
  fightType: Byte,
  fightId: Short,
  attackerId: Double,
  defenderId: Double
) extends Message(700)

object GameFightStartingMessage {
  implicit val codec: Codec[GameFightStartingMessage] =
    new Codec[GameFightStartingMessage] {
      def decode: Get[GameFightStartingMessage] =
        for {
          fightType <- byte.decode
          fightId <- varShort.decode
          attackerId <- double.decode
          defenderId <- double.decode
        } yield GameFightStartingMessage(fightType, fightId, attackerId, defenderId)

      def encode(value: GameFightStartingMessage): ByteVector =
        byte.encode(value.fightType) ++
        varShort.encode(value.fightId) ++
        double.encode(value.attackerId) ++
        double.encode(value.defenderId)
    }
}
