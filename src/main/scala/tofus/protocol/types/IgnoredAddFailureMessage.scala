package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredAddFailureMessage(
  reason: Byte
) extends Message(5679)

object IgnoredAddFailureMessage {
  implicit val codec: Codec[IgnoredAddFailureMessage] =
    new Codec[IgnoredAddFailureMessage] {
      def decode: Get[IgnoredAddFailureMessage] =
        for {
          reason <- byte.decode
        } yield IgnoredAddFailureMessage(reason)

      def encode(value: IgnoredAddFailureMessage): ByteVector =
        byte.encode(value.reason)
    }
}
