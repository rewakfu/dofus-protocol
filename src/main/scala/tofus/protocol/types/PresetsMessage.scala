package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetsMessage(
  presets: List[Preset]
) extends Message(6750)

object PresetsMessage {
  implicit val codec: Codec[PresetsMessage] =
    new Codec[PresetsMessage] {
      def decode: Get[PresetsMessage] =
        for {
          presets <- list(ushort, Codec[Preset]).decode
        } yield PresetsMessage(presets)

      def encode(value: PresetsMessage): ByteVector =
        list(ushort, Codec[Preset]).encode(value.presets)
    }
}
