package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendSetStatusShareMessage(
  share: Boolean
) extends Message(6822)

object FriendSetStatusShareMessage {
  implicit val codec: Codec[FriendSetStatusShareMessage] =
    new Codec[FriendSetStatusShareMessage] {
      def decode: Get[FriendSetStatusShareMessage] =
        for {
          share <- bool.decode
        } yield FriendSetStatusShareMessage(share)

      def encode(value: FriendSetStatusShareMessage): ByteVector =
        bool.encode(value.share)
    }
}
