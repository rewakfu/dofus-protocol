package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightOptionToggleMessage(
  option: Byte
) extends Message(707)

object GameFightOptionToggleMessage {
  implicit val codec: Codec[GameFightOptionToggleMessage] =
    new Codec[GameFightOptionToggleMessage] {
      def decode: Get[GameFightOptionToggleMessage] =
        for {
          option <- byte.decode
        } yield GameFightOptionToggleMessage(option)

      def encode(value: GameFightOptionToggleMessage): ByteVector =
        byte.encode(value.option)
    }
}
