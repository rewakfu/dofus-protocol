package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseGuildShareRequestMessage(
  houseId: Int,
  instanceId: Int,
  enable: Boolean,
  rights: Int
) extends Message(5704)

object HouseGuildShareRequestMessage {
  implicit val codec: Codec[HouseGuildShareRequestMessage] =
    new Codec[HouseGuildShareRequestMessage] {
      def decode: Get[HouseGuildShareRequestMessage] =
        for {
          houseId <- varInt.decode
          instanceId <- int.decode
          enable <- bool.decode
          rights <- varInt.decode
        } yield HouseGuildShareRequestMessage(houseId, instanceId, enable, rights)

      def encode(value: HouseGuildShareRequestMessage): ByteVector =
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId) ++
        bool.encode(value.enable) ++
        varInt.encode(value.rights)
    }
}
