package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseSellingUpdateMessage(
  houseId: Int,
  instanceId: Int,
  secondHand: Boolean,
  realPrice: Long,
  buyerName: String
) extends Message(6727)

object HouseSellingUpdateMessage {
  implicit val codec: Codec[HouseSellingUpdateMessage] =
    new Codec[HouseSellingUpdateMessage] {
      def decode: Get[HouseSellingUpdateMessage] =
        for {
          houseId <- varInt.decode
          instanceId <- int.decode
          secondHand <- bool.decode
          realPrice <- varLong.decode
          buyerName <- utf8(ushort).decode
        } yield HouseSellingUpdateMessage(houseId, instanceId, secondHand, realPrice, buyerName)

      def encode(value: HouseSellingUpdateMessage): ByteVector =
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId) ++
        bool.encode(value.secondHand) ++
        varLong.encode(value.realPrice) ++
        utf8(ushort).encode(value.buyerName)
    }
}
