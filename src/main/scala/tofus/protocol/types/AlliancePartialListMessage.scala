package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlliancePartialListMessage(
  alliances: List[AllianceFactSheetInformations]
) extends Message(6427)

object AlliancePartialListMessage {
  implicit val codec: Codec[AlliancePartialListMessage] =
    new Codec[AlliancePartialListMessage] {
      def decode: Get[AlliancePartialListMessage] =
        for {
          alliances <- list(ushort, Codec[AllianceFactSheetInformations]).decode
        } yield AlliancePartialListMessage(alliances)

      def encode(value: AlliancePartialListMessage): ByteVector =
        list(ushort, Codec[AllianceFactSheetInformations]).encode(value.alliances)
    }
}
