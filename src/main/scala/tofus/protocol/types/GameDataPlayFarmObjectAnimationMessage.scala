package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameDataPlayFarmObjectAnimationMessage(
  cellId: List[Short]
) extends Message(6026)

object GameDataPlayFarmObjectAnimationMessage {
  implicit val codec: Codec[GameDataPlayFarmObjectAnimationMessage] =
    new Codec[GameDataPlayFarmObjectAnimationMessage] {
      def decode: Get[GameDataPlayFarmObjectAnimationMessage] =
        for {
          cellId <- list(ushort, varShort).decode
        } yield GameDataPlayFarmObjectAnimationMessage(cellId)

      def encode(value: GameDataPlayFarmObjectAnimationMessage): ByteVector =
        list(ushort, varShort).encode(value.cellId)
    }
}
