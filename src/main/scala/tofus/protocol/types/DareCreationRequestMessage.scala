package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareCreationRequestMessage(
  flags0: Byte,
  subscriptionFee: Long,
  jackpot: Long,
  maxCountWinners: Int,
  delayBeforeStart: Long,
  duration: Long,
  criterions: List[DareCriteria]
) extends Message(6665)

object DareCreationRequestMessage {
  implicit val codec: Codec[DareCreationRequestMessage] =
    new Codec[DareCreationRequestMessage] {
      def decode: Get[DareCreationRequestMessage] =
        for {
          flags0 <- byte.decode
          subscriptionFee <- varLong.decode
          jackpot <- varLong.decode
          maxCountWinners <- ushort.decode
          delayBeforeStart <- uint.decode
          duration <- uint.decode
          criterions <- list(ushort, Codec[DareCriteria]).decode
        } yield DareCreationRequestMessage(flags0, subscriptionFee, jackpot, maxCountWinners, delayBeforeStart, duration, criterions)

      def encode(value: DareCreationRequestMessage): ByteVector =
        byte.encode(value.flags0) ++
        varLong.encode(value.subscriptionFee) ++
        varLong.encode(value.jackpot) ++
        ushort.encode(value.maxCountWinners) ++
        uint.encode(value.delayBeforeStart) ++
        uint.encode(value.duration) ++
        list(ushort, Codec[DareCriteria]).encode(value.criterions)
    }
}
