package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ItemDurability(
  durability: Short,
  durabilityMax: Short
) extends ProtocolType {
  override val protocolId = 168
}

object ItemDurability {
  implicit val codec: Codec[ItemDurability] =
    new Codec[ItemDurability] {
      def decode: Get[ItemDurability] =
        for {
          durability <- short.decode
          durabilityMax <- short.decode
        } yield ItemDurability(durability, durabilityMax)

      def encode(value: ItemDurability): ByteVector =
        short.encode(value.durability) ++
        short.encode(value.durabilityMax)
    }
}
