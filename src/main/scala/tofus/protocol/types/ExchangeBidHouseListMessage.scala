package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseListMessage(
  id: Short,
  follow: Boolean
) extends Message(5807)

object ExchangeBidHouseListMessage {
  implicit val codec: Codec[ExchangeBidHouseListMessage] =
    new Codec[ExchangeBidHouseListMessage] {
      def decode: Get[ExchangeBidHouseListMessage] =
        for {
          id <- varShort.decode
          follow <- bool.decode
        } yield ExchangeBidHouseListMessage(id, follow)

      def encode(value: ExchangeBidHouseListMessage): ByteVector =
        varShort.encode(value.id) ++
        bool.encode(value.follow)
    }
}
