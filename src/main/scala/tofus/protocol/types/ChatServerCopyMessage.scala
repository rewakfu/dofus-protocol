package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatServerCopyMessage(
  channel: Byte,
  content: String,
  timestamp: Int,
  fingerprint: String,
  receiverId: Long,
  receiverName: String
) extends Message(882)

object ChatServerCopyMessage {
  implicit val codec: Codec[ChatServerCopyMessage] =
    new Codec[ChatServerCopyMessage] {
      def decode: Get[ChatServerCopyMessage] =
        for {
          channel <- byte.decode
          content <- utf8(ushort).decode
          timestamp <- int.decode
          fingerprint <- utf8(ushort).decode
          receiverId <- varLong.decode
          receiverName <- utf8(ushort).decode
        } yield ChatServerCopyMessage(channel, content, timestamp, fingerprint, receiverId, receiverName)

      def encode(value: ChatServerCopyMessage): ByteVector =
        byte.encode(value.channel) ++
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        utf8(ushort).encode(value.fingerprint) ++
        varLong.encode(value.receiverId) ++
        utf8(ushort).encode(value.receiverName)
    }
}
