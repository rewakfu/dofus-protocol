package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportOnSameMapMessage(
  targetId: Double,
  cellId: Short
) extends Message(6048)

object TeleportOnSameMapMessage {
  implicit val codec: Codec[TeleportOnSameMapMessage] =
    new Codec[TeleportOnSameMapMessage] {
      def decode: Get[TeleportOnSameMapMessage] =
        for {
          targetId <- double.decode
          cellId <- varShort.decode
        } yield TeleportOnSameMapMessage(targetId, cellId)

      def encode(value: TeleportOnSameMapMessage): ByteVector =
        double.encode(value.targetId) ++
        varShort.encode(value.cellId)
    }
}
