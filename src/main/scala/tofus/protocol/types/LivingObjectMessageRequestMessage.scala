package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LivingObjectMessageRequestMessage(
  msgId: Short,
  parameters: List[String],
  livingObject: Int
) extends Message(6066)

object LivingObjectMessageRequestMessage {
  implicit val codec: Codec[LivingObjectMessageRequestMessage] =
    new Codec[LivingObjectMessageRequestMessage] {
      def decode: Get[LivingObjectMessageRequestMessage] =
        for {
          msgId <- varShort.decode
          parameters <- list(ushort, utf8(ushort)).decode
          livingObject <- varInt.decode
        } yield LivingObjectMessageRequestMessage(msgId, parameters, livingObject)

      def encode(value: LivingObjectMessageRequestMessage): ByteVector =
        varShort.encode(value.msgId) ++
        list(ushort, utf8(ushort)).encode(value.parameters) ++
        varInt.encode(value.livingObject)
    }
}
