package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareRewardsListMessage(
  rewards: List[DareReward]
) extends Message(6677)

object DareRewardsListMessage {
  implicit val codec: Codec[DareRewardsListMessage] =
    new Codec[DareRewardsListMessage] {
      def decode: Get[DareRewardsListMessage] =
        for {
          rewards <- list(ushort, Codec[DareReward]).decode
        } yield DareRewardsListMessage(rewards)

      def encode(value: DareRewardsListMessage): ByteVector =
        list(ushort, Codec[DareReward]).encode(value.rewards)
    }
}
