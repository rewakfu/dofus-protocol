package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerSelectionMessage(
  serverId: Short
) extends Message(40)

object ServerSelectionMessage {
  implicit val codec: Codec[ServerSelectionMessage] =
    new Codec[ServerSelectionMessage] {
      def decode: Get[ServerSelectionMessage] =
        for {
          serverId <- varShort.decode
        } yield ServerSelectionMessage(serverId)

      def encode(value: ServerSelectionMessage): ByteVector =
        varShort.encode(value.serverId)
    }
}
