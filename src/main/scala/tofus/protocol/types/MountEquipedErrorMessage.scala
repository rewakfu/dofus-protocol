package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountEquipedErrorMessage(
  errorType: Byte
) extends Message(5963)

object MountEquipedErrorMessage {
  implicit val codec: Codec[MountEquipedErrorMessage] =
    new Codec[MountEquipedErrorMessage] {
      def decode: Get[MountEquipedErrorMessage] =
        for {
          errorType <- byte.decode
        } yield MountEquipedErrorMessage(errorType)

      def encode(value: MountEquipedErrorMessage): ByteVector =
        byte.encode(value.errorType)
    }
}
