package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectGroundRemovedMultipleMessage(
  cells: List[Short]
) extends Message(5944)

object ObjectGroundRemovedMultipleMessage {
  implicit val codec: Codec[ObjectGroundRemovedMultipleMessage] =
    new Codec[ObjectGroundRemovedMultipleMessage] {
      def decode: Get[ObjectGroundRemovedMultipleMessage] =
        for {
          cells <- list(ushort, varShort).decode
        } yield ObjectGroundRemovedMultipleMessage(cells)

      def encode(value: ObjectGroundRemovedMultipleMessage): ByteVector =
        list(ushort, varShort).encode(value.cells)
    }
}
