package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpawnScaledMonsterInformation(
  creatureGenericId: Short,
  creatureLevel: Short
) extends BaseSpawnMonsterInformation {
  override val protocolId = 581
}

object SpawnScaledMonsterInformation {
  implicit val codec: Codec[SpawnScaledMonsterInformation] =
    new Codec[SpawnScaledMonsterInformation] {
      def decode: Get[SpawnScaledMonsterInformation] =
        for {
          creatureGenericId <- varShort.decode
          creatureLevel <- short.decode
        } yield SpawnScaledMonsterInformation(creatureGenericId, creatureLevel)

      def encode(value: SpawnScaledMonsterInformation): ByteVector =
        varShort.encode(value.creatureGenericId) ++
        short.encode(value.creatureLevel)
    }
}
