package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ZaapRespawnUpdatedMessage(
  mapId: Double
) extends Message(6571)

object ZaapRespawnUpdatedMessage {
  implicit val codec: Codec[ZaapRespawnUpdatedMessage] =
    new Codec[ZaapRespawnUpdatedMessage] {
      def decode: Get[ZaapRespawnUpdatedMessage] =
        for {
          mapId <- double.decode
        } yield ZaapRespawnUpdatedMessage(mapId)

      def encode(value: ZaapRespawnUpdatedMessage): ByteVector =
        double.encode(value.mapId)
    }
}
