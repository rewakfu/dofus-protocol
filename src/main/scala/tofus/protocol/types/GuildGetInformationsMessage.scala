package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildGetInformationsMessage(
  infoType: Byte
) extends Message(5550)

object GuildGetInformationsMessage {
  implicit val codec: Codec[GuildGetInformationsMessage] =
    new Codec[GuildGetInformationsMessage] {
      def decode: Get[GuildGetInformationsMessage] =
        for {
          infoType <- byte.decode
        } yield GuildGetInformationsMessage(infoType)

      def encode(value: GuildGetInformationsMessage): ByteVector =
        byte.encode(value.infoType)
    }
}
