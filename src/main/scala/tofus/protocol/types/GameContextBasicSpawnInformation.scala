package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextBasicSpawnInformation(
  teamId: Byte,
  alive: Boolean,
  informations: GameContextActorPositionInformations
) extends ProtocolType {
  override val protocolId = 568
}

object GameContextBasicSpawnInformation {
  implicit val codec: Codec[GameContextBasicSpawnInformation] =
    new Codec[GameContextBasicSpawnInformation] {
      def decode: Get[GameContextBasicSpawnInformation] =
        for {
          teamId <- byte.decode
          alive <- bool.decode
          informations <- Codec[GameContextActorPositionInformations].decode
        } yield GameContextBasicSpawnInformation(teamId, alive, informations)

      def encode(value: GameContextBasicSpawnInformation): ByteVector =
        byte.encode(value.teamId) ++
        bool.encode(value.alive) ++
        Codec[GameContextActorPositionInformations].encode(value.informations)
    }
}
