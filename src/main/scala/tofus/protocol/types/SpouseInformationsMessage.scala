package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpouseInformationsMessage(
  spouse: FriendSpouseInformations
) extends Message(6356)

object SpouseInformationsMessage {
  implicit val codec: Codec[SpouseInformationsMessage] =
    new Codec[SpouseInformationsMessage] {
      def decode: Get[SpouseInformationsMessage] =
        for {
          spouse <- Codec[FriendSpouseInformations].decode
        } yield SpouseInformationsMessage(spouse)

      def encode(value: SpouseInformationsMessage): ByteVector =
        Codec[FriendSpouseInformations].encode(value.spouse)
    }
}
