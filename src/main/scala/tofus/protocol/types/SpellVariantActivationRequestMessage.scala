package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpellVariantActivationRequestMessage(
  spellId: Short
) extends Message(6707)

object SpellVariantActivationRequestMessage {
  implicit val codec: Codec[SpellVariantActivationRequestMessage] =
    new Codec[SpellVariantActivationRequestMessage] {
      def decode: Get[SpellVariantActivationRequestMessage] =
        for {
          spellId <- varShort.decode
        } yield SpellVariantActivationRequestMessage(spellId)

      def encode(value: SpellVariantActivationRequestMessage): ByteVector =
        varShort.encode(value.spellId)
    }
}
