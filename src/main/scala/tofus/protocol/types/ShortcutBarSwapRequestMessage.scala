package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarSwapRequestMessage(
  barType: Byte,
  firstSlot: Byte,
  secondSlot: Byte
) extends Message(6230)

object ShortcutBarSwapRequestMessage {
  implicit val codec: Codec[ShortcutBarSwapRequestMessage] =
    new Codec[ShortcutBarSwapRequestMessage] {
      def decode: Get[ShortcutBarSwapRequestMessage] =
        for {
          barType <- byte.decode
          firstSlot <- byte.decode
          secondSlot <- byte.decode
        } yield ShortcutBarSwapRequestMessage(barType, firstSlot, secondSlot)

      def encode(value: ShortcutBarSwapRequestMessage): ByteVector =
        byte.encode(value.barType) ++
        byte.encode(value.firstSlot) ++
        byte.encode(value.secondSlot)
    }
}
