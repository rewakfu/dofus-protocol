package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapInformationsRequestMessage(
  mapId: Double
) extends Message(225)

object MapInformationsRequestMessage {
  implicit val codec: Codec[MapInformationsRequestMessage] =
    new Codec[MapInformationsRequestMessage] {
      def decode: Get[MapInformationsRequestMessage] =
        for {
          mapId <- double.decode
        } yield MapInformationsRequestMessage(mapId)

      def encode(value: MapInformationsRequestMessage): ByteVector =
        double.encode(value.mapId)
    }
}
