package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementAchievedRewardable(
  id: Short,
  achievedBy: Long,
  finishedlevel: Short
) extends AchievementAchieved {
  override val protocolId = 515
}

object AchievementAchievedRewardable {
  implicit val codec: Codec[AchievementAchievedRewardable] =
    new Codec[AchievementAchievedRewardable] {
      def decode: Get[AchievementAchievedRewardable] =
        for {
          id <- varShort.decode
          achievedBy <- varLong.decode
          finishedlevel <- varShort.decode
        } yield AchievementAchievedRewardable(id, achievedBy, finishedlevel)

      def encode(value: AchievementAchievedRewardable): ByteVector =
        varShort.encode(value.id) ++
        varLong.encode(value.achievedBy) ++
        varShort.encode(value.finishedlevel)
    }
}
