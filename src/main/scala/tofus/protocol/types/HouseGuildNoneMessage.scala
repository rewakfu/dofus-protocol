package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseGuildNoneMessage(
  houseId: Int,
  instanceId: Int,
  secondHand: Boolean
) extends Message(5701)

object HouseGuildNoneMessage {
  implicit val codec: Codec[HouseGuildNoneMessage] =
    new Codec[HouseGuildNoneMessage] {
      def decode: Get[HouseGuildNoneMessage] =
        for {
          houseId <- varInt.decode
          instanceId <- int.decode
          secondHand <- bool.decode
        } yield HouseGuildNoneMessage(houseId, instanceId, secondHand)

      def encode(value: HouseGuildNoneMessage): ByteVector =
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId) ++
        bool.encode(value.secondHand)
    }
}
