package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightTriggerGlyphTrapMessage(
  actionId: Short,
  sourceId: Double,
  markId: Short,
  markImpactCell: Short,
  triggeringCharacterId: Double,
  triggeredSpellId: Short
) extends Message(5741)

object GameActionFightTriggerGlyphTrapMessage {
  implicit val codec: Codec[GameActionFightTriggerGlyphTrapMessage] =
    new Codec[GameActionFightTriggerGlyphTrapMessage] {
      def decode: Get[GameActionFightTriggerGlyphTrapMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          markId <- short.decode
          markImpactCell <- varShort.decode
          triggeringCharacterId <- double.decode
          triggeredSpellId <- varShort.decode
        } yield GameActionFightTriggerGlyphTrapMessage(actionId, sourceId, markId, markImpactCell, triggeringCharacterId, triggeredSpellId)

      def encode(value: GameActionFightTriggerGlyphTrapMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        short.encode(value.markId) ++
        varShort.encode(value.markImpactCell) ++
        double.encode(value.triggeringCharacterId) ++
        varShort.encode(value.triggeredSpellId)
    }
}
