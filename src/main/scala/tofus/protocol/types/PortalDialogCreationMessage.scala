package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PortalDialogCreationMessage(
  mapId: Double,
  npcId: Int,
  `type`: Int
) extends Message(6737)

object PortalDialogCreationMessage {
  implicit val codec: Codec[PortalDialogCreationMessage] =
    new Codec[PortalDialogCreationMessage] {
      def decode: Get[PortalDialogCreationMessage] =
        for {
          mapId <- double.decode
          npcId <- int.decode
          `type` <- int.decode
        } yield PortalDialogCreationMessage(mapId, npcId, `type`)

      def encode(value: PortalDialogCreationMessage): ByteVector =
        double.encode(value.mapId) ++
        int.encode(value.npcId) ++
        int.encode(value.`type`)
    }
}
