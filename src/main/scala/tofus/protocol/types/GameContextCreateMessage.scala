package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextCreateMessage(
  context: Byte
) extends Message(200)

object GameContextCreateMessage {
  implicit val codec: Codec[GameContextCreateMessage] =
    new Codec[GameContextCreateMessage] {
      def decode: Get[GameContextCreateMessage] =
        for {
          context <- byte.decode
        } yield GameContextCreateMessage(context)

      def encode(value: GameContextCreateMessage): ByteVector =
        byte.encode(value.context)
    }
}
