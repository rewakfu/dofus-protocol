package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarSwapErrorMessage(
  error: Byte
) extends Message(6226)

object ShortcutBarSwapErrorMessage {
  implicit val codec: Codec[ShortcutBarSwapErrorMessage] =
    new Codec[ShortcutBarSwapErrorMessage] {
      def decode: Get[ShortcutBarSwapErrorMessage] =
        for {
          error <- byte.decode
        } yield ShortcutBarSwapErrorMessage(error)

      def encode(value: ShortcutBarSwapErrorMessage): ByteVector =
        byte.encode(value.error)
    }
}
