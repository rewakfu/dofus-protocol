package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait InteractiveElementSkill extends ProtocolType

final case class ConcreteInteractiveElementSkill(
  skillId: Int,
  skillInstanceUid: Int
) extends InteractiveElementSkill {
  override val protocolId = 219
}

object ConcreteInteractiveElementSkill {
  implicit val codec: Codec[ConcreteInteractiveElementSkill] =  
    new Codec[ConcreteInteractiveElementSkill] {
      def decode: Get[ConcreteInteractiveElementSkill] =
        for {
          skillId <- varInt.decode
          skillInstanceUid <- int.decode
        } yield ConcreteInteractiveElementSkill(skillId, skillInstanceUid)

      def encode(value: ConcreteInteractiveElementSkill): ByteVector =
        varInt.encode(value.skillId) ++
        int.encode(value.skillInstanceUid)
    }
}

object InteractiveElementSkill {
  implicit val codec: Codec[InteractiveElementSkill] =
    new Codec[InteractiveElementSkill] {
      def decode: Get[InteractiveElementSkill] =
        ushort.decode.flatMap {
          case 219 => Codec[ConcreteInteractiveElementSkill].decode
          case 220 => Codec[InteractiveElementNamedSkill].decode
        }

      def encode(value: InteractiveElementSkill): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteInteractiveElementSkill => Codec[ConcreteInteractiveElementSkill].encode(i)
          case i: InteractiveElementNamedSkill => Codec[InteractiveElementNamedSkill].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
