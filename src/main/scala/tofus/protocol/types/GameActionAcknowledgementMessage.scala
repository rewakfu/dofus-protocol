package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionAcknowledgementMessage(
  valid: Boolean,
  actionId: Byte
) extends Message(957)

object GameActionAcknowledgementMessage {
  implicit val codec: Codec[GameActionAcknowledgementMessage] =
    new Codec[GameActionAcknowledgementMessage] {
      def decode: Get[GameActionAcknowledgementMessage] =
        for {
          valid <- bool.decode
          actionId <- byte.decode
        } yield GameActionAcknowledgementMessage(valid, actionId)

      def encode(value: GameActionAcknowledgementMessage): ByteVector =
        bool.encode(value.valid) ++
        byte.encode(value.actionId)
    }
}
