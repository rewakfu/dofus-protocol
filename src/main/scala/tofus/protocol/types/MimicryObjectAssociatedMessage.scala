package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MimicryObjectAssociatedMessage(
  hostUID: Int
) extends Message(6462)

object MimicryObjectAssociatedMessage {
  implicit val codec: Codec[MimicryObjectAssociatedMessage] =
    new Codec[MimicryObjectAssociatedMessage] {
      def decode: Get[MimicryObjectAssociatedMessage] =
        for {
          hostUID <- varInt.decode
        } yield MimicryObjectAssociatedMessage(hostUID)

      def encode(value: MimicryObjectAssociatedMessage): ByteVector =
        varInt.encode(value.hostUID)
    }
}
