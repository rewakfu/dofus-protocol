package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AcquaintanceAddedMessage(
  acquaintanceAdded: AcquaintanceInformation
) extends Message(6818)

object AcquaintanceAddedMessage {
  implicit val codec: Codec[AcquaintanceAddedMessage] =
    new Codec[AcquaintanceAddedMessage] {
      def decode: Get[AcquaintanceAddedMessage] =
        for {
          acquaintanceAdded <- Codec[AcquaintanceInformation].decode
        } yield AcquaintanceAddedMessage(acquaintanceAdded)

      def encode(value: AcquaintanceAddedMessage): ByteVector =
        Codec[AcquaintanceInformation].encode(value.acquaintanceAdded)
    }
}
