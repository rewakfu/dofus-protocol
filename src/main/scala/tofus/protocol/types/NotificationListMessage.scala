package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NotificationListMessage(
  flags: List[Int]
) extends Message(6087)

object NotificationListMessage {
  implicit val codec: Codec[NotificationListMessage] =
    new Codec[NotificationListMessage] {
      def decode: Get[NotificationListMessage] =
        for {
          flags <- list(ushort, varInt).decode
        } yield NotificationListMessage(flags)

      def encode(value: NotificationListMessage): ByteVector =
        list(ushort, varInt).encode(value.flags)
    }
}
