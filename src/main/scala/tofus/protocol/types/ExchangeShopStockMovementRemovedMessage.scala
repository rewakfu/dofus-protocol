package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeShopStockMovementRemovedMessage(
  objectId: Int
) extends Message(5907)

object ExchangeShopStockMovementRemovedMessage {
  implicit val codec: Codec[ExchangeShopStockMovementRemovedMessage] =
    new Codec[ExchangeShopStockMovementRemovedMessage] {
      def decode: Get[ExchangeShopStockMovementRemovedMessage] =
        for {
          objectId <- varInt.decode
        } yield ExchangeShopStockMovementRemovedMessage(objectId)

      def encode(value: ExchangeShopStockMovementRemovedMessage): ByteVector =
        varInt.encode(value.objectId)
    }
}
