package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChangeThemeRequestMessage(
  theme: Byte
) extends Message(6639)

object ChangeThemeRequestMessage {
  implicit val codec: Codec[ChangeThemeRequestMessage] =
    new Codec[ChangeThemeRequestMessage] {
      def decode: Get[ChangeThemeRequestMessage] =
        for {
          theme <- byte.decode
        } yield ChangeThemeRequestMessage(theme)

      def encode(value: ChangeThemeRequestMessage): ByteVector =
        byte.encode(value.theme)
    }
}
