package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class UpdateMountCharacteristicsMessage(
  rideId: Int,
  boostToUpdateList: List[UpdateMountCharacteristic]
) extends Message(6753)

object UpdateMountCharacteristicsMessage {
  implicit val codec: Codec[UpdateMountCharacteristicsMessage] =
    new Codec[UpdateMountCharacteristicsMessage] {
      def decode: Get[UpdateMountCharacteristicsMessage] =
        for {
          rideId <- varInt.decode
          boostToUpdateList <- list(ushort, Codec[UpdateMountCharacteristic]).decode
        } yield UpdateMountCharacteristicsMessage(rideId, boostToUpdateList)

      def encode(value: UpdateMountCharacteristicsMessage): ByteVector =
        varInt.encode(value.rideId) ++
        list(ushort, Codec[UpdateMountCharacteristic]).encode(value.boostToUpdateList)
    }
}
