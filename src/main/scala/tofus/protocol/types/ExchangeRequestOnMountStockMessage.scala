package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeRequestOnMountStockMessage(

) extends Message(5986)

object ExchangeRequestOnMountStockMessage {
  implicit val codec: Codec[ExchangeRequestOnMountStockMessage] =
    Codec.const(ExchangeRequestOnMountStockMessage())
}
