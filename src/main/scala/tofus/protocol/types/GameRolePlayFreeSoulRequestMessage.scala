package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayFreeSoulRequestMessage(

) extends Message(745)

object GameRolePlayFreeSoulRequestMessage {
  implicit val codec: Codec[GameRolePlayFreeSoulRequestMessage] =
    Codec.const(GameRolePlayFreeSoulRequestMessage())
}
