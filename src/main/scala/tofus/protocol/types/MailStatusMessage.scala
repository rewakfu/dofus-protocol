package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MailStatusMessage(
  unread: Short,
  total: Short
) extends Message(6275)

object MailStatusMessage {
  implicit val codec: Codec[MailStatusMessage] =
    new Codec[MailStatusMessage] {
      def decode: Get[MailStatusMessage] =
        for {
          unread <- varShort.decode
          total <- varShort.decode
        } yield MailStatusMessage(unread, total)

      def encode(value: MailStatusMessage): ByteVector =
        varShort.encode(value.unread) ++
        varShort.encode(value.total)
    }
}
