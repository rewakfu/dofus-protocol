package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait MapCoordinates extends ProtocolType

final case class ConcreteMapCoordinates(
  worldX: Short,
  worldY: Short
) extends MapCoordinates {
  override val protocolId = 174
}

object ConcreteMapCoordinates {
  implicit val codec: Codec[ConcreteMapCoordinates] =  
    new Codec[ConcreteMapCoordinates] {
      def decode: Get[ConcreteMapCoordinates] =
        for {
          worldX <- short.decode
          worldY <- short.decode
        } yield ConcreteMapCoordinates(worldX, worldY)

      def encode(value: ConcreteMapCoordinates): ByteVector =
        short.encode(value.worldX) ++
        short.encode(value.worldY)
    }
}

object MapCoordinates {
  implicit val codec: Codec[MapCoordinates] =
    new Codec[MapCoordinates] {
      def decode: Get[MapCoordinates] =
        ushort.decode.flatMap {
          case 174 => Codec[ConcreteMapCoordinates].decode
          case 176 => Codec[MapCoordinatesExtended].decode
          case 392 => Codec[ConcreteMapCoordinatesAndId].decode
        }

      def encode(value: MapCoordinates): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteMapCoordinates => Codec[ConcreteMapCoordinates].encode(i)
          case i: MapCoordinatesExtended => Codec[MapCoordinatesExtended].encode(i)
          case i: ConcreteMapCoordinatesAndId => Codec[ConcreteMapCoordinatesAndId].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
