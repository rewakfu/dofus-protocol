package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class UpdateMapPlayersAgressableStatusMessage(
  playerIds: List[Long],
  enable: ByteVector
) extends Message(6454)

object UpdateMapPlayersAgressableStatusMessage {
  implicit val codec: Codec[UpdateMapPlayersAgressableStatusMessage] =
    new Codec[UpdateMapPlayersAgressableStatusMessage] {
      def decode: Get[UpdateMapPlayersAgressableStatusMessage] =
        for {
          playerIds <- list(ushort, varLong).decode
          enable <- bytes(ushort).decode
        } yield UpdateMapPlayersAgressableStatusMessage(playerIds, enable)

      def encode(value: UpdateMapPlayersAgressableStatusMessage): ByteVector =
        list(ushort, varLong).encode(value.playerIds) ++
        bytes(ushort).encode(value.enable)
    }
}
