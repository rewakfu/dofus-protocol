package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class RemodelingInformation(
  name: String,
  breed: Byte,
  sex: Boolean,
  cosmeticId: Short,
  colors: List[Int]
) extends ProtocolType {
  override val protocolId = 480
}

object RemodelingInformation {
  implicit val codec: Codec[RemodelingInformation] =
    new Codec[RemodelingInformation] {
      def decode: Get[RemodelingInformation] =
        for {
          name <- utf8(ushort).decode
          breed <- byte.decode
          sex <- bool.decode
          cosmeticId <- varShort.decode
          colors <- list(ushort, int).decode
        } yield RemodelingInformation(name, breed, sex, cosmeticId, colors)

      def encode(value: RemodelingInformation): ByteVector =
        utf8(ushort).encode(value.name) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        varShort.encode(value.cosmeticId) ++
        list(ushort, int).encode(value.colors)
    }
}
