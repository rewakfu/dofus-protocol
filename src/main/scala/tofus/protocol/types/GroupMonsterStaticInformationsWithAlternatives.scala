package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GroupMonsterStaticInformationsWithAlternatives(
  mainCreatureLightInfos: ConcreteMonsterInGroupLightInformations,
  underlings: List[MonsterInGroupInformations],
  alternatives: List[AlternativeMonstersInGroupLightInformations]
) extends GroupMonsterStaticInformations {
  override val protocolId = 396
}

object GroupMonsterStaticInformationsWithAlternatives {
  implicit val codec: Codec[GroupMonsterStaticInformationsWithAlternatives] =
    new Codec[GroupMonsterStaticInformationsWithAlternatives] {
      def decode: Get[GroupMonsterStaticInformationsWithAlternatives] =
        for {
          mainCreatureLightInfos <- Codec[ConcreteMonsterInGroupLightInformations].decode
          underlings <- list(ushort, Codec[MonsterInGroupInformations]).decode
          alternatives <- list(ushort, Codec[AlternativeMonstersInGroupLightInformations]).decode
        } yield GroupMonsterStaticInformationsWithAlternatives(mainCreatureLightInfos, underlings, alternatives)

      def encode(value: GroupMonsterStaticInformationsWithAlternatives): ByteVector =
        Codec[ConcreteMonsterInGroupLightInformations].encode(value.mainCreatureLightInfos) ++
        list(ushort, Codec[MonsterInGroupInformations]).encode(value.underlings) ++
        list(ushort, Codec[AlternativeMonstersInGroupLightInformations]).encode(value.alternatives)
    }
}
