package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ItemsPreset(
  id: Short,
  items: List[ItemForPreset],
  mountEquipped: Boolean,
  look: EntityLook
) extends Preset {
  override val protocolId = 517
}

object ItemsPreset {
  implicit val codec: Codec[ItemsPreset] =
    new Codec[ItemsPreset] {
      def decode: Get[ItemsPreset] =
        for {
          id <- short.decode
          items <- list(ushort, Codec[ItemForPreset]).decode
          mountEquipped <- bool.decode
          look <- Codec[EntityLook].decode
        } yield ItemsPreset(id, items, mountEquipped, look)

      def encode(value: ItemsPreset): ByteVector =
        short.encode(value.id) ++
        list(ushort, Codec[ItemForPreset]).encode(value.items) ++
        bool.encode(value.mountEquipped) ++
        Codec[EntityLook].encode(value.look)
    }
}
