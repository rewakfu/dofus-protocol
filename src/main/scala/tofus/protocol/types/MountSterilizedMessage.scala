package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountSterilizedMessage(
  mountId: Int
) extends Message(5977)

object MountSterilizedMessage {
  implicit val codec: Codec[MountSterilizedMessage] =
    new Codec[MountSterilizedMessage] {
      def decode: Get[MountSterilizedMessage] =
        for {
          mountId <- varInt.decode
        } yield MountSterilizedMessage(mountId)

      def encode(value: MountSterilizedMessage): ByteVector =
        varInt.encode(value.mountId)
    }
}
