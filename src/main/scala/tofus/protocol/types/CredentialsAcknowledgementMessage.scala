package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CredentialsAcknowledgementMessage(

) extends Message(6314)

object CredentialsAcknowledgementMessage {
  implicit val codec: Codec[CredentialsAcknowledgementMessage] =
    Codec.const(CredentialsAcknowledgementMessage())
}
