package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LockableStateUpdateStorageMessage(
  locked: Boolean,
  mapId: Double,
  elementId: Int
) extends Message(5669)

object LockableStateUpdateStorageMessage {
  implicit val codec: Codec[LockableStateUpdateStorageMessage] =
    new Codec[LockableStateUpdateStorageMessage] {
      def decode: Get[LockableStateUpdateStorageMessage] =
        for {
          locked <- bool.decode
          mapId <- double.decode
          elementId <- varInt.decode
        } yield LockableStateUpdateStorageMessage(locked, mapId, elementId)

      def encode(value: LockableStateUpdateStorageMessage): ByteVector =
        bool.encode(value.locked) ++
        double.encode(value.mapId) ++
        varInt.encode(value.elementId)
    }
}
