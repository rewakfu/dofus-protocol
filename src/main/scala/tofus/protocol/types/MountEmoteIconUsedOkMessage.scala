package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountEmoteIconUsedOkMessage(
  mountId: Int,
  reactionType: Byte
) extends Message(5978)

object MountEmoteIconUsedOkMessage {
  implicit val codec: Codec[MountEmoteIconUsedOkMessage] =
    new Codec[MountEmoteIconUsedOkMessage] {
      def decode: Get[MountEmoteIconUsedOkMessage] =
        for {
          mountId <- varInt.decode
          reactionType <- byte.decode
        } yield MountEmoteIconUsedOkMessage(mountId, reactionType)

      def encode(value: MountEmoteIconUsedOkMessage): ByteVector =
        varInt.encode(value.mountId) ++
        byte.encode(value.reactionType)
    }
}
