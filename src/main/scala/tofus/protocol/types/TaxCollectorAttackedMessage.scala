package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorAttackedMessage(
  firstNameId: Short,
  lastNameId: Short,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short,
  guild: ConcreteBasicGuildInformations
) extends Message(5918)

object TaxCollectorAttackedMessage {
  implicit val codec: Codec[TaxCollectorAttackedMessage] =
    new Codec[TaxCollectorAttackedMessage] {
      def decode: Get[TaxCollectorAttackedMessage] =
        for {
          firstNameId <- varShort.decode
          lastNameId <- varShort.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          guild <- Codec[ConcreteBasicGuildInformations].decode
        } yield TaxCollectorAttackedMessage(firstNameId, lastNameId, worldX, worldY, mapId, subAreaId, guild)

      def encode(value: TaxCollectorAttackedMessage): ByteVector =
        varShort.encode(value.firstNameId) ++
        varShort.encode(value.lastNameId) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        Codec[ConcreteBasicGuildInformations].encode(value.guild)
    }
}
