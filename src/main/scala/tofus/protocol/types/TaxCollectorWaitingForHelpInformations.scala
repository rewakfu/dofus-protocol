package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorWaitingForHelpInformations(
  waitingForHelpInfo: ProtectedEntityWaitingForHelpInfo
) extends TaxCollectorComplementaryInformations {
  override val protocolId = 447
}

object TaxCollectorWaitingForHelpInformations {
  implicit val codec: Codec[TaxCollectorWaitingForHelpInformations] =
    new Codec[TaxCollectorWaitingForHelpInformations] {
      def decode: Get[TaxCollectorWaitingForHelpInformations] =
        for {
          waitingForHelpInfo <- Codec[ProtectedEntityWaitingForHelpInfo].decode
        } yield TaxCollectorWaitingForHelpInformations(waitingForHelpInfo)

      def encode(value: TaxCollectorWaitingForHelpInformations): ByteVector =
        Codec[ProtectedEntityWaitingForHelpInfo].encode(value.waitingForHelpInfo)
    }
}
