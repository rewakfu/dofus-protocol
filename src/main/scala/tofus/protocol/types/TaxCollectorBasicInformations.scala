package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorBasicInformations(
  firstNameId: Short,
  lastNameId: Short,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short
) extends ProtocolType {
  override val protocolId = 96
}

object TaxCollectorBasicInformations {
  implicit val codec: Codec[TaxCollectorBasicInformations] =
    new Codec[TaxCollectorBasicInformations] {
      def decode: Get[TaxCollectorBasicInformations] =
        for {
          firstNameId <- varShort.decode
          lastNameId <- varShort.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
        } yield TaxCollectorBasicInformations(firstNameId, lastNameId, worldX, worldY, mapId, subAreaId)

      def encode(value: TaxCollectorBasicInformations): ByteVector =
        varShort.encode(value.firstNameId) ++
        varShort.encode(value.lastNameId) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId)
    }
}
