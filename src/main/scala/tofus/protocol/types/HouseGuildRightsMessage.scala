package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseGuildRightsMessage(
  houseId: Int,
  instanceId: Int,
  secondHand: Boolean,
  guildInfo: ConcreteGuildInformations,
  rights: Int
) extends Message(5703)

object HouseGuildRightsMessage {
  implicit val codec: Codec[HouseGuildRightsMessage] =
    new Codec[HouseGuildRightsMessage] {
      def decode: Get[HouseGuildRightsMessage] =
        for {
          houseId <- varInt.decode
          instanceId <- int.decode
          secondHand <- bool.decode
          guildInfo <- Codec[ConcreteGuildInformations].decode
          rights <- varInt.decode
        } yield HouseGuildRightsMessage(houseId, instanceId, secondHand, guildInfo, rights)

      def encode(value: HouseGuildRightsMessage): ByteVector =
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId) ++
        bool.encode(value.secondHand) ++
        Codec[ConcreteGuildInformations].encode(value.guildInfo) ++
        varInt.encode(value.rights)
    }
}
