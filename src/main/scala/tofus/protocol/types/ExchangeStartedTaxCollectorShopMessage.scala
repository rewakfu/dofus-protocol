package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartedTaxCollectorShopMessage(
  objects: List[ObjectItem],
  kamas: Long
) extends Message(6664)

object ExchangeStartedTaxCollectorShopMessage {
  implicit val codec: Codec[ExchangeStartedTaxCollectorShopMessage] =
    new Codec[ExchangeStartedTaxCollectorShopMessage] {
      def decode: Get[ExchangeStartedTaxCollectorShopMessage] =
        for {
          objects <- list(ushort, Codec[ObjectItem]).decode
          kamas <- varLong.decode
        } yield ExchangeStartedTaxCollectorShopMessage(objects, kamas)

      def encode(value: ExchangeStartedTaxCollectorShopMessage): ByteVector =
        list(ushort, Codec[ObjectItem]).encode(value.objects) ++
        varLong.encode(value.kamas)
    }
}
