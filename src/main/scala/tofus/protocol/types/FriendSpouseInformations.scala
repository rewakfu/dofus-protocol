package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FriendSpouseInformations extends ProtocolType

final case class ConcreteFriendSpouseInformations(
  spouseAccountId: Int,
  spouseId: Long,
  spouseName: String,
  spouseLevel: Short,
  breed: Byte,
  sex: Byte,
  spouseEntityLook: EntityLook,
  guildInfo: ConcreteGuildInformations,
  alignmentSide: Byte
) extends FriendSpouseInformations {
  override val protocolId = 77
}

object ConcreteFriendSpouseInformations {
  implicit val codec: Codec[ConcreteFriendSpouseInformations] =  
    new Codec[ConcreteFriendSpouseInformations] {
      def decode: Get[ConcreteFriendSpouseInformations] =
        for {
          spouseAccountId <- int.decode
          spouseId <- varLong.decode
          spouseName <- utf8(ushort).decode
          spouseLevel <- varShort.decode
          breed <- byte.decode
          sex <- byte.decode
          spouseEntityLook <- Codec[EntityLook].decode
          guildInfo <- Codec[ConcreteGuildInformations].decode
          alignmentSide <- byte.decode
        } yield ConcreteFriendSpouseInformations(spouseAccountId, spouseId, spouseName, spouseLevel, breed, sex, spouseEntityLook, guildInfo, alignmentSide)

      def encode(value: ConcreteFriendSpouseInformations): ByteVector =
        int.encode(value.spouseAccountId) ++
        varLong.encode(value.spouseId) ++
        utf8(ushort).encode(value.spouseName) ++
        varShort.encode(value.spouseLevel) ++
        byte.encode(value.breed) ++
        byte.encode(value.sex) ++
        Codec[EntityLook].encode(value.spouseEntityLook) ++
        Codec[ConcreteGuildInformations].encode(value.guildInfo) ++
        byte.encode(value.alignmentSide)
    }
}

object FriendSpouseInformations {
  implicit val codec: Codec[FriendSpouseInformations] =
    new Codec[FriendSpouseInformations] {
      def decode: Get[FriendSpouseInformations] =
        ushort.decode.flatMap {
          case 77 => Codec[ConcreteFriendSpouseInformations].decode
          case 93 => Codec[FriendSpouseOnlineInformations].decode
        }

      def encode(value: FriendSpouseInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFriendSpouseInformations => Codec[ConcreteFriendSpouseInformations].encode(i)
          case i: FriendSpouseOnlineInformations => Codec[FriendSpouseOnlineInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
