package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntShowLegendaryUIMessage(
  availableLegendaryIds: List[Short]
) extends Message(6498)

object TreasureHuntShowLegendaryUIMessage {
  implicit val codec: Codec[TreasureHuntShowLegendaryUIMessage] =
    new Codec[TreasureHuntShowLegendaryUIMessage] {
      def decode: Get[TreasureHuntShowLegendaryUIMessage] =
        for {
          availableLegendaryIds <- list(ushort, varShort).decode
        } yield TreasureHuntShowLegendaryUIMessage(availableLegendaryIds)

      def encode(value: TreasureHuntShowLegendaryUIMessage): ByteVector =
        list(ushort, varShort).encode(value.availableLegendaryIds)
    }
}
