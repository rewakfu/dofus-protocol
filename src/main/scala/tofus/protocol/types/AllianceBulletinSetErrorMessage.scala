package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceBulletinSetErrorMessage(
  reason: Byte
) extends Message(6692)

object AllianceBulletinSetErrorMessage {
  implicit val codec: Codec[AllianceBulletinSetErrorMessage] =
    new Codec[AllianceBulletinSetErrorMessage] {
      def decode: Get[AllianceBulletinSetErrorMessage] =
        for {
          reason <- byte.decode
        } yield AllianceBulletinSetErrorMessage(reason)

      def encode(value: AllianceBulletinSetErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
