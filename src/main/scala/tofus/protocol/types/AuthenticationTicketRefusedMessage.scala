package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AuthenticationTicketRefusedMessage(

) extends Message(112)

object AuthenticationTicketRefusedMessage {
  implicit val codec: Codec[AuthenticationTicketRefusedMessage] =
    Codec.const(AuthenticationTicketRefusedMessage())
}
