package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameRolePlayNpcInformations extends GameRolePlayActorInformations

final case class ConcreteGameRolePlayNpcInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  npcId: Short,
  sex: Boolean,
  specialArtworkId: Short
) extends GameRolePlayNpcInformations {
  override val protocolId = 156
}

object ConcreteGameRolePlayNpcInformations {
  implicit val codec: Codec[ConcreteGameRolePlayNpcInformations] =  
    new Codec[ConcreteGameRolePlayNpcInformations] {
      def decode: Get[ConcreteGameRolePlayNpcInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          npcId <- varShort.decode
          sex <- bool.decode
          specialArtworkId <- varShort.decode
        } yield ConcreteGameRolePlayNpcInformations(contextualId, disposition, look, npcId, sex, specialArtworkId)

      def encode(value: ConcreteGameRolePlayNpcInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        varShort.encode(value.npcId) ++
        bool.encode(value.sex) ++
        varShort.encode(value.specialArtworkId)
    }
}

object GameRolePlayNpcInformations {
  implicit val codec: Codec[GameRolePlayNpcInformations] =
    new Codec[GameRolePlayNpcInformations] {
      def decode: Get[GameRolePlayNpcInformations] =
        ushort.decode.flatMap {
          case 156 => Codec[ConcreteGameRolePlayNpcInformations].decode
          case 383 => Codec[GameRolePlayNpcWithQuestInformations].decode
        }

      def encode(value: GameRolePlayNpcInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameRolePlayNpcInformations => Codec[ConcreteGameRolePlayNpcInformations].encode(i)
          case i: GameRolePlayNpcWithQuestInformations => Codec[GameRolePlayNpcWithQuestInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
