package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderRegisterRequestMessage(
  dungeonIds: List[Short]
) extends Message(6249)

object DungeonPartyFinderRegisterRequestMessage {
  implicit val codec: Codec[DungeonPartyFinderRegisterRequestMessage] =
    new Codec[DungeonPartyFinderRegisterRequestMessage] {
      def decode: Get[DungeonPartyFinderRegisterRequestMessage] =
        for {
          dungeonIds <- list(ushort, varShort).decode
        } yield DungeonPartyFinderRegisterRequestMessage(dungeonIds)

      def encode(value: DungeonPartyFinderRegisterRequestMessage): ByteVector =
        list(ushort, varShort).encode(value.dungeonIds)
    }
}
