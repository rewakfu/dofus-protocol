package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InventoryWeightMessage(
  inventoryWeight: Int,
  shopWeight: Int,
  weightMax: Int
) extends Message(3009)

object InventoryWeightMessage {
  implicit val codec: Codec[InventoryWeightMessage] =
    new Codec[InventoryWeightMessage] {
      def decode: Get[InventoryWeightMessage] =
        for {
          inventoryWeight <- varInt.decode
          shopWeight <- varInt.decode
          weightMax <- varInt.decode
        } yield InventoryWeightMessage(inventoryWeight, shopWeight, weightMax)

      def encode(value: InventoryWeightMessage): ByteVector =
        varInt.encode(value.inventoryWeight) ++
        varInt.encode(value.shopWeight) ++
        varInt.encode(value.weightMax)
    }
}
