package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTeamMemberEntityInformation(
  id: Double,
  entityModelId: Byte,
  level: Short,
  masterId: Double
) extends FightTeamMemberInformations {
  override val protocolId = 549
}

object FightTeamMemberEntityInformation {
  implicit val codec: Codec[FightTeamMemberEntityInformation] =
    new Codec[FightTeamMemberEntityInformation] {
      def decode: Get[FightTeamMemberEntityInformation] =
        for {
          id <- double.decode
          entityModelId <- byte.decode
          level <- varShort.decode
          masterId <- double.decode
        } yield FightTeamMemberEntityInformation(id, entityModelId, level, masterId)

      def encode(value: FightTeamMemberEntityInformation): ByteVector =
        double.encode(value.id) ++
        byte.encode(value.entityModelId) ++
        varShort.encode(value.level) ++
        double.encode(value.masterId)
    }
}
