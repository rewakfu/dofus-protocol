package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectLadder(
  actionId: Short,
  monsterFamilyId: Short,
  monsterCount: Int
) extends ObjectEffectCreature {
  override val protocolId = 81
}

object ObjectEffectLadder {
  implicit val codec: Codec[ObjectEffectLadder] =
    new Codec[ObjectEffectLadder] {
      def decode: Get[ObjectEffectLadder] =
        for {
          actionId <- varShort.decode
          monsterFamilyId <- varShort.decode
          monsterCount <- varInt.decode
        } yield ObjectEffectLadder(actionId, monsterFamilyId, monsterCount)

      def encode(value: ObjectEffectLadder): ByteVector =
        varShort.encode(value.actionId) ++
        varShort.encode(value.monsterFamilyId) ++
        varInt.encode(value.monsterCount)
    }
}
