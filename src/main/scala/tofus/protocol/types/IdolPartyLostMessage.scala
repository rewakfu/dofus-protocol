package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolPartyLostMessage(
  idolId: Short
) extends Message(6580)

object IdolPartyLostMessage {
  implicit val codec: Codec[IdolPartyLostMessage] =
    new Codec[IdolPartyLostMessage] {
      def decode: Get[IdolPartyLostMessage] =
        for {
          idolId <- varShort.decode
        } yield IdolPartyLostMessage(idolId)

      def encode(value: IdolPartyLostMessage): ByteVector =
        varShort.encode(value.idolId)
    }
}
