package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseGuildedInformations(
  flags0: Byte,
  instanceId: Int,
  ownerName: String,
  price: Long,
  guildInfo: ConcreteGuildInformations
) extends HouseInstanceInformations {
  override val protocolId = 512
}

object HouseGuildedInformations {
  implicit val codec: Codec[HouseGuildedInformations] =
    new Codec[HouseGuildedInformations] {
      def decode: Get[HouseGuildedInformations] =
        for {
          flags0 <- byte.decode
          instanceId <- int.decode
          ownerName <- utf8(ushort).decode
          price <- varLong.decode
          guildInfo <- Codec[ConcreteGuildInformations].decode
        } yield HouseGuildedInformations(flags0, instanceId, ownerName, price, guildInfo)

      def encode(value: HouseGuildedInformations): ByteVector =
        byte.encode(value.flags0) ++
        int.encode(value.instanceId) ++
        utf8(ushort).encode(value.ownerName) ++
        varLong.encode(value.price) ++
        Codec[ConcreteGuildInformations].encode(value.guildInfo)
    }
}
