package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FollowedQuestsMessage(
  quests: List[QuestActiveDetailedInformations]
) extends Message(6717)

object FollowedQuestsMessage {
  implicit val codec: Codec[FollowedQuestsMessage] =
    new Codec[FollowedQuestsMessage] {
      def decode: Get[FollowedQuestsMessage] =
        for {
          quests <- list(ushort, Codec[QuestActiveDetailedInformations]).decode
        } yield FollowedQuestsMessage(quests)

      def encode(value: FollowedQuestsMessage): ByteVector =
        list(ushort, Codec[QuestActiveDetailedInformations]).encode(value.quests)
    }
}
