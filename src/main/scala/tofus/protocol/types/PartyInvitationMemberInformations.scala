package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PartyInvitationMemberInformations extends CharacterBaseInformations

final case class ConcretePartyInvitationMemberInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte,
  sex: Boolean,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short,
  entities: List[ConcretePartyEntityBaseInformation]
) extends PartyInvitationMemberInformations {
  override val protocolId = 376
}

object ConcretePartyInvitationMemberInformations {
  implicit val codec: Codec[ConcretePartyInvitationMemberInformations] =  
    new Codec[ConcretePartyInvitationMemberInformations] {
      def decode: Get[ConcretePartyInvitationMemberInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
          sex <- bool.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          entities <- list(ushort, Codec[ConcretePartyEntityBaseInformation]).decode
        } yield ConcretePartyInvitationMemberInformations(id, name, level, entityLook, breed, sex, worldX, worldY, mapId, subAreaId, entities)

      def encode(value: ConcretePartyInvitationMemberInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        list(ushort, Codec[ConcretePartyEntityBaseInformation]).encode(value.entities)
    }
}

object PartyInvitationMemberInformations {
  implicit val codec: Codec[PartyInvitationMemberInformations] =
    new Codec[PartyInvitationMemberInformations] {
      def decode: Get[PartyInvitationMemberInformations] =
        ushort.decode.flatMap {
          case 376 => Codec[ConcretePartyInvitationMemberInformations].decode

        }

      def encode(value: PartyInvitationMemberInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePartyInvitationMemberInformations => Codec[ConcretePartyInvitationMemberInformations].encode(i)

        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
