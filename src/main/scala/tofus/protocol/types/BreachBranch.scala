package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait BreachBranch extends ProtocolType

final case class ConcreteBreachBranch(
  room: Byte,
  element: Int,
  bosses: List[ConcreteMonsterInGroupLightInformations],
  map: Double,
  score: Short,
  relativeScore: Short,
  monsters: List[ConcreteMonsterInGroupLightInformations]
) extends BreachBranch {
  override val protocolId = 558
}

object ConcreteBreachBranch {
  implicit val codec: Codec[ConcreteBreachBranch] =  
    new Codec[ConcreteBreachBranch] {
      def decode: Get[ConcreteBreachBranch] =
        for {
          room <- byte.decode
          element <- int.decode
          bosses <- list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).decode
          map <- double.decode
          score <- short.decode
          relativeScore <- short.decode
          monsters <- list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).decode
        } yield ConcreteBreachBranch(room, element, bosses, map, score, relativeScore, monsters)

      def encode(value: ConcreteBreachBranch): ByteVector =
        byte.encode(value.room) ++
        int.encode(value.element) ++
        list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).encode(value.bosses) ++
        double.encode(value.map) ++
        short.encode(value.score) ++
        short.encode(value.relativeScore) ++
        list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).encode(value.monsters)
    }
}

object BreachBranch {
  implicit val codec: Codec[BreachBranch] =
    new Codec[BreachBranch] {
      def decode: Get[BreachBranch] =
        ushort.decode.flatMap {
          case 558 => Codec[ConcreteBreachBranch].decode
          case 578 => Codec[ExtendedLockedBreachBranch].decode
          case 560 => Codec[ConcreteExtendedBreachBranch].decode
        }

      def encode(value: BreachBranch): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteBreachBranch => Codec[ConcreteBreachBranch].encode(i)
          case i: ExtendedLockedBreachBranch => Codec[ExtendedLockedBreachBranch].encode(i)
          case i: ConcreteExtendedBreachBranch => Codec[ConcreteExtendedBreachBranch].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
