package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseToSellListMessage(
  pageIndex: Short,
  totalPage: Short,
  houseList: List[HouseInformationsForSell]
) extends Message(6140)

object HouseToSellListMessage {
  implicit val codec: Codec[HouseToSellListMessage] =
    new Codec[HouseToSellListMessage] {
      def decode: Get[HouseToSellListMessage] =
        for {
          pageIndex <- varShort.decode
          totalPage <- varShort.decode
          houseList <- list(ushort, Codec[HouseInformationsForSell]).decode
        } yield HouseToSellListMessage(pageIndex, totalPage, houseList)

      def encode(value: HouseToSellListMessage): ByteVector =
        varShort.encode(value.pageIndex) ++
        varShort.encode(value.totalPage) ++
        list(ushort, Codec[HouseInformationsForSell]).encode(value.houseList)
    }
}
