package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInvitationStateRecruterMessage(
  recrutedName: String,
  invitationState: Byte
) extends Message(5563)

object GuildInvitationStateRecruterMessage {
  implicit val codec: Codec[GuildInvitationStateRecruterMessage] =
    new Codec[GuildInvitationStateRecruterMessage] {
      def decode: Get[GuildInvitationStateRecruterMessage] =
        for {
          recrutedName <- utf8(ushort).decode
          invitationState <- byte.decode
        } yield GuildInvitationStateRecruterMessage(recrutedName, invitationState)

      def encode(value: GuildInvitationStateRecruterMessage): ByteVector =
        utf8(ushort).encode(value.recrutedName) ++
        byte.encode(value.invitationState)
    }
}
