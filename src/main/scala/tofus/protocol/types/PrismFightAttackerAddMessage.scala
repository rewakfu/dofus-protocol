package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightAttackerAddMessage(
  subAreaId: Short,
  fightId: Short,
  attacker: CharacterMinimalPlusLookInformations
) extends Message(5893)

object PrismFightAttackerAddMessage {
  implicit val codec: Codec[PrismFightAttackerAddMessage] =
    new Codec[PrismFightAttackerAddMessage] {
      def decode: Get[PrismFightAttackerAddMessage] =
        for {
          subAreaId <- varShort.decode
          fightId <- varShort.decode
          attacker <- Codec[CharacterMinimalPlusLookInformations].decode
        } yield PrismFightAttackerAddMessage(subAreaId, fightId, attacker)

      def encode(value: PrismFightAttackerAddMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        varShort.encode(value.fightId) ++
        Codec[CharacterMinimalPlusLookInformations].encode(value.attacker)
    }
}
