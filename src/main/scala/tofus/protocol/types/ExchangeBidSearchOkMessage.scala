package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidSearchOkMessage(

) extends Message(5802)

object ExchangeBidSearchOkMessage {
  implicit val codec: Codec[ExchangeBidSearchOkMessage] =
    Codec.const(ExchangeBidSearchOkMessage())
}
