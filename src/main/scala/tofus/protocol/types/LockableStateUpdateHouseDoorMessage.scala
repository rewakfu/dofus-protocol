package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LockableStateUpdateHouseDoorMessage(
  locked: Boolean,
  houseId: Int,
  instanceId: Int,
  secondHand: Boolean
) extends Message(5668)

object LockableStateUpdateHouseDoorMessage {
  implicit val codec: Codec[LockableStateUpdateHouseDoorMessage] =
    new Codec[LockableStateUpdateHouseDoorMessage] {
      def decode: Get[LockableStateUpdateHouseDoorMessage] =
        for {
          locked <- bool.decode
          houseId <- varInt.decode
          instanceId <- int.decode
          secondHand <- bool.decode
        } yield LockableStateUpdateHouseDoorMessage(locked, houseId, instanceId, secondHand)

      def encode(value: LockableStateUpdateHouseDoorMessage): ByteVector =
        bool.encode(value.locked) ++
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId) ++
        bool.encode(value.secondHand)
    }
}
