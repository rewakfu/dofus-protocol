package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildBulletinSetRequestMessage(
  content: String,
  notifyMembers: Boolean
) extends Message(6694)

object GuildBulletinSetRequestMessage {
  implicit val codec: Codec[GuildBulletinSetRequestMessage] =
    new Codec[GuildBulletinSetRequestMessage] {
      def decode: Get[GuildBulletinSetRequestMessage] =
        for {
          content <- utf8(ushort).decode
          notifyMembers <- bool.decode
        } yield GuildBulletinSetRequestMessage(content, notifyMembers)

      def encode(value: GuildBulletinSetRequestMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        bool.encode(value.notifyMembers)
    }
}
