package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredDeleteRequestMessage(
  accountId: Int,
  session: Boolean
) extends Message(5680)

object IgnoredDeleteRequestMessage {
  implicit val codec: Codec[IgnoredDeleteRequestMessage] =
    new Codec[IgnoredDeleteRequestMessage] {
      def decode: Get[IgnoredDeleteRequestMessage] =
        for {
          accountId <- int.decode
          session <- bool.decode
        } yield IgnoredDeleteRequestMessage(accountId, session)

      def encode(value: IgnoredDeleteRequestMessage): ByteVector =
        int.encode(value.accountId) ++
        bool.encode(value.session)
    }
}
