package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ContactLookErrorMessage(
  requestId: Int
) extends Message(6045)

object ContactLookErrorMessage {
  implicit val codec: Codec[ContactLookErrorMessage] =
    new Codec[ContactLookErrorMessage] {
      def decode: Get[ContactLookErrorMessage] =
        for {
          requestId <- varInt.decode
        } yield ContactLookErrorMessage(requestId)

      def encode(value: ContactLookErrorMessage): ByteVector =
        varInt.encode(value.requestId)
    }
}
