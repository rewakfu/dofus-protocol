package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapComplementaryInformationsBreachMessage(
  subAreaId: Short,
  mapId: Double,
  houses: List[HouseInformations],
  actors: List[GameRolePlayActorInformations],
  interactiveElements: List[InteractiveElement],
  statedElements: List[StatedElement],
  obstacles: List[MapObstacle],
  fights: List[FightCommonInformations],
  hasAggressiveMonsters: Boolean,
  fightStartPositions: FightStartingPositions,
  floor: Int,
  room: Byte,
  infinityMode: Short,
  branches: List[BreachBranch]
) extends Message(6791)

object MapComplementaryInformationsBreachMessage {
  implicit val codec: Codec[MapComplementaryInformationsBreachMessage] =
    new Codec[MapComplementaryInformationsBreachMessage] {
      def decode: Get[MapComplementaryInformationsBreachMessage] =
        for {
          subAreaId <- varShort.decode
          mapId <- double.decode
          houses <- list(ushort, Codec[HouseInformations]).decode
          actors <- list(ushort, Codec[GameRolePlayActorInformations]).decode
          interactiveElements <- list(ushort, Codec[InteractiveElement]).decode
          statedElements <- list(ushort, Codec[StatedElement]).decode
          obstacles <- list(ushort, Codec[MapObstacle]).decode
          fights <- list(ushort, Codec[FightCommonInformations]).decode
          hasAggressiveMonsters <- bool.decode
          fightStartPositions <- Codec[FightStartingPositions].decode
          floor <- varInt.decode
          room <- byte.decode
          infinityMode <- short.decode
          branches <- list(ushort, Codec[BreachBranch]).decode
        } yield MapComplementaryInformationsBreachMessage(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions, floor, room, infinityMode, branches)

      def encode(value: MapComplementaryInformationsBreachMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        double.encode(value.mapId) ++
        list(ushort, Codec[HouseInformations]).encode(value.houses) ++
        list(ushort, Codec[GameRolePlayActorInformations]).encode(value.actors) ++
        list(ushort, Codec[InteractiveElement]).encode(value.interactiveElements) ++
        list(ushort, Codec[StatedElement]).encode(value.statedElements) ++
        list(ushort, Codec[MapObstacle]).encode(value.obstacles) ++
        list(ushort, Codec[FightCommonInformations]).encode(value.fights) ++
        bool.encode(value.hasAggressiveMonsters) ++
        Codec[FightStartingPositions].encode(value.fightStartPositions) ++
        varInt.encode(value.floor) ++
        byte.encode(value.room) ++
        short.encode(value.infinityMode) ++
        list(ushort, Codec[BreachBranch]).encode(value.branches)
    }
}
