package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PlayerStatusUpdateRequestMessage(
  status: PlayerStatus
) extends Message(6387)

object PlayerStatusUpdateRequestMessage {
  implicit val codec: Codec[PlayerStatusUpdateRequestMessage] =
    new Codec[PlayerStatusUpdateRequestMessage] {
      def decode: Get[PlayerStatusUpdateRequestMessage] =
        for {
          status <- Codec[PlayerStatus].decode
        } yield PlayerStatusUpdateRequestMessage(status)

      def encode(value: PlayerStatusUpdateRequestMessage): ByteVector =
        Codec[PlayerStatus].encode(value.status)
    }
}
