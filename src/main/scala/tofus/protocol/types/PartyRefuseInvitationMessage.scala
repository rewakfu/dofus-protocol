package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyRefuseInvitationMessage(
  partyId: Int
) extends Message(5582)

object PartyRefuseInvitationMessage {
  implicit val codec: Codec[PartyRefuseInvitationMessage] =
    new Codec[PartyRefuseInvitationMessage] {
      def decode: Get[PartyRefuseInvitationMessage] =
        for {
          partyId <- varInt.decode
        } yield PartyRefuseInvitationMessage(partyId)

      def encode(value: PartyRefuseInvitationMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
