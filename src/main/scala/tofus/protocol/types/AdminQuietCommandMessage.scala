package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AdminQuietCommandMessage(
  content: String
) extends Message(5662)

object AdminQuietCommandMessage {
  implicit val codec: Codec[AdminQuietCommandMessage] =
    new Codec[AdminQuietCommandMessage] {
      def decode: Get[AdminQuietCommandMessage] =
        for {
          content <- utf8(ushort).decode
        } yield AdminQuietCommandMessage(content)

      def encode(value: AdminQuietCommandMessage): ByteVector =
        utf8(ushort).encode(value.content)
    }
}
