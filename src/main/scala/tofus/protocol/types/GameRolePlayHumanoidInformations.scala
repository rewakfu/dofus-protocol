package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameRolePlayHumanoidInformations extends GameRolePlayNamedActorInformations

final case class ConcreteGameRolePlayHumanoidInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  name: String,
  humanoidInfo: HumanInformations,
  accountId: Int
) extends GameRolePlayHumanoidInformations {
  override val protocolId = 159
}

object ConcreteGameRolePlayHumanoidInformations {
  implicit val codec: Codec[ConcreteGameRolePlayHumanoidInformations] =  
    new Codec[ConcreteGameRolePlayHumanoidInformations] {
      def decode: Get[ConcreteGameRolePlayHumanoidInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          name <- utf8(ushort).decode
          humanoidInfo <- Codec[HumanInformations].decode
          accountId <- int.decode
        } yield ConcreteGameRolePlayHumanoidInformations(contextualId, disposition, look, name, humanoidInfo, accountId)

      def encode(value: ConcreteGameRolePlayHumanoidInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        utf8(ushort).encode(value.name) ++
        Codec[HumanInformations].encode(value.humanoidInfo) ++
        int.encode(value.accountId)
    }
}

object GameRolePlayHumanoidInformations {
  implicit val codec: Codec[GameRolePlayHumanoidInformations] =
    new Codec[GameRolePlayHumanoidInformations] {
      def decode: Get[GameRolePlayHumanoidInformations] =
        ushort.decode.flatMap {
          case 159 => Codec[ConcreteGameRolePlayHumanoidInformations].decode
          case 3 => Codec[GameRolePlayMutantInformations].decode
          case 36 => Codec[GameRolePlayCharacterInformations].decode
        }

      def encode(value: GameRolePlayHumanoidInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameRolePlayHumanoidInformations => Codec[ConcreteGameRolePlayHumanoidInformations].encode(i)
          case i: GameRolePlayMutantInformations => Codec[GameRolePlayMutantInformations].encode(i)
          case i: GameRolePlayCharacterInformations => Codec[GameRolePlayCharacterInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
