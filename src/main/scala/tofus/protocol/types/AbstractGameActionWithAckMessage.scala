package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AbstractGameActionWithAckMessage(
  actionId: Short,
  sourceId: Double,
  waitAckId: Short
) extends Message(1001)

object AbstractGameActionWithAckMessage {
  implicit val codec: Codec[AbstractGameActionWithAckMessage] =
    new Codec[AbstractGameActionWithAckMessage] {
      def decode: Get[AbstractGameActionWithAckMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          waitAckId <- short.decode
        } yield AbstractGameActionWithAckMessage(actionId, sourceId, waitAckId)

      def encode(value: AbstractGameActionWithAckMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        short.encode(value.waitAckId)
    }
}
