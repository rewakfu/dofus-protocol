package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceBulletinSetRequestMessage(
  content: String,
  notifyMembers: Boolean
) extends Message(6693)

object AllianceBulletinSetRequestMessage {
  implicit val codec: Codec[AllianceBulletinSetRequestMessage] =
    new Codec[AllianceBulletinSetRequestMessage] {
      def decode: Get[AllianceBulletinSetRequestMessage] =
        for {
          content <- utf8(ushort).decode
          notifyMembers <- bool.decode
        } yield AllianceBulletinSetRequestMessage(content, notifyMembers)

      def encode(value: AllianceBulletinSetRequestMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        bool.encode(value.notifyMembers)
    }
}
