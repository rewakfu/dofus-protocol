package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInformationsPaddocksMessage(
  nbPaddockMax: Byte,
  paddocksInformations: List[PaddockContentInformations]
) extends Message(5959)

object GuildInformationsPaddocksMessage {
  implicit val codec: Codec[GuildInformationsPaddocksMessage] =
    new Codec[GuildInformationsPaddocksMessage] {
      def decode: Get[GuildInformationsPaddocksMessage] =
        for {
          nbPaddockMax <- byte.decode
          paddocksInformations <- list(ushort, Codec[PaddockContentInformations]).decode
        } yield GuildInformationsPaddocksMessage(nbPaddockMax, paddocksInformations)

      def encode(value: GuildInformationsPaddocksMessage): ByteVector =
        byte.encode(value.nbPaddockMax) ++
        list(ushort, Codec[PaddockContentInformations]).encode(value.paddocksInformations)
    }
}
