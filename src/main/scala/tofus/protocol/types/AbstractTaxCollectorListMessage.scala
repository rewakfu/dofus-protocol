package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AbstractTaxCollectorListMessage(
  informations: List[TaxCollectorInformations]
) extends Message(6568)

object AbstractTaxCollectorListMessage {
  implicit val codec: Codec[AbstractTaxCollectorListMessage] =
    new Codec[AbstractTaxCollectorListMessage] {
      def decode: Get[AbstractTaxCollectorListMessage] =
        for {
          informations <- list(ushort, Codec[TaxCollectorInformations]).decode
        } yield AbstractTaxCollectorListMessage(informations)

      def encode(value: AbstractTaxCollectorListMessage): ByteVector =
        list(ushort, Codec[TaxCollectorInformations]).encode(value.informations)
    }
}
