package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PrismSubareaEmptyInfo extends ProtocolType

final case class ConcretePrismSubareaEmptyInfo(
  subAreaId: Short,
  allianceId: Int
) extends PrismSubareaEmptyInfo {
  override val protocolId = 438
}

object ConcretePrismSubareaEmptyInfo {
  implicit val codec: Codec[ConcretePrismSubareaEmptyInfo] =  
    new Codec[ConcretePrismSubareaEmptyInfo] {
      def decode: Get[ConcretePrismSubareaEmptyInfo] =
        for {
          subAreaId <- varShort.decode
          allianceId <- varInt.decode
        } yield ConcretePrismSubareaEmptyInfo(subAreaId, allianceId)

      def encode(value: ConcretePrismSubareaEmptyInfo): ByteVector =
        varShort.encode(value.subAreaId) ++
        varInt.encode(value.allianceId)
    }
}

object PrismSubareaEmptyInfo {
  implicit val codec: Codec[PrismSubareaEmptyInfo] =
    new Codec[PrismSubareaEmptyInfo] {
      def decode: Get[PrismSubareaEmptyInfo] =
        ushort.decode.flatMap {
          case 438 => Codec[ConcretePrismSubareaEmptyInfo].decode
          case 434 => Codec[PrismGeolocalizedInformation].decode
        }

      def encode(value: PrismSubareaEmptyInfo): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePrismSubareaEmptyInfo => Codec[ConcretePrismSubareaEmptyInfo].encode(i)
          case i: PrismGeolocalizedInformation => Codec[PrismGeolocalizedInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
