package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AbstractFightTeamInformations extends ProtocolType

final case class ConcreteAbstractFightTeamInformations(
  teamId: Byte,
  leaderId: Double,
  teamSide: Byte,
  teamTypeId: Byte,
  nbWaves: Byte
) extends AbstractFightTeamInformations {
  override val protocolId = 116
}

object ConcreteAbstractFightTeamInformations {
  implicit val codec: Codec[ConcreteAbstractFightTeamInformations] =  
    new Codec[ConcreteAbstractFightTeamInformations] {
      def decode: Get[ConcreteAbstractFightTeamInformations] =
        for {
          teamId <- byte.decode
          leaderId <- double.decode
          teamSide <- byte.decode
          teamTypeId <- byte.decode
          nbWaves <- byte.decode
        } yield ConcreteAbstractFightTeamInformations(teamId, leaderId, teamSide, teamTypeId, nbWaves)

      def encode(value: ConcreteAbstractFightTeamInformations): ByteVector =
        byte.encode(value.teamId) ++
        double.encode(value.leaderId) ++
        byte.encode(value.teamSide) ++
        byte.encode(value.teamTypeId) ++
        byte.encode(value.nbWaves)
    }
}

object AbstractFightTeamInformations {
  implicit val codec: Codec[AbstractFightTeamInformations] =
    new Codec[AbstractFightTeamInformations] {
      def decode: Get[AbstractFightTeamInformations] =
        ushort.decode.flatMap {
          case 116 => Codec[ConcreteAbstractFightTeamInformations].decode
          case 439 => Codec[FightAllianceTeamInformations].decode
          case 33 => Codec[ConcreteFightTeamInformations].decode
          case 115 => Codec[FightTeamLightInformations].decode
        }

      def encode(value: AbstractFightTeamInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAbstractFightTeamInformations => Codec[ConcreteAbstractFightTeamInformations].encode(i)
          case i: FightAllianceTeamInformations => Codec[FightAllianceTeamInformations].encode(i)
          case i: ConcreteFightTeamInformations => Codec[ConcreteFightTeamInformations].encode(i)
          case i: FightTeamLightInformations => Codec[FightTeamLightInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
