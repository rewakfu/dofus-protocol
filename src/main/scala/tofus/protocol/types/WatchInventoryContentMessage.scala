package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class WatchInventoryContentMessage(
  objects: List[ObjectItem],
  kamas: Long
) extends Message(6849)

object WatchInventoryContentMessage {
  implicit val codec: Codec[WatchInventoryContentMessage] =
    new Codec[WatchInventoryContentMessage] {
      def decode: Get[WatchInventoryContentMessage] =
        for {
          objects <- list(ushort, Codec[ObjectItem]).decode
          kamas <- varLong.decode
        } yield WatchInventoryContentMessage(objects, kamas)

      def encode(value: WatchInventoryContentMessage): ByteVector =
        list(ushort, Codec[ObjectItem]).encode(value.objects) ++
        varLong.encode(value.kamas)
    }
}
