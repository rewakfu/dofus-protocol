package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarRemoveRequestMessage(
  barType: Byte,
  slot: Byte
) extends Message(6228)

object ShortcutBarRemoveRequestMessage {
  implicit val codec: Codec[ShortcutBarRemoveRequestMessage] =
    new Codec[ShortcutBarRemoveRequestMessage] {
      def decode: Get[ShortcutBarRemoveRequestMessage] =
        for {
          barType <- byte.decode
          slot <- byte.decode
        } yield ShortcutBarRemoveRequestMessage(barType, slot)

      def encode(value: ShortcutBarRemoveRequestMessage): ByteVector =
        byte.encode(value.barType) ++
        byte.encode(value.slot)
    }
}
