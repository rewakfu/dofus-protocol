package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AcquaintancesListMessage(
  acquaintanceList: List[AcquaintanceInformation]
) extends Message(6820)

object AcquaintancesListMessage {
  implicit val codec: Codec[AcquaintancesListMessage] =
    new Codec[AcquaintancesListMessage] {
      def decode: Get[AcquaintancesListMessage] =
        for {
          acquaintanceList <- list(ushort, Codec[AcquaintanceInformation]).decode
        } yield AcquaintancesListMessage(acquaintanceList)

      def encode(value: AcquaintancesListMessage): ByteVector =
        list(ushort, Codec[AcquaintanceInformation]).encode(value.acquaintanceList)
    }
}
