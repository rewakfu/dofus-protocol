package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TrustStatusMessage(
  flags0: Byte
) extends Message(6267)

object TrustStatusMessage {
  implicit val codec: Codec[TrustStatusMessage] =
    new Codec[TrustStatusMessage] {
      def decode: Get[TrustStatusMessage] =
        for {
          flags0 <- byte.decode
        } yield TrustStatusMessage(flags0)

      def encode(value: TrustStatusMessage): ByteVector =
        byte.encode(value.flags0)
    }
}
