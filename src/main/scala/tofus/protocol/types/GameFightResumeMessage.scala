package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightResumeMessage(
  effects: List[FightDispellableEffectExtendedInformations],
  marks: List[GameActionMark],
  gameTurn: Short,
  fightStart: Int,
  idols: List[ConcreteIdol],
  fxTriggerCounts: List[GameFightEffectTriggerCount],
  spellCooldowns: List[GameFightSpellCooldown],
  summonCount: Byte,
  bombCount: Byte
) extends Message(6067)

object GameFightResumeMessage {
  implicit val codec: Codec[GameFightResumeMessage] =
    new Codec[GameFightResumeMessage] {
      def decode: Get[GameFightResumeMessage] =
        for {
          effects <- list(ushort, Codec[FightDispellableEffectExtendedInformations]).decode
          marks <- list(ushort, Codec[GameActionMark]).decode
          gameTurn <- varShort.decode
          fightStart <- int.decode
          idols <- list(ushort, Codec[ConcreteIdol]).decode
          fxTriggerCounts <- list(ushort, Codec[GameFightEffectTriggerCount]).decode
          spellCooldowns <- list(ushort, Codec[GameFightSpellCooldown]).decode
          summonCount <- byte.decode
          bombCount <- byte.decode
        } yield GameFightResumeMessage(effects, marks, gameTurn, fightStart, idols, fxTriggerCounts, spellCooldowns, summonCount, bombCount)

      def encode(value: GameFightResumeMessage): ByteVector =
        list(ushort, Codec[FightDispellableEffectExtendedInformations]).encode(value.effects) ++
        list(ushort, Codec[GameActionMark]).encode(value.marks) ++
        varShort.encode(value.gameTurn) ++
        int.encode(value.fightStart) ++
        list(ushort, Codec[ConcreteIdol]).encode(value.idols) ++
        list(ushort, Codec[GameFightEffectTriggerCount]).encode(value.fxTriggerCounts) ++
        list(ushort, Codec[GameFightSpellCooldown]).encode(value.spellCooldowns) ++
        byte.encode(value.summonCount) ++
        byte.encode(value.bombCount)
    }
}
