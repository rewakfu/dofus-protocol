package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementSwapPositionsCancelMessage(
  requestId: Int
) extends Message(6543)

object GameFightPlacementSwapPositionsCancelMessage {
  implicit val codec: Codec[GameFightPlacementSwapPositionsCancelMessage] =
    new Codec[GameFightPlacementSwapPositionsCancelMessage] {
      def decode: Get[GameFightPlacementSwapPositionsCancelMessage] =
        for {
          requestId <- int.decode
        } yield GameFightPlacementSwapPositionsCancelMessage(requestId)

      def encode(value: GameFightPlacementSwapPositionsCancelMessage): ByteVector =
        int.encode(value.requestId)
    }
}
