package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareErrorMessage(
  error: Byte
) extends Message(6667)

object DareErrorMessage {
  implicit val codec: Codec[DareErrorMessage] =
    new Codec[DareErrorMessage] {
      def decode: Get[DareErrorMessage] =
        for {
          error <- byte.decode
        } yield DareErrorMessage(error)

      def encode(value: DareErrorMessage): ByteVector =
        byte.encode(value.error)
    }
}
