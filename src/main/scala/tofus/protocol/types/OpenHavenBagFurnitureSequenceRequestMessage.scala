package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class OpenHavenBagFurnitureSequenceRequestMessage(

) extends Message(6635)

object OpenHavenBagFurnitureSequenceRequestMessage {
  implicit val codec: Codec[OpenHavenBagFurnitureSequenceRequestMessage] =
    Codec.const(OpenHavenBagFurnitureSequenceRequestMessage())
}
