package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NotificationResetMessage(

) extends Message(6089)

object NotificationResetMessage {
  implicit val codec: Codec[NotificationResetMessage] =
    Codec.const(NotificationResetMessage())
}
