package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChallengeTargetsListRequestMessage(
  challengeId: Short
) extends Message(5614)

object ChallengeTargetsListRequestMessage {
  implicit val codec: Codec[ChallengeTargetsListRequestMessage] =
    new Codec[ChallengeTargetsListRequestMessage] {
      def decode: Get[ChallengeTargetsListRequestMessage] =
        for {
          challengeId <- varShort.decode
        } yield ChallengeTargetsListRequestMessage(challengeId)

      def encode(value: ChallengeTargetsListRequestMessage): ByteVector =
        varShort.encode(value.challengeId)
    }
}
