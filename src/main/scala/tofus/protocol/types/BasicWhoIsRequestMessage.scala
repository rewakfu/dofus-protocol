package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicWhoIsRequestMessage(
  verbose: Boolean,
  search: String
) extends Message(181)

object BasicWhoIsRequestMessage {
  implicit val codec: Codec[BasicWhoIsRequestMessage] =
    new Codec[BasicWhoIsRequestMessage] {
      def decode: Get[BasicWhoIsRequestMessage] =
        for {
          verbose <- bool.decode
          search <- utf8(ushort).decode
        } yield BasicWhoIsRequestMessage(verbose, search)

      def encode(value: BasicWhoIsRequestMessage): ByteVector =
        bool.encode(value.verbose) ++
        utf8(ushort).encode(value.search)
    }
}
