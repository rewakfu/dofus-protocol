package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockMoveItemRequestMessage(
  oldCellId: Short,
  newCellId: Short
) extends Message(6052)

object PaddockMoveItemRequestMessage {
  implicit val codec: Codec[PaddockMoveItemRequestMessage] =
    new Codec[PaddockMoveItemRequestMessage] {
      def decode: Get[PaddockMoveItemRequestMessage] =
        for {
          oldCellId <- varShort.decode
          newCellId <- varShort.decode
        } yield PaddockMoveItemRequestMessage(oldCellId, newCellId)

      def encode(value: PaddockMoveItemRequestMessage): ByteVector =
        varShort.encode(value.oldCellId) ++
        varShort.encode(value.newCellId)
    }
}
