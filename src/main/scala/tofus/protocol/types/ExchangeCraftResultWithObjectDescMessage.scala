package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftResultWithObjectDescMessage(
  craftResult: Byte,
  objectInfo: ObjectItemNotInContainer
) extends Message(5999)

object ExchangeCraftResultWithObjectDescMessage {
  implicit val codec: Codec[ExchangeCraftResultWithObjectDescMessage] =
    new Codec[ExchangeCraftResultWithObjectDescMessage] {
      def decode: Get[ExchangeCraftResultWithObjectDescMessage] =
        for {
          craftResult <- byte.decode
          objectInfo <- Codec[ObjectItemNotInContainer].decode
        } yield ExchangeCraftResultWithObjectDescMessage(craftResult, objectInfo)

      def encode(value: ExchangeCraftResultWithObjectDescMessage): ByteVector =
        byte.encode(value.craftResult) ++
        Codec[ObjectItemNotInContainer].encode(value.objectInfo)
    }
}
