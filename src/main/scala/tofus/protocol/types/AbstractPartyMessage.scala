package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AbstractPartyMessage(
  partyId: Int
) extends Message(6274)

object AbstractPartyMessage {
  implicit val codec: Codec[AbstractPartyMessage] =
    new Codec[AbstractPartyMessage] {
      def decode: Get[AbstractPartyMessage] =
        for {
          partyId <- varInt.decode
        } yield AbstractPartyMessage(partyId)

      def encode(value: AbstractPartyMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
