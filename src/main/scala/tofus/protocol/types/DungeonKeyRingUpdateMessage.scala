package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonKeyRingUpdateMessage(
  dungeonId: Short,
  available: Boolean
) extends Message(6296)

object DungeonKeyRingUpdateMessage {
  implicit val codec: Codec[DungeonKeyRingUpdateMessage] =
    new Codec[DungeonKeyRingUpdateMessage] {
      def decode: Get[DungeonKeyRingUpdateMessage] =
        for {
          dungeonId <- varShort.decode
          available <- bool.decode
        } yield DungeonKeyRingUpdateMessage(dungeonId, available)

      def encode(value: DungeonKeyRingUpdateMessage): ByteVector =
        varShort.encode(value.dungeonId) ++
        bool.encode(value.available)
    }
}
