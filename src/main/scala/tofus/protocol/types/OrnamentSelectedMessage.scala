package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class OrnamentSelectedMessage(
  ornamentId: Short
) extends Message(6369)

object OrnamentSelectedMessage {
  implicit val codec: Codec[OrnamentSelectedMessage] =
    new Codec[OrnamentSelectedMessage] {
      def decode: Get[OrnamentSelectedMessage] =
        for {
          ornamentId <- varShort.decode
        } yield OrnamentSelectedMessage(ornamentId)

      def encode(value: OrnamentSelectedMessage): ByteVector =
        varShort.encode(value.ornamentId)
    }
}
