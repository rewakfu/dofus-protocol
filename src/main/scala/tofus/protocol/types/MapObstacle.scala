package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapObstacle(
  obstacleCellId: Short,
  state: Byte
) extends ProtocolType {
  override val protocolId = 200
}

object MapObstacle {
  implicit val codec: Codec[MapObstacle] =
    new Codec[MapObstacle] {
      def decode: Get[MapObstacle] =
        for {
          obstacleCellId <- varShort.decode
          state <- byte.decode
        } yield MapObstacle(obstacleCellId, state)

      def encode(value: MapObstacle): ByteVector =
        varShort.encode(value.obstacleCellId) ++
        byte.encode(value.state)
    }
}
