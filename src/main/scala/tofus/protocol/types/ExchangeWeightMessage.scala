package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeWeightMessage(
  currentWeight: Int,
  maxWeight: Int
) extends Message(5793)

object ExchangeWeightMessage {
  implicit val codec: Codec[ExchangeWeightMessage] =
    new Codec[ExchangeWeightMessage] {
      def decode: Get[ExchangeWeightMessage] =
        for {
          currentWeight <- varInt.decode
          maxWeight <- varInt.decode
        } yield ExchangeWeightMessage(currentWeight, maxWeight)

      def encode(value: ExchangeWeightMessage): ByteVector =
        varInt.encode(value.currentWeight) ++
        varInt.encode(value.maxWeight)
    }
}
