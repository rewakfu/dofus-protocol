package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TitlesAndOrnamentsListRequestMessage(

) extends Message(6363)

object TitlesAndOrnamentsListRequestMessage {
  implicit val codec: Codec[TitlesAndOrnamentsListRequestMessage] =
    Codec.const(TitlesAndOrnamentsListRequestMessage())
}
