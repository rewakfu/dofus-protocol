package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LivingObjectDissociateMessage(
  livingUID: Int,
  livingPosition: Short
) extends Message(5723)

object LivingObjectDissociateMessage {
  implicit val codec: Codec[LivingObjectDissociateMessage] =
    new Codec[LivingObjectDissociateMessage] {
      def decode: Get[LivingObjectDissociateMessage] =
        for {
          livingUID <- varInt.decode
          livingPosition <- ubyte.decode
        } yield LivingObjectDissociateMessage(livingUID, livingPosition)

      def encode(value: LivingObjectDissociateMessage): ByteVector =
        varInt.encode(value.livingUID) ++
        ubyte.encode(value.livingPosition)
    }
}
