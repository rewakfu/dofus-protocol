package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutEmote(
  slot: Byte,
  emoteId: Short
) extends Shortcut {
  override val protocolId = 389
}

object ShortcutEmote {
  implicit val codec: Codec[ShortcutEmote] =
    new Codec[ShortcutEmote] {
      def decode: Get[ShortcutEmote] =
        for {
          slot <- byte.decode
          emoteId <- ubyte.decode
        } yield ShortcutEmote(slot, emoteId)

      def encode(value: ShortcutEmote): ByteVector =
        byte.encode(value.slot) ++
        ubyte.encode(value.emoteId)
    }
}
