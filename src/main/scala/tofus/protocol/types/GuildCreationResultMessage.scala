package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildCreationResultMessage(
  result: Byte
) extends Message(5554)

object GuildCreationResultMessage {
  implicit val codec: Codec[GuildCreationResultMessage] =
    new Codec[GuildCreationResultMessage] {
      def decode: Get[GuildCreationResultMessage] =
        for {
          result <- byte.decode
        } yield GuildCreationResultMessage(result)

      def encode(value: GuildCreationResultMessage): ByteVector =
        byte.encode(value.result)
    }
}
