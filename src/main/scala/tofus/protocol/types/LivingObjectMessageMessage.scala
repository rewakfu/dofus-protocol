package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LivingObjectMessageMessage(
  msgId: Short,
  timeStamp: Int,
  owner: String,
  objectGenericId: Short
) extends Message(6065)

object LivingObjectMessageMessage {
  implicit val codec: Codec[LivingObjectMessageMessage] =
    new Codec[LivingObjectMessageMessage] {
      def decode: Get[LivingObjectMessageMessage] =
        for {
          msgId <- varShort.decode
          timeStamp <- int.decode
          owner <- utf8(ushort).decode
          objectGenericId <- varShort.decode
        } yield LivingObjectMessageMessage(msgId, timeStamp, owner, objectGenericId)

      def encode(value: LivingObjectMessageMessage): ByteVector =
        varShort.encode(value.msgId) ++
        int.encode(value.timeStamp) ++
        utf8(ushort).encode(value.owner) ++
        varShort.encode(value.objectGenericId)
    }
}
