package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerStatusUpdateMessage(
  server: GameServerInformations
) extends Message(50)

object ServerStatusUpdateMessage {
  implicit val codec: Codec[ServerStatusUpdateMessage] =
    new Codec[ServerStatusUpdateMessage] {
      def decode: Get[ServerStatusUpdateMessage] =
        for {
          server <- Codec[GameServerInformations].decode
        } yield ServerStatusUpdateMessage(server)

      def encode(value: ServerStatusUpdateMessage): ByteVector =
        Codec[GameServerInformations].encode(value.server)
    }
}
