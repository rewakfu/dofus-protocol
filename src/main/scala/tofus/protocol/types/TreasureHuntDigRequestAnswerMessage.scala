package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntDigRequestAnswerMessage(
  questType: Byte,
  result: Byte
) extends Message(6484)

object TreasureHuntDigRequestAnswerMessage {
  implicit val codec: Codec[TreasureHuntDigRequestAnswerMessage] =
    new Codec[TreasureHuntDigRequestAnswerMessage] {
      def decode: Get[TreasureHuntDigRequestAnswerMessage] =
        for {
          questType <- byte.decode
          result <- byte.decode
        } yield TreasureHuntDigRequestAnswerMessage(questType, result)

      def encode(value: TreasureHuntDigRequestAnswerMessage): ByteVector =
        byte.encode(value.questType) ++
        byte.encode(value.result)
    }
}
