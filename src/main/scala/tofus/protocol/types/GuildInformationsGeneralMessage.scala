package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInformationsGeneralMessage(
  abandonnedPaddock: Boolean,
  level: Short,
  expLevelFloor: Long,
  experience: Long,
  expNextLevelFloor: Long,
  creationDate: Int,
  nbTotalMembers: Short,
  nbConnectedMembers: Short
) extends Message(5557)

object GuildInformationsGeneralMessage {
  implicit val codec: Codec[GuildInformationsGeneralMessage] =
    new Codec[GuildInformationsGeneralMessage] {
      def decode: Get[GuildInformationsGeneralMessage] =
        for {
          abandonnedPaddock <- bool.decode
          level <- ubyte.decode
          expLevelFloor <- varLong.decode
          experience <- varLong.decode
          expNextLevelFloor <- varLong.decode
          creationDate <- int.decode
          nbTotalMembers <- varShort.decode
          nbConnectedMembers <- varShort.decode
        } yield GuildInformationsGeneralMessage(abandonnedPaddock, level, expLevelFloor, experience, expNextLevelFloor, creationDate, nbTotalMembers, nbConnectedMembers)

      def encode(value: GuildInformationsGeneralMessage): ByteVector =
        bool.encode(value.abandonnedPaddock) ++
        ubyte.encode(value.level) ++
        varLong.encode(value.expLevelFloor) ++
        varLong.encode(value.experience) ++
        varLong.encode(value.expNextLevelFloor) ++
        int.encode(value.creationDate) ++
        varShort.encode(value.nbTotalMembers) ++
        varShort.encode(value.nbConnectedMembers)
    }
}
