package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlliancePrismDialogQuestionMessage(

) extends Message(6448)

object AlliancePrismDialogQuestionMessage {
  implicit val codec: Codec[AlliancePrismDialogQuestionMessage] =
    Codec.const(AlliancePrismDialogQuestionMessage())
}
