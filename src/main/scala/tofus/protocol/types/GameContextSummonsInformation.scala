package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextSummonsInformation(
  spawnInformation: SpawnInformation,
  wave: Byte,
  look: EntityLook,
  stats: GameFightMinimalStats,
  summons: List[GameContextBasicSpawnInformation]
) extends ProtocolType {
  override val protocolId = 567
}

object GameContextSummonsInformation {
  implicit val codec: Codec[GameContextSummonsInformation] =
    new Codec[GameContextSummonsInformation] {
      def decode: Get[GameContextSummonsInformation] =
        for {
          spawnInformation <- Codec[SpawnInformation].decode
          wave <- byte.decode
          look <- Codec[EntityLook].decode
          stats <- Codec[GameFightMinimalStats].decode
          summons <- list(ushort, Codec[GameContextBasicSpawnInformation]).decode
        } yield GameContextSummonsInformation(spawnInformation, wave, look, stats, summons)

      def encode(value: GameContextSummonsInformation): ByteVector =
        Codec[SpawnInformation].encode(value.spawnInformation) ++
        byte.encode(value.wave) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, Codec[GameContextBasicSpawnInformation]).encode(value.summons)
    }
}
