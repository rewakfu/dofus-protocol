package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait CharacterRemodelingInformation extends AbstractCharacterInformation

final case class ConcreteCharacterRemodelingInformation(
  id: Long,
  name: String,
  breed: Byte,
  sex: Boolean,
  cosmeticId: Short,
  colors: List[Int]
) extends CharacterRemodelingInformation {
  override val protocolId = 479
}

object ConcreteCharacterRemodelingInformation {
  implicit val codec: Codec[ConcreteCharacterRemodelingInformation] =  
    new Codec[ConcreteCharacterRemodelingInformation] {
      def decode: Get[ConcreteCharacterRemodelingInformation] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          breed <- byte.decode
          sex <- bool.decode
          cosmeticId <- varShort.decode
          colors <- list(ushort, int).decode
        } yield ConcreteCharacterRemodelingInformation(id, name, breed, sex, cosmeticId, colors)

      def encode(value: ConcreteCharacterRemodelingInformation): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        varShort.encode(value.cosmeticId) ++
        list(ushort, int).encode(value.colors)
    }
}

object CharacterRemodelingInformation {
  implicit val codec: Codec[CharacterRemodelingInformation] =
    new Codec[CharacterRemodelingInformation] {
      def decode: Get[CharacterRemodelingInformation] =
        ushort.decode.flatMap {
          case 479 => Codec[ConcreteCharacterRemodelingInformation].decode
          case 477 => Codec[CharacterToRemodelInformations].decode
        }

      def encode(value: CharacterRemodelingInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteCharacterRemodelingInformation => Codec[ConcreteCharacterRemodelingInformation].encode(i)
          case i: CharacterToRemodelInformations => Codec[CharacterToRemodelInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
