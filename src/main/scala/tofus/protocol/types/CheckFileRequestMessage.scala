package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CheckFileRequestMessage(
  filename: String,
  `type`: Byte
) extends Message(6154)

object CheckFileRequestMessage {
  implicit val codec: Codec[CheckFileRequestMessage] =
    new Codec[CheckFileRequestMessage] {
      def decode: Get[CheckFileRequestMessage] =
        for {
          filename <- utf8(ushort).decode
          `type` <- byte.decode
        } yield CheckFileRequestMessage(filename, `type`)

      def encode(value: CheckFileRequestMessage): ByteVector =
        utf8(ushort).encode(value.filename) ++
        byte.encode(value.`type`)
    }
}
