package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInvitedMessage(
  recruterId: Long,
  recruterName: String,
  allianceInfo: ConcreteBasicNamedAllianceInformations
) extends Message(6397)

object AllianceInvitedMessage {
  implicit val codec: Codec[AllianceInvitedMessage] =
    new Codec[AllianceInvitedMessage] {
      def decode: Get[AllianceInvitedMessage] =
        for {
          recruterId <- varLong.decode
          recruterName <- utf8(ushort).decode
          allianceInfo <- Codec[ConcreteBasicNamedAllianceInformations].decode
        } yield AllianceInvitedMessage(recruterId, recruterName, allianceInfo)

      def encode(value: AllianceInvitedMessage): ByteVector =
        varLong.encode(value.recruterId) ++
        utf8(ushort).encode(value.recruterName) ++
        Codec[ConcreteBasicNamedAllianceInformations].encode(value.allianceInfo)
    }
}
