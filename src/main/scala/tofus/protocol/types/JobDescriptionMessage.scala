package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobDescriptionMessage(
  jobsDescription: List[JobDescription]
) extends Message(5655)

object JobDescriptionMessage {
  implicit val codec: Codec[JobDescriptionMessage] =
    new Codec[JobDescriptionMessage] {
      def decode: Get[JobDescriptionMessage] =
        for {
          jobsDescription <- list(ushort, Codec[JobDescription]).decode
        } yield JobDescriptionMessage(jobsDescription)

      def encode(value: JobDescriptionMessage): ByteVector =
        list(ushort, Codec[JobDescription]).encode(value.jobsDescription)
    }
}
