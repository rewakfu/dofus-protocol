package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismSetSabotagedRefusedMessage(
  subAreaId: Short,
  reason: Byte
) extends Message(6466)

object PrismSetSabotagedRefusedMessage {
  implicit val codec: Codec[PrismSetSabotagedRefusedMessage] =
    new Codec[PrismSetSabotagedRefusedMessage] {
      def decode: Get[PrismSetSabotagedRefusedMessage] =
        for {
          subAreaId <- varShort.decode
          reason <- byte.decode
        } yield PrismSetSabotagedRefusedMessage(subAreaId, reason)

      def encode(value: PrismSetSabotagedRefusedMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        byte.encode(value.reason)
    }
}
