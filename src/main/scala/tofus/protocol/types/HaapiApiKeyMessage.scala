package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiApiKeyMessage(
  token: String
) extends Message(6649)

object HaapiApiKeyMessage {
  implicit val codec: Codec[HaapiApiKeyMessage] =
    new Codec[HaapiApiKeyMessage] {
      def decode: Get[HaapiApiKeyMessage] =
        for {
          token <- utf8(ushort).decode
        } yield HaapiApiKeyMessage(token)

      def encode(value: HaapiApiKeyMessage): ByteVector =
        utf8(ushort).encode(value.token)
    }
}
