package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportDestination(
  `type`: Byte,
  mapId: Double,
  subAreaId: Short,
  level: Short,
  cost: Short
) extends ProtocolType {
  override val protocolId = 563
}

object TeleportDestination {
  implicit val codec: Codec[TeleportDestination] =
    new Codec[TeleportDestination] {
      def decode: Get[TeleportDestination] =
        for {
          `type` <- byte.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          level <- varShort.decode
          cost <- varShort.decode
        } yield TeleportDestination(`type`, mapId, subAreaId, level, cost)

      def encode(value: TeleportDestination): ByteVector =
        byte.encode(value.`type`) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        varShort.encode(value.level) ++
        varShort.encode(value.cost)
    }
}
