package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareRewardWonMessage(
  reward: DareReward
) extends Message(6678)

object DareRewardWonMessage {
  implicit val codec: Codec[DareRewardWonMessage] =
    new Codec[DareRewardWonMessage] {
      def decode: Get[DareRewardWonMessage] =
        for {
          reward <- Codec[DareReward].decode
        } yield DareRewardWonMessage(reward)

      def encode(value: DareRewardWonMessage): ByteVector =
        Codec[DareReward].encode(value.reward)
    }
}
