package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightJoinMessage(
  flags0: Byte,
  timeMaxBeforeFightStart: Short,
  fightType: Byte
) extends Message(702)

object GameFightJoinMessage {
  implicit val codec: Codec[GameFightJoinMessage] =
    new Codec[GameFightJoinMessage] {
      def decode: Get[GameFightJoinMessage] =
        for {
          flags0 <- byte.decode
          timeMaxBeforeFightStart <- short.decode
          fightType <- byte.decode
        } yield GameFightJoinMessage(flags0, timeMaxBeforeFightStart, fightType)

      def encode(value: GameFightJoinMessage): ByteVector =
        byte.encode(value.flags0) ++
        short.encode(value.timeMaxBeforeFightStart) ++
        byte.encode(value.fightType)
    }
}
