package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatAbstractServerMessage(
  channel: Byte,
  content: String,
  timestamp: Int,
  fingerprint: String
) extends Message(880)

object ChatAbstractServerMessage {
  implicit val codec: Codec[ChatAbstractServerMessage] =
    new Codec[ChatAbstractServerMessage] {
      def decode: Get[ChatAbstractServerMessage] =
        for {
          channel <- byte.decode
          content <- utf8(ushort).decode
          timestamp <- int.decode
          fingerprint <- utf8(ushort).decode
        } yield ChatAbstractServerMessage(channel, content, timestamp, fingerprint)

      def encode(value: ChatAbstractServerMessage): ByteVector =
        byte.encode(value.channel) ++
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        utf8(ushort).encode(value.fingerprint)
    }
}
