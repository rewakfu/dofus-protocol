package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterCapabilitiesMessage(
  guildEmblemSymbolCategories: Int
) extends Message(6339)

object CharacterCapabilitiesMessage {
  implicit val codec: Codec[CharacterCapabilitiesMessage] =
    new Codec[CharacterCapabilitiesMessage] {
      def decode: Get[CharacterCapabilitiesMessage] =
        for {
          guildEmblemSymbolCategories <- varInt.decode
        } yield CharacterCapabilitiesMessage(guildEmblemSymbolCategories)

      def encode(value: CharacterCapabilitiesMessage): ByteVector =
        varInt.encode(value.guildEmblemSymbolCategories)
    }
}
