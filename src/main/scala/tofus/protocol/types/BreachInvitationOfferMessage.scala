package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachInvitationOfferMessage(
  host: ConcreteCharacterMinimalInformations,
  timeLeftBeforeCancel: Int
) extends Message(6805)

object BreachInvitationOfferMessage {
  implicit val codec: Codec[BreachInvitationOfferMessage] =
    new Codec[BreachInvitationOfferMessage] {
      def decode: Get[BreachInvitationOfferMessage] =
        for {
          host <- Codec[ConcreteCharacterMinimalInformations].decode
          timeLeftBeforeCancel <- varInt.decode
        } yield BreachInvitationOfferMessage(host, timeLeftBeforeCancel)

      def encode(value: BreachInvitationOfferMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.host) ++
        varInt.encode(value.timeLeftBeforeCancel)
    }
}
