package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachInvitationAnswerMessage(
  accept: Boolean
) extends Message(6795)

object BreachInvitationAnswerMessage {
  implicit val codec: Codec[BreachInvitationAnswerMessage] =
    new Codec[BreachInvitationAnswerMessage] {
      def decode: Get[BreachInvitationAnswerMessage] =
        for {
          accept <- bool.decode
        } yield BreachInvitationAnswerMessage(accept)

      def encode(value: BreachInvitationAnswerMessage): ByteVector =
        bool.encode(value.accept)
    }
}
