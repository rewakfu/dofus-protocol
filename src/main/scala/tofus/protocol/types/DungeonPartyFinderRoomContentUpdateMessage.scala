package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderRoomContentUpdateMessage(
  dungeonId: Short,
  addedPlayers: List[DungeonPartyFinderPlayer],
  removedPlayersIds: List[Long]
) extends Message(6250)

object DungeonPartyFinderRoomContentUpdateMessage {
  implicit val codec: Codec[DungeonPartyFinderRoomContentUpdateMessage] =
    new Codec[DungeonPartyFinderRoomContentUpdateMessage] {
      def decode: Get[DungeonPartyFinderRoomContentUpdateMessage] =
        for {
          dungeonId <- varShort.decode
          addedPlayers <- list(ushort, Codec[DungeonPartyFinderPlayer]).decode
          removedPlayersIds <- list(ushort, varLong).decode
        } yield DungeonPartyFinderRoomContentUpdateMessage(dungeonId, addedPlayers, removedPlayersIds)

      def encode(value: DungeonPartyFinderRoomContentUpdateMessage): ByteVector =
        varShort.encode(value.dungeonId) ++
        list(ushort, Codec[DungeonPartyFinderPlayer]).encode(value.addedPlayers) ++
        list(ushort, varLong).encode(value.removedPlayersIds)
    }
}
