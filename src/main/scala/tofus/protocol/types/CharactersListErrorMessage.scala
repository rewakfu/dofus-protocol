package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharactersListErrorMessage(

) extends Message(5545)

object CharactersListErrorMessage {
  implicit val codec: Codec[CharactersListErrorMessage] =
    Codec.const(CharactersListErrorMessage())
}
