package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmotePlayMassiveMessage(
  emoteId: Short,
  emoteStartTime: Double,
  actorIds: List[Double]
) extends Message(5691)

object EmotePlayMassiveMessage {
  implicit val codec: Codec[EmotePlayMassiveMessage] =
    new Codec[EmotePlayMassiveMessage] {
      def decode: Get[EmotePlayMassiveMessage] =
        for {
          emoteId <- ubyte.decode
          emoteStartTime <- double.decode
          actorIds <- list(ushort, double).decode
        } yield EmotePlayMassiveMessage(emoteId, emoteStartTime, actorIds)

      def encode(value: EmotePlayMassiveMessage): ByteVector =
        ubyte.encode(value.emoteId) ++
        double.encode(value.emoteStartTime) ++
        list(ushort, double).encode(value.actorIds)
    }
}
