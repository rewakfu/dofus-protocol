package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredDeleteResultMessage(
  flags0: Byte,
  name: String
) extends Message(5677)

object IgnoredDeleteResultMessage {
  implicit val codec: Codec[IgnoredDeleteResultMessage] =
    new Codec[IgnoredDeleteResultMessage] {
      def decode: Get[IgnoredDeleteResultMessage] =
        for {
          flags0 <- byte.decode
          name <- utf8(ushort).decode
        } yield IgnoredDeleteResultMessage(flags0, name)

      def encode(value: IgnoredDeleteResultMessage): ByteVector =
        byte.encode(value.flags0) ++
        utf8(ushort).encode(value.name)
    }
}
