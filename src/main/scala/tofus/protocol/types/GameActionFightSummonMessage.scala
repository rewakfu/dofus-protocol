package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightSummonMessage(
  actionId: Short,
  sourceId: Double,
  summons: List[GameFightFighterInformations]
) extends Message(5825)

object GameActionFightSummonMessage {
  implicit val codec: Codec[GameActionFightSummonMessage] =
    new Codec[GameActionFightSummonMessage] {
      def decode: Get[GameActionFightSummonMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          summons <- list(ushort, Codec[GameFightFighterInformations]).decode
        } yield GameActionFightSummonMessage(actionId, sourceId, summons)

      def encode(value: GameActionFightSummonMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        list(ushort, Codec[GameFightFighterInformations]).encode(value.summons)
    }
}
