package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendAddRequestMessage(
  name: String
) extends Message(4004)

object FriendAddRequestMessage {
  implicit val codec: Codec[FriendAddRequestMessage] =
    new Codec[FriendAddRequestMessage] {
      def decode: Get[FriendAddRequestMessage] =
        for {
          name <- utf8(ushort).decode
        } yield FriendAddRequestMessage(name)

      def encode(value: FriendAddRequestMessage): ByteVector =
        utf8(ushort).encode(value.name)
    }
}
