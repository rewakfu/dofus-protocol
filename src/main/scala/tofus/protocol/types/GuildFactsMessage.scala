package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFactsMessage(
  infos: GuildFactSheetInformations,
  creationDate: Int,
  nbTaxCollectors: Short,
  members: List[CharacterMinimalGuildPublicInformations]
) extends Message(6415)

object GuildFactsMessage {
  implicit val codec: Codec[GuildFactsMessage] =
    new Codec[GuildFactsMessage] {
      def decode: Get[GuildFactsMessage] =
        for {
          infos <- Codec[GuildFactSheetInformations].decode
          creationDate <- int.decode
          nbTaxCollectors <- varShort.decode
          members <- list(ushort, Codec[CharacterMinimalGuildPublicInformations]).decode
        } yield GuildFactsMessage(infos, creationDate, nbTaxCollectors, members)

      def encode(value: GuildFactsMessage): ByteVector =
        Codec[GuildFactSheetInformations].encode(value.infos) ++
        int.encode(value.creationDate) ++
        varShort.encode(value.nbTaxCollectors) ++
        list(ushort, Codec[CharacterMinimalGuildPublicInformations]).encode(value.members)
    }
}
