package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkMountMessage(
  stabledMountsDescription: List[MountClientData],
  paddockedMountsDescription: List[MountClientData]
) extends Message(5979)

object ExchangeStartOkMountMessage {
  implicit val codec: Codec[ExchangeStartOkMountMessage] =
    new Codec[ExchangeStartOkMountMessage] {
      def decode: Get[ExchangeStartOkMountMessage] =
        for {
          stabledMountsDescription <- list(ushort, Codec[MountClientData]).decode
          paddockedMountsDescription <- list(ushort, Codec[MountClientData]).decode
        } yield ExchangeStartOkMountMessage(stabledMountsDescription, paddockedMountsDescription)

      def encode(value: ExchangeStartOkMountMessage): ByteVector =
        list(ushort, Codec[MountClientData]).encode(value.stabledMountsDescription) ++
        list(ushort, Codec[MountClientData]).encode(value.paddockedMountsDescription)
    }
}
