package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayMountInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  name: String,
  ownerName: String,
  level: Short
) extends GameRolePlayNamedActorInformations {
  override val protocolId = 180
}

object GameRolePlayMountInformations {
  implicit val codec: Codec[GameRolePlayMountInformations] =
    new Codec[GameRolePlayMountInformations] {
      def decode: Get[GameRolePlayMountInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          name <- utf8(ushort).decode
          ownerName <- utf8(ushort).decode
          level <- ubyte.decode
        } yield GameRolePlayMountInformations(contextualId, disposition, look, name, ownerName, level)

      def encode(value: GameRolePlayMountInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        utf8(ushort).encode(value.name) ++
        utf8(ushort).encode(value.ownerName) ++
        ubyte.encode(value.level)
    }
}
