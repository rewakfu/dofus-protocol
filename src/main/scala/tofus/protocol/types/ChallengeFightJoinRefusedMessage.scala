package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChallengeFightJoinRefusedMessage(
  playerId: Long,
  reason: Byte
) extends Message(5908)

object ChallengeFightJoinRefusedMessage {
  implicit val codec: Codec[ChallengeFightJoinRefusedMessage] =
    new Codec[ChallengeFightJoinRefusedMessage] {
      def decode: Get[ChallengeFightJoinRefusedMessage] =
        for {
          playerId <- varLong.decode
          reason <- byte.decode
        } yield ChallengeFightJoinRefusedMessage(playerId, reason)

      def encode(value: ChallengeFightJoinRefusedMessage): ByteVector =
        varLong.encode(value.playerId) ++
        byte.encode(value.reason)
    }
}
