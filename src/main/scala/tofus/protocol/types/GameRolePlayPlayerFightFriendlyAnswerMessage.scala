package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayPlayerFightFriendlyAnswerMessage(
  fightId: Short,
  accept: Boolean
) extends Message(5732)

object GameRolePlayPlayerFightFriendlyAnswerMessage {
  implicit val codec: Codec[GameRolePlayPlayerFightFriendlyAnswerMessage] =
    new Codec[GameRolePlayPlayerFightFriendlyAnswerMessage] {
      def decode: Get[GameRolePlayPlayerFightFriendlyAnswerMessage] =
        for {
          fightId <- varShort.decode
          accept <- bool.decode
        } yield GameRolePlayPlayerFightFriendlyAnswerMessage(fightId, accept)

      def encode(value: GameRolePlayPlayerFightFriendlyAnswerMessage): ByteVector =
        varShort.encode(value.fightId) ++
        bool.encode(value.accept)
    }
}
