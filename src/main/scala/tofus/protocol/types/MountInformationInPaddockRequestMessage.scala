package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountInformationInPaddockRequestMessage(
  mapRideId: Int
) extends Message(5975)

object MountInformationInPaddockRequestMessage {
  implicit val codec: Codec[MountInformationInPaddockRequestMessage] =
    new Codec[MountInformationInPaddockRequestMessage] {
      def decode: Get[MountInformationInPaddockRequestMessage] =
        for {
          mapRideId <- varInt.decode
        } yield MountInformationInPaddockRequestMessage(mapRideId)

      def encode(value: MountInformationInPaddockRequestMessage): ByteVector =
        varInt.encode(value.mapRideId)
    }
}
