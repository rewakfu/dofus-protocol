package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyNameSetErrorMessage(
  partyId: Int,
  result: Byte
) extends Message(6501)

object PartyNameSetErrorMessage {
  implicit val codec: Codec[PartyNameSetErrorMessage] =
    new Codec[PartyNameSetErrorMessage] {
      def decode: Get[PartyNameSetErrorMessage] =
        for {
          partyId <- varInt.decode
          result <- byte.decode
        } yield PartyNameSetErrorMessage(partyId, result)

      def encode(value: PartyNameSetErrorMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.result)
    }
}
