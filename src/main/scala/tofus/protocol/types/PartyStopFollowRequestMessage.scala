package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyStopFollowRequestMessage(
  partyId: Int,
  playerId: Long
) extends Message(5574)

object PartyStopFollowRequestMessage {
  implicit val codec: Codec[PartyStopFollowRequestMessage] =
    new Codec[PartyStopFollowRequestMessage] {
      def decode: Get[PartyStopFollowRequestMessage] =
        for {
          partyId <- varInt.decode
          playerId <- varLong.decode
        } yield PartyStopFollowRequestMessage(partyId, playerId)

      def encode(value: PartyStopFollowRequestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.playerId)
    }
}
