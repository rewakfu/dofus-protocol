package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismSettingsErrorMessage(

) extends Message(6442)

object PrismSettingsErrorMessage {
  implicit val codec: Codec[PrismSettingsErrorMessage] =
    Codec.const(PrismSettingsErrorMessage())
}
