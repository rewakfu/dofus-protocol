package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendsListMessage(
  friendsList: List[FriendInformations]
) extends Message(4002)

object FriendsListMessage {
  implicit val codec: Codec[FriendsListMessage] =
    new Codec[FriendsListMessage] {
      def decode: Get[FriendsListMessage] =
        for {
          friendsList <- list(ushort, Codec[FriendInformations]).decode
        } yield FriendsListMessage(friendsList)

      def encode(value: FriendsListMessage): ByteVector =
        list(ushort, Codec[FriendInformations]).encode(value.friendsList)
    }
}
