package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredAddedMessage(
  ignoreAdded: IgnoredInformations,
  session: Boolean
) extends Message(5678)

object IgnoredAddedMessage {
  implicit val codec: Codec[IgnoredAddedMessage] =
    new Codec[IgnoredAddedMessage] {
      def decode: Get[IgnoredAddedMessage] =
        for {
          ignoreAdded <- Codec[IgnoredInformations].decode
          session <- bool.decode
        } yield IgnoredAddedMessage(ignoreAdded, session)

      def encode(value: IgnoredAddedMessage): ByteVector =
        Codec[IgnoredInformations].encode(value.ignoreAdded) ++
        bool.encode(value.session)
    }
}
