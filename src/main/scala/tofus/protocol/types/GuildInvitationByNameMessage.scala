package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInvitationByNameMessage(
  name: String
) extends Message(6115)

object GuildInvitationByNameMessage {
  implicit val codec: Codec[GuildInvitationByNameMessage] =
    new Codec[GuildInvitationByNameMessage] {
      def decode: Get[GuildInvitationByNameMessage] =
        for {
          name <- utf8(ushort).decode
        } yield GuildInvitationByNameMessage(name)

      def encode(value: GuildInvitationByNameMessage): ByteVector =
        utf8(ushort).encode(value.name)
    }
}
