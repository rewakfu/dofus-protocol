package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ServerSessionConstant extends ProtocolType

final case class ConcreteServerSessionConstant(
  id: Short
) extends ServerSessionConstant {
  override val protocolId = 430
}

object ConcreteServerSessionConstant {
  implicit val codec: Codec[ConcreteServerSessionConstant] =  
    new Codec[ConcreteServerSessionConstant] {
      def decode: Get[ConcreteServerSessionConstant] =
        for {
          id <- varShort.decode
        } yield ConcreteServerSessionConstant(id)

      def encode(value: ConcreteServerSessionConstant): ByteVector =
        varShort.encode(value.id)
    }
}

object ServerSessionConstant {
  implicit val codec: Codec[ServerSessionConstant] =
    new Codec[ServerSessionConstant] {
      def decode: Get[ServerSessionConstant] =
        ushort.decode.flatMap {
          case 430 => Codec[ConcreteServerSessionConstant].decode
          case 433 => Codec[ServerSessionConstantInteger].decode
          case 429 => Codec[ServerSessionConstantLong].decode
          case 436 => Codec[ServerSessionConstantString].decode
        }

      def encode(value: ServerSessionConstant): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteServerSessionConstant => Codec[ConcreteServerSessionConstant].encode(i)
          case i: ServerSessionConstantInteger => Codec[ServerSessionConstantInteger].encode(i)
          case i: ServerSessionConstantLong => Codec[ServerSessionConstantLong].encode(i)
          case i: ServerSessionConstantString => Codec[ServerSessionConstantString].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
