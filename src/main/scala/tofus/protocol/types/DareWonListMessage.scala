package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareWonListMessage(
  dareId: List[Double]
) extends Message(6682)

object DareWonListMessage {
  implicit val codec: Codec[DareWonListMessage] =
    new Codec[DareWonListMessage] {
      def decode: Get[DareWonListMessage] =
        for {
          dareId <- list(ushort, double).decode
        } yield DareWonListMessage(dareId)

      def encode(value: DareWonListMessage): ByteVector =
        list(ushort, double).encode(value.dareId)
    }
}
