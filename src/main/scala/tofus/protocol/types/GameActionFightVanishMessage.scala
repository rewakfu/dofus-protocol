package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightVanishMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double
) extends Message(6217)

object GameActionFightVanishMessage {
  implicit val codec: Codec[GameActionFightVanishMessage] =
    new Codec[GameActionFightVanishMessage] {
      def decode: Get[GameActionFightVanishMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
        } yield GameActionFightVanishMessage(actionId, sourceId, targetId)

      def encode(value: GameActionFightVanishMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId)
    }
}
