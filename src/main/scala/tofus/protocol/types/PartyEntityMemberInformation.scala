package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyEntityMemberInformation(
  indexId: Byte,
  entityModelId: Byte,
  entityLook: EntityLook,
  initiative: Short,
  lifePoints: Int,
  maxLifePoints: Int,
  prospecting: Short,
  regenRate: Short
) extends PartyEntityBaseInformation {
  override val protocolId = 550
}

object PartyEntityMemberInformation {
  implicit val codec: Codec[PartyEntityMemberInformation] =
    new Codec[PartyEntityMemberInformation] {
      def decode: Get[PartyEntityMemberInformation] =
        for {
          indexId <- byte.decode
          entityModelId <- byte.decode
          entityLook <- Codec[EntityLook].decode
          initiative <- varShort.decode
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
          prospecting <- varShort.decode
          regenRate <- ubyte.decode
        } yield PartyEntityMemberInformation(indexId, entityModelId, entityLook, initiative, lifePoints, maxLifePoints, prospecting, regenRate)

      def encode(value: PartyEntityMemberInformation): ByteVector =
        byte.encode(value.indexId) ++
        byte.encode(value.entityModelId) ++
        Codec[EntityLook].encode(value.entityLook) ++
        varShort.encode(value.initiative) ++
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints) ++
        varShort.encode(value.prospecting) ++
        ubyte.encode(value.regenRate)
    }
}
