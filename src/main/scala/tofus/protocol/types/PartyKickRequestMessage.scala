package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyKickRequestMessage(
  partyId: Int,
  playerId: Long
) extends Message(5592)

object PartyKickRequestMessage {
  implicit val codec: Codec[PartyKickRequestMessage] =
    new Codec[PartyKickRequestMessage] {
      def decode: Get[PartyKickRequestMessage] =
        for {
          partyId <- varInt.decode
          playerId <- varLong.decode
        } yield PartyKickRequestMessage(partyId, playerId)

      def encode(value: PartyKickRequestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.playerId)
    }
}
