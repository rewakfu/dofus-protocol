package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildVersatileInfoListMessage(
  guilds: List[GuildVersatileInformations]
) extends Message(6435)

object GuildVersatileInfoListMessage {
  implicit val codec: Codec[GuildVersatileInfoListMessage] =
    new Codec[GuildVersatileInfoListMessage] {
      def decode: Get[GuildVersatileInfoListMessage] =
        for {
          guilds <- list(ushort, Codec[GuildVersatileInformations]).decode
        } yield GuildVersatileInfoListMessage(guilds)

      def encode(value: GuildVersatileInfoListMessage): ByteVector =
        list(ushort, Codec[GuildVersatileInformations]).encode(value.guilds)
    }
}
