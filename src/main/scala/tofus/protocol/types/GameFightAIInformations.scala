package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameFightAIInformations extends GameFightFighterInformations

final case class ConcreteGameFightAIInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short]
) extends GameFightAIInformations {
  override val protocolId = 151
}

object ConcreteGameFightAIInformations {
  implicit val codec: Codec[ConcreteGameFightAIInformations] =  
    new Codec[ConcreteGameFightAIInformations] {
      def decode: Get[ConcreteGameFightAIInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
        } yield ConcreteGameFightAIInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions)

      def encode(value: ConcreteGameFightAIInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions)
    }
}

object GameFightAIInformations {
  implicit val codec: Codec[GameFightAIInformations] =
    new Codec[GameFightAIInformations] {
      def decode: Get[GameFightAIInformations] =
        ushort.decode.flatMap {
          case 151 => Codec[ConcreteGameFightAIInformations].decode
          case 203 => Codec[GameFightMonsterWithAlignmentInformations].decode
          case 29 => Codec[ConcreteGameFightMonsterInformations].decode
          case 48 => Codec[GameFightTaxCollectorInformations].decode
        }

      def encode(value: GameFightAIInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameFightAIInformations => Codec[ConcreteGameFightAIInformations].encode(i)
          case i: GameFightMonsterWithAlignmentInformations => Codec[GameFightMonsterWithAlignmentInformations].encode(i)
          case i: ConcreteGameFightMonsterInformations => Codec[ConcreteGameFightMonsterInformations].encode(i)
          case i: GameFightTaxCollectorInformations => Codec[GameFightTaxCollectorInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
