package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyDeletedMessage(
  partyId: Int
) extends Message(6261)

object PartyDeletedMessage {
  implicit val codec: Codec[PartyDeletedMessage] =
    new Codec[PartyDeletedMessage] {
      def decode: Get[PartyDeletedMessage] =
        for {
          partyId <- varInt.decode
        } yield PartyDeletedMessage(partyId)

      def encode(value: PartyDeletedMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
