package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarContentMessage(
  barType: Byte,
  shortcuts: List[Shortcut]
) extends Message(6231)

object ShortcutBarContentMessage {
  implicit val codec: Codec[ShortcutBarContentMessage] =
    new Codec[ShortcutBarContentMessage] {
      def decode: Get[ShortcutBarContentMessage] =
        for {
          barType <- byte.decode
          shortcuts <- list(ushort, Codec[Shortcut]).decode
        } yield ShortcutBarContentMessage(barType, shortcuts)

      def encode(value: ShortcutBarContentMessage): ByteVector =
        byte.encode(value.barType) ++
        list(ushort, Codec[Shortcut]).encode(value.shortcuts)
    }
}
