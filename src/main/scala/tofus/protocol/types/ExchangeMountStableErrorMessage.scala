package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountStableErrorMessage(

) extends Message(5981)

object ExchangeMountStableErrorMessage {
  implicit val codec: Codec[ExchangeMountStableErrorMessage] =
    Codec.const(ExchangeMountStableErrorMessage())
}
