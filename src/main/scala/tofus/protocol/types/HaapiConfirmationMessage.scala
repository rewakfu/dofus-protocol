package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiConfirmationMessage(
  kamas: Long,
  amount: Long,
  rate: Short,
  action: Byte,
  transaction: String
) extends Message(6848)

object HaapiConfirmationMessage {
  implicit val codec: Codec[HaapiConfirmationMessage] =
    new Codec[HaapiConfirmationMessage] {
      def decode: Get[HaapiConfirmationMessage] =
        for {
          kamas <- varLong.decode
          amount <- varLong.decode
          rate <- varShort.decode
          action <- byte.decode
          transaction <- utf8(ushort).decode
        } yield HaapiConfirmationMessage(kamas, amount, rate, action, transaction)

      def encode(value: HaapiConfirmationMessage): ByteVector =
        varLong.encode(value.kamas) ++
        varLong.encode(value.amount) ++
        varShort.encode(value.rate) ++
        byte.encode(value.action) ++
        utf8(ushort).encode(value.transaction)
    }
}
