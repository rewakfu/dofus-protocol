package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightAllianceTeamInformations(
  teamId: Byte,
  leaderId: Double,
  teamSide: Byte,
  teamTypeId: Byte,
  nbWaves: Byte,
  teamMembers: List[FightTeamMemberInformations],
  relation: Byte
) extends FightTeamInformations {
  override val protocolId = 439
}

object FightAllianceTeamInformations {
  implicit val codec: Codec[FightAllianceTeamInformations] =
    new Codec[FightAllianceTeamInformations] {
      def decode: Get[FightAllianceTeamInformations] =
        for {
          teamId <- byte.decode
          leaderId <- double.decode
          teamSide <- byte.decode
          teamTypeId <- byte.decode
          nbWaves <- byte.decode
          teamMembers <- list(ushort, Codec[FightTeamMemberInformations]).decode
          relation <- byte.decode
        } yield FightAllianceTeamInformations(teamId, leaderId, teamSide, teamTypeId, nbWaves, teamMembers, relation)

      def encode(value: FightAllianceTeamInformations): ByteVector =
        byte.encode(value.teamId) ++
        double.encode(value.leaderId) ++
        byte.encode(value.teamSide) ++
        byte.encode(value.teamTypeId) ++
        byte.encode(value.nbWaves) ++
        list(ushort, Codec[FightTeamMemberInformations]).encode(value.teamMembers) ++
        byte.encode(value.relation)
    }
}
