package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseInListUpdatedMessage(
  itemUID: Int,
  objectGID: Short,
  objectType: Int,
  effects: List[ObjectEffect],
  prices: List[Long]
) extends Message(6337)

object ExchangeBidHouseInListUpdatedMessage {
  implicit val codec: Codec[ExchangeBidHouseInListUpdatedMessage] =
    new Codec[ExchangeBidHouseInListUpdatedMessage] {
      def decode: Get[ExchangeBidHouseInListUpdatedMessage] =
        for {
          itemUID <- int.decode
          objectGID <- varShort.decode
          objectType <- int.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          prices <- list(ushort, varLong).decode
        } yield ExchangeBidHouseInListUpdatedMessage(itemUID, objectGID, objectType, effects, prices)

      def encode(value: ExchangeBidHouseInListUpdatedMessage): ByteVector =
        int.encode(value.itemUID) ++
        varShort.encode(value.objectGID) ++
        int.encode(value.objectType) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        list(ushort, varLong).encode(value.prices)
    }
}
