package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarAddRequestMessage(
  barType: Byte,
  shortcut: Shortcut
) extends Message(6225)

object ShortcutBarAddRequestMessage {
  implicit val codec: Codec[ShortcutBarAddRequestMessage] =
    new Codec[ShortcutBarAddRequestMessage] {
      def decode: Get[ShortcutBarAddRequestMessage] =
        for {
          barType <- byte.decode
          shortcut <- Codec[Shortcut].decode
        } yield ShortcutBarAddRequestMessage(barType, shortcut)

      def encode(value: ShortcutBarAddRequestMessage): ByteVector =
        byte.encode(value.barType) ++
        Codec[Shortcut].encode(value.shortcut)
    }
}
