package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryDefineSettingsMessage(
  settings: JobCrafterDirectorySettings
) extends Message(5649)

object JobCrafterDirectoryDefineSettingsMessage {
  implicit val codec: Codec[JobCrafterDirectoryDefineSettingsMessage] =
    new Codec[JobCrafterDirectoryDefineSettingsMessage] {
      def decode: Get[JobCrafterDirectoryDefineSettingsMessage] =
        for {
          settings <- Codec[JobCrafterDirectorySettings].decode
        } yield JobCrafterDirectoryDefineSettingsMessage(settings)

      def encode(value: JobCrafterDirectoryDefineSettingsMessage): ByteVector =
        Codec[JobCrafterDirectorySettings].encode(value.settings)
    }
}
