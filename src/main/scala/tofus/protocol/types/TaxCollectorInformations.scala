package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait TaxCollectorInformations extends ProtocolType

final case class ConcreteTaxCollectorInformations(
  uniqueId: Double,
  firtNameId: Short,
  lastNameId: Short,
  additionalInfos: AdditionalTaxCollectorInformations,
  worldX: Short,
  worldY: Short,
  subAreaId: Short,
  state: Byte,
  look: EntityLook,
  complements: List[TaxCollectorComplementaryInformations]
) extends TaxCollectorInformations {
  override val protocolId = 167
}

object ConcreteTaxCollectorInformations {
  implicit val codec: Codec[ConcreteTaxCollectorInformations] =  
    new Codec[ConcreteTaxCollectorInformations] {
      def decode: Get[ConcreteTaxCollectorInformations] =
        for {
          uniqueId <- double.decode
          firtNameId <- varShort.decode
          lastNameId <- varShort.decode
          additionalInfos <- Codec[AdditionalTaxCollectorInformations].decode
          worldX <- short.decode
          worldY <- short.decode
          subAreaId <- varShort.decode
          state <- byte.decode
          look <- Codec[EntityLook].decode
          complements <- list(ushort, Codec[TaxCollectorComplementaryInformations]).decode
        } yield ConcreteTaxCollectorInformations(uniqueId, firtNameId, lastNameId, additionalInfos, worldX, worldY, subAreaId, state, look, complements)

      def encode(value: ConcreteTaxCollectorInformations): ByteVector =
        double.encode(value.uniqueId) ++
        varShort.encode(value.firtNameId) ++
        varShort.encode(value.lastNameId) ++
        Codec[AdditionalTaxCollectorInformations].encode(value.additionalInfos) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        varShort.encode(value.subAreaId) ++
        byte.encode(value.state) ++
        Codec[EntityLook].encode(value.look) ++
        list(ushort, Codec[TaxCollectorComplementaryInformations]).encode(value.complements)
    }
}

object TaxCollectorInformations {
  implicit val codec: Codec[TaxCollectorInformations] =
    new Codec[TaxCollectorInformations] {
      def decode: Get[TaxCollectorInformations] =
        ushort.decode.flatMap {
          case 167 => Codec[ConcreteTaxCollectorInformations].decode

        }

      def encode(value: TaxCollectorInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteTaxCollectorInformations => Codec[ConcreteTaxCollectorInformations].encode(i)

        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
