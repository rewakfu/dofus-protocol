package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectDate(
  actionId: Short,
  year: Short,
  month: Byte,
  day: Byte,
  hour: Byte,
  minute: Byte
) extends ObjectEffect {
  override val protocolId = 72
}

object ObjectEffectDate {
  implicit val codec: Codec[ObjectEffectDate] =
    new Codec[ObjectEffectDate] {
      def decode: Get[ObjectEffectDate] =
        for {
          actionId <- varShort.decode
          year <- varShort.decode
          month <- byte.decode
          day <- byte.decode
          hour <- byte.decode
          minute <- byte.decode
        } yield ObjectEffectDate(actionId, year, month, day, hour, minute)

      def encode(value: ObjectEffectDate): ByteVector =
        varShort.encode(value.actionId) ++
        varShort.encode(value.year) ++
        byte.encode(value.month) ++
        byte.encode(value.day) ++
        byte.encode(value.hour) ++
        byte.encode(value.minute)
    }
}
