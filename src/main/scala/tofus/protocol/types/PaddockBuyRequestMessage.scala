package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockBuyRequestMessage(
  proposedPrice: Long
) extends Message(5951)

object PaddockBuyRequestMessage {
  implicit val codec: Codec[PaddockBuyRequestMessage] =
    new Codec[PaddockBuyRequestMessage] {
      def decode: Get[PaddockBuyRequestMessage] =
        for {
          proposedPrice <- varLong.decode
        } yield PaddockBuyRequestMessage(proposedPrice)

      def encode(value: PaddockBuyRequestMessage): ByteVector =
        varLong.encode(value.proposedPrice)
    }
}
