package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightDropCharacterMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  cellId: Short
) extends Message(5826)

object GameActionFightDropCharacterMessage {
  implicit val codec: Codec[GameActionFightDropCharacterMessage] =
    new Codec[GameActionFightDropCharacterMessage] {
      def decode: Get[GameActionFightDropCharacterMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          cellId <- short.decode
        } yield GameActionFightDropCharacterMessage(actionId, sourceId, targetId, cellId)

      def encode(value: GameActionFightDropCharacterMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.cellId)
    }
}
