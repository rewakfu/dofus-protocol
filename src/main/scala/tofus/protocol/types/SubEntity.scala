package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SubEntity(
  bindingPointCategory: Byte,
  bindingPointIndex: Byte,
  subEntityLook: EntityLook
) extends ProtocolType {
  override val protocolId = 54
}

object SubEntity {
  implicit val codec: Codec[SubEntity] =
    new Codec[SubEntity] {
      def decode: Get[SubEntity] =
        for {
          bindingPointCategory <- byte.decode
          bindingPointIndex <- byte.decode
          subEntityLook <- Codec[EntityLook].decode
        } yield SubEntity(bindingPointCategory, bindingPointIndex, subEntityLook)

      def encode(value: SubEntity): ByteVector =
        byte.encode(value.bindingPointCategory) ++
        byte.encode(value.bindingPointIndex) ++
        Codec[EntityLook].encode(value.subEntityLook)
    }
}
