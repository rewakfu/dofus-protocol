package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayGroupMonsterWaveInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  flags0: Byte,
  staticInfos: GroupMonsterStaticInformations,
  lootShare: Byte,
  alignmentSide: Byte,
  nbWaves: Byte,
  alternatives: List[GroupMonsterStaticInformations]
) extends GameRolePlayGroupMonsterInformations {
  override val protocolId = 464
}

object GameRolePlayGroupMonsterWaveInformations {
  implicit val codec: Codec[GameRolePlayGroupMonsterWaveInformations] =
    new Codec[GameRolePlayGroupMonsterWaveInformations] {
      def decode: Get[GameRolePlayGroupMonsterWaveInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          flags0 <- byte.decode
          staticInfos <- Codec[GroupMonsterStaticInformations].decode
          lootShare <- byte.decode
          alignmentSide <- byte.decode
          nbWaves <- byte.decode
          alternatives <- list(ushort, Codec[GroupMonsterStaticInformations]).decode
        } yield GameRolePlayGroupMonsterWaveInformations(contextualId, disposition, look, flags0, staticInfos, lootShare, alignmentSide, nbWaves, alternatives)

      def encode(value: GameRolePlayGroupMonsterWaveInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        byte.encode(value.flags0) ++
        Codec[GroupMonsterStaticInformations].encode(value.staticInfos) ++
        byte.encode(value.lootShare) ++
        byte.encode(value.alignmentSide) ++
        byte.encode(value.nbWaves) ++
        list(ushort, Codec[GroupMonsterStaticInformations]).encode(value.alternatives)
    }
}
