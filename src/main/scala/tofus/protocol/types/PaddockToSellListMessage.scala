package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockToSellListMessage(
  pageIndex: Short,
  totalPage: Short,
  paddockList: List[PaddockInformationsForSell]
) extends Message(6138)

object PaddockToSellListMessage {
  implicit val codec: Codec[PaddockToSellListMessage] =
    new Codec[PaddockToSellListMessage] {
      def decode: Get[PaddockToSellListMessage] =
        for {
          pageIndex <- varShort.decode
          totalPage <- varShort.decode
          paddockList <- list(ushort, Codec[PaddockInformationsForSell]).decode
        } yield PaddockToSellListMessage(pageIndex, totalPage, paddockList)

      def encode(value: PaddockToSellListMessage): ByteVector =
        varShort.encode(value.pageIndex) ++
        varShort.encode(value.totalPage) ++
        list(ushort, Codec[PaddockInformationsForSell]).encode(value.paddockList)
    }
}
