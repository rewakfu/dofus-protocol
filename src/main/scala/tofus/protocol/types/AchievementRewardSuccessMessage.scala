package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementRewardSuccessMessage(
  achievementId: Short
) extends Message(6376)

object AchievementRewardSuccessMessage {
  implicit val codec: Codec[AchievementRewardSuccessMessage] =
    new Codec[AchievementRewardSuccessMessage] {
      def decode: Get[AchievementRewardSuccessMessage] =
        for {
          achievementId <- short.decode
        } yield AchievementRewardSuccessMessage(achievementId)

      def encode(value: AchievementRewardSuccessMessage): ByteVector =
        short.encode(value.achievementId)
    }
}
