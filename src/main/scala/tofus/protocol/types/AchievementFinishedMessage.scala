package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementFinishedMessage(
  achievement: AchievementAchievedRewardable
) extends Message(6208)

object AchievementFinishedMessage {
  implicit val codec: Codec[AchievementFinishedMessage] =
    new Codec[AchievementFinishedMessage] {
      def decode: Get[AchievementFinishedMessage] =
        for {
          achievement <- Codec[AchievementAchievedRewardable].decode
        } yield AchievementFinishedMessage(achievement)

      def encode(value: AchievementFinishedMessage): ByteVector =
        Codec[AchievementAchievedRewardable].encode(value.achievement)
    }
}
