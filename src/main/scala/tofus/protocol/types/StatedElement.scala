package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatedElement(
  elementId: Int,
  elementCellId: Short,
  elementState: Int,
  onCurrentMap: Boolean
) extends ProtocolType {
  override val protocolId = 108
}

object StatedElement {
  implicit val codec: Codec[StatedElement] =
    new Codec[StatedElement] {
      def decode: Get[StatedElement] =
        for {
          elementId <- int.decode
          elementCellId <- varShort.decode
          elementState <- varInt.decode
          onCurrentMap <- bool.decode
        } yield StatedElement(elementId, elementCellId, elementState, onCurrentMap)

      def encode(value: StatedElement): ByteVector =
        int.encode(value.elementId) ++
        varShort.encode(value.elementCellId) ++
        varInt.encode(value.elementState) ++
        bool.encode(value.onCurrentMap)
    }
}
