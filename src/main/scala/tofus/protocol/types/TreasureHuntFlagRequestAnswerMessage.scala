package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntFlagRequestAnswerMessage(
  questType: Byte,
  result: Byte,
  index: Byte
) extends Message(6507)

object TreasureHuntFlagRequestAnswerMessage {
  implicit val codec: Codec[TreasureHuntFlagRequestAnswerMessage] =
    new Codec[TreasureHuntFlagRequestAnswerMessage] {
      def decode: Get[TreasureHuntFlagRequestAnswerMessage] =
        for {
          questType <- byte.decode
          result <- byte.decode
          index <- byte.decode
        } yield TreasureHuntFlagRequestAnswerMessage(questType, result, index)

      def encode(value: TreasureHuntFlagRequestAnswerMessage): ByteVector =
        byte.encode(value.questType) ++
        byte.encode(value.result) ++
        byte.encode(value.index)
    }
}
