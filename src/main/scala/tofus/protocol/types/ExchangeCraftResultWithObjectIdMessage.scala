package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftResultWithObjectIdMessage(
  craftResult: Byte,
  objectGenericId: Short
) extends Message(6000)

object ExchangeCraftResultWithObjectIdMessage {
  implicit val codec: Codec[ExchangeCraftResultWithObjectIdMessage] =
    new Codec[ExchangeCraftResultWithObjectIdMessage] {
      def decode: Get[ExchangeCraftResultWithObjectIdMessage] =
        for {
          craftResult <- byte.decode
          objectGenericId <- varShort.decode
        } yield ExchangeCraftResultWithObjectIdMessage(craftResult, objectGenericId)

      def encode(value: ExchangeCraftResultWithObjectIdMessage): ByteVector =
        byte.encode(value.craftResult) ++
        varShort.encode(value.objectGenericId)
    }
}
