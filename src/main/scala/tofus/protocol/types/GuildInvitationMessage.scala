package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInvitationMessage(
  targetId: Long
) extends Message(5551)

object GuildInvitationMessage {
  implicit val codec: Codec[GuildInvitationMessage] =
    new Codec[GuildInvitationMessage] {
      def decode: Get[GuildInvitationMessage] =
        for {
          targetId <- varLong.decode
        } yield GuildInvitationMessage(targetId)

      def encode(value: GuildInvitationMessage): ByteVector =
        varLong.encode(value.targetId)
    }
}
