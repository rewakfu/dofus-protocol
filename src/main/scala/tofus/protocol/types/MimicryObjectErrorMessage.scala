package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MimicryObjectErrorMessage(
  reason: Byte,
  errorCode: Byte,
  preview: Boolean
) extends Message(6461)

object MimicryObjectErrorMessage {
  implicit val codec: Codec[MimicryObjectErrorMessage] =
    new Codec[MimicryObjectErrorMessage] {
      def decode: Get[MimicryObjectErrorMessage] =
        for {
          reason <- byte.decode
          errorCode <- byte.decode
          preview <- bool.decode
        } yield MimicryObjectErrorMessage(reason, errorCode, preview)

      def encode(value: MimicryObjectErrorMessage): ByteVector =
        byte.encode(value.reason) ++
        byte.encode(value.errorCode) ++
        bool.encode(value.preview)
    }
}
