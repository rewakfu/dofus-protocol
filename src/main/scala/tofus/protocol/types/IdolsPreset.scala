package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolsPreset(
  id: Short,
  iconId: Short,
  idolIds: List[Short]
) extends Preset {
  override val protocolId = 491
}

object IdolsPreset {
  implicit val codec: Codec[IdolsPreset] =
    new Codec[IdolsPreset] {
      def decode: Get[IdolsPreset] =
        for {
          id <- short.decode
          iconId <- short.decode
          idolIds <- list(ushort, varShort).decode
        } yield IdolsPreset(id, iconId, idolIds)

      def encode(value: IdolsPreset): ByteVector =
        short.encode(value.id) ++
        short.encode(value.iconId) ++
        list(ushort, varShort).encode(value.idolIds)
    }
}
