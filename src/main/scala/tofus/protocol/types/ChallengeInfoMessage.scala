package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChallengeInfoMessage(
  challengeId: Short,
  targetId: Double,
  xpBonus: Int,
  dropBonus: Int
) extends Message(6022)

object ChallengeInfoMessage {
  implicit val codec: Codec[ChallengeInfoMessage] =
    new Codec[ChallengeInfoMessage] {
      def decode: Get[ChallengeInfoMessage] =
        for {
          challengeId <- varShort.decode
          targetId <- double.decode
          xpBonus <- varInt.decode
          dropBonus <- varInt.decode
        } yield ChallengeInfoMessage(challengeId, targetId, xpBonus, dropBonus)

      def encode(value: ChallengeInfoMessage): ByteVector =
        varShort.encode(value.challengeId) ++
        double.encode(value.targetId) ++
        varInt.encode(value.xpBonus) ++
        varInt.encode(value.dropBonus)
    }
}
