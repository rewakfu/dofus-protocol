package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInAllianceVersatileInformations(
  guildId: Int,
  leaderId: Long,
  guildLevel: Short,
  nbMembers: Short,
  allianceId: Int
) extends GuildVersatileInformations {
  override val protocolId = 437
}

object GuildInAllianceVersatileInformations {
  implicit val codec: Codec[GuildInAllianceVersatileInformations] =
    new Codec[GuildInAllianceVersatileInformations] {
      def decode: Get[GuildInAllianceVersatileInformations] =
        for {
          guildId <- varInt.decode
          leaderId <- varLong.decode
          guildLevel <- ubyte.decode
          nbMembers <- ubyte.decode
          allianceId <- varInt.decode
        } yield GuildInAllianceVersatileInformations(guildId, leaderId, guildLevel, nbMembers, allianceId)

      def encode(value: GuildInAllianceVersatileInformations): ByteVector =
        varInt.encode(value.guildId) ++
        varLong.encode(value.leaderId) ++
        ubyte.encode(value.guildLevel) ++
        ubyte.encode(value.nbMembers) ++
        varInt.encode(value.allianceId)
    }
}
