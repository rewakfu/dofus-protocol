package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SequenceNumberMessage(
  number: Int
) extends Message(6317)

object SequenceNumberMessage {
  implicit val codec: Codec[SequenceNumberMessage] =
    new Codec[SequenceNumberMessage] {
      def decode: Get[SequenceNumberMessage] =
        for {
          number <- ushort.decode
        } yield SequenceNumberMessage(number)

      def encode(value: SequenceNumberMessage): ByteVector =
        ushort.encode(value.number)
    }
}
