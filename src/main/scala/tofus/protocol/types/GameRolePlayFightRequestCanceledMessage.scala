package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayFightRequestCanceledMessage(
  fightId: Short,
  sourceId: Double,
  targetId: Double
) extends Message(5822)

object GameRolePlayFightRequestCanceledMessage {
  implicit val codec: Codec[GameRolePlayFightRequestCanceledMessage] =
    new Codec[GameRolePlayFightRequestCanceledMessage] {
      def decode: Get[GameRolePlayFightRequestCanceledMessage] =
        for {
          fightId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
        } yield GameRolePlayFightRequestCanceledMessage(fightId, sourceId, targetId)

      def encode(value: GameRolePlayFightRequestCanceledMessage): ByteVector =
        varShort.encode(value.fightId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId)
    }
}
