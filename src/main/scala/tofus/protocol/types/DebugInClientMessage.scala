package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DebugInClientMessage(
  level: Byte,
  message: String
) extends Message(6028)

object DebugInClientMessage {
  implicit val codec: Codec[DebugInClientMessage] =
    new Codec[DebugInClientMessage] {
      def decode: Get[DebugInClientMessage] =
        for {
          level <- byte.decode
          message <- utf8(ushort).decode
        } yield DebugInClientMessage(level, message)

      def encode(value: DebugInClientMessage): ByteVector =
        byte.encode(value.level) ++
        utf8(ushort).encode(value.message)
    }
}
