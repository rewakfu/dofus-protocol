package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapMovementConfirmMessage(

) extends Message(952)

object GameMapMovementConfirmMessage {
  implicit val codec: Codec[GameMapMovementConfirmMessage] =
    Codec.const(GameMapMovementConfirmMessage())
}
