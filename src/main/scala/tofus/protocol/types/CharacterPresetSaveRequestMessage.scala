package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterPresetSaveRequestMessage(
  presetId: Short,
  symbolId: Byte,
  updateData: Boolean,
  name: String
) extends Message(6756)

object CharacterPresetSaveRequestMessage {
  implicit val codec: Codec[CharacterPresetSaveRequestMessage] =
    new Codec[CharacterPresetSaveRequestMessage] {
      def decode: Get[CharacterPresetSaveRequestMessage] =
        for {
          presetId <- short.decode
          symbolId <- byte.decode
          updateData <- bool.decode
          name <- utf8(ushort).decode
        } yield CharacterPresetSaveRequestMessage(presetId, symbolId, updateData, name)

      def encode(value: CharacterPresetSaveRequestMessage): ByteVector =
        short.encode(value.presetId) ++
        byte.encode(value.symbolId) ++
        bool.encode(value.updateData) ++
        utf8(ushort).encode(value.name)
    }
}
