package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameRolePlayNamedActorInformations extends GameRolePlayActorInformations

final case class ConcreteGameRolePlayNamedActorInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  name: String
) extends GameRolePlayNamedActorInformations {
  override val protocolId = 154
}

object ConcreteGameRolePlayNamedActorInformations {
  implicit val codec: Codec[ConcreteGameRolePlayNamedActorInformations] =  
    new Codec[ConcreteGameRolePlayNamedActorInformations] {
      def decode: Get[ConcreteGameRolePlayNamedActorInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          name <- utf8(ushort).decode
        } yield ConcreteGameRolePlayNamedActorInformations(contextualId, disposition, look, name)

      def encode(value: ConcreteGameRolePlayNamedActorInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        utf8(ushort).encode(value.name)
    }
}

object GameRolePlayNamedActorInformations {
  implicit val codec: Codec[GameRolePlayNamedActorInformations] =
    new Codec[GameRolePlayNamedActorInformations] {
      def decode: Get[GameRolePlayNamedActorInformations] =
        ushort.decode.flatMap {
          case 154 => Codec[ConcreteGameRolePlayNamedActorInformations].decode
          case 3 => Codec[GameRolePlayMutantInformations].decode
          case 36 => Codec[GameRolePlayCharacterInformations].decode
          case 159 => Codec[ConcreteGameRolePlayHumanoidInformations].decode
          case 180 => Codec[GameRolePlayMountInformations].decode
          case 129 => Codec[GameRolePlayMerchantInformations].decode
        }

      def encode(value: GameRolePlayNamedActorInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameRolePlayNamedActorInformations => Codec[ConcreteGameRolePlayNamedActorInformations].encode(i)
          case i: GameRolePlayMutantInformations => Codec[GameRolePlayMutantInformations].encode(i)
          case i: GameRolePlayCharacterInformations => Codec[GameRolePlayCharacterInformations].encode(i)
          case i: ConcreteGameRolePlayHumanoidInformations => Codec[ConcreteGameRolePlayHumanoidInformations].encode(i)
          case i: GameRolePlayMountInformations => Codec[GameRolePlayMountInformations].encode(i)
          case i: GameRolePlayMerchantInformations => Codec[GameRolePlayMerchantInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
