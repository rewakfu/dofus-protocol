package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendSpouseFollowWithCompassRequestMessage(
  enable: Boolean
) extends Message(5606)

object FriendSpouseFollowWithCompassRequestMessage {
  implicit val codec: Codec[FriendSpouseFollowWithCompassRequestMessage] =
    new Codec[FriendSpouseFollowWithCompassRequestMessage] {
      def decode: Get[FriendSpouseFollowWithCompassRequestMessage] =
        for {
          enable <- bool.decode
        } yield FriendSpouseFollowWithCompassRequestMessage(enable)

      def encode(value: FriendSpouseFollowWithCompassRequestMessage): ByteVector =
        bool.encode(value.enable)
    }
}
