package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionSkillUse(
  elementId: Int,
  skillId: Short,
  skillEndTime: Double
) extends HumanOption {
  override val protocolId = 495
}

object HumanOptionSkillUse {
  implicit val codec: Codec[HumanOptionSkillUse] =
    new Codec[HumanOptionSkillUse] {
      def decode: Get[HumanOptionSkillUse] =
        for {
          elementId <- varInt.decode
          skillId <- varShort.decode
          skillEndTime <- double.decode
        } yield HumanOptionSkillUse(elementId, skillId, skillEndTime)

      def encode(value: HumanOptionSkillUse): ByteVector =
        varInt.encode(value.elementId) ++
        varShort.encode(value.skillId) ++
        double.encode(value.skillEndTime)
    }
}
