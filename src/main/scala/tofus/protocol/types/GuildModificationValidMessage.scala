package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildModificationValidMessage(
  guildName: String,
  guildEmblem: GuildEmblem
) extends Message(6323)

object GuildModificationValidMessage {
  implicit val codec: Codec[GuildModificationValidMessage] =
    new Codec[GuildModificationValidMessage] {
      def decode: Get[GuildModificationValidMessage] =
        for {
          guildName <- utf8(ushort).decode
          guildEmblem <- Codec[GuildEmblem].decode
        } yield GuildModificationValidMessage(guildName, guildEmblem)

      def encode(value: GuildModificationValidMessage): ByteVector =
        utf8(ushort).encode(value.guildName) ++
        Codec[GuildEmblem].encode(value.guildEmblem)
    }
}
