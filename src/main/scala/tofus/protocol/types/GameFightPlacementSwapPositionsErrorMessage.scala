package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementSwapPositionsErrorMessage(

) extends Message(6548)

object GameFightPlacementSwapPositionsErrorMessage {
  implicit val codec: Codec[GameFightPlacementSwapPositionsErrorMessage] =
    Codec.const(GameFightPlacementSwapPositionsErrorMessage())
}
