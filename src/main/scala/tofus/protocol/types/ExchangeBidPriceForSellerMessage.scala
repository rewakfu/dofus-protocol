package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidPriceForSellerMessage(
  genericId: Short,
  averagePrice: Long,
  allIdentical: Boolean,
  minimalPrices: List[Long]
) extends Message(6464)

object ExchangeBidPriceForSellerMessage {
  implicit val codec: Codec[ExchangeBidPriceForSellerMessage] =
    new Codec[ExchangeBidPriceForSellerMessage] {
      def decode: Get[ExchangeBidPriceForSellerMessage] =
        for {
          genericId <- varShort.decode
          averagePrice <- varLong.decode
          allIdentical <- bool.decode
          minimalPrices <- list(ushort, varLong).decode
        } yield ExchangeBidPriceForSellerMessage(genericId, averagePrice, allIdentical, minimalPrices)

      def encode(value: ExchangeBidPriceForSellerMessage): ByteVector =
        varShort.encode(value.genericId) ++
        varLong.encode(value.averagePrice) ++
        bool.encode(value.allIdentical) ++
        list(ushort, varLong).encode(value.minimalPrices)
    }
}
