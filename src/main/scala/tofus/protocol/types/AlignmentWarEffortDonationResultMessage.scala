package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlignmentWarEffortDonationResultMessage(
  result: Byte
) extends Message(6856)

object AlignmentWarEffortDonationResultMessage {
  implicit val codec: Codec[AlignmentWarEffortDonationResultMessage] =
    new Codec[AlignmentWarEffortDonationResultMessage] {
      def decode: Get[AlignmentWarEffortDonationResultMessage] =
        for {
          result <- byte.decode
        } yield AlignmentWarEffortDonationResultMessage(result)

      def encode(value: AlignmentWarEffortDonationResultMessage): ByteVector =
        byte.encode(value.result)
    }
}
