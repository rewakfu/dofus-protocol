package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlreadyConnectedMessage(

) extends Message(109)

object AlreadyConnectedMessage {
  implicit val codec: Codec[AlreadyConnectedMessage] =
    Codec.const(AlreadyConnectedMessage())
}
