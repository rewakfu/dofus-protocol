package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait CharacterMinimalInformations extends CharacterBasicMinimalInformations

final case class ConcreteCharacterMinimalInformations(
  id: Long,
  name: String,
  level: Short
) extends CharacterMinimalInformations {
  override val protocolId = 110
}

object ConcreteCharacterMinimalInformations {
  implicit val codec: Codec[ConcreteCharacterMinimalInformations] =  
    new Codec[ConcreteCharacterMinimalInformations] {
      def decode: Get[ConcreteCharacterMinimalInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
        } yield ConcreteCharacterMinimalInformations(id, name, level)

      def encode(value: ConcreteCharacterMinimalInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level)
    }
}

object CharacterMinimalInformations {
  implicit val codec: Codec[CharacterMinimalInformations] =
    new Codec[CharacterMinimalInformations] {
      def decode: Get[CharacterMinimalInformations] =
        ushort.decode.flatMap {
          case 110 => Codec[ConcreteCharacterMinimalInformations].decode
          case 391 => Codec[PartyMemberArenaInformations].decode
          case 90 => Codec[ConcretePartyMemberInformations].decode
          case 376 => Codec[PartyInvitationMemberInformations].decode
          case 474 => Codec[CharacterHardcoreOrEpicInformations].decode
          case 45 => Codec[ConcreteCharacterBaseInformations].decode
          case 193 => Codec[CharacterMinimalPlusLookAndGradeInformations].decode
          case 444 => Codec[CharacterMinimalAllianceInformations].decode
          case 445 => Codec[ConcreteCharacterMinimalGuildInformations].decode
          case 163 => Codec[ConcreteCharacterMinimalPlusLookInformations].decode
          case 88 => Codec[GuildMember].decode
          case 556 => Codec[CharacterMinimalGuildPublicInformations].decode
        }

      def encode(value: CharacterMinimalInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteCharacterMinimalInformations => Codec[ConcreteCharacterMinimalInformations].encode(i)
          case i: PartyMemberArenaInformations => Codec[PartyMemberArenaInformations].encode(i)
          case i: ConcretePartyMemberInformations => Codec[ConcretePartyMemberInformations].encode(i)
          case i: PartyInvitationMemberInformations => Codec[PartyInvitationMemberInformations].encode(i)
          case i: CharacterHardcoreOrEpicInformations => Codec[CharacterHardcoreOrEpicInformations].encode(i)
          case i: ConcreteCharacterBaseInformations => Codec[ConcreteCharacterBaseInformations].encode(i)
          case i: CharacterMinimalPlusLookAndGradeInformations => Codec[CharacterMinimalPlusLookAndGradeInformations].encode(i)
          case i: CharacterMinimalAllianceInformations => Codec[CharacterMinimalAllianceInformations].encode(i)
          case i: ConcreteCharacterMinimalGuildInformations => Codec[ConcreteCharacterMinimalGuildInformations].encode(i)
          case i: ConcreteCharacterMinimalPlusLookInformations => Codec[ConcreteCharacterMinimalPlusLookInformations].encode(i)
          case i: GuildMember => Codec[GuildMember].encode(i)
          case i: CharacterMinimalGuildPublicInformations => Codec[CharacterMinimalGuildPublicInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
