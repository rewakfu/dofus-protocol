package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnEndMessage(
  id: Double
) extends Message(719)

object GameFightTurnEndMessage {
  implicit val codec: Codec[GameFightTurnEndMessage] =
    new Codec[GameFightTurnEndMessage] {
      def decode: Get[GameFightTurnEndMessage] =
        for {
          id <- double.decode
        } yield GameFightTurnEndMessage(id)

      def encode(value: GameFightTurnEndMessage): ByteVector =
        double.encode(value.id)
    }
}
