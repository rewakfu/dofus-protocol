package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorMovement(
  movementType: Byte,
  basicInfos: TaxCollectorBasicInformations,
  playerId: Long,
  playerName: String
) extends ProtocolType {
  override val protocolId = 493
}

object TaxCollectorMovement {
  implicit val codec: Codec[TaxCollectorMovement] =
    new Codec[TaxCollectorMovement] {
      def decode: Get[TaxCollectorMovement] =
        for {
          movementType <- byte.decode
          basicInfos <- Codec[TaxCollectorBasicInformations].decode
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
        } yield TaxCollectorMovement(movementType, basicInfos, playerId, playerName)

      def encode(value: TaxCollectorMovement): ByteVector =
        byte.encode(value.movementType) ++
        Codec[TaxCollectorBasicInformations].encode(value.basicInfos) ++
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName)
    }
}
