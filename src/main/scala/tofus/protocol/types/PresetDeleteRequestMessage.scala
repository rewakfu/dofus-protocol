package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetDeleteRequestMessage(
  presetId: Short
) extends Message(6755)

object PresetDeleteRequestMessage {
  implicit val codec: Codec[PresetDeleteRequestMessage] =
    new Codec[PresetDeleteRequestMessage] {
      def decode: Get[PresetDeleteRequestMessage] =
        for {
          presetId <- short.decode
        } yield PresetDeleteRequestMessage(presetId)

      def encode(value: PresetDeleteRequestMessage): ByteVector =
        short.encode(value.presetId)
    }
}
