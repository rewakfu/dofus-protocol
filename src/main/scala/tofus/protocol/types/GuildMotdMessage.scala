package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMotdMessage(
  content: String,
  timestamp: Int,
  memberId: Long,
  memberName: String
) extends Message(6590)

object GuildMotdMessage {
  implicit val codec: Codec[GuildMotdMessage] =
    new Codec[GuildMotdMessage] {
      def decode: Get[GuildMotdMessage] =
        for {
          content <- utf8(ushort).decode
          timestamp <- int.decode
          memberId <- varLong.decode
          memberName <- utf8(ushort).decode
        } yield GuildMotdMessage(content, timestamp, memberId, memberName)

      def encode(value: GuildMotdMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        varLong.encode(value.memberId) ++
        utf8(ushort).encode(value.memberName)
    }
}
