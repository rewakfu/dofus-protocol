package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlternativeMonstersInGroupLightInformations(
  playerCount: Int,
  monsters: List[ConcreteMonsterInGroupLightInformations]
) extends ProtocolType {
  override val protocolId = 394
}

object AlternativeMonstersInGroupLightInformations {
  implicit val codec: Codec[AlternativeMonstersInGroupLightInformations] =
    new Codec[AlternativeMonstersInGroupLightInformations] {
      def decode: Get[AlternativeMonstersInGroupLightInformations] =
        for {
          playerCount <- int.decode
          monsters <- list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).decode
        } yield AlternativeMonstersInGroupLightInformations(playerCount, monsters)

      def encode(value: AlternativeMonstersInGroupLightInformations): ByteVector =
        int.encode(value.playerCount) ++
        list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).encode(value.monsters)
    }
}
