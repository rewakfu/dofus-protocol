package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SellerBuyerDescriptor(
  quantities: List[Int],
  types: List[Int],
  taxPercentage: Float,
  taxModificationPercentage: Float,
  maxItemLevel: Short,
  maxItemPerAccount: Int,
  npcContextualId: Int,
  unsoldDelay: Short
) extends ProtocolType {
  override val protocolId = 121
}

object SellerBuyerDescriptor {
  implicit val codec: Codec[SellerBuyerDescriptor] =
    new Codec[SellerBuyerDescriptor] {
      def decode: Get[SellerBuyerDescriptor] =
        for {
          quantities <- list(ushort, varInt).decode
          types <- list(ushort, varInt).decode
          taxPercentage <- float.decode
          taxModificationPercentage <- float.decode
          maxItemLevel <- ubyte.decode
          maxItemPerAccount <- varInt.decode
          npcContextualId <- int.decode
          unsoldDelay <- varShort.decode
        } yield SellerBuyerDescriptor(quantities, types, taxPercentage, taxModificationPercentage, maxItemLevel, maxItemPerAccount, npcContextualId, unsoldDelay)

      def encode(value: SellerBuyerDescriptor): ByteVector =
        list(ushort, varInt).encode(value.quantities) ++
        list(ushort, varInt).encode(value.types) ++
        float.encode(value.taxPercentage) ++
        float.encode(value.taxModificationPercentage) ++
        ubyte.encode(value.maxItemLevel) ++
        varInt.encode(value.maxItemPerAccount) ++
        int.encode(value.npcContextualId) ++
        varShort.encode(value.unsoldDelay)
    }
}
