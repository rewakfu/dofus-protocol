package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextCreateErrorMessage(

) extends Message(6024)

object GameContextCreateErrorMessage {
  implicit val codec: Codec[GameContextCreateErrorMessage] =
    Codec.const(GameContextCreateErrorMessage())
}
