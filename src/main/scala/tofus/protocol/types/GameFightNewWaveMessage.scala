package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightNewWaveMessage(
  id: Byte,
  teamId: Byte,
  nbTurnBeforeNextWave: Short
) extends Message(6490)

object GameFightNewWaveMessage {
  implicit val codec: Codec[GameFightNewWaveMessage] =
    new Codec[GameFightNewWaveMessage] {
      def decode: Get[GameFightNewWaveMessage] =
        for {
          id <- byte.decode
          teamId <- byte.decode
          nbTurnBeforeNextWave <- short.decode
        } yield GameFightNewWaveMessage(id, teamId, nbTurnBeforeNextWave)

      def encode(value: GameFightNewWaveMessage): ByteVector =
        byte.encode(value.id) ++
        byte.encode(value.teamId) ++
        short.encode(value.nbTurnBeforeNextWave)
    }
}
