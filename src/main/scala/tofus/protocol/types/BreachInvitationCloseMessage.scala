package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachInvitationCloseMessage(
  host: ConcreteCharacterMinimalInformations
) extends Message(6790)

object BreachInvitationCloseMessage {
  implicit val codec: Codec[BreachInvitationCloseMessage] =
    new Codec[BreachInvitationCloseMessage] {
      def decode: Get[BreachInvitationCloseMessage] =
        for {
          host <- Codec[ConcreteCharacterMinimalInformations].decode
        } yield BreachInvitationCloseMessage(host)

      def encode(value: BreachInvitationCloseMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.host)
    }
}
