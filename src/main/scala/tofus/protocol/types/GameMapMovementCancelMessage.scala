package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapMovementCancelMessage(
  cellId: Short
) extends Message(953)

object GameMapMovementCancelMessage {
  implicit val codec: Codec[GameMapMovementCancelMessage] =
    new Codec[GameMapMovementCancelMessage] {
      def decode: Get[GameMapMovementCancelMessage] =
        for {
          cellId <- varShort.decode
        } yield GameMapMovementCancelMessage(cellId)

      def encode(value: GameMapMovementCancelMessage): ByteVector =
        varShort.encode(value.cellId)
    }
}
