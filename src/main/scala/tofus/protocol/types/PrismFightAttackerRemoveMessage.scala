package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightAttackerRemoveMessage(
  subAreaId: Short,
  fightId: Short,
  fighterToRemoveId: Long
) extends Message(5897)

object PrismFightAttackerRemoveMessage {
  implicit val codec: Codec[PrismFightAttackerRemoveMessage] =
    new Codec[PrismFightAttackerRemoveMessage] {
      def decode: Get[PrismFightAttackerRemoveMessage] =
        for {
          subAreaId <- varShort.decode
          fightId <- varShort.decode
          fighterToRemoveId <- varLong.decode
        } yield PrismFightAttackerRemoveMessage(subAreaId, fightId, fighterToRemoveId)

      def encode(value: PrismFightAttackerRemoveMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        varShort.encode(value.fightId) ++
        varLong.encode(value.fighterToRemoveId)
    }
}
