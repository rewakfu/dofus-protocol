package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangePlayerRequestMessage(
  exchangeType: Byte,
  target: Long
) extends Message(5773)

object ExchangePlayerRequestMessage {
  implicit val codec: Codec[ExchangePlayerRequestMessage] =
    new Codec[ExchangePlayerRequestMessage] {
      def decode: Get[ExchangePlayerRequestMessage] =
        for {
          exchangeType <- byte.decode
          target <- varLong.decode
        } yield ExchangePlayerRequestMessage(exchangeType, target)

      def encode(value: ExchangePlayerRequestMessage): ByteVector =
        byte.encode(value.exchangeType) ++
        varLong.encode(value.target)
    }
}
