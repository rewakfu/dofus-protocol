package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportHavenBagAnswerMessage(
  accept: Boolean
) extends Message(6646)

object TeleportHavenBagAnswerMessage {
  implicit val codec: Codec[TeleportHavenBagAnswerMessage] =
    new Codec[TeleportHavenBagAnswerMessage] {
      def decode: Get[TeleportHavenBagAnswerMessage] =
        for {
          accept <- bool.decode
        } yield TeleportHavenBagAnswerMessage(accept)

      def encode(value: TeleportHavenBagAnswerMessage): ByteVector =
        bool.encode(value.accept)
    }
}
