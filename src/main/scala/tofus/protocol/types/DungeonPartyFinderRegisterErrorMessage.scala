package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderRegisterErrorMessage(

) extends Message(6243)

object DungeonPartyFinderRegisterErrorMessage {
  implicit val codec: Codec[DungeonPartyFinderRegisterErrorMessage] =
    Codec.const(DungeonPartyFinderRegisterErrorMessage())
}
