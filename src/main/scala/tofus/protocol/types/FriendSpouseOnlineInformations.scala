package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendSpouseOnlineInformations(
  spouseAccountId: Int,
  spouseId: Long,
  spouseName: String,
  spouseLevel: Short,
  breed: Byte,
  sex: Byte,
  spouseEntityLook: EntityLook,
  guildInfo: ConcreteGuildInformations,
  alignmentSide: Byte,
  flags0: Byte,
  mapId: Double,
  subAreaId: Short
) extends FriendSpouseInformations {
  override val protocolId = 93
}

object FriendSpouseOnlineInformations {
  implicit val codec: Codec[FriendSpouseOnlineInformations] =
    new Codec[FriendSpouseOnlineInformations] {
      def decode: Get[FriendSpouseOnlineInformations] =
        for {
          spouseAccountId <- int.decode
          spouseId <- varLong.decode
          spouseName <- utf8(ushort).decode
          spouseLevel <- varShort.decode
          breed <- byte.decode
          sex <- byte.decode
          spouseEntityLook <- Codec[EntityLook].decode
          guildInfo <- Codec[ConcreteGuildInformations].decode
          alignmentSide <- byte.decode
          flags0 <- byte.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
        } yield FriendSpouseOnlineInformations(spouseAccountId, spouseId, spouseName, spouseLevel, breed, sex, spouseEntityLook, guildInfo, alignmentSide, flags0, mapId, subAreaId)

      def encode(value: FriendSpouseOnlineInformations): ByteVector =
        int.encode(value.spouseAccountId) ++
        varLong.encode(value.spouseId) ++
        utf8(ushort).encode(value.spouseName) ++
        varShort.encode(value.spouseLevel) ++
        byte.encode(value.breed) ++
        byte.encode(value.sex) ++
        Codec[EntityLook].encode(value.spouseEntityLook) ++
        Codec[ConcreteGuildInformations].encode(value.guildInfo) ++
        byte.encode(value.alignmentSide) ++
        byte.encode(value.flags0) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId)
    }
}
