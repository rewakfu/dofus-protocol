package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryEntryRequestMessage(
  playerId: Long
) extends Message(6043)

object JobCrafterDirectoryEntryRequestMessage {
  implicit val codec: Codec[JobCrafterDirectoryEntryRequestMessage] =
    new Codec[JobCrafterDirectoryEntryRequestMessage] {
      def decode: Get[JobCrafterDirectoryEntryRequestMessage] =
        for {
          playerId <- varLong.decode
        } yield JobCrafterDirectoryEntryRequestMessage(playerId)

      def encode(value: JobCrafterDirectoryEntryRequestMessage): ByteVector =
        varLong.encode(value.playerId)
    }
}
