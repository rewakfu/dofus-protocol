package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectModifiedMessage(
  `object`: ObjectItem
) extends Message(3029)

object ObjectModifiedMessage {
  implicit val codec: Codec[ObjectModifiedMessage] =
    new Codec[ObjectModifiedMessage] {
      def decode: Get[ObjectModifiedMessage] =
        for {
          `object` <- Codec[ObjectItem].decode
        } yield ObjectModifiedMessage(`object`)

      def encode(value: ObjectModifiedMessage): ByteVector =
        Codec[ObjectItem].encode(value.`object`)
    }
}
