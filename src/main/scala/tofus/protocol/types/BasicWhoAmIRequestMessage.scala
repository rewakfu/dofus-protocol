package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicWhoAmIRequestMessage(
  verbose: Boolean
) extends Message(5664)

object BasicWhoAmIRequestMessage {
  implicit val codec: Codec[BasicWhoAmIRequestMessage] =
    new Codec[BasicWhoAmIRequestMessage] {
      def decode: Get[BasicWhoAmIRequestMessage] =
        for {
          verbose <- bool.decode
        } yield BasicWhoAmIRequestMessage(verbose)

      def encode(value: BasicWhoAmIRequestMessage): ByteVector =
        bool.encode(value.verbose)
    }
}
