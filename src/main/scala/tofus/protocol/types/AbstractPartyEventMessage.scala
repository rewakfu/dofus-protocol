package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AbstractPartyEventMessage(
  partyId: Int
) extends Message(6273)

object AbstractPartyEventMessage {
  implicit val codec: Codec[AbstractPartyEventMessage] =
    new Codec[AbstractPartyEventMessage] {
      def decode: Get[AbstractPartyEventMessage] =
        for {
          partyId <- varInt.decode
        } yield AbstractPartyEventMessage(partyId)

      def encode(value: AbstractPartyEventMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
