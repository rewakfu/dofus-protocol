package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildBulletinSetErrorMessage(
  reason: Byte
) extends Message(6691)

object GuildBulletinSetErrorMessage {
  implicit val codec: Codec[GuildBulletinSetErrorMessage] =
    new Codec[GuildBulletinSetErrorMessage] {
      def decode: Get[GuildBulletinSetErrorMessage] =
        for {
          reason <- byte.decode
        } yield GuildBulletinSetErrorMessage(reason)

      def encode(value: GuildBulletinSetErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
