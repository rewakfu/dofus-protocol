package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareListMessage(
  dares: List[DareInformations]
) extends Message(6661)

object DareListMessage {
  implicit val codec: Codec[DareListMessage] =
    new Codec[DareListMessage] {
      def decode: Get[DareListMessage] =
        for {
          dares <- list(ushort, Codec[DareInformations]).decode
        } yield DareListMessage(dares)

      def encode(value: DareListMessage): ByteVector =
        list(ushort, Codec[DareInformations]).encode(value.dares)
    }
}
