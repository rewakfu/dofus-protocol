package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMotdSetRequestMessage(
  content: String
) extends Message(6588)

object GuildMotdSetRequestMessage {
  implicit val codec: Codec[GuildMotdSetRequestMessage] =
    new Codec[GuildMotdSetRequestMessage] {
      def decode: Get[GuildMotdSetRequestMessage] =
        for {
          content <- utf8(ushort).decode
        } yield GuildMotdSetRequestMessage(content)

      def encode(value: GuildMotdSetRequestMessage): ByteVector =
        utf8(ushort).encode(value.content)
    }
}
