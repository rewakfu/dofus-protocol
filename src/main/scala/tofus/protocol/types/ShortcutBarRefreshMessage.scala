package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarRefreshMessage(
  barType: Byte,
  shortcut: Shortcut
) extends Message(6229)

object ShortcutBarRefreshMessage {
  implicit val codec: Codec[ShortcutBarRefreshMessage] =
    new Codec[ShortcutBarRefreshMessage] {
      def decode: Get[ShortcutBarRefreshMessage] =
        for {
          barType <- byte.decode
          shortcut <- Codec[Shortcut].decode
        } yield ShortcutBarRefreshMessage(barType, shortcut)

      def encode(value: ShortcutBarRefreshMessage): ByteVector =
        byte.encode(value.barType) ++
        Codec[Shortcut].encode(value.shortcut)
    }
}
