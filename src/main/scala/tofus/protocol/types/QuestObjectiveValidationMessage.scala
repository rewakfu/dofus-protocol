package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestObjectiveValidationMessage(
  questId: Short,
  objectiveId: Short
) extends Message(6085)

object QuestObjectiveValidationMessage {
  implicit val codec: Codec[QuestObjectiveValidationMessage] =
    new Codec[QuestObjectiveValidationMessage] {
      def decode: Get[QuestObjectiveValidationMessage] =
        for {
          questId <- varShort.decode
          objectiveId <- varShort.decode
        } yield QuestObjectiveValidationMessage(questId, objectiveId)

      def encode(value: QuestObjectiveValidationMessage): ByteVector =
        varShort.encode(value.questId) ++
        varShort.encode(value.objectiveId)
    }
}
