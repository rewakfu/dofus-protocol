package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntStepFollowDirectionToHint(
  direction: Byte,
  npcId: Short
) extends TreasureHuntStep {
  override val protocolId = 472
}

object TreasureHuntStepFollowDirectionToHint {
  implicit val codec: Codec[TreasureHuntStepFollowDirectionToHint] =
    new Codec[TreasureHuntStepFollowDirectionToHint] {
      def decode: Get[TreasureHuntStepFollowDirectionToHint] =
        for {
          direction <- byte.decode
          npcId <- varShort.decode
        } yield TreasureHuntStepFollowDirectionToHint(direction, npcId)

      def encode(value: TreasureHuntStepFollowDirectionToHint): ByteVector =
        byte.encode(value.direction) ++
        varShort.encode(value.npcId)
    }
}
