package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AcquaintanceSearchMessage(
  nickname: String
) extends Message(6144)

object AcquaintanceSearchMessage {
  implicit val codec: Codec[AcquaintanceSearchMessage] =
    new Codec[AcquaintanceSearchMessage] {
      def decode: Get[AcquaintanceSearchMessage] =
        for {
          nickname <- utf8(ushort).decode
        } yield AcquaintanceSearchMessage(nickname)

      def encode(value: AcquaintanceSearchMessage): ByteVector =
        utf8(ushort).encode(value.nickname)
    }
}
