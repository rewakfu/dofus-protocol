package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFightLeaveRequestMessage(
  taxCollectorId: Double,
  characterId: Long
) extends Message(5715)

object GuildFightLeaveRequestMessage {
  implicit val codec: Codec[GuildFightLeaveRequestMessage] =
    new Codec[GuildFightLeaveRequestMessage] {
      def decode: Get[GuildFightLeaveRequestMessage] =
        for {
          taxCollectorId <- double.decode
          characterId <- varLong.decode
        } yield GuildFightLeaveRequestMessage(taxCollectorId, characterId)

      def encode(value: GuildFightLeaveRequestMessage): ByteVector =
        double.encode(value.taxCollectorId) ++
        varLong.encode(value.characterId)
    }
}
