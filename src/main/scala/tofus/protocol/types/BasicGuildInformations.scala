package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait BasicGuildInformations extends AbstractSocialGroupInfos

final case class ConcreteBasicGuildInformations(
  guildId: Int,
  guildName: String,
  guildLevel: Short
) extends BasicGuildInformations {
  override val protocolId = 365
}

object ConcreteBasicGuildInformations {
  implicit val codec: Codec[ConcreteBasicGuildInformations] =  
    new Codec[ConcreteBasicGuildInformations] {
      def decode: Get[ConcreteBasicGuildInformations] =
        for {
          guildId <- varInt.decode
          guildName <- utf8(ushort).decode
          guildLevel <- ubyte.decode
        } yield ConcreteBasicGuildInformations(guildId, guildName, guildLevel)

      def encode(value: ConcreteBasicGuildInformations): ByteVector =
        varInt.encode(value.guildId) ++
        utf8(ushort).encode(value.guildName) ++
        ubyte.encode(value.guildLevel)
    }
}

object BasicGuildInformations {
  implicit val codec: Codec[BasicGuildInformations] =
    new Codec[BasicGuildInformations] {
      def decode: Get[BasicGuildInformations] =
        ushort.decode.flatMap {
          case 365 => Codec[ConcreteBasicGuildInformations].decode
          case 423 => Codec[GuildInsiderFactSheetInformations].decode
          case 424 => Codec[ConcreteGuildFactSheetInformations].decode
          case 420 => Codec[GuildInAllianceInformations].decode
          case 422 => Codec[AlliancedGuildFactSheetInformations].decode
          case 127 => Codec[ConcreteGuildInformations].decode
        }

      def encode(value: BasicGuildInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteBasicGuildInformations => Codec[ConcreteBasicGuildInformations].encode(i)
          case i: GuildInsiderFactSheetInformations => Codec[GuildInsiderFactSheetInformations].encode(i)
          case i: ConcreteGuildFactSheetInformations => Codec[ConcreteGuildFactSheetInformations].encode(i)
          case i: GuildInAllianceInformations => Codec[GuildInAllianceInformations].encode(i)
          case i: AlliancedGuildFactSheetInformations => Codec[AlliancedGuildFactSheetInformations].encode(i)
          case i: ConcreteGuildInformations => Codec[ConcreteGuildInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
