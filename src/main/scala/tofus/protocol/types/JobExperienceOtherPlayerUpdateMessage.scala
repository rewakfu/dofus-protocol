package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobExperienceOtherPlayerUpdateMessage(
  experiencesUpdate: JobExperience,
  playerId: Long
) extends Message(6599)

object JobExperienceOtherPlayerUpdateMessage {
  implicit val codec: Codec[JobExperienceOtherPlayerUpdateMessage] =
    new Codec[JobExperienceOtherPlayerUpdateMessage] {
      def decode: Get[JobExperienceOtherPlayerUpdateMessage] =
        for {
          experiencesUpdate <- Codec[JobExperience].decode
          playerId <- varLong.decode
        } yield JobExperienceOtherPlayerUpdateMessage(experiencesUpdate, playerId)

      def encode(value: JobExperienceOtherPlayerUpdateMessage): ByteVector =
        Codec[JobExperience].encode(value.experiencesUpdate) ++
        varLong.encode(value.playerId)
    }
}
