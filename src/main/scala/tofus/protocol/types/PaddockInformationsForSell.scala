package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockInformationsForSell(
  guildOwner: String,
  worldX: Short,
  worldY: Short,
  subAreaId: Short,
  nbMount: Byte,
  nbObject: Byte,
  price: Long
) extends ProtocolType {
  override val protocolId = 222
}

object PaddockInformationsForSell {
  implicit val codec: Codec[PaddockInformationsForSell] =
    new Codec[PaddockInformationsForSell] {
      def decode: Get[PaddockInformationsForSell] =
        for {
          guildOwner <- utf8(ushort).decode
          worldX <- short.decode
          worldY <- short.decode
          subAreaId <- varShort.decode
          nbMount <- byte.decode
          nbObject <- byte.decode
          price <- varLong.decode
        } yield PaddockInformationsForSell(guildOwner, worldX, worldY, subAreaId, nbMount, nbObject, price)

      def encode(value: PaddockInformationsForSell): ByteVector =
        utf8(ushort).encode(value.guildOwner) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        varShort.encode(value.subAreaId) ++
        byte.encode(value.nbMount) ++
        byte.encode(value.nbObject) ++
        varLong.encode(value.price)
    }
}
