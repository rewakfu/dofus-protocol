package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareSubscribeRequestMessage(
  dareId: Double,
  subscribe: Boolean
) extends Message(6666)

object DareSubscribeRequestMessage {
  implicit val codec: Codec[DareSubscribeRequestMessage] =
    new Codec[DareSubscribeRequestMessage] {
      def decode: Get[DareSubscribeRequestMessage] =
        for {
          dareId <- double.decode
          subscribe <- bool.decode
        } yield DareSubscribeRequestMessage(dareId, subscribe)

      def encode(value: DareSubscribeRequestMessage): ByteVector =
        double.encode(value.dareId) ++
        bool.encode(value.subscribe)
    }
}
