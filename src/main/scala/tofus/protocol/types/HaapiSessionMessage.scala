package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiSessionMessage(
  key: String,
  `type`: Byte
) extends Message(6769)

object HaapiSessionMessage {
  implicit val codec: Codec[HaapiSessionMessage] =
    new Codec[HaapiSessionMessage] {
      def decode: Get[HaapiSessionMessage] =
        for {
          key <- utf8(ushort).decode
          `type` <- byte.decode
        } yield HaapiSessionMessage(key, `type`)

      def encode(value: HaapiSessionMessage): ByteVector =
        utf8(ushort).encode(value.key) ++
        byte.encode(value.`type`)
    }
}
