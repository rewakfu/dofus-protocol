package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatedElementUpdatedMessage(
  statedElement: StatedElement
) extends Message(5709)

object StatedElementUpdatedMessage {
  implicit val codec: Codec[StatedElementUpdatedMessage] =
    new Codec[StatedElementUpdatedMessage] {
      def decode: Get[StatedElementUpdatedMessage] =
        for {
          statedElement <- Codec[StatedElement].decode
        } yield StatedElementUpdatedMessage(statedElement)

      def encode(value: StatedElementUpdatedMessage): ByteVector =
        Codec[StatedElement].encode(value.statedElement)
    }
}
