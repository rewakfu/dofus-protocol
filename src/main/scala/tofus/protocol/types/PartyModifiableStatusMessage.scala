package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyModifiableStatusMessage(
  partyId: Int,
  enabled: Boolean
) extends Message(6277)

object PartyModifiableStatusMessage {
  implicit val codec: Codec[PartyModifiableStatusMessage] =
    new Codec[PartyModifiableStatusMessage] {
      def decode: Get[PartyModifiableStatusMessage] =
        for {
          partyId <- varInt.decode
          enabled <- bool.decode
        } yield PartyModifiableStatusMessage(partyId, enabled)

      def encode(value: PartyModifiableStatusMessage): ByteVector =
        varInt.encode(value.partyId) ++
        bool.encode(value.enabled)
    }
}
