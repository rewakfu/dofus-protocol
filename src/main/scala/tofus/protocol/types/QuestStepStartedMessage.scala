package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestStepStartedMessage(
  questId: Short,
  stepId: Short
) extends Message(6096)

object QuestStepStartedMessage {
  implicit val codec: Codec[QuestStepStartedMessage] =
    new Codec[QuestStepStartedMessage] {
      def decode: Get[QuestStepStartedMessage] =
        for {
          questId <- varShort.decode
          stepId <- varShort.decode
        } yield QuestStepStartedMessage(questId, stepId)

      def encode(value: QuestStepStartedMessage): ByteVector =
        varShort.encode(value.questId) ++
        varShort.encode(value.stepId)
    }
}
