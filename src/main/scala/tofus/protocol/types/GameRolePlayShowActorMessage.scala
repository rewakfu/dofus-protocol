package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayShowActorMessage(
  informations: GameRolePlayActorInformations
) extends Message(5632)

object GameRolePlayShowActorMessage {
  implicit val codec: Codec[GameRolePlayShowActorMessage] =
    new Codec[GameRolePlayShowActorMessage] {
      def decode: Get[GameRolePlayShowActorMessage] =
        for {
          informations <- Codec[GameRolePlayActorInformations].decode
        } yield GameRolePlayShowActorMessage(informations)

      def encode(value: GameRolePlayShowActorMessage): ByteVector =
        Codec[GameRolePlayActorInformations].encode(value.informations)
    }
}
