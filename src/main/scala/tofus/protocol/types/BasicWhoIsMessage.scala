package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicWhoIsMessage(
  flags0: Byte,
  position: Byte,
  accountNickname: String,
  accountId: Int,
  playerName: String,
  playerId: Long,
  areaId: Short,
  serverId: Short,
  originServerId: Short,
  socialGroups: List[AbstractSocialGroupInfos],
  playerState: Byte
) extends Message(180)

object BasicWhoIsMessage {
  implicit val codec: Codec[BasicWhoIsMessage] =
    new Codec[BasicWhoIsMessage] {
      def decode: Get[BasicWhoIsMessage] =
        for {
          flags0 <- byte.decode
          position <- byte.decode
          accountNickname <- utf8(ushort).decode
          accountId <- int.decode
          playerName <- utf8(ushort).decode
          playerId <- varLong.decode
          areaId <- short.decode
          serverId <- short.decode
          originServerId <- short.decode
          socialGroups <- list(ushort, Codec[AbstractSocialGroupInfos]).decode
          playerState <- byte.decode
        } yield BasicWhoIsMessage(flags0, position, accountNickname, accountId, playerName, playerId, areaId, serverId, originServerId, socialGroups, playerState)

      def encode(value: BasicWhoIsMessage): ByteVector =
        byte.encode(value.flags0) ++
        byte.encode(value.position) ++
        utf8(ushort).encode(value.accountNickname) ++
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.playerName) ++
        varLong.encode(value.playerId) ++
        short.encode(value.areaId) ++
        short.encode(value.serverId) ++
        short.encode(value.originServerId) ++
        list(ushort, Codec[AbstractSocialGroupInfos]).encode(value.socialGroups) ++
        byte.encode(value.playerState)
    }
}
