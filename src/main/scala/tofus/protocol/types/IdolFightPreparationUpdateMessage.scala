package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolFightPreparationUpdateMessage(
  idolSource: Byte,
  idols: List[Idol]
) extends Message(6586)

object IdolFightPreparationUpdateMessage {
  implicit val codec: Codec[IdolFightPreparationUpdateMessage] =
    new Codec[IdolFightPreparationUpdateMessage] {
      def decode: Get[IdolFightPreparationUpdateMessage] =
        for {
          idolSource <- byte.decode
          idols <- list(ushort, Codec[Idol]).decode
        } yield IdolFightPreparationUpdateMessage(idolSource, idols)

      def encode(value: IdolFightPreparationUpdateMessage): ByteVector =
        byte.encode(value.idolSource) ++
        list(ushort, Codec[Idol]).encode(value.idols)
    }
}
