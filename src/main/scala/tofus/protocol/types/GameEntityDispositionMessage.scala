package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameEntityDispositionMessage(
  disposition: IdentifiedEntityDispositionInformations
) extends Message(5693)

object GameEntityDispositionMessage {
  implicit val codec: Codec[GameEntityDispositionMessage] =
    new Codec[GameEntityDispositionMessage] {
      def decode: Get[GameEntityDispositionMessage] =
        for {
          disposition <- Codec[IdentifiedEntityDispositionInformations].decode
        } yield GameEntityDispositionMessage(disposition)

      def encode(value: GameEntityDispositionMessage): ByteVector =
        Codec[IdentifiedEntityDispositionInformations].encode(value.disposition)
    }
}
