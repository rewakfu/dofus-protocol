package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectDeletedMessage(
  objectUID: Int
) extends Message(3024)

object ObjectDeletedMessage {
  implicit val codec: Codec[ObjectDeletedMessage] =
    new Codec[ObjectDeletedMessage] {
      def decode: Get[ObjectDeletedMessage] =
        for {
          objectUID <- varInt.decode
        } yield ObjectDeletedMessage(objectUID)

      def encode(value: ObjectDeletedMessage): ByteVector =
        varInt.encode(value.objectUID)
    }
}
