package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AcquaintanceSearchErrorMessage(
  reason: Byte
) extends Message(6143)

object AcquaintanceSearchErrorMessage {
  implicit val codec: Codec[AcquaintanceSearchErrorMessage] =
    new Codec[AcquaintanceSearchErrorMessage] {
      def decode: Get[AcquaintanceSearchErrorMessage] =
        for {
          reason <- byte.decode
        } yield AcquaintanceSearchErrorMessage(reason)

      def encode(value: AcquaintanceSearchErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
