package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationRequestMessage(
  name: String
) extends Message(5585)

object PartyInvitationRequestMessage {
  implicit val codec: Codec[PartyInvitationRequestMessage] =
    new Codec[PartyInvitationRequestMessage] {
      def decode: Get[PartyInvitationRequestMessage] =
        for {
          name <- utf8(ushort).decode
        } yield PartyInvitationRequestMessage(name)

      def encode(value: PartyInvitationRequestMessage): ByteVector =
        utf8(ushort).encode(value.name)
    }
}
