package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyFollowStatusUpdateMessage(
  partyId: Int,
  flags0: Byte,
  followedId: Long
) extends Message(5581)

object PartyFollowStatusUpdateMessage {
  implicit val codec: Codec[PartyFollowStatusUpdateMessage] =
    new Codec[PartyFollowStatusUpdateMessage] {
      def decode: Get[PartyFollowStatusUpdateMessage] =
        for {
          partyId <- varInt.decode
          flags0 <- byte.decode
          followedId <- varLong.decode
        } yield PartyFollowStatusUpdateMessage(partyId, flags0, followedId)

      def encode(value: PartyFollowStatusUpdateMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.flags0) ++
        varLong.encode(value.followedId)
    }
}
