package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NumericWhoIsMessage(
  playerId: Long,
  accountId: Int
) extends Message(6297)

object NumericWhoIsMessage {
  implicit val codec: Codec[NumericWhoIsMessage] =
    new Codec[NumericWhoIsMessage] {
      def decode: Get[NumericWhoIsMessage] =
        for {
          playerId <- varLong.decode
          accountId <- int.decode
        } yield NumericWhoIsMessage(playerId, accountId)

      def encode(value: NumericWhoIsMessage): ByteVector =
        varLong.encode(value.playerId) ++
        int.encode(value.accountId)
    }
}
