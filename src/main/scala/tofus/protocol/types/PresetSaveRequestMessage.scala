package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetSaveRequestMessage(
  presetId: Short,
  symbolId: Byte,
  updateData: Boolean
) extends Message(6761)

object PresetSaveRequestMessage {
  implicit val codec: Codec[PresetSaveRequestMessage] =
    new Codec[PresetSaveRequestMessage] {
      def decode: Get[PresetSaveRequestMessage] =
        for {
          presetId <- short.decode
          symbolId <- byte.decode
          updateData <- bool.decode
        } yield PresetSaveRequestMessage(presetId, symbolId, updateData)

      def encode(value: PresetSaveRequestMessage): ByteVector =
        short.encode(value.presetId) ++
        byte.encode(value.symbolId) ++
        bool.encode(value.updateData)
    }
}
