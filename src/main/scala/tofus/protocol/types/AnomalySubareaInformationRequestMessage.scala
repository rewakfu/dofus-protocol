package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AnomalySubareaInformationRequestMessage(

) extends Message(6835)

object AnomalySubareaInformationRequestMessage {
  implicit val codec: Codec[AnomalySubareaInformationRequestMessage] =
    Codec.const(AnomalySubareaInformationRequestMessage())
}
