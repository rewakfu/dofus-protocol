package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SocialNoticeMessage(
  content: String,
  timestamp: Int,
  memberId: Long,
  memberName: String
) extends Message(6688)

object SocialNoticeMessage {
  implicit val codec: Codec[SocialNoticeMessage] =
    new Codec[SocialNoticeMessage] {
      def decode: Get[SocialNoticeMessage] =
        for {
          content <- utf8(ushort).decode
          timestamp <- int.decode
          memberId <- varLong.decode
          memberName <- utf8(ushort).decode
        } yield SocialNoticeMessage(content, timestamp, memberId, memberName)

      def encode(value: SocialNoticeMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        varLong.encode(value.memberId) ++
        utf8(ushort).encode(value.memberName)
    }
}
