package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightEndMessage(
  duration: Int,
  rewardRate: Short,
  lootShareLimitMalus: Short,
  results: List[FightResultListEntry],
  namedPartyTeamsOutcomes: List[NamedPartyTeamWithOutcome]
) extends Message(720)

object GameFightEndMessage {
  implicit val codec: Codec[GameFightEndMessage] =
    new Codec[GameFightEndMessage] {
      def decode: Get[GameFightEndMessage] =
        for {
          duration <- int.decode
          rewardRate <- varShort.decode
          lootShareLimitMalus <- short.decode
          results <- list(ushort, Codec[FightResultListEntry]).decode
          namedPartyTeamsOutcomes <- list(ushort, Codec[NamedPartyTeamWithOutcome]).decode
        } yield GameFightEndMessage(duration, rewardRate, lootShareLimitMalus, results, namedPartyTeamsOutcomes)

      def encode(value: GameFightEndMessage): ByteVector =
        int.encode(value.duration) ++
        varShort.encode(value.rewardRate) ++
        short.encode(value.lootShareLimitMalus) ++
        list(ushort, Codec[FightResultListEntry]).encode(value.results) ++
        list(ushort, Codec[NamedPartyTeamWithOutcome]).encode(value.namedPartyTeamsOutcomes)
    }
}
