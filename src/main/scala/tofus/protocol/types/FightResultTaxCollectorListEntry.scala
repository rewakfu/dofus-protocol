package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightResultTaxCollectorListEntry(
  outcome: Short,
  wave: Byte,
  rewards: FightLoot,
  id: Double,
  alive: Boolean,
  level: Short,
  guildInfo: ConcreteBasicGuildInformations,
  experienceForGuild: Int
) extends FightResultFighterListEntry {
  override val protocolId = 84
}

object FightResultTaxCollectorListEntry {
  implicit val codec: Codec[FightResultTaxCollectorListEntry] =
    new Codec[FightResultTaxCollectorListEntry] {
      def decode: Get[FightResultTaxCollectorListEntry] =
        for {
          outcome <- varShort.decode
          wave <- byte.decode
          rewards <- Codec[FightLoot].decode
          id <- double.decode
          alive <- bool.decode
          level <- ubyte.decode
          guildInfo <- Codec[ConcreteBasicGuildInformations].decode
          experienceForGuild <- int.decode
        } yield FightResultTaxCollectorListEntry(outcome, wave, rewards, id, alive, level, guildInfo, experienceForGuild)

      def encode(value: FightResultTaxCollectorListEntry): ByteVector =
        varShort.encode(value.outcome) ++
        byte.encode(value.wave) ++
        Codec[FightLoot].encode(value.rewards) ++
        double.encode(value.id) ++
        bool.encode(value.alive) ++
        ubyte.encode(value.level) ++
        Codec[ConcreteBasicGuildInformations].encode(value.guildInfo) ++
        int.encode(value.experienceForGuild)
    }
}
