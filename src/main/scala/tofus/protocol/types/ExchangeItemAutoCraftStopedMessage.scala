package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeItemAutoCraftStopedMessage(
  reason: Byte
) extends Message(5810)

object ExchangeItemAutoCraftStopedMessage {
  implicit val codec: Codec[ExchangeItemAutoCraftStopedMessage] =
    new Codec[ExchangeItemAutoCraftStopedMessage] {
      def decode: Get[ExchangeItemAutoCraftStopedMessage] =
        for {
          reason <- byte.decode
        } yield ExchangeItemAutoCraftStopedMessage(reason)

      def encode(value: ExchangeItemAutoCraftStopedMessage): ByteVector =
        byte.encode(value.reason)
    }
}
