package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterAlignmentWarEffortProgressionMessage(
  alignmentWarEffortDailyLimit: Long,
  alignmentWarEffortDailyDonation: Long,
  alignmentWarEffortPersonalDonation: Long
) extends Message(6851)

object CharacterAlignmentWarEffortProgressionMessage {
  implicit val codec: Codec[CharacterAlignmentWarEffortProgressionMessage] =
    new Codec[CharacterAlignmentWarEffortProgressionMessage] {
      def decode: Get[CharacterAlignmentWarEffortProgressionMessage] =
        for {
          alignmentWarEffortDailyLimit <- varLong.decode
          alignmentWarEffortDailyDonation <- varLong.decode
          alignmentWarEffortPersonalDonation <- varLong.decode
        } yield CharacterAlignmentWarEffortProgressionMessage(alignmentWarEffortDailyLimit, alignmentWarEffortDailyDonation, alignmentWarEffortPersonalDonation)

      def encode(value: CharacterAlignmentWarEffortProgressionMessage): ByteVector =
        varLong.encode(value.alignmentWarEffortDailyLimit) ++
        varLong.encode(value.alignmentWarEffortDailyDonation) ++
        varLong.encode(value.alignmentWarEffortPersonalDonation)
    }
}
