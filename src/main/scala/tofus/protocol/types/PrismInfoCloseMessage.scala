package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismInfoCloseMessage(

) extends Message(5853)

object PrismInfoCloseMessage {
  implicit val codec: Codec[PrismInfoCloseMessage] =
    Codec.const(PrismInfoCloseMessage())
}
