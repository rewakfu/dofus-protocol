package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapComplementaryInformationsDataInHavenBagMessage(
  subAreaId: Short,
  mapId: Double,
  houses: List[HouseInformations],
  actors: List[GameRolePlayActorInformations],
  interactiveElements: List[InteractiveElement],
  statedElements: List[StatedElement],
  obstacles: List[MapObstacle],
  fights: List[FightCommonInformations],
  hasAggressiveMonsters: Boolean,
  fightStartPositions: FightStartingPositions,
  ownerInformations: ConcreteCharacterMinimalInformations,
  theme: Byte,
  roomId: Byte,
  maxRoomId: Byte
) extends Message(6622)

object MapComplementaryInformationsDataInHavenBagMessage {
  implicit val codec: Codec[MapComplementaryInformationsDataInHavenBagMessage] =
    new Codec[MapComplementaryInformationsDataInHavenBagMessage] {
      def decode: Get[MapComplementaryInformationsDataInHavenBagMessage] =
        for {
          subAreaId <- varShort.decode
          mapId <- double.decode
          houses <- list(ushort, Codec[HouseInformations]).decode
          actors <- list(ushort, Codec[GameRolePlayActorInformations]).decode
          interactiveElements <- list(ushort, Codec[InteractiveElement]).decode
          statedElements <- list(ushort, Codec[StatedElement]).decode
          obstacles <- list(ushort, Codec[MapObstacle]).decode
          fights <- list(ushort, Codec[FightCommonInformations]).decode
          hasAggressiveMonsters <- bool.decode
          fightStartPositions <- Codec[FightStartingPositions].decode
          ownerInformations <- Codec[ConcreteCharacterMinimalInformations].decode
          theme <- byte.decode
          roomId <- byte.decode
          maxRoomId <- byte.decode
        } yield MapComplementaryInformationsDataInHavenBagMessage(subAreaId, mapId, houses, actors, interactiveElements, statedElements, obstacles, fights, hasAggressiveMonsters, fightStartPositions, ownerInformations, theme, roomId, maxRoomId)

      def encode(value: MapComplementaryInformationsDataInHavenBagMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        double.encode(value.mapId) ++
        list(ushort, Codec[HouseInformations]).encode(value.houses) ++
        list(ushort, Codec[GameRolePlayActorInformations]).encode(value.actors) ++
        list(ushort, Codec[InteractiveElement]).encode(value.interactiveElements) ++
        list(ushort, Codec[StatedElement]).encode(value.statedElements) ++
        list(ushort, Codec[MapObstacle]).encode(value.obstacles) ++
        list(ushort, Codec[FightCommonInformations]).encode(value.fights) ++
        bool.encode(value.hasAggressiveMonsters) ++
        Codec[FightStartingPositions].encode(value.fightStartPositions) ++
        Codec[ConcreteCharacterMinimalInformations].encode(value.ownerInformations) ++
        byte.encode(value.theme) ++
        byte.encode(value.roomId) ++
        byte.encode(value.maxRoomId)
    }
}
