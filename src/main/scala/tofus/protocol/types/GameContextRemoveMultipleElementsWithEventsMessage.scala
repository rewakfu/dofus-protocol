package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextRemoveMultipleElementsWithEventsMessage(
  elementsIds: List[Double],
  elementEventIds: ByteVector
) extends Message(6416)

object GameContextRemoveMultipleElementsWithEventsMessage {
  implicit val codec: Codec[GameContextRemoveMultipleElementsWithEventsMessage] =
    new Codec[GameContextRemoveMultipleElementsWithEventsMessage] {
      def decode: Get[GameContextRemoveMultipleElementsWithEventsMessage] =
        for {
          elementsIds <- list(ushort, double).decode
          elementEventIds <- bytes(ushort).decode
        } yield GameContextRemoveMultipleElementsWithEventsMessage(elementsIds, elementEventIds)

      def encode(value: GameContextRemoveMultipleElementsWithEventsMessage): ByteVector =
        list(ushort, double).encode(value.elementsIds) ++
        bytes(ushort).encode(value.elementEventIds)
    }
}
