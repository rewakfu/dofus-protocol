package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismsInfoValidMessage(
  fights: List[PrismFightersInformation]
) extends Message(6451)

object PrismsInfoValidMessage {
  implicit val codec: Codec[PrismsInfoValidMessage] =
    new Codec[PrismsInfoValidMessage] {
      def decode: Get[PrismsInfoValidMessage] =
        for {
          fights <- list(ushort, Codec[PrismFightersInformation]).decode
        } yield PrismsInfoValidMessage(fights)

      def encode(value: PrismsInfoValidMessage): ByteVector =
        list(ushort, Codec[PrismFightersInformation]).encode(value.fights)
    }
}
