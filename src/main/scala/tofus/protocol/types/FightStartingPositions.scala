package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightStartingPositions(
  positionsForChallengers: List[Short],
  positionsForDefenders: List[Short]
) extends ProtocolType {
  override val protocolId = 513
}

object FightStartingPositions {
  implicit val codec: Codec[FightStartingPositions] =
    new Codec[FightStartingPositions] {
      def decode: Get[FightStartingPositions] =
        for {
          positionsForChallengers <- list(ushort, varShort).decode
          positionsForDefenders <- list(ushort, varShort).decode
        } yield FightStartingPositions(positionsForChallengers, positionsForDefenders)

      def encode(value: FightStartingPositions): ByteVector =
        list(ushort, varShort).encode(value.positionsForChallengers) ++
        list(ushort, varShort).encode(value.positionsForDefenders)
    }
}
