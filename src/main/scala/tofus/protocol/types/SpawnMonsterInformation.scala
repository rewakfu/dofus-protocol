package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpawnMonsterInformation(
  creatureGenericId: Short,
  creatureGrade: Byte
) extends BaseSpawnMonsterInformation {
  override val protocolId = 572
}

object SpawnMonsterInformation {
  implicit val codec: Codec[SpawnMonsterInformation] =
    new Codec[SpawnMonsterInformation] {
      def decode: Get[SpawnMonsterInformation] =
        for {
          creatureGenericId <- varShort.decode
          creatureGrade <- byte.decode
        } yield SpawnMonsterInformation(creatureGenericId, creatureGrade)

      def encode(value: SpawnMonsterInformation): ByteVector =
        varShort.encode(value.creatureGenericId) ++
        byte.encode(value.creatureGrade)
    }
}
