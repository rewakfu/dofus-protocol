package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdentificationFailedForBadVersionMessage(
  reason: Byte,
  requiredVersion: Version
) extends Message(21)

object IdentificationFailedForBadVersionMessage {
  implicit val codec: Codec[IdentificationFailedForBadVersionMessage] =
    new Codec[IdentificationFailedForBadVersionMessage] {
      def decode: Get[IdentificationFailedForBadVersionMessage] =
        for {
          reason <- byte.decode
          requiredVersion <- Codec[Version].decode
        } yield IdentificationFailedForBadVersionMessage(reason, requiredVersion)

      def encode(value: IdentificationFailedForBadVersionMessage): ByteVector =
        byte.encode(value.reason) ++
        Codec[Version].encode(value.requiredVersion)
    }
}
