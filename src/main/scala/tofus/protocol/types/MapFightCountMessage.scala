package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapFightCountMessage(
  fightCount: Short
) extends Message(210)

object MapFightCountMessage {
  implicit val codec: Codec[MapFightCountMessage] =
    new Codec[MapFightCountMessage] {
      def decode: Get[MapFightCountMessage] =
        for {
          fightCount <- varShort.decode
        } yield MapFightCountMessage(fightCount)

      def encode(value: MapFightCountMessage): ByteVector =
        varShort.encode(value.fightCount)
    }
}
