package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatisticDataInt(
  value: Int
) extends StatisticData {
  override val protocolId = 485
}

object StatisticDataInt {
  implicit val codec: Codec[StatisticDataInt] =
    new Codec[StatisticDataInt] {
      def decode: Get[StatisticDataInt] =
        for {
          value <- int.decode
        } yield StatisticDataInt(value)

      def encode(value: StatisticDataInt): ByteVector =
        int.encode(value.value)
    }
}
