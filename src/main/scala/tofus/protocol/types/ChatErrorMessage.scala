package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatErrorMessage(
  reason: Byte
) extends Message(870)

object ChatErrorMessage {
  implicit val codec: Codec[ChatErrorMessage] =
    new Codec[ChatErrorMessage] {
      def decode: Get[ChatErrorMessage] =
        for {
          reason <- byte.decode
        } yield ChatErrorMessage(reason)

      def encode(value: ChatErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
