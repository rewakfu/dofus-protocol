package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightLifePointsGainMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  delta: Int
) extends Message(6311)

object GameActionFightLifePointsGainMessage {
  implicit val codec: Codec[GameActionFightLifePointsGainMessage] =
    new Codec[GameActionFightLifePointsGainMessage] {
      def decode: Get[GameActionFightLifePointsGainMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          delta <- varInt.decode
        } yield GameActionFightLifePointsGainMessage(actionId, sourceId, targetId, delta)

      def encode(value: GameActionFightLifePointsGainMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varInt.encode(value.delta)
    }
}
