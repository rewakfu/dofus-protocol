package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DebtsDeleteMessage(
  reason: Byte,
  debts: List[Double]
) extends Message(6866)

object DebtsDeleteMessage {
  implicit val codec: Codec[DebtsDeleteMessage] =
    new Codec[DebtsDeleteMessage] {
      def decode: Get[DebtsDeleteMessage] =
        for {
          reason <- byte.decode
          debts <- list(ushort, double).decode
        } yield DebtsDeleteMessage(reason, debts)

      def encode(value: DebtsDeleteMessage): ByteVector =
        byte.encode(value.reason) ++
        list(ushort, double).encode(value.debts)
    }
}
