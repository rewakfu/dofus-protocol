package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatCommunityChannelSetCommunityRequestMessage(
  communityId: Short
) extends Message(6729)

object ChatCommunityChannelSetCommunityRequestMessage {
  implicit val codec: Codec[ChatCommunityChannelSetCommunityRequestMessage] =
    new Codec[ChatCommunityChannelSetCommunityRequestMessage] {
      def decode: Get[ChatCommunityChannelSetCommunityRequestMessage] =
        for {
          communityId <- short.decode
        } yield ChatCommunityChannelSetCommunityRequestMessage(communityId)

      def encode(value: ChatCommunityChannelSetCommunityRequestMessage): ByteVector =
        short.encode(value.communityId)
    }
}
