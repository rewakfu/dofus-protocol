package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChallengeTargetsListMessage(
  targetIds: List[Double],
  targetCells: List[Short]
) extends Message(5613)

object ChallengeTargetsListMessage {
  implicit val codec: Codec[ChallengeTargetsListMessage] =
    new Codec[ChallengeTargetsListMessage] {
      def decode: Get[ChallengeTargetsListMessage] =
        for {
          targetIds <- list(ushort, double).decode
          targetCells <- list(ushort, short).decode
        } yield ChallengeTargetsListMessage(targetIds, targetCells)

      def encode(value: ChallengeTargetsListMessage): ByteVector =
        list(ushort, double).encode(value.targetIds) ++
        list(ushort, short).encode(value.targetCells)
    }
}
