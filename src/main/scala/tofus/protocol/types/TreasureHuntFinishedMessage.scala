package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntFinishedMessage(
  questType: Byte
) extends Message(6483)

object TreasureHuntFinishedMessage {
  implicit val codec: Codec[TreasureHuntFinishedMessage] =
    new Codec[TreasureHuntFinishedMessage] {
      def decode: Get[TreasureHuntFinishedMessage] =
        for {
          questType <- byte.decode
        } yield TreasureHuntFinishedMessage(questType)

      def encode(value: TreasureHuntFinishedMessage): ByteVector =
        byte.encode(value.questType)
    }
}
