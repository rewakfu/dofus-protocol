package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectGroundRemovedMessage(
  cell: Short
) extends Message(3014)

object ObjectGroundRemovedMessage {
  implicit val codec: Codec[ObjectGroundRemovedMessage] =
    new Codec[ObjectGroundRemovedMessage] {
      def decode: Get[ObjectGroundRemovedMessage] =
        for {
          cell <- varShort.decode
        } yield ObjectGroundRemovedMessage(cell)

      def encode(value: ObjectGroundRemovedMessage): ByteVector =
        varShort.encode(value.cell)
    }
}
