package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightRemovedMessage(
  subAreaId: Short
) extends Message(6453)

object PrismFightRemovedMessage {
  implicit val codec: Codec[PrismFightRemovedMessage] =
    new Codec[PrismFightRemovedMessage] {
      def decode: Get[PrismFightRemovedMessage] =
        for {
          subAreaId <- varShort.decode
        } yield PrismFightRemovedMessage(subAreaId)

      def encode(value: PrismFightRemovedMessage): ByteVector =
        varShort.encode(value.subAreaId)
    }
}
