package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInvitationAnswerMessage(
  accept: Boolean
) extends Message(6401)

object AllianceInvitationAnswerMessage {
  implicit val codec: Codec[AllianceInvitationAnswerMessage] =
    new Codec[AllianceInvitationAnswerMessage] {
      def decode: Get[AllianceInvitationAnswerMessage] =
        for {
          accept <- bool.decode
        } yield AllianceInvitationAnswerMessage(accept)

      def encode(value: AllianceInvitationAnswerMessage): ByteVector =
        bool.encode(value.accept)
    }
}
