package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InviteInHavenBagMessage(
  guestInformations: ConcreteCharacterMinimalInformations,
  accept: Boolean
) extends Message(6642)

object InviteInHavenBagMessage {
  implicit val codec: Codec[InviteInHavenBagMessage] =
    new Codec[InviteInHavenBagMessage] {
      def decode: Get[InviteInHavenBagMessage] =
        for {
          guestInformations <- Codec[ConcreteCharacterMinimalInformations].decode
          accept <- bool.decode
        } yield InviteInHavenBagMessage(guestInformations, accept)

      def encode(value: InviteInHavenBagMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.guestInformations) ++
        bool.encode(value.accept)
    }
}
