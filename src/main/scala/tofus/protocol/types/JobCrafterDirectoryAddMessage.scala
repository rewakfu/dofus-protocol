package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryAddMessage(
  listEntry: JobCrafterDirectoryListEntry
) extends Message(5651)

object JobCrafterDirectoryAddMessage {
  implicit val codec: Codec[JobCrafterDirectoryAddMessage] =
    new Codec[JobCrafterDirectoryAddMessage] {
      def decode: Get[JobCrafterDirectoryAddMessage] =
        for {
          listEntry <- Codec[JobCrafterDirectoryListEntry].decode
        } yield JobCrafterDirectoryAddMessage(listEntry)

      def encode(value: JobCrafterDirectoryAddMessage): ByteVector =
        Codec[JobCrafterDirectoryListEntry].encode(value.listEntry)
    }
}
