package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FightResultListEntry extends ProtocolType

final case class ConcreteFightResultListEntry(
  outcome: Short,
  wave: Byte,
  rewards: FightLoot
) extends FightResultListEntry {
  override val protocolId = 16
}

object ConcreteFightResultListEntry {
  implicit val codec: Codec[ConcreteFightResultListEntry] =  
    new Codec[ConcreteFightResultListEntry] {
      def decode: Get[ConcreteFightResultListEntry] =
        for {
          outcome <- varShort.decode
          wave <- byte.decode
          rewards <- Codec[FightLoot].decode
        } yield ConcreteFightResultListEntry(outcome, wave, rewards)

      def encode(value: ConcreteFightResultListEntry): ByteVector =
        varShort.encode(value.outcome) ++
        byte.encode(value.wave) ++
        Codec[FightLoot].encode(value.rewards)
    }
}

object FightResultListEntry {
  implicit val codec: Codec[FightResultListEntry] =
    new Codec[FightResultListEntry] {
      def decode: Get[FightResultListEntry] =
        ushort.decode.flatMap {
          case 16 => Codec[ConcreteFightResultListEntry].decode
          case 24 => Codec[FightResultPlayerListEntry].decode
          case 216 => Codec[FightResultMutantListEntry].decode
          case 84 => Codec[FightResultTaxCollectorListEntry].decode
          case 189 => Codec[ConcreteFightResultFighterListEntry].decode
        }

      def encode(value: FightResultListEntry): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFightResultListEntry => Codec[ConcreteFightResultListEntry].encode(i)
          case i: FightResultPlayerListEntry => Codec[FightResultPlayerListEntry].encode(i)
          case i: FightResultMutantListEntry => Codec[FightResultMutantListEntry].encode(i)
          case i: FightResultTaxCollectorListEntry => Codec[FightResultTaxCollectorListEntry].encode(i)
          case i: ConcreteFightResultFighterListEntry => Codec[ConcreteFightResultFighterListEntry].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
