package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightDispellSpellMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  verboseCast: Boolean,
  spellId: Short
) extends Message(6176)

object GameActionFightDispellSpellMessage {
  implicit val codec: Codec[GameActionFightDispellSpellMessage] =
    new Codec[GameActionFightDispellSpellMessage] {
      def decode: Get[GameActionFightDispellSpellMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          verboseCast <- bool.decode
          spellId <- varShort.decode
        } yield GameActionFightDispellSpellMessage(actionId, sourceId, targetId, verboseCast, spellId)

      def encode(value: GameActionFightDispellSpellMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        bool.encode(value.verboseCast) ++
        varShort.encode(value.spellId)
    }
}
