package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolsPresetSaveRequestMessage(
  presetId: Short,
  symbolId: Byte,
  updateData: Boolean
) extends Message(6758)

object IdolsPresetSaveRequestMessage {
  implicit val codec: Codec[IdolsPresetSaveRequestMessage] =
    new Codec[IdolsPresetSaveRequestMessage] {
      def decode: Get[IdolsPresetSaveRequestMessage] =
        for {
          presetId <- short.decode
          symbolId <- byte.decode
          updateData <- bool.decode
        } yield IdolsPresetSaveRequestMessage(presetId, symbolId, updateData)

      def encode(value: IdolsPresetSaveRequestMessage): ByteVector =
        short.encode(value.presetId) ++
        byte.encode(value.symbolId) ++
        bool.encode(value.updateData)
    }
}
