package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftInformationObjectMessage(
  craftResult: Byte,
  objectGenericId: Short,
  playerId: Long
) extends Message(5794)

object ExchangeCraftInformationObjectMessage {
  implicit val codec: Codec[ExchangeCraftInformationObjectMessage] =
    new Codec[ExchangeCraftInformationObjectMessage] {
      def decode: Get[ExchangeCraftInformationObjectMessage] =
        for {
          craftResult <- byte.decode
          objectGenericId <- varShort.decode
          playerId <- varLong.decode
        } yield ExchangeCraftInformationObjectMessage(craftResult, objectGenericId, playerId)

      def encode(value: ExchangeCraftInformationObjectMessage): ByteVector =
        byte.encode(value.craftResult) ++
        varShort.encode(value.objectGenericId) ++
        varLong.encode(value.playerId)
    }
}
