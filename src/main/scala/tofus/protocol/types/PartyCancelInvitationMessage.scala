package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyCancelInvitationMessage(
  partyId: Int,
  guestId: Long
) extends Message(6254)

object PartyCancelInvitationMessage {
  implicit val codec: Codec[PartyCancelInvitationMessage] =
    new Codec[PartyCancelInvitationMessage] {
      def decode: Get[PartyCancelInvitationMessage] =
        for {
          partyId <- varInt.decode
          guestId <- varLong.decode
        } yield PartyCancelInvitationMessage(partyId, guestId)

      def encode(value: PartyCancelInvitationMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.guestId)
    }
}
