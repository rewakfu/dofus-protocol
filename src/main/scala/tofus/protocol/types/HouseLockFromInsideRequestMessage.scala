package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseLockFromInsideRequestMessage(
  code: String
) extends Message(5885)

object HouseLockFromInsideRequestMessage {
  implicit val codec: Codec[HouseLockFromInsideRequestMessage] =
    new Codec[HouseLockFromInsideRequestMessage] {
      def decode: Get[HouseLockFromInsideRequestMessage] =
        for {
          code <- utf8(ushort).decode
        } yield HouseLockFromInsideRequestMessage(code)

      def encode(value: HouseLockFromInsideRequestMessage): ByteVector =
        utf8(ushort).encode(value.code)
    }
}
