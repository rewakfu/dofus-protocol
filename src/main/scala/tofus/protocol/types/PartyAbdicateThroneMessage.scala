package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyAbdicateThroneMessage(
  partyId: Int,
  playerId: Long
) extends Message(6080)

object PartyAbdicateThroneMessage {
  implicit val codec: Codec[PartyAbdicateThroneMessage] =
    new Codec[PartyAbdicateThroneMessage] {
      def decode: Get[PartyAbdicateThroneMessage] =
        for {
          partyId <- varInt.decode
          playerId <- varLong.decode
        } yield PartyAbdicateThroneMessage(partyId, playerId)

      def encode(value: PartyAbdicateThroneMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.playerId)
    }
}
