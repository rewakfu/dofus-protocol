package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachRoomUnlockResultMessage(
  roomId: Byte,
  result: Byte
) extends Message(6864)

object BreachRoomUnlockResultMessage {
  implicit val codec: Codec[BreachRoomUnlockResultMessage] =
    new Codec[BreachRoomUnlockResultMessage] {
      def decode: Get[BreachRoomUnlockResultMessage] =
        for {
          roomId <- byte.decode
          result <- byte.decode
        } yield BreachRoomUnlockResultMessage(roomId, result)

      def encode(value: BreachRoomUnlockResultMessage): ByteVector =
        byte.encode(value.roomId) ++
        byte.encode(value.result)
    }
}
