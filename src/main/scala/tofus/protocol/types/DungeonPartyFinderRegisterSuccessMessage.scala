package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderRegisterSuccessMessage(
  dungeonIds: List[Short]
) extends Message(6241)

object DungeonPartyFinderRegisterSuccessMessage {
  implicit val codec: Codec[DungeonPartyFinderRegisterSuccessMessage] =
    new Codec[DungeonPartyFinderRegisterSuccessMessage] {
      def decode: Get[DungeonPartyFinderRegisterSuccessMessage] =
        for {
          dungeonIds <- list(ushort, varShort).decode
        } yield DungeonPartyFinderRegisterSuccessMessage(dungeonIds)

      def encode(value: DungeonPartyFinderRegisterSuccessMessage): ByteVector =
        list(ushort, varShort).encode(value.dungeonIds)
    }
}
