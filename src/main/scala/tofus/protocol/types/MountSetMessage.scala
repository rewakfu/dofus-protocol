package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountSetMessage(
  mountData: MountClientData
) extends Message(5968)

object MountSetMessage {
  implicit val codec: Codec[MountSetMessage] =
    new Codec[MountSetMessage] {
      def decode: Get[MountSetMessage] =
        for {
          mountData <- Codec[MountClientData].decode
        } yield MountSetMessage(mountData)

      def encode(value: MountSetMessage): ByteVector =
        Codec[MountClientData].encode(value.mountData)
    }
}
