package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountSetXpRatioRequestMessage(
  xpRatio: Byte
) extends Message(5989)

object MountSetXpRatioRequestMessage {
  implicit val codec: Codec[MountSetXpRatioRequestMessage] =
    new Codec[MountSetXpRatioRequestMessage] {
      def decode: Get[MountSetXpRatioRequestMessage] =
        for {
          xpRatio <- byte.decode
        } yield MountSetXpRatioRequestMessage(xpRatio)

      def encode(value: MountSetXpRatioRequestMessage): ByteVector =
        byte.encode(value.xpRatio)
    }
}
