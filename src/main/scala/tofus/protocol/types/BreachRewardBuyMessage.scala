package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachRewardBuyMessage(
  id: Int
) extends Message(6803)

object BreachRewardBuyMessage {
  implicit val codec: Codec[BreachRewardBuyMessage] =
    new Codec[BreachRewardBuyMessage] {
      def decode: Get[BreachRewardBuyMessage] =
        for {
          id <- varInt.decode
        } yield BreachRewardBuyMessage(id)

      def encode(value: BreachRewardBuyMessage): ByteVector =
        varInt.encode(value.id)
    }
}
