package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementDetailsMessage(
  achievement: Achievement
) extends Message(6378)

object AchievementDetailsMessage {
  implicit val codec: Codec[AchievementDetailsMessage] =
    new Codec[AchievementDetailsMessage] {
      def decode: Get[AchievementDetailsMessage] =
        for {
          achievement <- Codec[Achievement].decode
        } yield AchievementDetailsMessage(achievement)

      def encode(value: AchievementDetailsMessage): ByteVector =
        Codec[Achievement].encode(value.achievement)
    }
}
