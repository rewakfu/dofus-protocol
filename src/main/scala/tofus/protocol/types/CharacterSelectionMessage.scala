package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterSelectionMessage(
  id: Long
) extends Message(152)

object CharacterSelectionMessage {
  implicit val codec: Codec[CharacterSelectionMessage] =
    new Codec[CharacterSelectionMessage] {
      def decode: Get[CharacterSelectionMessage] =
        for {
          id <- varLong.decode
        } yield CharacterSelectionMessage(id)

      def encode(value: CharacterSelectionMessage): ByteVector =
        varLong.encode(value.id)
    }
}
