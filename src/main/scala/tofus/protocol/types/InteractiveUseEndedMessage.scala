package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveUseEndedMessage(
  elemId: Int,
  skillId: Short
) extends Message(6112)

object InteractiveUseEndedMessage {
  implicit val codec: Codec[InteractiveUseEndedMessage] =
    new Codec[InteractiveUseEndedMessage] {
      def decode: Get[InteractiveUseEndedMessage] =
        for {
          elemId <- varInt.decode
          skillId <- varShort.decode
        } yield InteractiveUseEndedMessage(elemId, skillId)

      def encode(value: InteractiveUseEndedMessage): ByteVector =
        varInt.encode(value.elemId) ++
        varShort.encode(value.skillId)
    }
}
