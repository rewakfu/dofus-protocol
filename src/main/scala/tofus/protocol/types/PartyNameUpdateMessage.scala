package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyNameUpdateMessage(
  partyId: Int,
  partyName: String
) extends Message(6502)

object PartyNameUpdateMessage {
  implicit val codec: Codec[PartyNameUpdateMessage] =
    new Codec[PartyNameUpdateMessage] {
      def decode: Get[PartyNameUpdateMessage] =
        for {
          partyId <- varInt.decode
          partyName <- utf8(ushort).decode
        } yield PartyNameUpdateMessage(partyId, partyName)

      def encode(value: PartyNameUpdateMessage): ByteVector =
        varInt.encode(value.partyId) ++
        utf8(ushort).encode(value.partyName)
    }
}
