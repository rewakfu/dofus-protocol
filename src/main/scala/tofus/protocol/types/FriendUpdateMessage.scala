package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendUpdateMessage(
  friendUpdated: FriendInformations
) extends Message(5924)

object FriendUpdateMessage {
  implicit val codec: Codec[FriendUpdateMessage] =
    new Codec[FriendUpdateMessage] {
      def decode: Get[FriendUpdateMessage] =
        for {
          friendUpdated <- Codec[FriendInformations].decode
        } yield FriendUpdateMessage(friendUpdated)

      def encode(value: FriendUpdateMessage): ByteVector =
        Codec[FriendInformations].encode(value.friendUpdated)
    }
}
