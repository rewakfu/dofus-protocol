package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceVersatileInfoListMessage(
  alliances: List[AllianceVersatileInformations]
) extends Message(6436)

object AllianceVersatileInfoListMessage {
  implicit val codec: Codec[AllianceVersatileInfoListMessage] =
    new Codec[AllianceVersatileInfoListMessage] {
      def decode: Get[AllianceVersatileInfoListMessage] =
        for {
          alliances <- list(ushort, Codec[AllianceVersatileInformations]).decode
        } yield AllianceVersatileInfoListMessage(alliances)

      def encode(value: AllianceVersatileInfoListMessage): ByteVector =
        list(ushort, Codec[AllianceVersatileInformations]).encode(value.alliances)
    }
}
