package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuestModeMessage(
  active: Boolean
) extends Message(6505)

object GuestModeMessage {
  implicit val codec: Codec[GuestModeMessage] =
    new Codec[GuestModeMessage] {
      def decode: Get[GuestModeMessage] =
        for {
          active <- bool.decode
        } yield GuestModeMessage(active)

      def encode(value: GuestModeMessage): ByteVector =
        bool.encode(value.active)
    }
}
