package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapObstacleUpdateMessage(
  obstacles: List[MapObstacle]
) extends Message(6051)

object MapObstacleUpdateMessage {
  implicit val codec: Codec[MapObstacleUpdateMessage] =
    new Codec[MapObstacleUpdateMessage] {
      def decode: Get[MapObstacleUpdateMessage] =
        for {
          obstacles <- list(ushort, Codec[MapObstacle]).decode
        } yield MapObstacleUpdateMessage(obstacles)

      def encode(value: MapObstacleUpdateMessage): ByteVector =
        list(ushort, Codec[MapObstacle]).encode(value.obstacles)
    }
}
