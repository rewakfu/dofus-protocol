package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicLatencyStatsMessage(
  latency: Int,
  sampleCount: Short,
  max: Short
) extends Message(5663)

object BasicLatencyStatsMessage {
  implicit val codec: Codec[BasicLatencyStatsMessage] =
    new Codec[BasicLatencyStatsMessage] {
      def decode: Get[BasicLatencyStatsMessage] =
        for {
          latency <- ushort.decode
          sampleCount <- varShort.decode
          max <- varShort.decode
        } yield BasicLatencyStatsMessage(latency, sampleCount, max)

      def encode(value: BasicLatencyStatsMessage): ByteVector =
        ushort.encode(value.latency) ++
        varShort.encode(value.sampleCount) ++
        varShort.encode(value.max)
    }
}
