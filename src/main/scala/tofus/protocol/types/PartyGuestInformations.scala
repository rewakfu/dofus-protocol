package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyGuestInformations(
  guestId: Long,
  hostId: Long,
  name: String,
  guestLook: EntityLook,
  breed: Byte,
  sex: Boolean,
  status: PlayerStatus,
  entities: List[ConcretePartyEntityBaseInformation]
) extends ProtocolType {
  override val protocolId = 374
}

object PartyGuestInformations {
  implicit val codec: Codec[PartyGuestInformations] =
    new Codec[PartyGuestInformations] {
      def decode: Get[PartyGuestInformations] =
        for {
          guestId <- varLong.decode
          hostId <- varLong.decode
          name <- utf8(ushort).decode
          guestLook <- Codec[EntityLook].decode
          breed <- byte.decode
          sex <- bool.decode
          status <- Codec[PlayerStatus].decode
          entities <- list(ushort, Codec[ConcretePartyEntityBaseInformation]).decode
        } yield PartyGuestInformations(guestId, hostId, name, guestLook, breed, sex, status, entities)

      def encode(value: PartyGuestInformations): ByteVector =
        varLong.encode(value.guestId) ++
        varLong.encode(value.hostId) ++
        utf8(ushort).encode(value.name) ++
        Codec[EntityLook].encode(value.guestLook) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        Codec[PlayerStatus].encode(value.status) ++
        list(ushort, Codec[ConcretePartyEntityBaseInformation]).encode(value.entities)
    }
}
