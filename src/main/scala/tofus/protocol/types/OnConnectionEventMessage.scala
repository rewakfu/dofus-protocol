package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class OnConnectionEventMessage(
  eventType: Byte
) extends Message(5726)

object OnConnectionEventMessage {
  implicit val codec: Codec[OnConnectionEventMessage] =
    new Codec[OnConnectionEventMessage] {
      def decode: Get[OnConnectionEventMessage] =
        for {
          eventType <- byte.decode
        } yield OnConnectionEventMessage(eventType)

      def encode(value: OnConnectionEventMessage): ByteVector =
        byte.encode(value.eventType)
    }
}
