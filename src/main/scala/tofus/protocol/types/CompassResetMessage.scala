package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CompassResetMessage(
  `type`: Byte
) extends Message(5584)

object CompassResetMessage {
  implicit val codec: Codec[CompassResetMessage] =
    new Codec[CompassResetMessage] {
      def decode: Get[CompassResetMessage] =
        for {
          `type` <- byte.decode
        } yield CompassResetMessage(`type`)

      def encode(value: CompassResetMessage): ByteVector =
        byte.encode(value.`type`)
    }
}
