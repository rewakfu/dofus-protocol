package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeShopStockMultiMovementUpdatedMessage(
  objectInfoList: List[ConcreteObjectItemToSell]
) extends Message(6038)

object ExchangeShopStockMultiMovementUpdatedMessage {
  implicit val codec: Codec[ExchangeShopStockMultiMovementUpdatedMessage] =
    new Codec[ExchangeShopStockMultiMovementUpdatedMessage] {
      def decode: Get[ExchangeShopStockMultiMovementUpdatedMessage] =
        for {
          objectInfoList <- list(ushort, Codec[ConcreteObjectItemToSell]).decode
        } yield ExchangeShopStockMultiMovementUpdatedMessage(objectInfoList)

      def encode(value: ExchangeShopStockMultiMovementUpdatedMessage): ByteVector =
        list(ushort, Codec[ConcreteObjectItemToSell]).encode(value.objectInfoList)
    }
}
