package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeShopStockMovementUpdatedMessage(
  objectInfo: ConcreteObjectItemToSell
) extends Message(5909)

object ExchangeShopStockMovementUpdatedMessage {
  implicit val codec: Codec[ExchangeShopStockMovementUpdatedMessage] =
    new Codec[ExchangeShopStockMovementUpdatedMessage] {
      def decode: Get[ExchangeShopStockMovementUpdatedMessage] =
        for {
          objectInfo <- Codec[ConcreteObjectItemToSell].decode
        } yield ExchangeShopStockMovementUpdatedMessage(objectInfo)

      def encode(value: ExchangeShopStockMovementUpdatedMessage): ByteVector =
        Codec[ConcreteObjectItemToSell].encode(value.objectInfo)
    }
}
