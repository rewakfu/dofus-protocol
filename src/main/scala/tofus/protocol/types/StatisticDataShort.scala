package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatisticDataShort(
  value: Short
) extends StatisticData {
  override val protocolId = 488
}

object StatisticDataShort {
  implicit val codec: Codec[StatisticDataShort] =
    new Codec[StatisticDataShort] {
      def decode: Get[StatisticDataShort] =
        for {
          value <- short.decode
        } yield StatisticDataShort(value)

      def encode(value: StatisticDataShort): ByteVector =
        short.encode(value.value)
    }
}
