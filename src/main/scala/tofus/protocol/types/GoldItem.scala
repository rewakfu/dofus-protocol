package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GoldItem(
  sum: Long
) extends Item {
  override val protocolId = 123
}

object GoldItem {
  implicit val codec: Codec[GoldItem] =
    new Codec[GoldItem] {
      def decode: Get[GoldItem] =
        for {
          sum <- varLong.decode
        } yield GoldItem(sum)

      def encode(value: GoldItem): ByteVector =
        varLong.encode(value.sum)
    }
}
