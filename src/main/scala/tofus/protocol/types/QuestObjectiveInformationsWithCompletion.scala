package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestObjectiveInformationsWithCompletion(
  objectiveId: Short,
  objectiveStatus: Boolean,
  dialogParams: List[String],
  curCompletion: Short,
  maxCompletion: Short
) extends QuestObjectiveInformations {
  override val protocolId = 386
}

object QuestObjectiveInformationsWithCompletion {
  implicit val codec: Codec[QuestObjectiveInformationsWithCompletion] =
    new Codec[QuestObjectiveInformationsWithCompletion] {
      def decode: Get[QuestObjectiveInformationsWithCompletion] =
        for {
          objectiveId <- varShort.decode
          objectiveStatus <- bool.decode
          dialogParams <- list(ushort, utf8(ushort)).decode
          curCompletion <- varShort.decode
          maxCompletion <- varShort.decode
        } yield QuestObjectiveInformationsWithCompletion(objectiveId, objectiveStatus, dialogParams, curCompletion, maxCompletion)

      def encode(value: QuestObjectiveInformationsWithCompletion): ByteVector =
        varShort.encode(value.objectiveId) ++
        bool.encode(value.objectiveStatus) ++
        list(ushort, utf8(ushort)).encode(value.dialogParams) ++
        varShort.encode(value.curCompletion) ++
        varShort.encode(value.maxCompletion)
    }
}
