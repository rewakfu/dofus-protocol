package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionAlliance(
  allianceInformations: ConcreteAllianceInformations,
  aggressable: Byte
) extends HumanOption {
  override val protocolId = 425
}

object HumanOptionAlliance {
  implicit val codec: Codec[HumanOptionAlliance] =
    new Codec[HumanOptionAlliance] {
      def decode: Get[HumanOptionAlliance] =
        for {
          allianceInformations <- Codec[ConcreteAllianceInformations].decode
          aggressable <- byte.decode
        } yield HumanOptionAlliance(allianceInformations, aggressable)

      def encode(value: HumanOptionAlliance): ByteVector =
        Codec[ConcreteAllianceInformations].encode(value.allianceInformations) ++
        byte.encode(value.aggressable)
    }
}
