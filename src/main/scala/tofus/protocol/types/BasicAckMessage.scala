package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicAckMessage(
  seq: Int,
  lastPacketId: Short
) extends Message(6362)

object BasicAckMessage {
  implicit val codec: Codec[BasicAckMessage] =
    new Codec[BasicAckMessage] {
      def decode: Get[BasicAckMessage] =
        for {
          seq <- varInt.decode
          lastPacketId <- varShort.decode
        } yield BasicAckMessage(seq, lastPacketId)

      def encode(value: BasicAckMessage): ByteVector =
        varInt.encode(value.seq) ++
        varShort.encode(value.lastPacketId)
    }
}
