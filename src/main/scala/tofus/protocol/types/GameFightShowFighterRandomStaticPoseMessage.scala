package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightShowFighterRandomStaticPoseMessage(
  informations: GameFightFighterInformations
) extends Message(6218)

object GameFightShowFighterRandomStaticPoseMessage {
  implicit val codec: Codec[GameFightShowFighterRandomStaticPoseMessage] =
    new Codec[GameFightShowFighterRandomStaticPoseMessage] {
      def decode: Get[GameFightShowFighterRandomStaticPoseMessage] =
        for {
          informations <- Codec[GameFightFighterInformations].decode
        } yield GameFightShowFighterRandomStaticPoseMessage(informations)

      def encode(value: GameFightShowFighterRandomStaticPoseMessage): ByteVector =
        Codec[GameFightFighterInformations].encode(value.informations)
    }
}
