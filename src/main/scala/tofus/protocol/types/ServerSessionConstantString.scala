package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerSessionConstantString(
  id: Short,
  value: String
) extends ServerSessionConstant {
  override val protocolId = 436
}

object ServerSessionConstantString {
  implicit val codec: Codec[ServerSessionConstantString] =
    new Codec[ServerSessionConstantString] {
      def decode: Get[ServerSessionConstantString] =
        for {
          id <- varShort.decode
          value <- utf8(ushort).decode
        } yield ServerSessionConstantString(id, value)

      def encode(value: ServerSessionConstantString): ByteVector =
        varShort.encode(value.id) ++
        utf8(ushort).encode(value.value)
    }
}
