package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextMoveElementMessage(
  movement: EntityMovementInformations
) extends Message(253)

object GameContextMoveElementMessage {
  implicit val codec: Codec[GameContextMoveElementMessage] =
    new Codec[GameContextMoveElementMessage] {
      def decode: Get[GameContextMoveElementMessage] =
        for {
          movement <- Codec[EntityMovementInformations].decode
        } yield GameContextMoveElementMessage(movement)

      def encode(value: GameContextMoveElementMessage): ByteVector =
        Codec[EntityMovementInformations].encode(value.movement)
    }
}
