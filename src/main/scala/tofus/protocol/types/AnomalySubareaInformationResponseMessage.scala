package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AnomalySubareaInformationResponseMessage(
  subareas: List[AnomalySubareaInformation]
) extends Message(6836)

object AnomalySubareaInformationResponseMessage {
  implicit val codec: Codec[AnomalySubareaInformationResponseMessage] =
    new Codec[AnomalySubareaInformationResponseMessage] {
      def decode: Get[AnomalySubareaInformationResponseMessage] =
        for {
          subareas <- list(ushort, Codec[AnomalySubareaInformation]).decode
        } yield AnomalySubareaInformationResponseMessage(subareas)

      def encode(value: AnomalySubareaInformationResponseMessage): ByteVector =
        list(ushort, Codec[AnomalySubareaInformation]).encode(value.subareas)
    }
}
