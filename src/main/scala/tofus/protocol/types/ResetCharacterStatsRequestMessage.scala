package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ResetCharacterStatsRequestMessage(

) extends Message(6739)

object ResetCharacterStatsRequestMessage {
  implicit val codec: Codec[ResetCharacterStatsRequestMessage] =
    Codec.const(ResetCharacterStatsRequestMessage())
}
