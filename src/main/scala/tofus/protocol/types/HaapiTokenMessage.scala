package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiTokenMessage(
  token: String
) extends Message(6767)

object HaapiTokenMessage {
  implicit val codec: Codec[HaapiTokenMessage] =
    new Codec[HaapiTokenMessage] {
      def decode: Get[HaapiTokenMessage] =
        for {
          token <- utf8(ushort).decode
        } yield HaapiTokenMessage(token)

      def encode(value: HaapiTokenMessage): ByteVector =
        utf8(ushort).encode(value.token)
    }
}
