package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicLatencyStatsRequestMessage(

) extends Message(5816)

object BasicLatencyStatsRequestMessage {
  implicit val codec: Codec[BasicLatencyStatsRequestMessage] =
    Codec.const(BasicLatencyStatsRequestMessage())
}
