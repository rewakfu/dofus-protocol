package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FightResultFighterListEntry extends FightResultListEntry

final case class ConcreteFightResultFighterListEntry(
  outcome: Short,
  wave: Byte,
  rewards: FightLoot,
  id: Double,
  alive: Boolean
) extends FightResultFighterListEntry {
  override val protocolId = 189
}

object ConcreteFightResultFighterListEntry {
  implicit val codec: Codec[ConcreteFightResultFighterListEntry] =  
    new Codec[ConcreteFightResultFighterListEntry] {
      def decode: Get[ConcreteFightResultFighterListEntry] =
        for {
          outcome <- varShort.decode
          wave <- byte.decode
          rewards <- Codec[FightLoot].decode
          id <- double.decode
          alive <- bool.decode
        } yield ConcreteFightResultFighterListEntry(outcome, wave, rewards, id, alive)

      def encode(value: ConcreteFightResultFighterListEntry): ByteVector =
        varShort.encode(value.outcome) ++
        byte.encode(value.wave) ++
        Codec[FightLoot].encode(value.rewards) ++
        double.encode(value.id) ++
        bool.encode(value.alive)
    }
}

object FightResultFighterListEntry {
  implicit val codec: Codec[FightResultFighterListEntry] =
    new Codec[FightResultFighterListEntry] {
      def decode: Get[FightResultFighterListEntry] =
        ushort.decode.flatMap {
          case 189 => Codec[ConcreteFightResultFighterListEntry].decode
          case 24 => Codec[FightResultPlayerListEntry].decode
          case 216 => Codec[FightResultMutantListEntry].decode
          case 84 => Codec[FightResultTaxCollectorListEntry].decode
        }

      def encode(value: FightResultFighterListEntry): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFightResultFighterListEntry => Codec[ConcreteFightResultFighterListEntry].encode(i)
          case i: FightResultPlayerListEntry => Codec[FightResultPlayerListEntry].encode(i)
          case i: FightResultMutantListEntry => Codec[FightResultMutantListEntry].encode(i)
          case i: FightResultTaxCollectorListEntry => Codec[FightResultTaxCollectorListEntry].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
