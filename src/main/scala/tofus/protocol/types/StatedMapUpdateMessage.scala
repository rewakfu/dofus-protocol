package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatedMapUpdateMessage(
  statedElements: List[StatedElement]
) extends Message(5716)

object StatedMapUpdateMessage {
  implicit val codec: Codec[StatedMapUpdateMessage] =
    new Codec[StatedMapUpdateMessage] {
      def decode: Get[StatedMapUpdateMessage] =
        for {
          statedElements <- list(ushort, Codec[StatedElement]).decode
        } yield StatedMapUpdateMessage(statedElements)

      def encode(value: StatedMapUpdateMessage): ByteVector =
        list(ushort, Codec[StatedElement]).encode(value.statedElements)
    }
}
