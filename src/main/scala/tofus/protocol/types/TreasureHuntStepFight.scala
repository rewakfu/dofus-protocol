package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntStepFight(

) extends ProtocolType {
  override val protocolId = 462
}

object TreasureHuntStepFight {
  implicit val codec: Codec[TreasureHuntStepFight] =
    Codec.const(TreasureHuntStepFight())
}
