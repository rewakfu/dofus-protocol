package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightCharacterInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short],
  name: String,
  status: ConcretePlayerStatus,
  leagueId: Short,
  ladderPosition: Int,
  hiddenInPrefight: Boolean,
  level: Short,
  alignmentInfos: ConcreteActorAlignmentInformations,
  breed: Byte,
  sex: Boolean
) extends GameFightFighterNamedInformations {
  override val protocolId = 46
}

object GameFightCharacterInformations {
  implicit val codec: Codec[GameFightCharacterInformations] =
    new Codec[GameFightCharacterInformations] {
      def decode: Get[GameFightCharacterInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
          name <- utf8(ushort).decode
          status <- Codec[ConcretePlayerStatus].decode
          leagueId <- varShort.decode
          ladderPosition <- int.decode
          hiddenInPrefight <- bool.decode
          level <- varShort.decode
          alignmentInfos <- Codec[ConcreteActorAlignmentInformations].decode
          breed <- byte.decode
          sex <- bool.decode
        } yield GameFightCharacterInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions, name, status, leagueId, ladderPosition, hiddenInPrefight, level, alignmentInfos, breed, sex)

      def encode(value: GameFightCharacterInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions) ++
        utf8(ushort).encode(value.name) ++
        Codec[ConcretePlayerStatus].encode(value.status) ++
        varShort.encode(value.leagueId) ++
        int.encode(value.ladderPosition) ++
        bool.encode(value.hiddenInPrefight) ++
        varShort.encode(value.level) ++
        Codec[ConcreteActorAlignmentInformations].encode(value.alignmentInfos) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex)
    }
}
