package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestStepValidatedMessage(
  questId: Short,
  stepId: Short
) extends Message(6099)

object QuestStepValidatedMessage {
  implicit val codec: Codec[QuestStepValidatedMessage] =
    new Codec[QuestStepValidatedMessage] {
      def decode: Get[QuestStepValidatedMessage] =
        for {
          questId <- varShort.decode
          stepId <- varShort.decode
        } yield QuestStepValidatedMessage(questId, stepId)

      def encode(value: QuestStepValidatedMessage): ByteVector =
        varShort.encode(value.questId) ++
        varShort.encode(value.stepId)
    }
}
