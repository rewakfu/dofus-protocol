package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NpcDialogQuestionMessage(
  messageId: Int,
  dialogParams: List[String],
  visibleReplies: List[Int]
) extends Message(5617)

object NpcDialogQuestionMessage {
  implicit val codec: Codec[NpcDialogQuestionMessage] =
    new Codec[NpcDialogQuestionMessage] {
      def decode: Get[NpcDialogQuestionMessage] =
        for {
          messageId <- varInt.decode
          dialogParams <- list(ushort, utf8(ushort)).decode
          visibleReplies <- list(ushort, varInt).decode
        } yield NpcDialogQuestionMessage(messageId, dialogParams, visibleReplies)

      def encode(value: NpcDialogQuestionMessage): ByteVector =
        varInt.encode(value.messageId) ++
        list(ushort, utf8(ushort)).encode(value.dialogParams) ++
        list(ushort, varInt).encode(value.visibleReplies)
    }
}
