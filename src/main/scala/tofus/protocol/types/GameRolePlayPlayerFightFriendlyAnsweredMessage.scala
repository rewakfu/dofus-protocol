package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayPlayerFightFriendlyAnsweredMessage(
  fightId: Short,
  sourceId: Long,
  targetId: Long,
  accept: Boolean
) extends Message(5733)

object GameRolePlayPlayerFightFriendlyAnsweredMessage {
  implicit val codec: Codec[GameRolePlayPlayerFightFriendlyAnsweredMessage] =
    new Codec[GameRolePlayPlayerFightFriendlyAnsweredMessage] {
      def decode: Get[GameRolePlayPlayerFightFriendlyAnsweredMessage] =
        for {
          fightId <- varShort.decode
          sourceId <- varLong.decode
          targetId <- varLong.decode
          accept <- bool.decode
        } yield GameRolePlayPlayerFightFriendlyAnsweredMessage(fightId, sourceId, targetId, accept)

      def encode(value: GameRolePlayPlayerFightFriendlyAnsweredMessage): ByteVector =
        varShort.encode(value.fightId) ++
        varLong.encode(value.sourceId) ++
        varLong.encode(value.targetId) ++
        bool.encode(value.accept)
    }
}
