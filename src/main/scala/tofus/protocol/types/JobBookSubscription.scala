package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobBookSubscription(
  jobId: Byte,
  subscribed: Boolean
) extends ProtocolType {
  override val protocolId = 500
}

object JobBookSubscription {
  implicit val codec: Codec[JobBookSubscription] =
    new Codec[JobBookSubscription] {
      def decode: Get[JobBookSubscription] =
        for {
          jobId <- byte.decode
          subscribed <- bool.decode
        } yield JobBookSubscription(jobId, subscribed)

      def encode(value: JobBookSubscription): ByteVector =
        byte.encode(value.jobId) ++
        bool.encode(value.subscribed)
    }
}
