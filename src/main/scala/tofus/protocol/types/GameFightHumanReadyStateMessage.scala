package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightHumanReadyStateMessage(
  characterId: Long,
  isReady: Boolean
) extends Message(740)

object GameFightHumanReadyStateMessage {
  implicit val codec: Codec[GameFightHumanReadyStateMessage] =
    new Codec[GameFightHumanReadyStateMessage] {
      def decode: Get[GameFightHumanReadyStateMessage] =
        for {
          characterId <- varLong.decode
          isReady <- bool.decode
        } yield GameFightHumanReadyStateMessage(characterId, isReady)

      def encode(value: GameFightHumanReadyStateMessage): ByteVector =
        varLong.encode(value.characterId) ++
        bool.encode(value.isReady)
    }
}
