package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementDetailedListMessage(
  startedAchievements: List[Achievement],
  finishedAchievements: List[Achievement]
) extends Message(6358)

object AchievementDetailedListMessage {
  implicit val codec: Codec[AchievementDetailedListMessage] =
    new Codec[AchievementDetailedListMessage] {
      def decode: Get[AchievementDetailedListMessage] =
        for {
          startedAchievements <- list(ushort, Codec[Achievement]).decode
          finishedAchievements <- list(ushort, Codec[Achievement]).decode
        } yield AchievementDetailedListMessage(startedAchievements, finishedAchievements)

      def encode(value: AchievementDetailedListMessage): ByteVector =
        list(ushort, Codec[Achievement]).encode(value.startedAchievements) ++
        list(ushort, Codec[Achievement]).encode(value.finishedAchievements)
    }
}
