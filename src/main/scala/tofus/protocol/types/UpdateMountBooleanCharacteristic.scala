package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class UpdateMountBooleanCharacteristic(
  `type`: Byte,
  value: Boolean
) extends UpdateMountCharacteristic {
  override val protocolId = 538
}

object UpdateMountBooleanCharacteristic {
  implicit val codec: Codec[UpdateMountBooleanCharacteristic] =
    new Codec[UpdateMountBooleanCharacteristic] {
      def decode: Get[UpdateMountBooleanCharacteristic] =
        for {
          `type` <- byte.decode
          value <- bool.decode
        } yield UpdateMountBooleanCharacteristic(`type`, value)

      def encode(value: UpdateMountBooleanCharacteristic): ByteVector =
        byte.encode(value.`type`) ++
        bool.encode(value.value)
    }
}
