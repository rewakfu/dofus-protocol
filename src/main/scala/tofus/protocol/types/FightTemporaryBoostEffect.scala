package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FightTemporaryBoostEffect extends AbstractFightDispellableEffect

final case class ConcreteFightTemporaryBoostEffect(
  uid: Int,
  targetId: Double,
  turnDuration: Short,
  dispelable: Byte,
  spellId: Short,
  effectId: Int,
  parentBoostUid: Int,
  delta: Int
) extends FightTemporaryBoostEffect {
  override val protocolId = 209
}

object ConcreteFightTemporaryBoostEffect {
  implicit val codec: Codec[ConcreteFightTemporaryBoostEffect] =  
    new Codec[ConcreteFightTemporaryBoostEffect] {
      def decode: Get[ConcreteFightTemporaryBoostEffect] =
        for {
          uid <- varInt.decode
          targetId <- double.decode
          turnDuration <- short.decode
          dispelable <- byte.decode
          spellId <- varShort.decode
          effectId <- varInt.decode
          parentBoostUid <- varInt.decode
          delta <- int.decode
        } yield ConcreteFightTemporaryBoostEffect(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, delta)

      def encode(value: ConcreteFightTemporaryBoostEffect): ByteVector =
        varInt.encode(value.uid) ++
        double.encode(value.targetId) ++
        short.encode(value.turnDuration) ++
        byte.encode(value.dispelable) ++
        varShort.encode(value.spellId) ++
        varInt.encode(value.effectId) ++
        varInt.encode(value.parentBoostUid) ++
        int.encode(value.delta)
    }
}

object FightTemporaryBoostEffect {
  implicit val codec: Codec[FightTemporaryBoostEffect] =
    new Codec[FightTemporaryBoostEffect] {
      def decode: Get[FightTemporaryBoostEffect] =
        ushort.decode.flatMap {
          case 209 => Codec[ConcreteFightTemporaryBoostEffect].decode
          case 207 => Codec[FightTemporarySpellBoostEffect].decode
          case 214 => Codec[FightTemporaryBoostStateEffect].decode
          case 211 => Codec[FightTemporaryBoostWeaponDamagesEffect].decode
        }

      def encode(value: FightTemporaryBoostEffect): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFightTemporaryBoostEffect => Codec[ConcreteFightTemporaryBoostEffect].encode(i)
          case i: FightTemporarySpellBoostEffect => Codec[FightTemporarySpellBoostEffect].encode(i)
          case i: FightTemporaryBoostStateEffect => Codec[FightTemporaryBoostStateEffect].encode(i)
          case i: FightTemporaryBoostWeaponDamagesEffect => Codec[FightTemporaryBoostWeaponDamagesEffect].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
