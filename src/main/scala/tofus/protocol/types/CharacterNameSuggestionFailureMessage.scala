package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterNameSuggestionFailureMessage(
  reason: Byte
) extends Message(164)

object CharacterNameSuggestionFailureMessage {
  implicit val codec: Codec[CharacterNameSuggestionFailureMessage] =
    new Codec[CharacterNameSuggestionFailureMessage] {
      def decode: Get[CharacterNameSuggestionFailureMessage] =
        for {
          reason <- byte.decode
        } yield CharacterNameSuggestionFailureMessage(reason)

      def encode(value: CharacterNameSuggestionFailureMessage): ByteVector =
        byte.encode(value.reason)
    }
}
