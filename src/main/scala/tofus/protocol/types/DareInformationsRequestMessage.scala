package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareInformationsRequestMessage(
  dareId: Double
) extends Message(6659)

object DareInformationsRequestMessage {
  implicit val codec: Codec[DareInformationsRequestMessage] =
    new Codec[DareInformationsRequestMessage] {
      def decode: Get[DareInformationsRequestMessage] =
        for {
          dareId <- double.decode
        } yield DareInformationsRequestMessage(dareId)

      def encode(value: DareInformationsRequestMessage): ByteVector =
        double.encode(value.dareId)
    }
}
