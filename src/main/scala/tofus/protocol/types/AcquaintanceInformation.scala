package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AcquaintanceInformation extends AbstractContactInformations

final case class ConcreteAcquaintanceInformation(
  accountId: Int,
  accountName: String,
  playerState: Byte
) extends AcquaintanceInformation {
  override val protocolId = 561
}

object ConcreteAcquaintanceInformation {
  implicit val codec: Codec[ConcreteAcquaintanceInformation] =  
    new Codec[ConcreteAcquaintanceInformation] {
      def decode: Get[ConcreteAcquaintanceInformation] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
          playerState <- byte.decode
        } yield ConcreteAcquaintanceInformation(accountId, accountName, playerState)

      def encode(value: ConcreteAcquaintanceInformation): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName) ++
        byte.encode(value.playerState)
    }
}

object AcquaintanceInformation {
  implicit val codec: Codec[AcquaintanceInformation] =
    new Codec[AcquaintanceInformation] {
      def decode: Get[AcquaintanceInformation] =
        ushort.decode.flatMap {
          case 561 => Codec[ConcreteAcquaintanceInformation].decode
          case 562 => Codec[AcquaintanceOnlineInformation].decode
        }

      def encode(value: AcquaintanceInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAcquaintanceInformation => Codec[ConcreteAcquaintanceInformation].encode(i)
          case i: AcquaintanceOnlineInformation => Codec[AcquaintanceOnlineInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
