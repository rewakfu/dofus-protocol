package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBuyOkMessage(

) extends Message(5759)

object ExchangeBuyOkMessage {
  implicit val codec: Codec[ExchangeBuyOkMessage] =
    Codec.const(ExchangeBuyOkMessage())
}
