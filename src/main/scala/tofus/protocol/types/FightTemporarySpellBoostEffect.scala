package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTemporarySpellBoostEffect(
  uid: Int,
  targetId: Double,
  turnDuration: Short,
  dispelable: Byte,
  spellId: Short,
  effectId: Int,
  parentBoostUid: Int,
  delta: Int,
  boostedSpellId: Short
) extends FightTemporaryBoostEffect {
  override val protocolId = 207
}

object FightTemporarySpellBoostEffect {
  implicit val codec: Codec[FightTemporarySpellBoostEffect] =
    new Codec[FightTemporarySpellBoostEffect] {
      def decode: Get[FightTemporarySpellBoostEffect] =
        for {
          uid <- varInt.decode
          targetId <- double.decode
          turnDuration <- short.decode
          dispelable <- byte.decode
          spellId <- varShort.decode
          effectId <- varInt.decode
          parentBoostUid <- varInt.decode
          delta <- int.decode
          boostedSpellId <- varShort.decode
        } yield FightTemporarySpellBoostEffect(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, delta, boostedSpellId)

      def encode(value: FightTemporarySpellBoostEffect): ByteVector =
        varInt.encode(value.uid) ++
        double.encode(value.targetId) ++
        short.encode(value.turnDuration) ++
        byte.encode(value.dispelable) ++
        varShort.encode(value.spellId) ++
        varInt.encode(value.effectId) ++
        varInt.encode(value.parentBoostUid) ++
        int.encode(value.delta) ++
        varShort.encode(value.boostedSpellId)
    }
}
