package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TrustCertificate(
  id: Int,
  hash: String
) extends ProtocolType {
  override val protocolId = 377
}

object TrustCertificate {
  implicit val codec: Codec[TrustCertificate] =
    new Codec[TrustCertificate] {
      def decode: Get[TrustCertificate] =
        for {
          id <- int.decode
          hash <- utf8(ushort).decode
        } yield TrustCertificate(id, hash)

      def encode(value: TrustCertificate): ByteVector =
        int.encode(value.id) ++
        utf8(ushort).encode(value.hash)
    }
}
