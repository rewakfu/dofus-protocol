package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationMessage(
  partyId: Int,
  partyType: Byte,
  partyName: String,
  maxParticipants: Byte,
  fromId: Long,
  fromName: String,
  toId: Long
) extends Message(5586)

object PartyInvitationMessage {
  implicit val codec: Codec[PartyInvitationMessage] =
    new Codec[PartyInvitationMessage] {
      def decode: Get[PartyInvitationMessage] =
        for {
          partyId <- varInt.decode
          partyType <- byte.decode
          partyName <- utf8(ushort).decode
          maxParticipants <- byte.decode
          fromId <- varLong.decode
          fromName <- utf8(ushort).decode
          toId <- varLong.decode
        } yield PartyInvitationMessage(partyId, partyType, partyName, maxParticipants, fromId, fromName, toId)

      def encode(value: PartyInvitationMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.partyType) ++
        utf8(ushort).encode(value.partyName) ++
        byte.encode(value.maxParticipants) ++
        varLong.encode(value.fromId) ++
        utf8(ushort).encode(value.fromName) ++
        varLong.encode(value.toId)
    }
}
