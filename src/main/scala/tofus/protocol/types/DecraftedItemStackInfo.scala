package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DecraftedItemStackInfo(
  objectUID: Int,
  bonusMin: Float,
  bonusMax: Float,
  runesId: List[Short],
  runesQty: List[Int]
) extends ProtocolType {
  override val protocolId = 481
}

object DecraftedItemStackInfo {
  implicit val codec: Codec[DecraftedItemStackInfo] =
    new Codec[DecraftedItemStackInfo] {
      def decode: Get[DecraftedItemStackInfo] =
        for {
          objectUID <- varInt.decode
          bonusMin <- float.decode
          bonusMax <- float.decode
          runesId <- list(ushort, varShort).decode
          runesQty <- list(ushort, varInt).decode
        } yield DecraftedItemStackInfo(objectUID, bonusMin, bonusMax, runesId, runesQty)

      def encode(value: DecraftedItemStackInfo): ByteVector =
        varInt.encode(value.objectUID) ++
        float.encode(value.bonusMin) ++
        float.encode(value.bonusMax) ++
        list(ushort, varShort).encode(value.runesId) ++
        list(ushort, varInt).encode(value.runesQty)
    }
}
