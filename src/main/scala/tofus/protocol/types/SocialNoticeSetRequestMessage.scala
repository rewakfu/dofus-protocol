package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SocialNoticeSetRequestMessage(

) extends Message(6686)

object SocialNoticeSetRequestMessage {
  implicit val codec: Codec[SocialNoticeSetRequestMessage] =
    Codec.const(SocialNoticeSetRequestMessage())
}
