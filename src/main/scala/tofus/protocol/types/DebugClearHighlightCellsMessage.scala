package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DebugClearHighlightCellsMessage(

) extends Message(2002)

object DebugClearHighlightCellsMessage {
  implicit val codec: Codec[DebugClearHighlightCellsMessage] =
    Codec.const(DebugClearHighlightCellsMessage())
}
