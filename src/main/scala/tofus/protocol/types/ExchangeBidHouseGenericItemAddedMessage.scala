package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseGenericItemAddedMessage(
  objGenericId: Short
) extends Message(5947)

object ExchangeBidHouseGenericItemAddedMessage {
  implicit val codec: Codec[ExchangeBidHouseGenericItemAddedMessage] =
    new Codec[ExchangeBidHouseGenericItemAddedMessage] {
      def decode: Get[ExchangeBidHouseGenericItemAddedMessage] =
        for {
          objGenericId <- varShort.decode
        } yield ExchangeBidHouseGenericItemAddedMessage(objGenericId)

      def encode(value: ExchangeBidHouseGenericItemAddedMessage): ByteVector =
        varShort.encode(value.objGenericId)
    }
}
