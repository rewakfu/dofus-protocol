package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountRidingMessage(
  flags0: Byte
) extends Message(5967)

object MountRidingMessage {
  implicit val codec: Codec[MountRidingMessage] =
    new Codec[MountRidingMessage] {
      def decode: Get[MountRidingMessage] =
        for {
          flags0 <- byte.decode
        } yield MountRidingMessage(flags0)

      def encode(value: MountRidingMessage): ByteVector =
        byte.encode(value.flags0)
    }
}
