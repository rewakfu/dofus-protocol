package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServersListMessage(
  servers: List[GameServerInformations],
  alreadyConnectedToServerId: Short,
  canCreateNewCharacter: Boolean
) extends Message(30)

object ServersListMessage {
  implicit val codec: Codec[ServersListMessage] =
    new Codec[ServersListMessage] {
      def decode: Get[ServersListMessage] =
        for {
          servers <- list(ushort, Codec[GameServerInformations]).decode
          alreadyConnectedToServerId <- varShort.decode
          canCreateNewCharacter <- bool.decode
        } yield ServersListMessage(servers, alreadyConnectedToServerId, canCreateNewCharacter)

      def encode(value: ServersListMessage): ByteVector =
        list(ushort, Codec[GameServerInformations]).encode(value.servers) ++
        varShort.encode(value.alreadyConnectedToServerId) ++
        bool.encode(value.canCreateNewCharacter)
    }
}
