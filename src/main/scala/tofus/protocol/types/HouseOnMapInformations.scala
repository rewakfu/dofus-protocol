package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseOnMapInformations(
  houseId: Int,
  modelId: Short,
  doorsOnMap: List[Int],
  houseInstances: List[ConcreteHouseInstanceInformations]
) extends HouseInformations {
  override val protocolId = 510
}

object HouseOnMapInformations {
  implicit val codec: Codec[HouseOnMapInformations] =
    new Codec[HouseOnMapInformations] {
      def decode: Get[HouseOnMapInformations] =
        for {
          houseId <- varInt.decode
          modelId <- varShort.decode
          doorsOnMap <- list(ushort, int).decode
          houseInstances <- list(ushort, Codec[ConcreteHouseInstanceInformations]).decode
        } yield HouseOnMapInformations(houseId, modelId, doorsOnMap, houseInstances)

      def encode(value: HouseOnMapInformations): ByteVector =
        varInt.encode(value.houseId) ++
        varShort.encode(value.modelId) ++
        list(ushort, int).encode(value.doorsOnMap) ++
        list(ushort, Codec[ConcreteHouseInstanceInformations]).encode(value.houseInstances)
    }
}
