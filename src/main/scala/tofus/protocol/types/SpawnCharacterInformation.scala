package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpawnCharacterInformation(
  name: String,
  level: Short
) extends SpawnInformation {
  override val protocolId = 574
}

object SpawnCharacterInformation {
  implicit val codec: Codec[SpawnCharacterInformation] =
    new Codec[SpawnCharacterInformation] {
      def decode: Get[SpawnCharacterInformation] =
        for {
          name <- utf8(ushort).decode
          level <- varShort.decode
        } yield SpawnCharacterInformation(name, level)

      def encode(value: SpawnCharacterInformation): ByteVector =
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level)
    }
}
