package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutObjectItem(
  slot: Byte,
  itemUID: Int,
  itemGID: Int
) extends ShortcutObject {
  override val protocolId = 371
}

object ShortcutObjectItem {
  implicit val codec: Codec[ShortcutObjectItem] =
    new Codec[ShortcutObjectItem] {
      def decode: Get[ShortcutObjectItem] =
        for {
          slot <- byte.decode
          itemUID <- int.decode
          itemGID <- int.decode
        } yield ShortcutObjectItem(slot, itemUID, itemGID)

      def encode(value: ShortcutObjectItem): ByteVector =
        byte.encode(value.slot) ++
        int.encode(value.itemUID) ++
        int.encode(value.itemGID)
    }
}
