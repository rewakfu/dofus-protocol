package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorListMessage(
  informations: List[TaxCollectorInformations],
  nbcollectorMax: Byte,
  fightersInformations: List[TaxCollectorFightersInformation],
  infoType: Byte
) extends Message(5930)

object TaxCollectorListMessage {
  implicit val codec: Codec[TaxCollectorListMessage] =
    new Codec[TaxCollectorListMessage] {
      def decode: Get[TaxCollectorListMessage] =
        for {
          informations <- list(ushort, Codec[TaxCollectorInformations]).decode
          nbcollectorMax <- byte.decode
          fightersInformations <- list(ushort, Codec[TaxCollectorFightersInformation]).decode
          infoType <- byte.decode
        } yield TaxCollectorListMessage(informations, nbcollectorMax, fightersInformations, infoType)

      def encode(value: TaxCollectorListMessage): ByteVector =
        list(ushort, Codec[TaxCollectorInformations]).encode(value.informations) ++
        byte.encode(value.nbcollectorMax) ++
        list(ushort, Codec[TaxCollectorFightersInformation]).encode(value.fightersInformations) ++
        byte.encode(value.infoType)
    }
}
