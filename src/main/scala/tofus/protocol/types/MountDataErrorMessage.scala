package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountDataErrorMessage(
  reason: Byte
) extends Message(6172)

object MountDataErrorMessage {
  implicit val codec: Codec[MountDataErrorMessage] =
    new Codec[MountDataErrorMessage] {
      def decode: Get[MountDataErrorMessage] =
        for {
          reason <- byte.decode
        } yield MountDataErrorMessage(reason)

      def encode(value: MountDataErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
