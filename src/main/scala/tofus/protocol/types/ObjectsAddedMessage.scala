package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectsAddedMessage(
  `object`: List[ObjectItem]
) extends Message(6033)

object ObjectsAddedMessage {
  implicit val codec: Codec[ObjectsAddedMessage] =
    new Codec[ObjectsAddedMessage] {
      def decode: Get[ObjectsAddedMessage] =
        for {
          `object` <- list(ushort, Codec[ObjectItem]).decode
        } yield ObjectsAddedMessage(`object`)

      def encode(value: ObjectsAddedMessage): ByteVector =
        list(ushort, Codec[ObjectItem]).encode(value.`object`)
    }
}
