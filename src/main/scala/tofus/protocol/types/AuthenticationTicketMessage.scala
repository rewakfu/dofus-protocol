package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AuthenticationTicketMessage(
  lang: String,
  ticket: String
) extends Message(110)

object AuthenticationTicketMessage {
  implicit val codec: Codec[AuthenticationTicketMessage] =
    new Codec[AuthenticationTicketMessage] {
      def decode: Get[AuthenticationTicketMessage] =
        for {
          lang <- utf8(ushort).decode
          ticket <- utf8(ushort).decode
        } yield AuthenticationTicketMessage(lang, ticket)

      def encode(value: AuthenticationTicketMessage): ByteVector =
        utf8(ushort).encode(value.lang) ++
        utf8(ushort).encode(value.ticket)
    }
}
