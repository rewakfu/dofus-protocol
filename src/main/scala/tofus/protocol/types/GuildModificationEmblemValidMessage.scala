package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildModificationEmblemValidMessage(
  guildEmblem: GuildEmblem
) extends Message(6328)

object GuildModificationEmblemValidMessage {
  implicit val codec: Codec[GuildModificationEmblemValidMessage] =
    new Codec[GuildModificationEmblemValidMessage] {
      def decode: Get[GuildModificationEmblemValidMessage] =
        for {
          guildEmblem <- Codec[GuildEmblem].decode
        } yield GuildModificationEmblemValidMessage(guildEmblem)

      def encode(value: GuildModificationEmblemValidMessage): ByteVector =
        Codec[GuildEmblem].encode(value.guildEmblem)
    }
}
