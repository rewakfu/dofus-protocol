package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StartupActionsAllAttributionMessage(
  characterId: Long
) extends Message(6537)

object StartupActionsAllAttributionMessage {
  implicit val codec: Codec[StartupActionsAllAttributionMessage] =
    new Codec[StartupActionsAllAttributionMessage] {
      def decode: Get[StartupActionsAllAttributionMessage] =
        for {
          characterId <- varLong.decode
        } yield StartupActionsAllAttributionMessage(characterId)

      def encode(value: StartupActionsAllAttributionMessage): ByteVector =
        varLong.encode(value.characterId)
    }
}
