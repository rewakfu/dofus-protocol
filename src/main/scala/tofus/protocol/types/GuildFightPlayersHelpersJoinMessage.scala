package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFightPlayersHelpersJoinMessage(
  fightId: Double,
  playerInfo: ConcreteCharacterMinimalPlusLookInformations
) extends Message(5720)

object GuildFightPlayersHelpersJoinMessage {
  implicit val codec: Codec[GuildFightPlayersHelpersJoinMessage] =
    new Codec[GuildFightPlayersHelpersJoinMessage] {
      def decode: Get[GuildFightPlayersHelpersJoinMessage] =
        for {
          fightId <- double.decode
          playerInfo <- Codec[ConcreteCharacterMinimalPlusLookInformations].decode
        } yield GuildFightPlayersHelpersJoinMessage(fightId, playerInfo)

      def encode(value: GuildFightPlayersHelpersJoinMessage): ByteVector =
        double.encode(value.fightId) ++
        Codec[ConcreteCharacterMinimalPlusLookInformations].encode(value.playerInfo)
    }
}
