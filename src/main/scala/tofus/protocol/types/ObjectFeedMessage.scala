package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectFeedMessage(
  objectUID: Int,
  meal: List[ObjectItemQuantity]
) extends Message(6290)

object ObjectFeedMessage {
  implicit val codec: Codec[ObjectFeedMessage] =
    new Codec[ObjectFeedMessage] {
      def decode: Get[ObjectFeedMessage] =
        for {
          objectUID <- varInt.decode
          meal <- list(ushort, Codec[ObjectItemQuantity]).decode
        } yield ObjectFeedMessage(objectUID, meal)

      def encode(value: ObjectFeedMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        list(ushort, Codec[ObjectItemQuantity]).encode(value.meal)
    }
}
