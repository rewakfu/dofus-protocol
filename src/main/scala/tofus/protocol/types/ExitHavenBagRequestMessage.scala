package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExitHavenBagRequestMessage(

) extends Message(6631)

object ExitHavenBagRequestMessage {
  implicit val codec: Codec[ExitHavenBagRequestMessage] =
    Codec.const(ExitHavenBagRequestMessage())
}
