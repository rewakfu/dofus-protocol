package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItemQuantity(
  objectUID: Int,
  quantity: Int
) extends Item {
  override val protocolId = 119
}

object ObjectItemQuantity {
  implicit val codec: Codec[ObjectItemQuantity] =
    new Codec[ObjectItemQuantity] {
      def decode: Get[ObjectItemQuantity] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ObjectItemQuantity(objectUID, quantity)

      def encode(value: ObjectItemQuantity): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
