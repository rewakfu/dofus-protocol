package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareRewardConsumeValidationMessage(
  dareId: Double,
  `type`: Byte
) extends Message(6675)

object DareRewardConsumeValidationMessage {
  implicit val codec: Codec[DareRewardConsumeValidationMessage] =
    new Codec[DareRewardConsumeValidationMessage] {
      def decode: Get[DareRewardConsumeValidationMessage] =
        for {
          dareId <- double.decode
          `type` <- byte.decode
        } yield DareRewardConsumeValidationMessage(dareId, `type`)

      def encode(value: DareRewardConsumeValidationMessage): ByteVector =
        double.encode(value.dareId) ++
        byte.encode(value.`type`)
    }
}
