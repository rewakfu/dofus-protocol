package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait HumanInformations extends ProtocolType

final case class ConcreteHumanInformations(
  restrictions: ActorRestrictionsInformations,
  sex: Boolean,
  options: List[HumanOption]
) extends HumanInformations {
  override val protocolId = 157
}

object ConcreteHumanInformations {
  implicit val codec: Codec[ConcreteHumanInformations] =  
    new Codec[ConcreteHumanInformations] {
      def decode: Get[ConcreteHumanInformations] =
        for {
          restrictions <- Codec[ActorRestrictionsInformations].decode
          sex <- bool.decode
          options <- list(ushort, Codec[HumanOption]).decode
        } yield ConcreteHumanInformations(restrictions, sex, options)

      def encode(value: ConcreteHumanInformations): ByteVector =
        Codec[ActorRestrictionsInformations].encode(value.restrictions) ++
        bool.encode(value.sex) ++
        list(ushort, Codec[HumanOption]).encode(value.options)
    }
}

object HumanInformations {
  implicit val codec: Codec[HumanInformations] =
    new Codec[HumanInformations] {
      def decode: Get[HumanInformations] =
        ushort.decode.flatMap {
          case 157 => Codec[ConcreteHumanInformations].decode

        }

      def encode(value: HumanInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteHumanInformations => Codec[ConcreteHumanInformations].encode(i)

        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
