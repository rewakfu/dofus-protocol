package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderRoomContentMessage(
  dungeonId: Short,
  players: List[DungeonPartyFinderPlayer]
) extends Message(6247)

object DungeonPartyFinderRoomContentMessage {
  implicit val codec: Codec[DungeonPartyFinderRoomContentMessage] =
    new Codec[DungeonPartyFinderRoomContentMessage] {
      def decode: Get[DungeonPartyFinderRoomContentMessage] =
        for {
          dungeonId <- varShort.decode
          players <- list(ushort, Codec[DungeonPartyFinderPlayer]).decode
        } yield DungeonPartyFinderRoomContentMessage(dungeonId, players)

      def encode(value: DungeonPartyFinderRoomContentMessage): ByteVector =
        varShort.encode(value.dungeonId) ++
        list(ushort, Codec[DungeonPartyFinderPlayer]).encode(value.players)
    }
}
