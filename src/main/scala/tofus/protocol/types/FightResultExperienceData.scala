package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightResultExperienceData(
  flags0: Byte,
  experience: Long,
  experienceLevelFloor: Long,
  experienceNextLevelFloor: Long,
  experienceFightDelta: Long,
  experienceForGuild: Long,
  experienceForMount: Long,
  rerollExperienceMul: Byte
) extends FightResultAdditionalData {
  override val protocolId = 192
}

object FightResultExperienceData {
  implicit val codec: Codec[FightResultExperienceData] =
    new Codec[FightResultExperienceData] {
      def decode: Get[FightResultExperienceData] =
        for {
          flags0 <- byte.decode
          experience <- varLong.decode
          experienceLevelFloor <- varLong.decode
          experienceNextLevelFloor <- varLong.decode
          experienceFightDelta <- varLong.decode
          experienceForGuild <- varLong.decode
          experienceForMount <- varLong.decode
          rerollExperienceMul <- byte.decode
        } yield FightResultExperienceData(flags0, experience, experienceLevelFloor, experienceNextLevelFloor, experienceFightDelta, experienceForGuild, experienceForMount, rerollExperienceMul)

      def encode(value: FightResultExperienceData): ByteVector =
        byte.encode(value.flags0) ++
        varLong.encode(value.experience) ++
        varLong.encode(value.experienceLevelFloor) ++
        varLong.encode(value.experienceNextLevelFloor) ++
        varLong.encode(value.experienceFightDelta) ++
        varLong.encode(value.experienceForGuild) ++
        varLong.encode(value.experienceForMount) ++
        byte.encode(value.rerollExperienceMul)
    }
}
