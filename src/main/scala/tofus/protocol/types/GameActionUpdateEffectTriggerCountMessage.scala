package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionUpdateEffectTriggerCountMessage(
  targetIds: List[GameFightEffectTriggerCount]
) extends Message(6838)

object GameActionUpdateEffectTriggerCountMessage {
  implicit val codec: Codec[GameActionUpdateEffectTriggerCountMessage] =
    new Codec[GameActionUpdateEffectTriggerCountMessage] {
      def decode: Get[GameActionUpdateEffectTriggerCountMessage] =
        for {
          targetIds <- list(ushort, Codec[GameFightEffectTriggerCount]).decode
        } yield GameActionUpdateEffectTriggerCountMessage(targetIds)

      def encode(value: GameActionUpdateEffectTriggerCountMessage): ByteVector =
        list(ushort, Codec[GameFightEffectTriggerCount]).encode(value.targetIds)
    }
}
