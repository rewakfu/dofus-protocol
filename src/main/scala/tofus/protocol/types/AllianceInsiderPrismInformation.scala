package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInsiderPrismInformation(
  typeId: Byte,
  state: Byte,
  nextVulnerabilityDate: Int,
  placementDate: Int,
  rewardTokenCount: Int,
  lastTimeSlotModificationDate: Int,
  lastTimeSlotModificationAuthorGuildId: Int,
  lastTimeSlotModificationAuthorId: Long,
  lastTimeSlotModificationAuthorName: String,
  modulesObjects: List[ObjectItem]
) extends PrismInformation {
  override val protocolId = 431
}

object AllianceInsiderPrismInformation {
  implicit val codec: Codec[AllianceInsiderPrismInformation] =
    new Codec[AllianceInsiderPrismInformation] {
      def decode: Get[AllianceInsiderPrismInformation] =
        for {
          typeId <- byte.decode
          state <- byte.decode
          nextVulnerabilityDate <- int.decode
          placementDate <- int.decode
          rewardTokenCount <- varInt.decode
          lastTimeSlotModificationDate <- int.decode
          lastTimeSlotModificationAuthorGuildId <- varInt.decode
          lastTimeSlotModificationAuthorId <- varLong.decode
          lastTimeSlotModificationAuthorName <- utf8(ushort).decode
          modulesObjects <- list(ushort, Codec[ObjectItem]).decode
        } yield AllianceInsiderPrismInformation(typeId, state, nextVulnerabilityDate, placementDate, rewardTokenCount, lastTimeSlotModificationDate, lastTimeSlotModificationAuthorGuildId, lastTimeSlotModificationAuthorId, lastTimeSlotModificationAuthorName, modulesObjects)

      def encode(value: AllianceInsiderPrismInformation): ByteVector =
        byte.encode(value.typeId) ++
        byte.encode(value.state) ++
        int.encode(value.nextVulnerabilityDate) ++
        int.encode(value.placementDate) ++
        varInt.encode(value.rewardTokenCount) ++
        int.encode(value.lastTimeSlotModificationDate) ++
        varInt.encode(value.lastTimeSlotModificationAuthorGuildId) ++
        varLong.encode(value.lastTimeSlotModificationAuthorId) ++
        utf8(ushort).encode(value.lastTimeSlotModificationAuthorName) ++
        list(ushort, Codec[ObjectItem]).encode(value.modulesObjects)
    }
}
