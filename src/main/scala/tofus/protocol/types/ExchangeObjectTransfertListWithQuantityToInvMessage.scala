package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectTransfertListWithQuantityToInvMessage(
  ids: List[Int],
  qtys: List[Int]
) extends Message(6470)

object ExchangeObjectTransfertListWithQuantityToInvMessage {
  implicit val codec: Codec[ExchangeObjectTransfertListWithQuantityToInvMessage] =
    new Codec[ExchangeObjectTransfertListWithQuantityToInvMessage] {
      def decode: Get[ExchangeObjectTransfertListWithQuantityToInvMessage] =
        for {
          ids <- list(ushort, varInt).decode
          qtys <- list(ushort, varInt).decode
        } yield ExchangeObjectTransfertListWithQuantityToInvMessage(ids, qtys)

      def encode(value: ExchangeObjectTransfertListWithQuantityToInvMessage): ByteVector =
        list(ushort, varInt).encode(value.ids) ++
        list(ushort, varInt).encode(value.qtys)
    }
}
