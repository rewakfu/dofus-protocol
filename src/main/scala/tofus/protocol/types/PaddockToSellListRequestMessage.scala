package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockToSellListRequestMessage(
  pageIndex: Short
) extends Message(6141)

object PaddockToSellListRequestMessage {
  implicit val codec: Codec[PaddockToSellListRequestMessage] =
    new Codec[PaddockToSellListRequestMessage] {
      def decode: Get[PaddockToSellListRequestMessage] =
        for {
          pageIndex <- varShort.decode
        } yield PaddockToSellListRequestMessage(pageIndex)

      def encode(value: PaddockToSellListRequestMessage): ByteVector =
        varShort.encode(value.pageIndex)
    }
}
