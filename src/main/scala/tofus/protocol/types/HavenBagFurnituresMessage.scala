package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagFurnituresMessage(
  furnituresInfos: List[HavenBagFurnitureInformation]
) extends Message(6634)

object HavenBagFurnituresMessage {
  implicit val codec: Codec[HavenBagFurnituresMessage] =
    new Codec[HavenBagFurnituresMessage] {
      def decode: Get[HavenBagFurnituresMessage] =
        for {
          furnituresInfos <- list(ushort, Codec[HavenBagFurnitureInformation]).decode
        } yield HavenBagFurnituresMessage(furnituresInfos)

      def encode(value: HavenBagFurnituresMessage): ByteVector =
        list(ushort, Codec[HavenBagFurnitureInformation]).encode(value.furnituresInfos)
    }
}
