package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait BasicAllianceInformations extends AbstractSocialGroupInfos

final case class ConcreteBasicAllianceInformations(
  allianceId: Int,
  allianceTag: String
) extends BasicAllianceInformations {
  override val protocolId = 419
}

object ConcreteBasicAllianceInformations {
  implicit val codec: Codec[ConcreteBasicAllianceInformations] =  
    new Codec[ConcreteBasicAllianceInformations] {
      def decode: Get[ConcreteBasicAllianceInformations] =
        for {
          allianceId <- varInt.decode
          allianceTag <- utf8(ushort).decode
        } yield ConcreteBasicAllianceInformations(allianceId, allianceTag)

      def encode(value: ConcreteBasicAllianceInformations): ByteVector =
        varInt.encode(value.allianceId) ++
        utf8(ushort).encode(value.allianceTag)
    }
}

object BasicAllianceInformations {
  implicit val codec: Codec[BasicAllianceInformations] =
    new Codec[BasicAllianceInformations] {
      def decode: Get[BasicAllianceInformations] =
        ushort.decode.flatMap {
          case 419 => Codec[ConcreteBasicAllianceInformations].decode
          case 421 => Codec[AllianceFactSheetInformations].decode
          case 417 => Codec[ConcreteAllianceInformations].decode
          case 418 => Codec[ConcreteBasicNamedAllianceInformations].decode
        }

      def encode(value: BasicAllianceInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteBasicAllianceInformations => Codec[ConcreteBasicAllianceInformations].encode(i)
          case i: AllianceFactSheetInformations => Codec[AllianceFactSheetInformations].encode(i)
          case i: ConcreteAllianceInformations => Codec[ConcreteAllianceInformations].encode(i)
          case i: ConcreteBasicNamedAllianceInformations => Codec[ConcreteBasicNamedAllianceInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
