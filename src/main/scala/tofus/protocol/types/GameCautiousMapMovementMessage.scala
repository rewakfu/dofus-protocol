package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameCautiousMapMovementMessage(
  keyMovements: List[Short],
  forcedDirection: Short,
  actorId: Double
) extends Message(6497)

object GameCautiousMapMovementMessage {
  implicit val codec: Codec[GameCautiousMapMovementMessage] =
    new Codec[GameCautiousMapMovementMessage] {
      def decode: Get[GameCautiousMapMovementMessage] =
        for {
          keyMovements <- list(ushort, short).decode
          forcedDirection <- short.decode
          actorId <- double.decode
        } yield GameCautiousMapMovementMessage(keyMovements, forcedDirection, actorId)

      def encode(value: GameCautiousMapMovementMessage): ByteVector =
        list(ushort, short).encode(value.keyMovements) ++
        short.encode(value.forcedDirection) ++
        double.encode(value.actorId)
    }
}
