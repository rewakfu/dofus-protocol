package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectMoveMessage(
  objectUID: Int,
  quantity: Int
) extends Message(5518)

object ExchangeObjectMoveMessage {
  implicit val codec: Codec[ExchangeObjectMoveMessage] =
    new Codec[ExchangeObjectMoveMessage] {
      def decode: Get[ExchangeObjectMoveMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ExchangeObjectMoveMessage(objectUID, quantity)

      def encode(value: ExchangeObjectMoveMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
