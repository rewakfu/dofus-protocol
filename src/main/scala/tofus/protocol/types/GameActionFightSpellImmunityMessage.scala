package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightSpellImmunityMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  spellId: Short
) extends Message(6221)

object GameActionFightSpellImmunityMessage {
  implicit val codec: Codec[GameActionFightSpellImmunityMessage] =
    new Codec[GameActionFightSpellImmunityMessage] {
      def decode: Get[GameActionFightSpellImmunityMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          spellId <- varShort.decode
        } yield GameActionFightSpellImmunityMessage(actionId, sourceId, targetId, spellId)

      def encode(value: GameActionFightSpellImmunityMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varShort.encode(value.spellId)
    }
}
