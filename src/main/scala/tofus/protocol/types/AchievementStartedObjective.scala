package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementStartedObjective(
  id: Int,
  maxValue: Short,
  value: Short
) extends AchievementObjective {
  override val protocolId = 402
}

object AchievementStartedObjective {
  implicit val codec: Codec[AchievementStartedObjective] =
    new Codec[AchievementStartedObjective] {
      def decode: Get[AchievementStartedObjective] =
        for {
          id <- varInt.decode
          maxValue <- varShort.decode
          value <- varShort.decode
        } yield AchievementStartedObjective(id, maxValue, value)

      def encode(value: AchievementStartedObjective): ByteVector =
        varInt.encode(value.id) ++
        varShort.encode(value.maxValue) ++
        varShort.encode(value.value)
    }
}
