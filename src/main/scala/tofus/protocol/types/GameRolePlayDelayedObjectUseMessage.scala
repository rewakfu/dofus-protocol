package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayDelayedObjectUseMessage(
  delayedCharacterId: Double,
  delayTypeId: Byte,
  delayEndTime: Double,
  objectGID: Short
) extends Message(6425)

object GameRolePlayDelayedObjectUseMessage {
  implicit val codec: Codec[GameRolePlayDelayedObjectUseMessage] =
    new Codec[GameRolePlayDelayedObjectUseMessage] {
      def decode: Get[GameRolePlayDelayedObjectUseMessage] =
        for {
          delayedCharacterId <- double.decode
          delayTypeId <- byte.decode
          delayEndTime <- double.decode
          objectGID <- varShort.decode
        } yield GameRolePlayDelayedObjectUseMessage(delayedCharacterId, delayTypeId, delayEndTime, objectGID)

      def encode(value: GameRolePlayDelayedObjectUseMessage): ByteVector =
        double.encode(value.delayedCharacterId) ++
        byte.encode(value.delayTypeId) ++
        double.encode(value.delayEndTime) ++
        varShort.encode(value.objectGID)
    }
}
