package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseBuyMessage(
  uid: Int,
  qty: Int,
  price: Long
) extends Message(5804)

object ExchangeBidHouseBuyMessage {
  implicit val codec: Codec[ExchangeBidHouseBuyMessage] =
    new Codec[ExchangeBidHouseBuyMessage] {
      def decode: Get[ExchangeBidHouseBuyMessage] =
        for {
          uid <- varInt.decode
          qty <- varInt.decode
          price <- varLong.decode
        } yield ExchangeBidHouseBuyMessage(uid, qty, price)

      def encode(value: ExchangeBidHouseBuyMessage): ByteVector =
        varInt.encode(value.uid) ++
        varInt.encode(value.qty) ++
        varLong.encode(value.price)
    }
}
