package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait InteractiveElement extends ProtocolType

final case class ConcreteInteractiveElement(
  elementId: Int,
  elementTypeId: Int,
  enabledSkills: List[InteractiveElementSkill],
  disabledSkills: List[InteractiveElementSkill],
  onCurrentMap: Boolean
) extends InteractiveElement {
  override val protocolId = 80
}

object ConcreteInteractiveElement {
  implicit val codec: Codec[ConcreteInteractiveElement] =  
    new Codec[ConcreteInteractiveElement] {
      def decode: Get[ConcreteInteractiveElement] =
        for {
          elementId <- int.decode
          elementTypeId <- int.decode
          enabledSkills <- list(ushort, Codec[InteractiveElementSkill]).decode
          disabledSkills <- list(ushort, Codec[InteractiveElementSkill]).decode
          onCurrentMap <- bool.decode
        } yield ConcreteInteractiveElement(elementId, elementTypeId, enabledSkills, disabledSkills, onCurrentMap)

      def encode(value: ConcreteInteractiveElement): ByteVector =
        int.encode(value.elementId) ++
        int.encode(value.elementTypeId) ++
        list(ushort, Codec[InteractiveElementSkill]).encode(value.enabledSkills) ++
        list(ushort, Codec[InteractiveElementSkill]).encode(value.disabledSkills) ++
        bool.encode(value.onCurrentMap)
    }
}

object InteractiveElement {
  implicit val codec: Codec[InteractiveElement] =
    new Codec[InteractiveElement] {
      def decode: Get[InteractiveElement] =
        ushort.decode.flatMap {
          case 80 => Codec[ConcreteInteractiveElement].decode
          case 398 => Codec[InteractiveElementWithAgeBonus].decode
        }

      def encode(value: InteractiveElement): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteInteractiveElement => Codec[ConcreteInteractiveElement].encode(i)
          case i: InteractiveElementWithAgeBonus => Codec[InteractiveElementWithAgeBonus].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
