package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightInvisibilityMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  state: Byte
) extends Message(5821)

object GameActionFightInvisibilityMessage {
  implicit val codec: Codec[GameActionFightInvisibilityMessage] =
    new Codec[GameActionFightInvisibilityMessage] {
      def decode: Get[GameActionFightInvisibilityMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          state <- byte.decode
        } yield GameActionFightInvisibilityMessage(actionId, sourceId, targetId, state)

      def encode(value: GameActionFightInvisibilityMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        byte.encode(value.state)
    }
}
