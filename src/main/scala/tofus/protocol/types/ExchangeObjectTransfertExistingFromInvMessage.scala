package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectTransfertExistingFromInvMessage(

) extends Message(6325)

object ExchangeObjectTransfertExistingFromInvMessage {
  implicit val codec: Codec[ExchangeObjectTransfertExistingFromInvMessage] =
    Codec.const(ExchangeObjectTransfertExistingFromInvMessage())
}
