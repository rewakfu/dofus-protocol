package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItemToSellInHumanVendorShop(
  objectGID: Short,
  effects: List[ObjectEffect],
  objectUID: Int,
  quantity: Int,
  objectPrice: Long,
  publicPrice: Long
) extends Item {
  override val protocolId = 359
}

object ObjectItemToSellInHumanVendorShop {
  implicit val codec: Codec[ObjectItemToSellInHumanVendorShop] =
    new Codec[ObjectItemToSellInHumanVendorShop] {
      def decode: Get[ObjectItemToSellInHumanVendorShop] =
        for {
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          objectUID <- varInt.decode
          quantity <- varInt.decode
          objectPrice <- varLong.decode
          publicPrice <- varLong.decode
        } yield ObjectItemToSellInHumanVendorShop(objectGID, effects, objectUID, quantity, objectPrice, publicPrice)

      def encode(value: ObjectItemToSellInHumanVendorShop): ByteVector =
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity) ++
        varLong.encode(value.objectPrice) ++
        varLong.encode(value.publicPrice)
    }
}
