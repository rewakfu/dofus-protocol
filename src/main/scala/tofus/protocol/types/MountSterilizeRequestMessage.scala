package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountSterilizeRequestMessage(

) extends Message(5962)

object MountSterilizeRequestMessage {
  implicit val codec: Codec[MountSterilizeRequestMessage] =
    Codec.const(MountSterilizeRequestMessage())
}
