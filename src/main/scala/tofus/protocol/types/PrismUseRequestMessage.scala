package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismUseRequestMessage(
  moduleToUse: Byte
) extends Message(6041)

object PrismUseRequestMessage {
  implicit val codec: Codec[PrismUseRequestMessage] =
    new Codec[PrismUseRequestMessage] {
      def decode: Get[PrismUseRequestMessage] =
        for {
          moduleToUse <- byte.decode
        } yield PrismUseRequestMessage(moduleToUse)

      def encode(value: PrismUseRequestMessage): ByteVector =
        byte.encode(value.moduleToUse)
    }
}
