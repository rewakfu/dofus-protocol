package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountRenamedMessage(
  mountId: Int,
  name: String
) extends Message(5983)

object MountRenamedMessage {
  implicit val codec: Codec[MountRenamedMessage] =
    new Codec[MountRenamedMessage] {
      def decode: Get[MountRenamedMessage] =
        for {
          mountId <- varInt.decode
          name <- utf8(ushort).decode
        } yield MountRenamedMessage(mountId, name)

      def encode(value: MountRenamedMessage): ByteVector =
        varInt.encode(value.mountId) ++
        utf8(ushort).encode(value.name)
    }
}
