package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockPropertiesMessage(
  properties: PaddockInstancesInformations
) extends Message(5824)

object PaddockPropertiesMessage {
  implicit val codec: Codec[PaddockPropertiesMessage] =
    new Codec[PaddockPropertiesMessage] {
      def decode: Get[PaddockPropertiesMessage] =
        for {
          properties <- Codec[PaddockInstancesInformations].decode
        } yield PaddockPropertiesMessage(properties)

      def encode(value: PaddockPropertiesMessage): ByteVector =
        Codec[PaddockInstancesInformations].encode(value.properties)
    }
}
