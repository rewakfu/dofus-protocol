package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeOnHumanVendorRequestMessage(
  humanVendorId: Long,
  humanVendorCell: Short
) extends Message(5772)

object ExchangeOnHumanVendorRequestMessage {
  implicit val codec: Codec[ExchangeOnHumanVendorRequestMessage] =
    new Codec[ExchangeOnHumanVendorRequestMessage] {
      def decode: Get[ExchangeOnHumanVendorRequestMessage] =
        for {
          humanVendorId <- varLong.decode
          humanVendorCell <- varShort.decode
        } yield ExchangeOnHumanVendorRequestMessage(humanVendorId, humanVendorCell)

      def encode(value: ExchangeOnHumanVendorRequestMessage): ByteVector =
        varLong.encode(value.humanVendorId) ++
        varShort.encode(value.humanVendorCell)
    }
}
