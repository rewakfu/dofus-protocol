package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapMovementMessage(
  keyMovements: List[Short],
  forcedDirection: Short,
  actorId: Double
) extends Message(951)

object GameMapMovementMessage {
  implicit val codec: Codec[GameMapMovementMessage] =
    new Codec[GameMapMovementMessage] {
      def decode: Get[GameMapMovementMessage] =
        for {
          keyMovements <- list(ushort, short).decode
          forcedDirection <- short.decode
          actorId <- double.decode
        } yield GameMapMovementMessage(keyMovements, forcedDirection, actorId)

      def encode(value: GameMapMovementMessage): ByteVector =
        list(ushort, short).encode(value.keyMovements) ++
        short.encode(value.forcedDirection) ++
        double.encode(value.actorId)
    }
}
