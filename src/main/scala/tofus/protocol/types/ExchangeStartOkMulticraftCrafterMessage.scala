package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkMulticraftCrafterMessage(
  skillId: Int
) extends Message(5818)

object ExchangeStartOkMulticraftCrafterMessage {
  implicit val codec: Codec[ExchangeStartOkMulticraftCrafterMessage] =
    new Codec[ExchangeStartOkMulticraftCrafterMessage] {
      def decode: Get[ExchangeStartOkMulticraftCrafterMessage] =
        for {
          skillId <- varInt.decode
        } yield ExchangeStartOkMulticraftCrafterMessage(skillId)

      def encode(value: ExchangeStartOkMulticraftCrafterMessage): ByteVector =
        varInt.encode(value.skillId)
    }
}
