package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChangeHavenBagRoomRequestMessage(
  roomId: Byte
) extends Message(6638)

object ChangeHavenBagRoomRequestMessage {
  implicit val codec: Codec[ChangeHavenBagRoomRequestMessage] =
    new Codec[ChangeHavenBagRoomRequestMessage] {
      def decode: Get[ChangeHavenBagRoomRequestMessage] =
        for {
          roomId <- byte.decode
        } yield ChangeHavenBagRoomRequestMessage(roomId)

      def encode(value: ChangeHavenBagRoomRequestMessage): ByteVector =
        byte.encode(value.roomId)
    }
}
