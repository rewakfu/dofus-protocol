package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayPlayerLifeStatusMessage(
  state: Byte,
  phenixMapId: Double
) extends Message(5996)

object GameRolePlayPlayerLifeStatusMessage {
  implicit val codec: Codec[GameRolePlayPlayerLifeStatusMessage] =
    new Codec[GameRolePlayPlayerLifeStatusMessage] {
      def decode: Get[GameRolePlayPlayerLifeStatusMessage] =
        for {
          state <- byte.decode
          phenixMapId <- double.decode
        } yield GameRolePlayPlayerLifeStatusMessage(state, phenixMapId)

      def encode(value: GameRolePlayPlayerLifeStatusMessage): ByteVector =
        byte.encode(value.state) ++
        double.encode(value.phenixMapId)
    }
}
