package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportToBuddyOfferMessage(
  dungeonId: Short,
  buddyId: Long,
  timeLeft: Int
) extends Message(6287)

object TeleportToBuddyOfferMessage {
  implicit val codec: Codec[TeleportToBuddyOfferMessage] =
    new Codec[TeleportToBuddyOfferMessage] {
      def decode: Get[TeleportToBuddyOfferMessage] =
        for {
          dungeonId <- varShort.decode
          buddyId <- varLong.decode
          timeLeft <- varInt.decode
        } yield TeleportToBuddyOfferMessage(dungeonId, buddyId, timeLeft)

      def encode(value: TeleportToBuddyOfferMessage): ByteVector =
        varShort.encode(value.dungeonId) ++
        varLong.encode(value.buddyId) ++
        varInt.encode(value.timeLeft)
    }
}
