package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlignmentWarEffortProgressionRequestMessage(

) extends Message(6854)

object AlignmentWarEffortProgressionRequestMessage {
  implicit val codec: Codec[AlignmentWarEffortProgressionRequestMessage] =
    Codec.const(AlignmentWarEffortProgressionRequestMessage())
}
