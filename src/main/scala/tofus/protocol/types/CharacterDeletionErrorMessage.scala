package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterDeletionErrorMessage(
  reason: Byte
) extends Message(166)

object CharacterDeletionErrorMessage {
  implicit val codec: Codec[CharacterDeletionErrorMessage] =
    new Codec[CharacterDeletionErrorMessage] {
      def decode: Get[CharacterDeletionErrorMessage] =
        for {
          reason <- byte.decode
        } yield CharacterDeletionErrorMessage(reason)

      def encode(value: CharacterDeletionErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
