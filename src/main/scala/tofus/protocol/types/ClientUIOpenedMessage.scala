package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ClientUIOpenedMessage(
  `type`: Byte
) extends Message(6459)

object ClientUIOpenedMessage {
  implicit val codec: Codec[ClientUIOpenedMessage] =
    new Codec[ClientUIOpenedMessage] {
      def decode: Get[ClientUIOpenedMessage] =
        for {
          `type` <- byte.decode
        } yield ClientUIOpenedMessage(`type`)

      def encode(value: ClientUIOpenedMessage): ByteVector =
        byte.encode(value.`type`)
    }
}
