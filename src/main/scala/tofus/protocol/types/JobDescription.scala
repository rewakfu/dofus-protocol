package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobDescription(
  jobId: Byte,
  skills: List[SkillActionDescription]
) extends ProtocolType {
  override val protocolId = 101
}

object JobDescription {
  implicit val codec: Codec[JobDescription] =
    new Codec[JobDescription] {
      def decode: Get[JobDescription] =
        for {
          jobId <- byte.decode
          skills <- list(ushort, Codec[SkillActionDescription]).decode
        } yield JobDescription(jobId, skills)

      def encode(value: JobDescription): ByteVector =
        byte.encode(value.jobId) ++
        list(ushort, Codec[SkillActionDescription]).encode(value.skills)
    }
}
