package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class Achievement(
  id: Short,
  finishedObjective: List[ConcreteAchievementObjective],
  startedObjectives: List[AchievementStartedObjective]
) extends ProtocolType {
  override val protocolId = 363
}

object Achievement {
  implicit val codec: Codec[Achievement] =
    new Codec[Achievement] {
      def decode: Get[Achievement] =
        for {
          id <- varShort.decode
          finishedObjective <- list(ushort, Codec[ConcreteAchievementObjective]).decode
          startedObjectives <- list(ushort, Codec[AchievementStartedObjective]).decode
        } yield Achievement(id, finishedObjective, startedObjectives)

      def encode(value: Achievement): ByteVector =
        varShort.encode(value.id) ++
        list(ushort, Codec[ConcreteAchievementObjective]).encode(value.finishedObjective) ++
        list(ushort, Codec[AchievementStartedObjective]).encode(value.startedObjectives)
    }
}
