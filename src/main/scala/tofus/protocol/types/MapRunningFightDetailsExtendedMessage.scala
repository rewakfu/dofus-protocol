package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapRunningFightDetailsExtendedMessage(
  fightId: Short,
  attackers: List[GameFightFighterLightInformations],
  defenders: List[GameFightFighterLightInformations],
  namedPartyTeams: List[NamedPartyTeam]
) extends Message(6500)

object MapRunningFightDetailsExtendedMessage {
  implicit val codec: Codec[MapRunningFightDetailsExtendedMessage] =
    new Codec[MapRunningFightDetailsExtendedMessage] {
      def decode: Get[MapRunningFightDetailsExtendedMessage] =
        for {
          fightId <- varShort.decode
          attackers <- list(ushort, Codec[GameFightFighterLightInformations]).decode
          defenders <- list(ushort, Codec[GameFightFighterLightInformations]).decode
          namedPartyTeams <- list(ushort, Codec[NamedPartyTeam]).decode
        } yield MapRunningFightDetailsExtendedMessage(fightId, attackers, defenders, namedPartyTeams)

      def encode(value: MapRunningFightDetailsExtendedMessage): ByteVector =
        varShort.encode(value.fightId) ++
        list(ushort, Codec[GameFightFighterLightInformations]).encode(value.attackers) ++
        list(ushort, Codec[GameFightFighterLightInformations]).encode(value.defenders) ++
        list(ushort, Codec[NamedPartyTeam]).encode(value.namedPartyTeams)
    }
}
