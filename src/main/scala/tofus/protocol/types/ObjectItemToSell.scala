package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ObjectItemToSell extends Item

final case class ConcreteObjectItemToSell(
  objectGID: Short,
  effects: List[ObjectEffect],
  objectUID: Int,
  quantity: Int,
  objectPrice: Long
) extends ObjectItemToSell {
  override val protocolId = 120
}

object ConcreteObjectItemToSell {
  implicit val codec: Codec[ConcreteObjectItemToSell] =  
    new Codec[ConcreteObjectItemToSell] {
      def decode: Get[ConcreteObjectItemToSell] =
        for {
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          objectUID <- varInt.decode
          quantity <- varInt.decode
          objectPrice <- varLong.decode
        } yield ConcreteObjectItemToSell(objectGID, effects, objectUID, quantity, objectPrice)

      def encode(value: ConcreteObjectItemToSell): ByteVector =
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity) ++
        varLong.encode(value.objectPrice)
    }
}

object ObjectItemToSell {
  implicit val codec: Codec[ObjectItemToSell] =
    new Codec[ObjectItemToSell] {
      def decode: Get[ObjectItemToSell] =
        ushort.decode.flatMap {
          case 120 => Codec[ConcreteObjectItemToSell].decode
          case 164 => Codec[ObjectItemToSellInBid].decode
        }

      def encode(value: ObjectItemToSell): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteObjectItemToSell => Codec[ConcreteObjectItemToSell].encode(i)
          case i: ObjectItemToSellInBid => Codec[ObjectItemToSellInBid].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
