package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonKeyRingMessage(
  availables: List[Short],
  unavailables: List[Short]
) extends Message(6299)

object DungeonKeyRingMessage {
  implicit val codec: Codec[DungeonKeyRingMessage] =
    new Codec[DungeonKeyRingMessage] {
      def decode: Get[DungeonKeyRingMessage] =
        for {
          availables <- list(ushort, varShort).decode
          unavailables <- list(ushort, varShort).decode
        } yield DungeonKeyRingMessage(availables, unavailables)

      def encode(value: DungeonKeyRingMessage): ByteVector =
        list(ushort, varShort).encode(value.availables) ++
        list(ushort, varShort).encode(value.unavailables)
    }
}
