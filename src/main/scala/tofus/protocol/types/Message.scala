package tofus.protocol.types

abstract class Message(val messageTypeId: Int) extends Product with Serializable

trait ProtocolType extends Product with Serializable {
  def protocolId: Int
}
