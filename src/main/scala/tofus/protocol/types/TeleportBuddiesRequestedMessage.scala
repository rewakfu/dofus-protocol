package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportBuddiesRequestedMessage(
  dungeonId: Short,
  inviterId: Long,
  invalidBuddiesIds: List[Long]
) extends Message(6302)

object TeleportBuddiesRequestedMessage {
  implicit val codec: Codec[TeleportBuddiesRequestedMessage] =
    new Codec[TeleportBuddiesRequestedMessage] {
      def decode: Get[TeleportBuddiesRequestedMessage] =
        for {
          dungeonId <- varShort.decode
          inviterId <- varLong.decode
          invalidBuddiesIds <- list(ushort, varLong).decode
        } yield TeleportBuddiesRequestedMessage(dungeonId, inviterId, invalidBuddiesIds)

      def encode(value: TeleportBuddiesRequestedMessage): ByteVector =
        varShort.encode(value.dungeonId) ++
        varLong.encode(value.inviterId) ++
        list(ushort, varLong).encode(value.invalidBuddiesIds)
    }
}
