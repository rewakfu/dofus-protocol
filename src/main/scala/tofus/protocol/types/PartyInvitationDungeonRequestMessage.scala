package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationDungeonRequestMessage(
  name: String,
  dungeonId: Short
) extends Message(6245)

object PartyInvitationDungeonRequestMessage {
  implicit val codec: Codec[PartyInvitationDungeonRequestMessage] =
    new Codec[PartyInvitationDungeonRequestMessage] {
      def decode: Get[PartyInvitationDungeonRequestMessage] =
        for {
          name <- utf8(ushort).decode
          dungeonId <- varShort.decode
        } yield PartyInvitationDungeonRequestMessage(name, dungeonId)

      def encode(value: PartyInvitationDungeonRequestMessage): ByteVector =
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.dungeonId)
    }
}
