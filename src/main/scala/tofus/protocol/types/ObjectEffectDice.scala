package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectDice(
  actionId: Short,
  diceNum: Int,
  diceSide: Int,
  diceConst: Int
) extends ObjectEffect {
  override val protocolId = 73
}

object ObjectEffectDice {
  implicit val codec: Codec[ObjectEffectDice] =
    new Codec[ObjectEffectDice] {
      def decode: Get[ObjectEffectDice] =
        for {
          actionId <- varShort.decode
          diceNum <- varInt.decode
          diceSide <- varInt.decode
          diceConst <- varInt.decode
        } yield ObjectEffectDice(actionId, diceNum, diceSide, diceConst)

      def encode(value: ObjectEffectDice): ByteVector =
        varShort.encode(value.actionId) ++
        varInt.encode(value.diceNum) ++
        varInt.encode(value.diceSide) ++
        varInt.encode(value.diceConst)
    }
}
