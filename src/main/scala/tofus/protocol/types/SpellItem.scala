package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpellItem(
  spellId: Int,
  spellLevel: Short
) extends Item {
  override val protocolId = 49
}

object SpellItem {
  implicit val codec: Codec[SpellItem] =
    new Codec[SpellItem] {
      def decode: Get[SpellItem] =
        for {
          spellId <- int.decode
          spellLevel <- short.decode
        } yield SpellItem(spellId, spellLevel)

      def encode(value: SpellItem): ByteVector =
        int.encode(value.spellId) ++
        short.encode(value.spellLevel)
    }
}
