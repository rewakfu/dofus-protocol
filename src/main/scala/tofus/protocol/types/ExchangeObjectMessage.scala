package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectMessage(
  remote: Boolean
) extends Message(5515)

object ExchangeObjectMessage {
  implicit val codec: Codec[ExchangeObjectMessage] =
    new Codec[ExchangeObjectMessage] {
      def decode: Get[ExchangeObjectMessage] =
        for {
          remote <- bool.decode
        } yield ExchangeObjectMessage(remote)

      def encode(value: ExchangeObjectMessage): ByteVector =
        bool.encode(value.remote)
    }
}
