package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutSmiley(
  slot: Byte,
  smileyId: Short
) extends Shortcut {
  override val protocolId = 388
}

object ShortcutSmiley {
  implicit val codec: Codec[ShortcutSmiley] =
    new Codec[ShortcutSmiley] {
      def decode: Get[ShortcutSmiley] =
        for {
          slot <- byte.decode
          smileyId <- varShort.decode
        } yield ShortcutSmiley(slot, smileyId)

      def encode(value: ShortcutSmiley): ByteVector =
        byte.encode(value.slot) ++
        varShort.encode(value.smileyId)
    }
}
