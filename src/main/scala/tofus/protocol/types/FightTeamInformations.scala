package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FightTeamInformations extends AbstractFightTeamInformations

final case class ConcreteFightTeamInformations(
  teamId: Byte,
  leaderId: Double,
  teamSide: Byte,
  teamTypeId: Byte,
  nbWaves: Byte,
  teamMembers: List[FightTeamMemberInformations]
) extends FightTeamInformations {
  override val protocolId = 33
}

object ConcreteFightTeamInformations {
  implicit val codec: Codec[ConcreteFightTeamInformations] =  
    new Codec[ConcreteFightTeamInformations] {
      def decode: Get[ConcreteFightTeamInformations] =
        for {
          teamId <- byte.decode
          leaderId <- double.decode
          teamSide <- byte.decode
          teamTypeId <- byte.decode
          nbWaves <- byte.decode
          teamMembers <- list(ushort, Codec[FightTeamMemberInformations]).decode
        } yield ConcreteFightTeamInformations(teamId, leaderId, teamSide, teamTypeId, nbWaves, teamMembers)

      def encode(value: ConcreteFightTeamInformations): ByteVector =
        byte.encode(value.teamId) ++
        double.encode(value.leaderId) ++
        byte.encode(value.teamSide) ++
        byte.encode(value.teamTypeId) ++
        byte.encode(value.nbWaves) ++
        list(ushort, Codec[FightTeamMemberInformations]).encode(value.teamMembers)
    }
}

object FightTeamInformations {
  implicit val codec: Codec[FightTeamInformations] =
    new Codec[FightTeamInformations] {
      def decode: Get[FightTeamInformations] =
        ushort.decode.flatMap {
          case 33 => Codec[ConcreteFightTeamInformations].decode
          case 439 => Codec[FightAllianceTeamInformations].decode
        }

      def encode(value: FightTeamInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFightTeamInformations => Codec[ConcreteFightTeamInformations].encode(i)
          case i: FightAllianceTeamInformations => Codec[FightAllianceTeamInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
