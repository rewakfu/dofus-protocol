package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementPossiblePositionsMessage(
  positionsForChallengers: List[Short],
  positionsForDefenders: List[Short],
  teamNumber: Byte
) extends Message(703)

object GameFightPlacementPossiblePositionsMessage {
  implicit val codec: Codec[GameFightPlacementPossiblePositionsMessage] =
    new Codec[GameFightPlacementPossiblePositionsMessage] {
      def decode: Get[GameFightPlacementPossiblePositionsMessage] =
        for {
          positionsForChallengers <- list(ushort, varShort).decode
          positionsForDefenders <- list(ushort, varShort).decode
          teamNumber <- byte.decode
        } yield GameFightPlacementPossiblePositionsMessage(positionsForChallengers, positionsForDefenders, teamNumber)

      def encode(value: GameFightPlacementPossiblePositionsMessage): ByteVector =
        list(ushort, varShort).encode(value.positionsForChallengers) ++
        list(ushort, varShort).encode(value.positionsForDefenders) ++
        byte.encode(value.teamNumber)
    }
}
