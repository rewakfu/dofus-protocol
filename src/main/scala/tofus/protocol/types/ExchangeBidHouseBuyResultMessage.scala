package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseBuyResultMessage(
  uid: Int,
  bought: Boolean
) extends Message(6272)

object ExchangeBidHouseBuyResultMessage {
  implicit val codec: Codec[ExchangeBidHouseBuyResultMessage] =
    new Codec[ExchangeBidHouseBuyResultMessage] {
      def decode: Get[ExchangeBidHouseBuyResultMessage] =
        for {
          uid <- varInt.decode
          bought <- bool.decode
        } yield ExchangeBidHouseBuyResultMessage(uid, bought)

      def encode(value: ExchangeBidHouseBuyResultMessage): ByteVector =
        varInt.encode(value.uid) ++
        bool.encode(value.bought)
    }
}
