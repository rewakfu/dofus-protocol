package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MonsterInGroupInformations(
  genericId: Int,
  grade: Byte,
  level: Short,
  look: EntityLook
) extends MonsterInGroupLightInformations {
  override val protocolId = 144
}

object MonsterInGroupInformations {
  implicit val codec: Codec[MonsterInGroupInformations] =
    new Codec[MonsterInGroupInformations] {
      def decode: Get[MonsterInGroupInformations] =
        for {
          genericId <- int.decode
          grade <- byte.decode
          level <- short.decode
          look <- Codec[EntityLook].decode
        } yield MonsterInGroupInformations(genericId, grade, level, look)

      def encode(value: MonsterInGroupInformations): ByteVector =
        int.encode(value.genericId) ++
        byte.encode(value.grade) ++
        short.encode(value.level) ++
        Codec[EntityLook].encode(value.look)
    }
}
