package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AdminCommandMessage(
  content: String
) extends Message(76)

object AdminCommandMessage {
  implicit val codec: Codec[AdminCommandMessage] =
    new Codec[AdminCommandMessage] {
      def decode: Get[AdminCommandMessage] =
        for {
          content <- utf8(ushort).decode
        } yield AdminCommandMessage(content)

      def encode(value: AdminCommandMessage): ByteVector =
        utf8(ushort).encode(value.content)
    }
}
