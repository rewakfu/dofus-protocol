package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicStatWithDataMessage(
  timeSpent: Double,
  statId: Short,
  datas: List[StatisticData]
) extends Message(6573)

object BasicStatWithDataMessage {
  implicit val codec: Codec[BasicStatWithDataMessage] =
    new Codec[BasicStatWithDataMessage] {
      def decode: Get[BasicStatWithDataMessage] =
        for {
          timeSpent <- double.decode
          statId <- varShort.decode
          datas <- list(ushort, Codec[StatisticData]).decode
        } yield BasicStatWithDataMessage(timeSpent, statId, datas)

      def encode(value: BasicStatWithDataMessage): ByteVector =
        double.encode(value.timeSpent) ++
        varShort.encode(value.statId) ++
        list(ushort, Codec[StatisticData]).encode(value.datas)
    }
}
