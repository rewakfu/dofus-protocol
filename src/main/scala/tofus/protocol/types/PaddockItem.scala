package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockItem(
  cellId: Short,
  objectGID: Short,
  durability: ItemDurability
) extends ObjectItemInRolePlay {
  override val protocolId = 185
}

object PaddockItem {
  implicit val codec: Codec[PaddockItem] =
    new Codec[PaddockItem] {
      def decode: Get[PaddockItem] =
        for {
          cellId <- varShort.decode
          objectGID <- varShort.decode
          durability <- Codec[ItemDurability].decode
        } yield PaddockItem(cellId, objectGID, durability)

      def encode(value: PaddockItem): ByteVector =
        varShort.encode(value.cellId) ++
        varShort.encode(value.objectGID) ++
        Codec[ItemDurability].encode(value.durability)
    }
}
