package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait IgnoredInformations extends AbstractContactInformations

final case class ConcreteIgnoredInformations(
  accountId: Int,
  accountName: String
) extends IgnoredInformations {
  override val protocolId = 106
}

object ConcreteIgnoredInformations {
  implicit val codec: Codec[ConcreteIgnoredInformations] =  
    new Codec[ConcreteIgnoredInformations] {
      def decode: Get[ConcreteIgnoredInformations] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
        } yield ConcreteIgnoredInformations(accountId, accountName)

      def encode(value: ConcreteIgnoredInformations): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName)
    }
}

object IgnoredInformations {
  implicit val codec: Codec[IgnoredInformations] =
    new Codec[IgnoredInformations] {
      def decode: Get[IgnoredInformations] =
        ushort.decode.flatMap {
          case 106 => Codec[ConcreteIgnoredInformations].decode
          case 105 => Codec[IgnoredOnlineInformations].decode
        }

      def encode(value: IgnoredInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteIgnoredInformations => Codec[ConcreteIgnoredInformations].encode(i)
          case i: IgnoredOnlineInformations => Codec[IgnoredOnlineInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
