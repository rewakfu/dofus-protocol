package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagPackListMessage(
  packIds: ByteVector
) extends Message(6620)

object HavenBagPackListMessage {
  implicit val codec: Codec[HavenBagPackListMessage] =
    new Codec[HavenBagPackListMessage] {
      def decode: Get[HavenBagPackListMessage] =
        for {
          packIds <- bytes(ushort).decode
        } yield HavenBagPackListMessage(packIds)

      def encode(value: HavenBagPackListMessage): ByteVector =
        bytes(ushort).encode(value.packIds)
    }
}
