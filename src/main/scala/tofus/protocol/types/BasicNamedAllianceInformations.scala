package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait BasicNamedAllianceInformations extends BasicAllianceInformations

final case class ConcreteBasicNamedAllianceInformations(
  allianceId: Int,
  allianceTag: String,
  allianceName: String
) extends BasicNamedAllianceInformations {
  override val protocolId = 418
}

object ConcreteBasicNamedAllianceInformations {
  implicit val codec: Codec[ConcreteBasicNamedAllianceInformations] =  
    new Codec[ConcreteBasicNamedAllianceInformations] {
      def decode: Get[ConcreteBasicNamedAllianceInformations] =
        for {
          allianceId <- varInt.decode
          allianceTag <- utf8(ushort).decode
          allianceName <- utf8(ushort).decode
        } yield ConcreteBasicNamedAllianceInformations(allianceId, allianceTag, allianceName)

      def encode(value: ConcreteBasicNamedAllianceInformations): ByteVector =
        varInt.encode(value.allianceId) ++
        utf8(ushort).encode(value.allianceTag) ++
        utf8(ushort).encode(value.allianceName)
    }
}

object BasicNamedAllianceInformations {
  implicit val codec: Codec[BasicNamedAllianceInformations] =
    new Codec[BasicNamedAllianceInformations] {
      def decode: Get[BasicNamedAllianceInformations] =
        ushort.decode.flatMap {
          case 418 => Codec[ConcreteBasicNamedAllianceInformations].decode
          case 421 => Codec[AllianceFactSheetInformations].decode
          case 417 => Codec[ConcreteAllianceInformations].decode
        }

      def encode(value: BasicNamedAllianceInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteBasicNamedAllianceInformations => Codec[ConcreteBasicNamedAllianceInformations].encode(i)
          case i: AllianceFactSheetInformations => Codec[AllianceFactSheetInformations].encode(i)
          case i: ConcreteAllianceInformations => Codec[ConcreteAllianceInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
