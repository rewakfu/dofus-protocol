package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntFlagRemoveRequestMessage(
  questType: Byte,
  index: Byte
) extends Message(6510)

object TreasureHuntFlagRemoveRequestMessage {
  implicit val codec: Codec[TreasureHuntFlagRemoveRequestMessage] =
    new Codec[TreasureHuntFlagRemoveRequestMessage] {
      def decode: Get[TreasureHuntFlagRemoveRequestMessage] =
        for {
          questType <- byte.decode
          index <- byte.decode
        } yield TreasureHuntFlagRemoveRequestMessage(questType, index)

      def encode(value: TreasureHuntFlagRemoveRequestMessage): ByteVector =
        byte.encode(value.questType) ++
        byte.encode(value.index)
    }
}
