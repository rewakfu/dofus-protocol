package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightFighterNamedLightInformations(
  flags0: Byte,
  id: Double,
  wave: Byte,
  level: Short,
  breed: Byte,
  name: String
) extends GameFightFighterLightInformations {
  override val protocolId = 456
}

object GameFightFighterNamedLightInformations {
  implicit val codec: Codec[GameFightFighterNamedLightInformations] =
    new Codec[GameFightFighterNamedLightInformations] {
      def decode: Get[GameFightFighterNamedLightInformations] =
        for {
          flags0 <- byte.decode
          id <- double.decode
          wave <- byte.decode
          level <- varShort.decode
          breed <- byte.decode
          name <- utf8(ushort).decode
        } yield GameFightFighterNamedLightInformations(flags0, id, wave, level, breed, name)

      def encode(value: GameFightFighterNamedLightInformations): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.id) ++
        byte.encode(value.wave) ++
        varShort.encode(value.level) ++
        byte.encode(value.breed) ++
        utf8(ushort).encode(value.name)
    }
}
