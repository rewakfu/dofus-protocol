package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MigratedServerListMessage(
  migratedServerIds: List[Short]
) extends Message(6731)

object MigratedServerListMessage {
  implicit val codec: Codec[MigratedServerListMessage] =
    new Codec[MigratedServerListMessage] {
      def decode: Get[MigratedServerListMessage] =
        for {
          migratedServerIds <- list(ushort, varShort).decode
        } yield MigratedServerListMessage(migratedServerIds)

      def encode(value: MigratedServerListMessage): ByteVector =
        list(ushort, varShort).encode(value.migratedServerIds)
    }
}
