package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterSpellModification(
  modificationType: Byte,
  spellId: Short,
  value: CharacterBaseCharacteristic
) extends ProtocolType {
  override val protocolId = 215
}

object CharacterSpellModification {
  implicit val codec: Codec[CharacterSpellModification] =
    new Codec[CharacterSpellModification] {
      def decode: Get[CharacterSpellModification] =
        for {
          modificationType <- byte.decode
          spellId <- varShort.decode
          value <- Codec[CharacterBaseCharacteristic].decode
        } yield CharacterSpellModification(modificationType, spellId, value)

      def encode(value: CharacterSpellModification): ByteVector =
        byte.encode(value.modificationType) ++
        varShort.encode(value.spellId) ++
        Codec[CharacterBaseCharacteristic].encode(value.value)
    }
}
