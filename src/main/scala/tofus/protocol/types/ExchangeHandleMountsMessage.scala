package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeHandleMountsMessage(
  actionType: Byte,
  ridesId: List[Int]
) extends Message(6752)

object ExchangeHandleMountsMessage {
  implicit val codec: Codec[ExchangeHandleMountsMessage] =
    new Codec[ExchangeHandleMountsMessage] {
      def decode: Get[ExchangeHandleMountsMessage] =
        for {
          actionType <- byte.decode
          ridesId <- list(ushort, varInt).decode
        } yield ExchangeHandleMountsMessage(actionType, ridesId)

      def encode(value: ExchangeHandleMountsMessage): ByteVector =
        byte.encode(value.actionType) ++
        list(ushort, varInt).encode(value.ridesId)
    }
}
