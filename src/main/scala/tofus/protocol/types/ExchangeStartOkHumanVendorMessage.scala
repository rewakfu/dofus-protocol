package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkHumanVendorMessage(
  sellerId: Double,
  objectsInfos: List[ObjectItemToSellInHumanVendorShop]
) extends Message(5767)

object ExchangeStartOkHumanVendorMessage {
  implicit val codec: Codec[ExchangeStartOkHumanVendorMessage] =
    new Codec[ExchangeStartOkHumanVendorMessage] {
      def decode: Get[ExchangeStartOkHumanVendorMessage] =
        for {
          sellerId <- double.decode
          objectsInfos <- list(ushort, Codec[ObjectItemToSellInHumanVendorShop]).decode
        } yield ExchangeStartOkHumanVendorMessage(sellerId, objectsInfos)

      def encode(value: ExchangeStartOkHumanVendorMessage): ByteVector =
        double.encode(value.sellerId) ++
        list(ushort, Codec[ObjectItemToSellInHumanVendorShop]).encode(value.objectsInfos)
    }
}
