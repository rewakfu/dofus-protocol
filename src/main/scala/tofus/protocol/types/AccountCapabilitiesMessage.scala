package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccountCapabilitiesMessage(
  flags0: Byte,
  accountId: Int,
  breedsVisible: Int,
  breedsAvailable: Int,
  status: Byte
) extends Message(6216)

object AccountCapabilitiesMessage {
  implicit val codec: Codec[AccountCapabilitiesMessage] =
    new Codec[AccountCapabilitiesMessage] {
      def decode: Get[AccountCapabilitiesMessage] =
        for {
          flags0 <- byte.decode
          accountId <- int.decode
          breedsVisible <- varInt.decode
          breedsAvailable <- varInt.decode
          status <- byte.decode
        } yield AccountCapabilitiesMessage(flags0, accountId, breedsVisible, breedsAvailable, status)

      def encode(value: AccountCapabilitiesMessage): ByteVector =
        byte.encode(value.flags0) ++
        int.encode(value.accountId) ++
        varInt.encode(value.breedsVisible) ++
        varInt.encode(value.breedsAvailable) ++
        byte.encode(value.status)
    }
}
