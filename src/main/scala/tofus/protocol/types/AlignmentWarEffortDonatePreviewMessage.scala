package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlignmentWarEffortDonatePreviewMessage(
  xp: Double
) extends Message(6853)

object AlignmentWarEffortDonatePreviewMessage {
  implicit val codec: Codec[AlignmentWarEffortDonatePreviewMessage] =
    new Codec[AlignmentWarEffortDonatePreviewMessage] {
      def decode: Get[AlignmentWarEffortDonatePreviewMessage] =
        for {
          xp <- double.decode
        } yield AlignmentWarEffortDonatePreviewMessage(xp)

      def encode(value: AlignmentWarEffortDonatePreviewMessage): ByteVector =
        double.encode(value.xp)
    }
}
