package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartedMessage(
  exchangeType: Byte
) extends Message(5512)

object ExchangeStartedMessage {
  implicit val codec: Codec[ExchangeStartedMessage] =
    new Codec[ExchangeStartedMessage] {
      def decode: Get[ExchangeStartedMessage] =
        for {
          exchangeType <- byte.decode
        } yield ExchangeStartedMessage(exchangeType)

      def encode(value: ExchangeStartedMessage): ByteVector =
        byte.encode(value.exchangeType)
    }
}
