package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SlaveSwitchContextMessage(
  masterId: Double,
  slaveId: Double,
  slaveSpells: List[SpellItem],
  slaveStats: CharacterCharacteristicsInformations,
  shortcuts: List[Shortcut]
) extends Message(6214)

object SlaveSwitchContextMessage {
  implicit val codec: Codec[SlaveSwitchContextMessage] =
    new Codec[SlaveSwitchContextMessage] {
      def decode: Get[SlaveSwitchContextMessage] =
        for {
          masterId <- double.decode
          slaveId <- double.decode
          slaveSpells <- list(ushort, Codec[SpellItem]).decode
          slaveStats <- Codec[CharacterCharacteristicsInformations].decode
          shortcuts <- list(ushort, Codec[Shortcut]).decode
        } yield SlaveSwitchContextMessage(masterId, slaveId, slaveSpells, slaveStats, shortcuts)

      def encode(value: SlaveSwitchContextMessage): ByteVector =
        double.encode(value.masterId) ++
        double.encode(value.slaveId) ++
        list(ushort, Codec[SpellItem]).encode(value.slaveSpells) ++
        Codec[CharacterCharacteristicsInformations].encode(value.slaveStats) ++
        list(ushort, Codec[Shortcut]).encode(value.shortcuts)
    }
}
