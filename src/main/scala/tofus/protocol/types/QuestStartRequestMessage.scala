package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestStartRequestMessage(
  questId: Short
) extends Message(5643)

object QuestStartRequestMessage {
  implicit val codec: Codec[QuestStartRequestMessage] =
    new Codec[QuestStartRequestMessage] {
      def decode: Get[QuestStartRequestMessage] =
        for {
          questId <- varShort.decode
        } yield QuestStartRequestMessage(questId)

      def encode(value: QuestStartRequestMessage): ByteVector =
        varShort.encode(value.questId)
    }
}
