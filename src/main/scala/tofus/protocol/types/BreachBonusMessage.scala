package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachBonusMessage(
  bonus: ObjectEffectInteger
) extends Message(6800)

object BreachBonusMessage {
  implicit val codec: Codec[BreachBonusMessage] =
    new Codec[BreachBonusMessage] {
      def decode: Get[BreachBonusMessage] =
        for {
          bonus <- Codec[ObjectEffectInteger].decode
        } yield BreachBonusMessage(bonus)

      def encode(value: BreachBonusMessage): ByteVector =
        Codec[ObjectEffectInteger].encode(value.bonus)
    }
}
