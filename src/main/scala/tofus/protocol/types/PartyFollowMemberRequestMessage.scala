package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyFollowMemberRequestMessage(
  partyId: Int,
  playerId: Long
) extends Message(5577)

object PartyFollowMemberRequestMessage {
  implicit val codec: Codec[PartyFollowMemberRequestMessage] =
    new Codec[PartyFollowMemberRequestMessage] {
      def decode: Get[PartyFollowMemberRequestMessage] =
        for {
          partyId <- varInt.decode
          playerId <- varLong.decode
        } yield PartyFollowMemberRequestMessage(partyId, playerId)

      def encode(value: PartyFollowMemberRequestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.playerId)
    }
}
