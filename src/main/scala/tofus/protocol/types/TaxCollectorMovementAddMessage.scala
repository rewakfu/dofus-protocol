package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorMovementAddMessage(
  informations: TaxCollectorInformations
) extends Message(5917)

object TaxCollectorMovementAddMessage {
  implicit val codec: Codec[TaxCollectorMovementAddMessage] =
    new Codec[TaxCollectorMovementAddMessage] {
      def decode: Get[TaxCollectorMovementAddMessage] =
        for {
          informations <- Codec[TaxCollectorInformations].decode
        } yield TaxCollectorMovementAddMessage(informations)

      def encode(value: TaxCollectorMovementAddMessage): ByteVector =
        Codec[TaxCollectorInformations].encode(value.informations)
    }
}
