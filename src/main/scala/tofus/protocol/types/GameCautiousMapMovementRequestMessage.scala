package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameCautiousMapMovementRequestMessage(
  keyMovements: List[Short],
  mapId: Double
) extends Message(6496)

object GameCautiousMapMovementRequestMessage {
  implicit val codec: Codec[GameCautiousMapMovementRequestMessage] =
    new Codec[GameCautiousMapMovementRequestMessage] {
      def decode: Get[GameCautiousMapMovementRequestMessage] =
        for {
          keyMovements <- list(ushort, short).decode
          mapId <- double.decode
        } yield GameCautiousMapMovementRequestMessage(keyMovements, mapId)

      def encode(value: GameCautiousMapMovementRequestMessage): ByteVector =
        list(ushort, short).encode(value.keyMovements) ++
        double.encode(value.mapId)
    }
}
