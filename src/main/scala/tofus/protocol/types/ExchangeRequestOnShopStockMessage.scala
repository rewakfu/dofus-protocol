package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeRequestOnShopStockMessage(

) extends Message(5753)

object ExchangeRequestOnShopStockMessage {
  implicit val codec: Codec[ExchangeRequestOnShopStockMessage] =
    Codec.const(ExchangeRequestOnShopStockMessage())
}
