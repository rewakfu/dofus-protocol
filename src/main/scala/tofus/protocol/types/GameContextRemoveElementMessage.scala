package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextRemoveElementMessage(
  id: Double
) extends Message(251)

object GameContextRemoveElementMessage {
  implicit val codec: Codec[GameContextRemoveElementMessage] =
    new Codec[GameContextRemoveElementMessage] {
      def decode: Get[GameContextRemoveElementMessage] =
        for {
          id <- double.decode
        } yield GameContextRemoveElementMessage(id)

      def encode(value: GameContextRemoveElementMessage): ByteVector =
        double.encode(value.id)
    }
}
