package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdentificationFailedMessage(
  reason: Byte
) extends Message(20)

object IdentificationFailedMessage {
  implicit val codec: Codec[IdentificationFailedMessage] =
    new Codec[IdentificationFailedMessage] {
      def decode: Get[IdentificationFailedMessage] =
        for {
          reason <- byte.decode
        } yield IdentificationFailedMessage(reason)

      def encode(value: IdentificationFailedMessage): ByteVector =
        byte.encode(value.reason)
    }
}
