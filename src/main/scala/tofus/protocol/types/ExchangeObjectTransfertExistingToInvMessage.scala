package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectTransfertExistingToInvMessage(

) extends Message(6326)

object ExchangeObjectTransfertExistingToInvMessage {
  implicit val codec: Codec[ExchangeObjectTransfertExistingToInvMessage] =
    Codec.const(ExchangeObjectTransfertExistingToInvMessage())
}
