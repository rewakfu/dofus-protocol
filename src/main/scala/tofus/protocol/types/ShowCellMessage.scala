package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShowCellMessage(
  sourceId: Double,
  cellId: Short
) extends Message(5612)

object ShowCellMessage {
  implicit val codec: Codec[ShowCellMessage] =
    new Codec[ShowCellMessage] {
      def decode: Get[ShowCellMessage] =
        for {
          sourceId <- double.decode
          cellId <- varShort.decode
        } yield ShowCellMessage(sourceId, cellId)

      def encode(value: ShowCellMessage): ByteVector =
        double.encode(value.sourceId) ++
        varShort.encode(value.cellId)
    }
}
