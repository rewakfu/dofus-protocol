package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceMotdSetErrorMessage(
  reason: Byte
) extends Message(6683)

object AllianceMotdSetErrorMessage {
  implicit val codec: Codec[AllianceMotdSetErrorMessage] =
    new Codec[AllianceMotdSetErrorMessage] {
      def decode: Get[AllianceMotdSetErrorMessage] =
        for {
          reason <- byte.decode
        } yield AllianceMotdSetErrorMessage(reason)

      def encode(value: AllianceMotdSetErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
