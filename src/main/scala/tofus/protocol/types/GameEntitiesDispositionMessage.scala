package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameEntitiesDispositionMessage(
  dispositions: List[IdentifiedEntityDispositionInformations]
) extends Message(5696)

object GameEntitiesDispositionMessage {
  implicit val codec: Codec[GameEntitiesDispositionMessage] =
    new Codec[GameEntitiesDispositionMessage] {
      def decode: Get[GameEntitiesDispositionMessage] =
        for {
          dispositions <- list(ushort, Codec[IdentifiedEntityDispositionInformations]).decode
        } yield GameEntitiesDispositionMessage(dispositions)

      def encode(value: GameEntitiesDispositionMessage): ByteVector =
        list(ushort, Codec[IdentifiedEntityDispositionInformations]).encode(value.dispositions)
    }
}
