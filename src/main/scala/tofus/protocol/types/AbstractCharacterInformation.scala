package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AbstractCharacterInformation extends ProtocolType

final case class ConcreteAbstractCharacterInformation(
  id: Long
) extends AbstractCharacterInformation {
  override val protocolId = 400
}

object ConcreteAbstractCharacterInformation {
  implicit val codec: Codec[ConcreteAbstractCharacterInformation] =  
    new Codec[ConcreteAbstractCharacterInformation] {
      def decode: Get[ConcreteAbstractCharacterInformation] =
        for {
          id <- varLong.decode
        } yield ConcreteAbstractCharacterInformation(id)

      def encode(value: ConcreteAbstractCharacterInformation): ByteVector =
        varLong.encode(value.id)
    }
}

object AbstractCharacterInformation {
  implicit val codec: Codec[AbstractCharacterInformation] =
    new Codec[AbstractCharacterInformation] {
      def decode: Get[AbstractCharacterInformation] =
        ushort.decode.flatMap {
          case 400 => Codec[ConcreteAbstractCharacterInformation].decode
          case 391 => Codec[PartyMemberArenaInformations].decode
          case 90 => Codec[ConcretePartyMemberInformations].decode
          case 376 => Codec[PartyInvitationMemberInformations].decode
          case 474 => Codec[CharacterHardcoreOrEpicInformations].decode
          case 45 => Codec[ConcreteCharacterBaseInformations].decode
          case 193 => Codec[CharacterMinimalPlusLookAndGradeInformations].decode
          case 444 => Codec[CharacterMinimalAllianceInformations].decode
          case 445 => Codec[ConcreteCharacterMinimalGuildInformations].decode
          case 163 => Codec[ConcreteCharacterMinimalPlusLookInformations].decode
          case 88 => Codec[GuildMember].decode
          case 556 => Codec[CharacterMinimalGuildPublicInformations].decode
          case 110 => Codec[ConcreteCharacterMinimalInformations].decode
          case 503 => Codec[ConcreteCharacterBasicMinimalInformations].decode
          case 477 => Codec[CharacterToRemodelInformations].decode
          case 479 => Codec[ConcreteCharacterRemodelingInformation].decode
        }

      def encode(value: AbstractCharacterInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAbstractCharacterInformation => Codec[ConcreteAbstractCharacterInformation].encode(i)
          case i: PartyMemberArenaInformations => Codec[PartyMemberArenaInformations].encode(i)
          case i: ConcretePartyMemberInformations => Codec[ConcretePartyMemberInformations].encode(i)
          case i: PartyInvitationMemberInformations => Codec[PartyInvitationMemberInformations].encode(i)
          case i: CharacterHardcoreOrEpicInformations => Codec[CharacterHardcoreOrEpicInformations].encode(i)
          case i: ConcreteCharacterBaseInformations => Codec[ConcreteCharacterBaseInformations].encode(i)
          case i: CharacterMinimalPlusLookAndGradeInformations => Codec[CharacterMinimalPlusLookAndGradeInformations].encode(i)
          case i: CharacterMinimalAllianceInformations => Codec[CharacterMinimalAllianceInformations].encode(i)
          case i: ConcreteCharacterMinimalGuildInformations => Codec[ConcreteCharacterMinimalGuildInformations].encode(i)
          case i: ConcreteCharacterMinimalPlusLookInformations => Codec[ConcreteCharacterMinimalPlusLookInformations].encode(i)
          case i: GuildMember => Codec[GuildMember].encode(i)
          case i: CharacterMinimalGuildPublicInformations => Codec[CharacterMinimalGuildPublicInformations].encode(i)
          case i: ConcreteCharacterMinimalInformations => Codec[ConcreteCharacterMinimalInformations].encode(i)
          case i: ConcreteCharacterBasicMinimalInformations => Codec[ConcreteCharacterBasicMinimalInformations].encode(i)
          case i: CharacterToRemodelInformations => Codec[CharacterToRemodelInformations].encode(i)
          case i: ConcreteCharacterRemodelingInformation => Codec[ConcreteCharacterRemodelingInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
