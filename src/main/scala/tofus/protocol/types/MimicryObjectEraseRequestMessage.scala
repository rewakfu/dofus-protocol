package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MimicryObjectEraseRequestMessage(
  hostUID: Int,
  hostPos: Short
) extends Message(6457)

object MimicryObjectEraseRequestMessage {
  implicit val codec: Codec[MimicryObjectEraseRequestMessage] =
    new Codec[MimicryObjectEraseRequestMessage] {
      def decode: Get[MimicryObjectEraseRequestMessage] =
        for {
          hostUID <- varInt.decode
          hostPos <- ubyte.decode
        } yield MimicryObjectEraseRequestMessage(hostUID, hostPos)

      def encode(value: MimicryObjectEraseRequestMessage): ByteVector =
        varInt.encode(value.hostUID) ++
        ubyte.encode(value.hostPos)
    }
}
