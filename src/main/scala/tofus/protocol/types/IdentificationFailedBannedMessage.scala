package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdentificationFailedBannedMessage(
  reason: Byte,
  banEndDate: Double
) extends Message(6174)

object IdentificationFailedBannedMessage {
  implicit val codec: Codec[IdentificationFailedBannedMessage] =
    new Codec[IdentificationFailedBannedMessage] {
      def decode: Get[IdentificationFailedBannedMessage] =
        for {
          reason <- byte.decode
          banEndDate <- double.decode
        } yield IdentificationFailedBannedMessage(reason, banEndDate)

      def encode(value: IdentificationFailedBannedMessage): ByteVector =
        byte.encode(value.reason) ++
        double.encode(value.banEndDate)
    }
}
