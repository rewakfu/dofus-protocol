package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ConsoleCommandsListMessage(
  aliases: List[String],
  args: List[String],
  descriptions: List[String]
) extends Message(6127)

object ConsoleCommandsListMessage {
  implicit val codec: Codec[ConsoleCommandsListMessage] =
    new Codec[ConsoleCommandsListMessage] {
      def decode: Get[ConsoleCommandsListMessage] =
        for {
          aliases <- list(ushort, utf8(ushort)).decode
          args <- list(ushort, utf8(ushort)).decode
          descriptions <- list(ushort, utf8(ushort)).decode
        } yield ConsoleCommandsListMessage(aliases, args, descriptions)

      def encode(value: ConsoleCommandsListMessage): ByteVector =
        list(ushort, utf8(ushort)).encode(value.aliases) ++
        list(ushort, utf8(ushort)).encode(value.args) ++
        list(ushort, utf8(ushort)).encode(value.descriptions)
    }
}
