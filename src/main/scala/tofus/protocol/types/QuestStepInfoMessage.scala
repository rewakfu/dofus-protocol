package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestStepInfoMessage(
  infos: QuestActiveInformations
) extends Message(5625)

object QuestStepInfoMessage {
  implicit val codec: Codec[QuestStepInfoMessage] =
    new Codec[QuestStepInfoMessage] {
      def decode: Get[QuestStepInfoMessage] =
        for {
          infos <- Codec[QuestActiveInformations].decode
        } yield QuestStepInfoMessage(infos)

      def encode(value: QuestStepInfoMessage): ByteVector =
        Codec[QuestActiveInformations].encode(value.infos)
    }
}
