package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait Item extends ProtocolType

final case class ConcreteItem(

) extends Item {
  override val protocolId = 7
}

object ConcreteItem {
  implicit val codec: Codec[ConcreteItem] =  
    Codec.const(ConcreteItem())
}

object Item {
  implicit val codec: Codec[Item] =
    new Codec[Item] {
      def decode: Get[Item] =
        ushort.decode.flatMap {
          case 7 => Codec[ConcreteItem].decode
          case 123 => Codec[GoldItem].decode
          case 49 => Codec[SpellItem].decode
          case 359 => Codec[ObjectItemToSellInHumanVendorShop].decode
          case 119 => Codec[ObjectItemQuantity].decode
          case 134 => Codec[ObjectItemNotInContainer].decode
          case 577 => Codec[ObjectItemQuantityPriceDateEffects].decode
          case 483 => Codec[ConcreteObjectItemGenericQuantity].decode
          case 164 => Codec[ObjectItemToSellInBid].decode
          case 120 => Codec[ConcreteObjectItemToSell].decode
          case 37 => Codec[ObjectItem].decode
          case 352 => Codec[ObjectItemToSellInNpcShop].decode
          case 387 => Codec[ObjectItemInformationWithQuantity].decode
          case 124 => Codec[ConcreteObjectItemMinimalInformation].decode
        }

      def encode(value: Item): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteItem => Codec[ConcreteItem].encode(i)
          case i: GoldItem => Codec[GoldItem].encode(i)
          case i: SpellItem => Codec[SpellItem].encode(i)
          case i: ObjectItemToSellInHumanVendorShop => Codec[ObjectItemToSellInHumanVendorShop].encode(i)
          case i: ObjectItemQuantity => Codec[ObjectItemQuantity].encode(i)
          case i: ObjectItemNotInContainer => Codec[ObjectItemNotInContainer].encode(i)
          case i: ObjectItemQuantityPriceDateEffects => Codec[ObjectItemQuantityPriceDateEffects].encode(i)
          case i: ConcreteObjectItemGenericQuantity => Codec[ConcreteObjectItemGenericQuantity].encode(i)
          case i: ObjectItemToSellInBid => Codec[ObjectItemToSellInBid].encode(i)
          case i: ConcreteObjectItemToSell => Codec[ConcreteObjectItemToSell].encode(i)
          case i: ObjectItem => Codec[ObjectItem].encode(i)
          case i: ObjectItemToSellInNpcShop => Codec[ObjectItemToSellInNpcShop].encode(i)
          case i: ObjectItemInformationWithQuantity => Codec[ObjectItemInformationWithQuantity].encode(i)
          case i: ConcreteObjectItemMinimalInformation => Codec[ConcreteObjectItemMinimalInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
