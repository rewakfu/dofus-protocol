package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class WrapperObjectErrorMessage(
  reason: Byte,
  errorCode: Byte
) extends Message(6529)

object WrapperObjectErrorMessage {
  implicit val codec: Codec[WrapperObjectErrorMessage] =
    new Codec[WrapperObjectErrorMessage] {
      def decode: Get[WrapperObjectErrorMessage] =
        for {
          reason <- byte.decode
          errorCode <- byte.decode
        } yield WrapperObjectErrorMessage(reason, errorCode)

      def encode(value: WrapperObjectErrorMessage): ByteVector =
        byte.encode(value.reason) ++
        byte.encode(value.errorCode)
    }
}
