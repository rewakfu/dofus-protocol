package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectModifiedInBagMessage(
  remote: Boolean,
  `object`: ObjectItem
) extends Message(6008)

object ExchangeObjectModifiedInBagMessage {
  implicit val codec: Codec[ExchangeObjectModifiedInBagMessage] =
    new Codec[ExchangeObjectModifiedInBagMessage] {
      def decode: Get[ExchangeObjectModifiedInBagMessage] =
        for {
          remote <- bool.decode
          `object` <- Codec[ObjectItem].decode
        } yield ExchangeObjectModifiedInBagMessage(remote, `object`)

      def encode(value: ExchangeObjectModifiedInBagMessage): ByteVector =
        bool.encode(value.remote) ++
        Codec[ObjectItem].encode(value.`object`)
    }
}
