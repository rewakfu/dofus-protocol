package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachCharactersMessage(
  characters: List[Long]
) extends Message(6811)

object BreachCharactersMessage {
  implicit val codec: Codec[BreachCharactersMessage] =
    new Codec[BreachCharactersMessage] {
      def decode: Get[BreachCharactersMessage] =
        for {
          characters <- list(ushort, varLong).decode
        } yield BreachCharactersMessage(characters)

      def encode(value: BreachCharactersMessage): ByteVector =
        list(ushort, varLong).encode(value.characters)
    }
}
