package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockInstancesInformations(
  maxOutdoorMount: Short,
  maxItems: Short,
  paddocks: List[PaddockBuyableInformations]
) extends PaddockInformations {
  override val protocolId = 509
}

object PaddockInstancesInformations {
  implicit val codec: Codec[PaddockInstancesInformations] =
    new Codec[PaddockInstancesInformations] {
      def decode: Get[PaddockInstancesInformations] =
        for {
          maxOutdoorMount <- varShort.decode
          maxItems <- varShort.decode
          paddocks <- list(ushort, Codec[PaddockBuyableInformations]).decode
        } yield PaddockInstancesInformations(maxOutdoorMount, maxItems, paddocks)

      def encode(value: PaddockInstancesInformations): ByteVector =
        varShort.encode(value.maxOutdoorMount) ++
        varShort.encode(value.maxItems) ++
        list(ushort, Codec[PaddockBuyableInformations]).encode(value.paddocks)
    }
}
