package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayTaxCollectorFightRequestMessage(

) extends Message(5954)

object GameRolePlayTaxCollectorFightRequestMessage {
  implicit val codec: Codec[GameRolePlayTaxCollectorFightRequestMessage] =
    Codec.const(GameRolePlayTaxCollectorFightRequestMessage())
}
