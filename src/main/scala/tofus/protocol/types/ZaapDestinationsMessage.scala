package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ZaapDestinationsMessage(
  `type`: Byte,
  destinations: List[TeleportDestination],
  spawnMapId: Double
) extends Message(6830)

object ZaapDestinationsMessage {
  implicit val codec: Codec[ZaapDestinationsMessage] =
    new Codec[ZaapDestinationsMessage] {
      def decode: Get[ZaapDestinationsMessage] =
        for {
          `type` <- byte.decode
          destinations <- list(ushort, Codec[TeleportDestination]).decode
          spawnMapId <- double.decode
        } yield ZaapDestinationsMessage(`type`, destinations, spawnMapId)

      def encode(value: ZaapDestinationsMessage): ByteVector =
        byte.encode(value.`type`) ++
        list(ushort, Codec[TeleportDestination]).encode(value.destinations) ++
        double.encode(value.spawnMapId)
    }
}
