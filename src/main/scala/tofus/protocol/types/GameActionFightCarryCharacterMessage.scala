package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightCarryCharacterMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  cellId: Short
) extends Message(5830)

object GameActionFightCarryCharacterMessage {
  implicit val codec: Codec[GameActionFightCarryCharacterMessage] =
    new Codec[GameActionFightCarryCharacterMessage] {
      def decode: Get[GameActionFightCarryCharacterMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          cellId <- short.decode
        } yield GameActionFightCarryCharacterMessage(actionId, sourceId, targetId, cellId)

      def encode(value: GameActionFightCarryCharacterMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.cellId)
    }
}
