package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterMinimalGuildPublicInformations(
  id: Long,
  name: String,
  level: Short,
  rank: Int
) extends CharacterMinimalInformations {
  override val protocolId = 556
}

object CharacterMinimalGuildPublicInformations {
  implicit val codec: Codec[CharacterMinimalGuildPublicInformations] =
    new Codec[CharacterMinimalGuildPublicInformations] {
      def decode: Get[CharacterMinimalGuildPublicInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          rank <- varInt.decode
        } yield CharacterMinimalGuildPublicInformations(id, name, level, rank)

      def encode(value: CharacterMinimalGuildPublicInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        varInt.encode(value.rank)
    }
}
