package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DebugHighlightCellsMessage(
  color: Double,
  cells: List[Short]
) extends Message(2001)

object DebugHighlightCellsMessage {
  implicit val codec: Codec[DebugHighlightCellsMessage] =
    new Codec[DebugHighlightCellsMessage] {
      def decode: Get[DebugHighlightCellsMessage] =
        for {
          color <- double.decode
          cells <- list(ushort, varShort).decode
        } yield DebugHighlightCellsMessage(color, cells)

      def encode(value: DebugHighlightCellsMessage): ByteVector =
        double.encode(value.color) ++
        list(ushort, varShort).encode(value.cells)
    }
}
