package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaRegistrationStatusMessage(
  registered: Boolean,
  step: Byte,
  battleMode: Int
) extends Message(6284)

object GameRolePlayArenaRegistrationStatusMessage {
  implicit val codec: Codec[GameRolePlayArenaRegistrationStatusMessage] =
    new Codec[GameRolePlayArenaRegistrationStatusMessage] {
      def decode: Get[GameRolePlayArenaRegistrationStatusMessage] =
        for {
          registered <- bool.decode
          step <- byte.decode
          battleMode <- int.decode
        } yield GameRolePlayArenaRegistrationStatusMessage(registered, step, battleMode)

      def encode(value: GameRolePlayArenaRegistrationStatusMessage): ByteVector =
        bool.encode(value.registered) ++
        byte.encode(value.step) ++
        int.encode(value.battleMode)
    }
}
