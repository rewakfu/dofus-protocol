package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightMarkCellsMessage(
  actionId: Short,
  sourceId: Double,
  mark: GameActionMark
) extends Message(5540)

object GameActionFightMarkCellsMessage {
  implicit val codec: Codec[GameActionFightMarkCellsMessage] =
    new Codec[GameActionFightMarkCellsMessage] {
      def decode: Get[GameActionFightMarkCellsMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          mark <- Codec[GameActionMark].decode
        } yield GameActionFightMarkCellsMessage(actionId, sourceId, mark)

      def encode(value: GameActionFightMarkCellsMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        Codec[GameActionMark].encode(value.mark)
    }
}
