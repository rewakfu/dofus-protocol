package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightMutantInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short],
  name: String,
  status: ConcretePlayerStatus,
  leagueId: Short,
  ladderPosition: Int,
  hiddenInPrefight: Boolean,
  powerLevel: Byte
) extends GameFightFighterNamedInformations {
  override val protocolId = 50
}

object GameFightMutantInformations {
  implicit val codec: Codec[GameFightMutantInformations] =
    new Codec[GameFightMutantInformations] {
      def decode: Get[GameFightMutantInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
          name <- utf8(ushort).decode
          status <- Codec[ConcretePlayerStatus].decode
          leagueId <- varShort.decode
          ladderPosition <- int.decode
          hiddenInPrefight <- bool.decode
          powerLevel <- byte.decode
        } yield GameFightMutantInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions, name, status, leagueId, ladderPosition, hiddenInPrefight, powerLevel)

      def encode(value: GameFightMutantInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions) ++
        utf8(ushort).encode(value.name) ++
        Codec[ConcretePlayerStatus].encode(value.status) ++
        varShort.encode(value.leagueId) ++
        int.encode(value.ladderPosition) ++
        bool.encode(value.hiddenInPrefight) ++
        byte.encode(value.powerLevel)
    }
}
