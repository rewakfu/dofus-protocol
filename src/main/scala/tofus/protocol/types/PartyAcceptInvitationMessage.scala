package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyAcceptInvitationMessage(
  partyId: Int
) extends Message(5580)

object PartyAcceptInvitationMessage {
  implicit val codec: Codec[PartyAcceptInvitationMessage] =
    new Codec[PartyAcceptInvitationMessage] {
      def decode: Get[PartyAcceptInvitationMessage] =
        for {
          partyId <- varInt.decode
        } yield PartyAcceptInvitationMessage(partyId)

      def encode(value: PartyAcceptInvitationMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
