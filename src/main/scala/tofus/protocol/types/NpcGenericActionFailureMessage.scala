package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NpcGenericActionFailureMessage(

) extends Message(5900)

object NpcGenericActionFailureMessage {
  implicit val codec: Codec[NpcGenericActionFailureMessage] =
    Codec.const(NpcGenericActionFailureMessage())
}
