package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class UpdateSelfAgressableStatusMessage(
  status: Byte,
  probationTime: Int
) extends Message(6456)

object UpdateSelfAgressableStatusMessage {
  implicit val codec: Codec[UpdateSelfAgressableStatusMessage] =
    new Codec[UpdateSelfAgressableStatusMessage] {
      def decode: Get[UpdateSelfAgressableStatusMessage] =
        for {
          status <- byte.decode
          probationTime <- int.decode
        } yield UpdateSelfAgressableStatusMessage(status, probationTime)

      def encode(value: UpdateSelfAgressableStatusMessage): ByteVector =
        byte.encode(value.status) ++
        int.encode(value.probationTime)
    }
}
