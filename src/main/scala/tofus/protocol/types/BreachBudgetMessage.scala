package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachBudgetMessage(
  bugdet: Int
) extends Message(6786)

object BreachBudgetMessage {
  implicit val codec: Codec[BreachBudgetMessage] =
    new Codec[BreachBudgetMessage] {
      def decode: Get[BreachBudgetMessage] =
        for {
          bugdet <- varInt.decode
        } yield BreachBudgetMessage(bugdet)

      def encode(value: BreachBudgetMessage): ByteVector =
        varInt.encode(value.bugdet)
    }
}
