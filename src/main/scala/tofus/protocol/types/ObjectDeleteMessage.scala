package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectDeleteMessage(
  objectUID: Int,
  quantity: Int
) extends Message(3022)

object ObjectDeleteMessage {
  implicit val codec: Codec[ObjectDeleteMessage] =
    new Codec[ObjectDeleteMessage] {
      def decode: Get[ObjectDeleteMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ObjectDeleteMessage(objectUID, quantity)

      def encode(value: ObjectDeleteMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
