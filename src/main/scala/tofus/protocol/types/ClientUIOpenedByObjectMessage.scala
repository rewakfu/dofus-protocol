package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ClientUIOpenedByObjectMessage(
  `type`: Byte,
  uid: Int
) extends Message(6463)

object ClientUIOpenedByObjectMessage {
  implicit val codec: Codec[ClientUIOpenedByObjectMessage] =
    new Codec[ClientUIOpenedByObjectMessage] {
      def decode: Get[ClientUIOpenedByObjectMessage] =
        for {
          `type` <- byte.decode
          uid <- varInt.decode
        } yield ClientUIOpenedByObjectMessage(`type`, uid)

      def encode(value: ClientUIOpenedByObjectMessage): ByteVector =
        byte.encode(value.`type`) ++
        varInt.encode(value.uid)
    }
}
