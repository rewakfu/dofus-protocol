package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightCommonInformations(
  fightId: Short,
  fightType: Byte,
  fightTeams: List[FightTeamInformations],
  fightTeamsPositions: List[Short],
  fightTeamsOptions: List[FightOptionsInformations]
) extends ProtocolType {
  override val protocolId = 43
}

object FightCommonInformations {
  implicit val codec: Codec[FightCommonInformations] =
    new Codec[FightCommonInformations] {
      def decode: Get[FightCommonInformations] =
        for {
          fightId <- varShort.decode
          fightType <- byte.decode
          fightTeams <- list(ushort, Codec[FightTeamInformations]).decode
          fightTeamsPositions <- list(ushort, varShort).decode
          fightTeamsOptions <- list(ushort, Codec[FightOptionsInformations]).decode
        } yield FightCommonInformations(fightId, fightType, fightTeams, fightTeamsPositions, fightTeamsOptions)

      def encode(value: FightCommonInformations): ByteVector =
        varShort.encode(value.fightId) ++
        byte.encode(value.fightType) ++
        list(ushort, Codec[FightTeamInformations]).encode(value.fightTeams) ++
        list(ushort, varShort).encode(value.fightTeamsPositions) ++
        list(ushort, Codec[FightOptionsInformations]).encode(value.fightTeamsOptions)
    }
}
