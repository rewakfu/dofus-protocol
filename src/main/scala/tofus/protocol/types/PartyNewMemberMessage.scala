package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyNewMemberMessage(
  partyId: Int,
  memberInformations: PartyMemberInformations
) extends Message(6306)

object PartyNewMemberMessage {
  implicit val codec: Codec[PartyNewMemberMessage] =
    new Codec[PartyNewMemberMessage] {
      def decode: Get[PartyNewMemberMessage] =
        for {
          partyId <- varInt.decode
          memberInformations <- Codec[PartyMemberInformations].decode
        } yield PartyNewMemberMessage(partyId, memberInformations)

      def encode(value: PartyNewMemberMessage): ByteVector =
        varInt.encode(value.partyId) ++
        Codec[PartyMemberInformations].encode(value.memberInformations)
    }
}
