package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismInfoJoinLeaveRequestMessage(
  join: Boolean
) extends Message(5844)

object PrismInfoJoinLeaveRequestMessage {
  implicit val codec: Codec[PrismInfoJoinLeaveRequestMessage] =
    new Codec[PrismInfoJoinLeaveRequestMessage] {
      def decode: Get[PrismInfoJoinLeaveRequestMessage] =
        for {
          join <- bool.decode
        } yield PrismInfoJoinLeaveRequestMessage(join)

      def encode(value: PrismInfoJoinLeaveRequestMessage): ByteVector =
        bool.encode(value.join)
    }
}
