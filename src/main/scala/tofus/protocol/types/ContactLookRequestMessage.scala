package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ContactLookRequestMessage(
  requestId: Short,
  contactType: Byte
) extends Message(5932)

object ContactLookRequestMessage {
  implicit val codec: Codec[ContactLookRequestMessage] =
    new Codec[ContactLookRequestMessage] {
      def decode: Get[ContactLookRequestMessage] =
        for {
          requestId <- ubyte.decode
          contactType <- byte.decode
        } yield ContactLookRequestMessage(requestId, contactType)

      def encode(value: ContactLookRequestMessage): ByteVector =
        ubyte.encode(value.requestId) ++
        byte.encode(value.contactType)
    }
}
