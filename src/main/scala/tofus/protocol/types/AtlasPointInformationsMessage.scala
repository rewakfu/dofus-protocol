package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AtlasPointInformationsMessage(
  `type`: AtlasPointsInformations
) extends Message(5956)

object AtlasPointInformationsMessage {
  implicit val codec: Codec[AtlasPointInformationsMessage] =
    new Codec[AtlasPointInformationsMessage] {
      def decode: Get[AtlasPointInformationsMessage] =
        for {
          `type` <- Codec[AtlasPointsInformations].decode
        } yield AtlasPointInformationsMessage(`type`)

      def encode(value: AtlasPointInformationsMessage): ByteVector =
        Codec[AtlasPointsInformations].encode(value.`type`)
    }
}
