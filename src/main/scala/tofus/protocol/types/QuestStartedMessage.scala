package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestStartedMessage(
  questId: Short
) extends Message(6091)

object QuestStartedMessage {
  implicit val codec: Codec[QuestStartedMessage] =
    new Codec[QuestStartedMessage] {
      def decode: Get[QuestStartedMessage] =
        for {
          questId <- varShort.decode
        } yield QuestStartedMessage(questId)

      def encode(value: QuestStartedMessage): ByteVector =
        varShort.encode(value.questId)
    }
}
