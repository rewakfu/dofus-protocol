package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AbstractFightDispellableEffect extends ProtocolType

final case class ConcreteAbstractFightDispellableEffect(
  uid: Int,
  targetId: Double,
  turnDuration: Short,
  dispelable: Byte,
  spellId: Short,
  effectId: Int,
  parentBoostUid: Int
) extends AbstractFightDispellableEffect {
  override val protocolId = 206
}

object ConcreteAbstractFightDispellableEffect {
  implicit val codec: Codec[ConcreteAbstractFightDispellableEffect] =  
    new Codec[ConcreteAbstractFightDispellableEffect] {
      def decode: Get[ConcreteAbstractFightDispellableEffect] =
        for {
          uid <- varInt.decode
          targetId <- double.decode
          turnDuration <- short.decode
          dispelable <- byte.decode
          spellId <- varShort.decode
          effectId <- varInt.decode
          parentBoostUid <- varInt.decode
        } yield ConcreteAbstractFightDispellableEffect(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid)

      def encode(value: ConcreteAbstractFightDispellableEffect): ByteVector =
        varInt.encode(value.uid) ++
        double.encode(value.targetId) ++
        short.encode(value.turnDuration) ++
        byte.encode(value.dispelable) ++
        varShort.encode(value.spellId) ++
        varInt.encode(value.effectId) ++
        varInt.encode(value.parentBoostUid)
    }
}

object AbstractFightDispellableEffect {
  implicit val codec: Codec[AbstractFightDispellableEffect] =
    new Codec[AbstractFightDispellableEffect] {
      def decode: Get[AbstractFightDispellableEffect] =
        ushort.decode.flatMap {
          case 206 => Codec[ConcreteAbstractFightDispellableEffect].decode
          case 207 => Codec[FightTemporarySpellBoostEffect].decode
          case 214 => Codec[FightTemporaryBoostStateEffect].decode
          case 211 => Codec[FightTemporaryBoostWeaponDamagesEffect].decode
          case 209 => Codec[ConcreteFightTemporaryBoostEffect].decode
          case 366 => Codec[FightTemporarySpellImmunityEffect].decode
          case 210 => Codec[FightTriggeredEffect].decode
        }

      def encode(value: AbstractFightDispellableEffect): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAbstractFightDispellableEffect => Codec[ConcreteAbstractFightDispellableEffect].encode(i)
          case i: FightTemporarySpellBoostEffect => Codec[FightTemporarySpellBoostEffect].encode(i)
          case i: FightTemporaryBoostStateEffect => Codec[FightTemporaryBoostStateEffect].encode(i)
          case i: FightTemporaryBoostWeaponDamagesEffect => Codec[FightTemporaryBoostWeaponDamagesEffect].encode(i)
          case i: ConcreteFightTemporaryBoostEffect => Codec[ConcreteFightTemporaryBoostEffect].encode(i)
          case i: FightTemporarySpellImmunityEffect => Codec[FightTemporarySpellImmunityEffect].encode(i)
          case i: FightTriggeredEffect => Codec[FightTriggeredEffect].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
