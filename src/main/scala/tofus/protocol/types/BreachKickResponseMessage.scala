package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachKickResponseMessage(
  target: ConcreteCharacterMinimalInformations,
  kicked: Boolean
) extends Message(6789)

object BreachKickResponseMessage {
  implicit val codec: Codec[BreachKickResponseMessage] =
    new Codec[BreachKickResponseMessage] {
      def decode: Get[BreachKickResponseMessage] =
        for {
          target <- Codec[ConcreteCharacterMinimalInformations].decode
          kicked <- bool.decode
        } yield BreachKickResponseMessage(target, kicked)

      def encode(value: BreachKickResponseMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.target) ++
        bool.encode(value.kicked)
    }
}
