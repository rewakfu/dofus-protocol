package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkMulticraftCustomerMessage(
  skillId: Int,
  crafterJobLevel: Short
) extends Message(5817)

object ExchangeStartOkMulticraftCustomerMessage {
  implicit val codec: Codec[ExchangeStartOkMulticraftCustomerMessage] =
    new Codec[ExchangeStartOkMulticraftCustomerMessage] {
      def decode: Get[ExchangeStartOkMulticraftCustomerMessage] =
        for {
          skillId <- varInt.decode
          crafterJobLevel <- ubyte.decode
        } yield ExchangeStartOkMulticraftCustomerMessage(skillId, crafterJobLevel)

      def encode(value: ExchangeStartOkMulticraftCustomerMessage): ByteVector =
        varInt.encode(value.skillId) ++
        ubyte.encode(value.crafterJobLevel)
    }
}
