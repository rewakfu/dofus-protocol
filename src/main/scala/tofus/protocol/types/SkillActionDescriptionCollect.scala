package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SkillActionDescriptionCollect(
  skillId: Short,
  time: Short,
  min: Short,
  max: Short
) extends SkillActionDescriptionTimed {
  override val protocolId = 99
}

object SkillActionDescriptionCollect {
  implicit val codec: Codec[SkillActionDescriptionCollect] =
    new Codec[SkillActionDescriptionCollect] {
      def decode: Get[SkillActionDescriptionCollect] =
        for {
          skillId <- varShort.decode
          time <- ubyte.decode
          min <- varShort.decode
          max <- varShort.decode
        } yield SkillActionDescriptionCollect(skillId, time, min, max)

      def encode(value: SkillActionDescriptionCollect): ByteVector =
        varShort.encode(value.skillId) ++
        ubyte.encode(value.time) ++
        varShort.encode(value.min) ++
        varShort.encode(value.max)
    }
}
