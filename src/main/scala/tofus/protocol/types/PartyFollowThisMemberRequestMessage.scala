package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyFollowThisMemberRequestMessage(
  partyId: Int,
  playerId: Long,
  enabled: Boolean
) extends Message(5588)

object PartyFollowThisMemberRequestMessage {
  implicit val codec: Codec[PartyFollowThisMemberRequestMessage] =
    new Codec[PartyFollowThisMemberRequestMessage] {
      def decode: Get[PartyFollowThisMemberRequestMessage] =
        for {
          partyId <- varInt.decode
          playerId <- varLong.decode
          enabled <- bool.decode
        } yield PartyFollowThisMemberRequestMessage(partyId, playerId, enabled)

      def encode(value: PartyFollowThisMemberRequestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.playerId) ++
        bool.encode(value.enabled)
    }
}
