package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightAddedMessage(
  fight: PrismFightersInformation
) extends Message(6452)

object PrismFightAddedMessage {
  implicit val codec: Codec[PrismFightAddedMessage] =
    new Codec[PrismFightAddedMessage] {
      def decode: Get[PrismFightAddedMessage] =
        for {
          fight <- Codec[PrismFightersInformation].decode
        } yield PrismFightAddedMessage(fight)

      def encode(value: PrismFightAddedMessage): ByteVector =
        Codec[PrismFightersInformation].encode(value.fight)
    }
}
