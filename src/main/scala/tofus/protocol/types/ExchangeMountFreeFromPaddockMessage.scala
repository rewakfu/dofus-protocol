package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountFreeFromPaddockMessage(
  name: String,
  worldX: Short,
  worldY: Short,
  liberator: String
) extends Message(6055)

object ExchangeMountFreeFromPaddockMessage {
  implicit val codec: Codec[ExchangeMountFreeFromPaddockMessage] =
    new Codec[ExchangeMountFreeFromPaddockMessage] {
      def decode: Get[ExchangeMountFreeFromPaddockMessage] =
        for {
          name <- utf8(ushort).decode
          worldX <- short.decode
          worldY <- short.decode
          liberator <- utf8(ushort).decode
        } yield ExchangeMountFreeFromPaddockMessage(name, worldX, worldY, liberator)

      def encode(value: ExchangeMountFreeFromPaddockMessage): ByteVector =
        utf8(ushort).encode(value.name) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        utf8(ushort).encode(value.liberator)
    }
}
