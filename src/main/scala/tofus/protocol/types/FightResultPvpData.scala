package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightResultPvpData(
  grade: Short,
  minHonorForGrade: Short,
  maxHonorForGrade: Short,
  honor: Short,
  honorDelta: Short
) extends FightResultAdditionalData {
  override val protocolId = 190
}

object FightResultPvpData {
  implicit val codec: Codec[FightResultPvpData] =
    new Codec[FightResultPvpData] {
      def decode: Get[FightResultPvpData] =
        for {
          grade <- ubyte.decode
          minHonorForGrade <- varShort.decode
          maxHonorForGrade <- varShort.decode
          honor <- varShort.decode
          honorDelta <- varShort.decode
        } yield FightResultPvpData(grade, minHonorForGrade, maxHonorForGrade, honor, honorDelta)

      def encode(value: FightResultPvpData): ByteVector =
        ubyte.encode(value.grade) ++
        varShort.encode(value.minHonorForGrade) ++
        varShort.encode(value.maxHonorForGrade) ++
        varShort.encode(value.honor) ++
        varShort.encode(value.honorDelta)
    }
}
