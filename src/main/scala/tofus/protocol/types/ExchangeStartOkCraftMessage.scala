package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkCraftMessage(

) extends Message(5813)

object ExchangeStartOkCraftMessage {
  implicit val codec: Codec[ExchangeStartOkCraftMessage] =
    Codec.const(ExchangeStartOkCraftMessage())
}
