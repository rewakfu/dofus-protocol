package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextQuitMessage(

) extends Message(255)

object GameContextQuitMessage {
  implicit val codec: Codec[GameContextQuitMessage] =
    Codec.const(GameContextQuitMessage())
}
