package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectErrorMessage(
  reason: Byte
) extends Message(3004)

object ObjectErrorMessage {
  implicit val codec: Codec[ObjectErrorMessage] =
    new Codec[ObjectErrorMessage] {
      def decode: Get[ObjectErrorMessage] =
        for {
          reason <- byte.decode
        } yield ObjectErrorMessage(reason)

      def encode(value: ObjectErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
