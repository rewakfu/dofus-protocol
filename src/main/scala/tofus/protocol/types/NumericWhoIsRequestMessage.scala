package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NumericWhoIsRequestMessage(
  playerId: Long
) extends Message(6298)

object NumericWhoIsRequestMessage {
  implicit val codec: Codec[NumericWhoIsRequestMessage] =
    new Codec[NumericWhoIsRequestMessage] {
      def decode: Get[NumericWhoIsRequestMessage] =
        for {
          playerId <- varLong.decode
        } yield NumericWhoIsRequestMessage(playerId)

      def encode(value: NumericWhoIsRequestMessage): ByteVector =
        varLong.encode(value.playerId)
    }
}
