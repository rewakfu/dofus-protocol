package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayGameOverMessage(

) extends Message(746)

object GameRolePlayGameOverMessage {
  implicit val codec: Codec[GameRolePlayGameOverMessage] =
    Codec.const(GameRolePlayGameOverMessage())
}
