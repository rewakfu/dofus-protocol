package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SelectedServerDataExtendedMessage(
  serverId: Short,
  address: String,
  ports: List[Int],
  canCreateNewCharacter: Boolean,
  ticket: ByteVector,
  servers: List[GameServerInformations]
) extends Message(6469)

object SelectedServerDataExtendedMessage {
  implicit val codec: Codec[SelectedServerDataExtendedMessage] =
    new Codec[SelectedServerDataExtendedMessage] {
      def decode: Get[SelectedServerDataExtendedMessage] =
        for {
          serverId <- varShort.decode
          address <- utf8(ushort).decode
          ports <- list(ushort, int).decode
          canCreateNewCharacter <- bool.decode
          ticket <- bytes(varInt).decode
          servers <- list(ushort, Codec[GameServerInformations]).decode
        } yield SelectedServerDataExtendedMessage(serverId, address, ports, canCreateNewCharacter, ticket, servers)

      def encode(value: SelectedServerDataExtendedMessage): ByteVector =
        varShort.encode(value.serverId) ++
        utf8(ushort).encode(value.address) ++
        list(ushort, int).encode(value.ports) ++
        bool.encode(value.canCreateNewCharacter) ++
        bytes(varInt).encode(value.ticket) ++
        list(ushort, Codec[GameServerInformations]).encode(value.servers)
    }
}
