package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountsPaddockAddMessage(
  mountDescription: List[MountClientData]
) extends Message(6561)

object ExchangeMountsPaddockAddMessage {
  implicit val codec: Codec[ExchangeMountsPaddockAddMessage] =
    new Codec[ExchangeMountsPaddockAddMessage] {
      def decode: Get[ExchangeMountsPaddockAddMessage] =
        for {
          mountDescription <- list(ushort, Codec[MountClientData]).decode
        } yield ExchangeMountsPaddockAddMessage(mountDescription)

      def encode(value: ExchangeMountsPaddockAddMessage): ByteVector =
        list(ushort, Codec[MountClientData]).encode(value.mountDescription)
    }
}
