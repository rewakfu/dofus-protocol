package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TextInformationMessage(
  msgType: Byte,
  msgId: Short,
  parameters: List[String]
) extends Message(780)

object TextInformationMessage {
  implicit val codec: Codec[TextInformationMessage] =
    new Codec[TextInformationMessage] {
      def decode: Get[TextInformationMessage] =
        for {
          msgType <- byte.decode
          msgId <- varShort.decode
          parameters <- list(ushort, utf8(ushort)).decode
        } yield TextInformationMessage(msgType, msgId, parameters)

      def encode(value: TextInformationMessage): ByteVector =
        byte.encode(value.msgType) ++
        varShort.encode(value.msgId) ++
        list(ushort, utf8(ushort)).encode(value.parameters)
    }
}
