package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapRewardRateMessage(
  mapRate: Short,
  subAreaRate: Short,
  totalRate: Short
) extends Message(6827)

object MapRewardRateMessage {
  implicit val codec: Codec[MapRewardRateMessage] =
    new Codec[MapRewardRateMessage] {
      def decode: Get[MapRewardRateMessage] =
        for {
          mapRate <- varShort.decode
          subAreaRate <- varShort.decode
          totalRate <- varShort.decode
        } yield MapRewardRateMessage(mapRate, subAreaRate, totalRate)

      def encode(value: MapRewardRateMessage): ByteVector =
        varShort.encode(value.mapRate) ++
        varShort.encode(value.subAreaRate) ++
        varShort.encode(value.totalRate)
    }
}
