package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseInformationsInside(
  houseId: Int,
  modelId: Short,
  houseInfos: HouseInstanceInformations,
  worldX: Short,
  worldY: Short
) extends HouseInformations {
  override val protocolId = 218
}

object HouseInformationsInside {
  implicit val codec: Codec[HouseInformationsInside] =
    new Codec[HouseInformationsInside] {
      def decode: Get[HouseInformationsInside] =
        for {
          houseId <- varInt.decode
          modelId <- varShort.decode
          houseInfos <- Codec[HouseInstanceInformations].decode
          worldX <- short.decode
          worldY <- short.decode
        } yield HouseInformationsInside(houseId, modelId, houseInfos, worldX, worldY)

      def encode(value: HouseInformationsInside): ByteVector =
        varInt.encode(value.houseId) ++
        varShort.encode(value.modelId) ++
        Codec[HouseInstanceInformations].encode(value.houseInfos) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY)
    }
}
