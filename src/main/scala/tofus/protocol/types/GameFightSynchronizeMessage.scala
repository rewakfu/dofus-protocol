package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightSynchronizeMessage(
  fighters: List[GameFightFighterInformations]
) extends Message(5921)

object GameFightSynchronizeMessage {
  implicit val codec: Codec[GameFightSynchronizeMessage] =
    new Codec[GameFightSynchronizeMessage] {
      def decode: Get[GameFightSynchronizeMessage] =
        for {
          fighters <- list(ushort, Codec[GameFightFighterInformations]).decode
        } yield GameFightSynchronizeMessage(fighters)

      def encode(value: GameFightSynchronizeMessage): ByteVector =
        list(ushort, Codec[GameFightFighterInformations]).encode(value.fighters)
    }
}
