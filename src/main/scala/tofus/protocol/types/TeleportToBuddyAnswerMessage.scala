package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportToBuddyAnswerMessage(
  dungeonId: Short,
  buddyId: Long,
  accept: Boolean
) extends Message(6293)

object TeleportToBuddyAnswerMessage {
  implicit val codec: Codec[TeleportToBuddyAnswerMessage] =
    new Codec[TeleportToBuddyAnswerMessage] {
      def decode: Get[TeleportToBuddyAnswerMessage] =
        for {
          dungeonId <- varShort.decode
          buddyId <- varLong.decode
          accept <- bool.decode
        } yield TeleportToBuddyAnswerMessage(dungeonId, buddyId, accept)

      def encode(value: TeleportToBuddyAnswerMessage): ByteVector =
        varShort.encode(value.dungeonId) ++
        varLong.encode(value.buddyId) ++
        bool.encode(value.accept)
    }
}
