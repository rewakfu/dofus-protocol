package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChannelEnablingChangeMessage(
  channel: Byte,
  enable: Boolean
) extends Message(891)

object ChannelEnablingChangeMessage {
  implicit val codec: Codec[ChannelEnablingChangeMessage] =
    new Codec[ChannelEnablingChangeMessage] {
      def decode: Get[ChannelEnablingChangeMessage] =
        for {
          channel <- byte.decode
          enable <- bool.decode
        } yield ChannelEnablingChangeMessage(channel, enable)

      def encode(value: ChannelEnablingChangeMessage): ByteVector =
        byte.encode(value.channel) ++
        bool.encode(value.enable)
    }
}
