package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendDeleteRequestMessage(
  accountId: Int
) extends Message(5603)

object FriendDeleteRequestMessage {
  implicit val codec: Codec[FriendDeleteRequestMessage] =
    new Codec[FriendDeleteRequestMessage] {
      def decode: Get[FriendDeleteRequestMessage] =
        for {
          accountId <- int.decode
        } yield FriendDeleteRequestMessage(accountId)

      def encode(value: FriendDeleteRequestMessage): ByteVector =
        int.encode(value.accountId)
    }
}
