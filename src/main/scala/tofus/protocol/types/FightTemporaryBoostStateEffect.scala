package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTemporaryBoostStateEffect(
  uid: Int,
  targetId: Double,
  turnDuration: Short,
  dispelable: Byte,
  spellId: Short,
  effectId: Int,
  parentBoostUid: Int,
  delta: Int,
  stateId: Short
) extends FightTemporaryBoostEffect {
  override val protocolId = 214
}

object FightTemporaryBoostStateEffect {
  implicit val codec: Codec[FightTemporaryBoostStateEffect] =
    new Codec[FightTemporaryBoostStateEffect] {
      def decode: Get[FightTemporaryBoostStateEffect] =
        for {
          uid <- varInt.decode
          targetId <- double.decode
          turnDuration <- short.decode
          dispelable <- byte.decode
          spellId <- varShort.decode
          effectId <- varInt.decode
          parentBoostUid <- varInt.decode
          delta <- int.decode
          stateId <- short.decode
        } yield FightTemporaryBoostStateEffect(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, delta, stateId)

      def encode(value: FightTemporaryBoostStateEffect): ByteVector =
        varInt.encode(value.uid) ++
        double.encode(value.targetId) ++
        short.encode(value.turnDuration) ++
        byte.encode(value.dispelable) ++
        varShort.encode(value.spellId) ++
        varInt.encode(value.effectId) ++
        varInt.encode(value.parentBoostUid) ++
        int.encode(value.delta) ++
        short.encode(value.stateId)
    }
}
