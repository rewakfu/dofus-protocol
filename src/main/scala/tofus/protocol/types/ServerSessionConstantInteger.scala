package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerSessionConstantInteger(
  id: Short,
  value: Int
) extends ServerSessionConstant {
  override val protocolId = 433
}

object ServerSessionConstantInteger {
  implicit val codec: Codec[ServerSessionConstantInteger] =
    new Codec[ServerSessionConstantInteger] {
      def decode: Get[ServerSessionConstantInteger] =
        for {
          id <- varShort.decode
          value <- int.decode
        } yield ServerSessionConstantInteger(id, value)

      def encode(value: ServerSessionConstantInteger): ByteVector =
        varShort.encode(value.id) ++
        int.encode(value.value)
    }
}
