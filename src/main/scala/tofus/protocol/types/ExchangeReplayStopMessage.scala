package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeReplayStopMessage(

) extends Message(6001)

object ExchangeReplayStopMessage {
  implicit val codec: Codec[ExchangeReplayStopMessage] =
    Codec.const(ExchangeReplayStopMessage())
}
