package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItemToSellInBid(
  objectGID: Short,
  effects: List[ObjectEffect],
  objectUID: Int,
  quantity: Int,
  objectPrice: Long,
  unsoldDelay: Int
) extends ObjectItemToSell {
  override val protocolId = 164
}

object ObjectItemToSellInBid {
  implicit val codec: Codec[ObjectItemToSellInBid] =
    new Codec[ObjectItemToSellInBid] {
      def decode: Get[ObjectItemToSellInBid] =
        for {
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          objectUID <- varInt.decode
          quantity <- varInt.decode
          objectPrice <- varLong.decode
          unsoldDelay <- int.decode
        } yield ObjectItemToSellInBid(objectGID, effects, objectUID, quantity, objectPrice, unsoldDelay)

      def encode(value: ObjectItemToSellInBid): ByteVector =
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity) ++
        varLong.encode(value.objectPrice) ++
        int.encode(value.unsoldDelay)
    }
}
