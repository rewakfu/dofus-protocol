package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MimicryObjectPreviewMessage(
  result: ObjectItem
) extends Message(6458)

object MimicryObjectPreviewMessage {
  implicit val codec: Codec[MimicryObjectPreviewMessage] =
    new Codec[MimicryObjectPreviewMessage] {
      def decode: Get[MimicryObjectPreviewMessage] =
        for {
          result <- Codec[ObjectItem].decode
        } yield MimicryObjectPreviewMessage(result)

      def encode(value: MimicryObjectPreviewMessage): ByteVector =
        Codec[ObjectItem].encode(value.result)
    }
}
