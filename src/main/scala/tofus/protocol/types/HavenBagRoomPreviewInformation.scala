package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagRoomPreviewInformation(
  roomId: Short,
  themeId: Byte
) extends ProtocolType {
  override val protocolId = 576
}

object HavenBagRoomPreviewInformation {
  implicit val codec: Codec[HavenBagRoomPreviewInformation] =
    new Codec[HavenBagRoomPreviewInformation] {
      def decode: Get[HavenBagRoomPreviewInformation] =
        for {
          roomId <- ubyte.decode
          themeId <- byte.decode
        } yield HavenBagRoomPreviewInformation(roomId, themeId)

      def encode(value: HavenBagRoomPreviewInformation): ByteVector =
        ubyte.encode(value.roomId) ++
        byte.encode(value.themeId)
    }
}
