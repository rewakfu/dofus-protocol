package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AbstractGameActionMessage(
  actionId: Short,
  sourceId: Double
) extends Message(1000)

object AbstractGameActionMessage {
  implicit val codec: Codec[AbstractGameActionMessage] =
    new Codec[AbstractGameActionMessage] {
      def decode: Get[AbstractGameActionMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
        } yield AbstractGameActionMessage(actionId, sourceId)

      def encode(value: AbstractGameActionMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId)
    }
}
