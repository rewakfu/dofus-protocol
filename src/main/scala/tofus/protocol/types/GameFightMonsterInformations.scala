package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameFightMonsterInformations extends GameFightAIInformations

final case class ConcreteGameFightMonsterInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short],
  creatureGenericId: Short,
  creatureGrade: Byte,
  creatureLevel: Short
) extends GameFightMonsterInformations {
  override val protocolId = 29
}

object ConcreteGameFightMonsterInformations {
  implicit val codec: Codec[ConcreteGameFightMonsterInformations] =  
    new Codec[ConcreteGameFightMonsterInformations] {
      def decode: Get[ConcreteGameFightMonsterInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
          creatureGenericId <- varShort.decode
          creatureGrade <- byte.decode
          creatureLevel <- short.decode
        } yield ConcreteGameFightMonsterInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions, creatureGenericId, creatureGrade, creatureLevel)

      def encode(value: ConcreteGameFightMonsterInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions) ++
        varShort.encode(value.creatureGenericId) ++
        byte.encode(value.creatureGrade) ++
        short.encode(value.creatureLevel)
    }
}

object GameFightMonsterInformations {
  implicit val codec: Codec[GameFightMonsterInformations] =
    new Codec[GameFightMonsterInformations] {
      def decode: Get[GameFightMonsterInformations] =
        ushort.decode.flatMap {
          case 29 => Codec[ConcreteGameFightMonsterInformations].decode
          case 203 => Codec[GameFightMonsterWithAlignmentInformations].decode
        }

      def encode(value: GameFightMonsterInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameFightMonsterInformations => Codec[ConcreteGameFightMonsterInformations].encode(i)
          case i: GameFightMonsterWithAlignmentInformations => Codec[GameFightMonsterWithAlignmentInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
