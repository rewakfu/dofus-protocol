package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMotdSetErrorMessage(
  reason: Byte
) extends Message(6591)

object GuildMotdSetErrorMessage {
  implicit val codec: Codec[GuildMotdSetErrorMessage] =
    new Codec[GuildMotdSetErrorMessage] {
      def decode: Get[GuildMotdSetErrorMessage] =
        for {
          reason <- byte.decode
        } yield GuildMotdSetErrorMessage(reason)

      def encode(value: GuildMotdSetErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
