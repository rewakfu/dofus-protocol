package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayNpcQuestFlag(
  questsToValidId: List[Short],
  questsToStartId: List[Short]
) extends ProtocolType {
  override val protocolId = 384
}

object GameRolePlayNpcQuestFlag {
  implicit val codec: Codec[GameRolePlayNpcQuestFlag] =
    new Codec[GameRolePlayNpcQuestFlag] {
      def decode: Get[GameRolePlayNpcQuestFlag] =
        for {
          questsToValidId <- list(ushort, varShort).decode
          questsToStartId <- list(ushort, varShort).decode
        } yield GameRolePlayNpcQuestFlag(questsToValidId, questsToStartId)

      def encode(value: GameRolePlayNpcQuestFlag): ByteVector =
        list(ushort, varShort).encode(value.questsToValidId) ++
        list(ushort, varShort).encode(value.questsToStartId)
    }
}
