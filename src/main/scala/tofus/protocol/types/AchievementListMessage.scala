package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementListMessage(
  finishedAchievements: List[AchievementAchieved]
) extends Message(6205)

object AchievementListMessage {
  implicit val codec: Codec[AchievementListMessage] =
    new Codec[AchievementListMessage] {
      def decode: Get[AchievementListMessage] =
        for {
          finishedAchievements <- list(ushort, Codec[AchievementAchieved]).decode
        } yield AchievementListMessage(finishedAchievements)

      def encode(value: AchievementListMessage): ByteVector =
        list(ushort, Codec[AchievementAchieved]).encode(value.finishedAchievements)
    }
}
