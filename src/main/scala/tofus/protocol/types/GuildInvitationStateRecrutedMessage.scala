package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInvitationStateRecrutedMessage(
  invitationState: Byte
) extends Message(5548)

object GuildInvitationStateRecrutedMessage {
  implicit val codec: Codec[GuildInvitationStateRecrutedMessage] =
    new Codec[GuildInvitationStateRecrutedMessage] {
      def decode: Get[GuildInvitationStateRecrutedMessage] =
        for {
          invitationState <- byte.decode
        } yield GuildInvitationStateRecrutedMessage(invitationState)

      def encode(value: GuildInvitationStateRecrutedMessage): ByteVector =
        byte.encode(value.invitationState)
    }
}
