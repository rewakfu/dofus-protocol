package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountXpRatioMessage(
  ratio: Byte
) extends Message(5970)

object MountXpRatioMessage {
  implicit val codec: Codec[MountXpRatioMessage] =
    new Codec[MountXpRatioMessage] {
      def decode: Get[MountXpRatioMessage] =
        for {
          ratio <- byte.decode
        } yield MountXpRatioMessage(ratio)

      def encode(value: MountXpRatioMessage): ByteVector =
        byte.encode(value.ratio)
    }
}
