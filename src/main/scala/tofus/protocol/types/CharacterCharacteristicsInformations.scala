package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterCharacteristicsInformations(
  experience: Long,
  experienceLevelFloor: Long,
  experienceNextLevelFloor: Long,
  experienceBonusLimit: Long,
  kamas: Long,
  statsPoints: Short,
  additionnalPoints: Short,
  spellsPoints: Short,
  alignmentInfos: ActorExtendedAlignmentInformations,
  lifePoints: Int,
  maxLifePoints: Int,
  energyPoints: Short,
  maxEnergyPoints: Short,
  actionPointsCurrent: Short,
  movementPointsCurrent: Short,
  initiative: CharacterBaseCharacteristic,
  prospecting: CharacterBaseCharacteristic,
  actionPoints: CharacterBaseCharacteristic,
  movementPoints: CharacterBaseCharacteristic,
  strength: CharacterBaseCharacteristic,
  vitality: CharacterBaseCharacteristic,
  wisdom: CharacterBaseCharacteristic,
  chance: CharacterBaseCharacteristic,
  agility: CharacterBaseCharacteristic,
  intelligence: CharacterBaseCharacteristic,
  range: CharacterBaseCharacteristic,
  summonableCreaturesBoost: CharacterBaseCharacteristic,
  reflect: CharacterBaseCharacteristic,
  criticalHit: CharacterBaseCharacteristic,
  criticalHitWeapon: Short,
  criticalMiss: CharacterBaseCharacteristic,
  healBonus: CharacterBaseCharacteristic,
  allDamagesBonus: CharacterBaseCharacteristic,
  weaponDamagesBonusPercent: CharacterBaseCharacteristic,
  damagesBonusPercent: CharacterBaseCharacteristic,
  trapBonus: CharacterBaseCharacteristic,
  trapBonusPercent: CharacterBaseCharacteristic,
  glyphBonusPercent: CharacterBaseCharacteristic,
  runeBonusPercent: CharacterBaseCharacteristic,
  permanentDamagePercent: CharacterBaseCharacteristic,
  tackleBlock: CharacterBaseCharacteristic,
  tackleEvade: CharacterBaseCharacteristic,
  PAAttack: CharacterBaseCharacteristic,
  PMAttack: CharacterBaseCharacteristic,
  pushDamageBonus: CharacterBaseCharacteristic,
  criticalDamageBonus: CharacterBaseCharacteristic,
  neutralDamageBonus: CharacterBaseCharacteristic,
  earthDamageBonus: CharacterBaseCharacteristic,
  waterDamageBonus: CharacterBaseCharacteristic,
  airDamageBonus: CharacterBaseCharacteristic,
  fireDamageBonus: CharacterBaseCharacteristic,
  dodgePALostProbability: CharacterBaseCharacteristic,
  dodgePMLostProbability: CharacterBaseCharacteristic,
  neutralElementResistPercent: CharacterBaseCharacteristic,
  earthElementResistPercent: CharacterBaseCharacteristic,
  waterElementResistPercent: CharacterBaseCharacteristic,
  airElementResistPercent: CharacterBaseCharacteristic,
  fireElementResistPercent: CharacterBaseCharacteristic,
  neutralElementReduction: CharacterBaseCharacteristic,
  earthElementReduction: CharacterBaseCharacteristic,
  waterElementReduction: CharacterBaseCharacteristic,
  airElementReduction: CharacterBaseCharacteristic,
  fireElementReduction: CharacterBaseCharacteristic,
  pushDamageReduction: CharacterBaseCharacteristic,
  criticalDamageReduction: CharacterBaseCharacteristic,
  pvpNeutralElementResistPercent: CharacterBaseCharacteristic,
  pvpEarthElementResistPercent: CharacterBaseCharacteristic,
  pvpWaterElementResistPercent: CharacterBaseCharacteristic,
  pvpAirElementResistPercent: CharacterBaseCharacteristic,
  pvpFireElementResistPercent: CharacterBaseCharacteristic,
  pvpNeutralElementReduction: CharacterBaseCharacteristic,
  pvpEarthElementReduction: CharacterBaseCharacteristic,
  pvpWaterElementReduction: CharacterBaseCharacteristic,
  pvpAirElementReduction: CharacterBaseCharacteristic,
  pvpFireElementReduction: CharacterBaseCharacteristic,
  meleeDamageDonePercent: CharacterBaseCharacteristic,
  meleeDamageReceivedPercent: CharacterBaseCharacteristic,
  rangedDamageDonePercent: CharacterBaseCharacteristic,
  rangedDamageReceivedPercent: CharacterBaseCharacteristic,
  weaponDamageDonePercent: CharacterBaseCharacteristic,
  weaponDamageReceivedPercent: CharacterBaseCharacteristic,
  spellDamageDonePercent: CharacterBaseCharacteristic,
  spellDamageReceivedPercent: CharacterBaseCharacteristic,
  spellModifications: List[CharacterSpellModification],
  probationTime: Int
) extends ProtocolType {
  override val protocolId = 8
}

object CharacterCharacteristicsInformations {
  implicit val codec: Codec[CharacterCharacteristicsInformations] =
    new Codec[CharacterCharacteristicsInformations] {
      def decode: Get[CharacterCharacteristicsInformations] =
        for {
          experience <- varLong.decode
          experienceLevelFloor <- varLong.decode
          experienceNextLevelFloor <- varLong.decode
          experienceBonusLimit <- varLong.decode
          kamas <- varLong.decode
          statsPoints <- varShort.decode
          additionnalPoints <- varShort.decode
          spellsPoints <- varShort.decode
          alignmentInfos <- Codec[ActorExtendedAlignmentInformations].decode
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
          energyPoints <- varShort.decode
          maxEnergyPoints <- varShort.decode
          actionPointsCurrent <- varShort.decode
          movementPointsCurrent <- varShort.decode
          initiative <- Codec[CharacterBaseCharacteristic].decode
          prospecting <- Codec[CharacterBaseCharacteristic].decode
          actionPoints <- Codec[CharacterBaseCharacteristic].decode
          movementPoints <- Codec[CharacterBaseCharacteristic].decode
          strength <- Codec[CharacterBaseCharacteristic].decode
          vitality <- Codec[CharacterBaseCharacteristic].decode
          wisdom <- Codec[CharacterBaseCharacteristic].decode
          chance <- Codec[CharacterBaseCharacteristic].decode
          agility <- Codec[CharacterBaseCharacteristic].decode
          intelligence <- Codec[CharacterBaseCharacteristic].decode
          range <- Codec[CharacterBaseCharacteristic].decode
          summonableCreaturesBoost <- Codec[CharacterBaseCharacteristic].decode
          reflect <- Codec[CharacterBaseCharacteristic].decode
          criticalHit <- Codec[CharacterBaseCharacteristic].decode
          criticalHitWeapon <- varShort.decode
          criticalMiss <- Codec[CharacterBaseCharacteristic].decode
          healBonus <- Codec[CharacterBaseCharacteristic].decode
          allDamagesBonus <- Codec[CharacterBaseCharacteristic].decode
          weaponDamagesBonusPercent <- Codec[CharacterBaseCharacteristic].decode
          damagesBonusPercent <- Codec[CharacterBaseCharacteristic].decode
          trapBonus <- Codec[CharacterBaseCharacteristic].decode
          trapBonusPercent <- Codec[CharacterBaseCharacteristic].decode
          glyphBonusPercent <- Codec[CharacterBaseCharacteristic].decode
          runeBonusPercent <- Codec[CharacterBaseCharacteristic].decode
          permanentDamagePercent <- Codec[CharacterBaseCharacteristic].decode
          tackleBlock <- Codec[CharacterBaseCharacteristic].decode
          tackleEvade <- Codec[CharacterBaseCharacteristic].decode
          PAAttack <- Codec[CharacterBaseCharacteristic].decode
          PMAttack <- Codec[CharacterBaseCharacteristic].decode
          pushDamageBonus <- Codec[CharacterBaseCharacteristic].decode
          criticalDamageBonus <- Codec[CharacterBaseCharacteristic].decode
          neutralDamageBonus <- Codec[CharacterBaseCharacteristic].decode
          earthDamageBonus <- Codec[CharacterBaseCharacteristic].decode
          waterDamageBonus <- Codec[CharacterBaseCharacteristic].decode
          airDamageBonus <- Codec[CharacterBaseCharacteristic].decode
          fireDamageBonus <- Codec[CharacterBaseCharacteristic].decode
          dodgePALostProbability <- Codec[CharacterBaseCharacteristic].decode
          dodgePMLostProbability <- Codec[CharacterBaseCharacteristic].decode
          neutralElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          earthElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          waterElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          airElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          fireElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          neutralElementReduction <- Codec[CharacterBaseCharacteristic].decode
          earthElementReduction <- Codec[CharacterBaseCharacteristic].decode
          waterElementReduction <- Codec[CharacterBaseCharacteristic].decode
          airElementReduction <- Codec[CharacterBaseCharacteristic].decode
          fireElementReduction <- Codec[CharacterBaseCharacteristic].decode
          pushDamageReduction <- Codec[CharacterBaseCharacteristic].decode
          criticalDamageReduction <- Codec[CharacterBaseCharacteristic].decode
          pvpNeutralElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          pvpEarthElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          pvpWaterElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          pvpAirElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          pvpFireElementResistPercent <- Codec[CharacterBaseCharacteristic].decode
          pvpNeutralElementReduction <- Codec[CharacterBaseCharacteristic].decode
          pvpEarthElementReduction <- Codec[CharacterBaseCharacteristic].decode
          pvpWaterElementReduction <- Codec[CharacterBaseCharacteristic].decode
          pvpAirElementReduction <- Codec[CharacterBaseCharacteristic].decode
          pvpFireElementReduction <- Codec[CharacterBaseCharacteristic].decode
          meleeDamageDonePercent <- Codec[CharacterBaseCharacteristic].decode
          meleeDamageReceivedPercent <- Codec[CharacterBaseCharacteristic].decode
          rangedDamageDonePercent <- Codec[CharacterBaseCharacteristic].decode
          rangedDamageReceivedPercent <- Codec[CharacterBaseCharacteristic].decode
          weaponDamageDonePercent <- Codec[CharacterBaseCharacteristic].decode
          weaponDamageReceivedPercent <- Codec[CharacterBaseCharacteristic].decode
          spellDamageDonePercent <- Codec[CharacterBaseCharacteristic].decode
          spellDamageReceivedPercent <- Codec[CharacterBaseCharacteristic].decode
          spellModifications <- list(ushort, Codec[CharacterSpellModification]).decode
          probationTime <- int.decode
        } yield CharacterCharacteristicsInformations(experience, experienceLevelFloor, experienceNextLevelFloor, experienceBonusLimit, kamas, statsPoints, additionnalPoints, spellsPoints, alignmentInfos, lifePoints, maxLifePoints, energyPoints, maxEnergyPoints, actionPointsCurrent, movementPointsCurrent, initiative, prospecting, actionPoints, movementPoints, strength, vitality, wisdom, chance, agility, intelligence, range, summonableCreaturesBoost, reflect, criticalHit, criticalHitWeapon, criticalMiss, healBonus, allDamagesBonus, weaponDamagesBonusPercent, damagesBonusPercent, trapBonus, trapBonusPercent, glyphBonusPercent, runeBonusPercent, permanentDamagePercent, tackleBlock, tackleEvade, PAAttack, PMAttack, pushDamageBonus, criticalDamageBonus, neutralDamageBonus, earthDamageBonus, waterDamageBonus, airDamageBonus, fireDamageBonus, dodgePALostProbability, dodgePMLostProbability, neutralElementResistPercent, earthElementResistPercent, waterElementResistPercent, airElementResistPercent, fireElementResistPercent, neutralElementReduction, earthElementReduction, waterElementReduction, airElementReduction, fireElementReduction, pushDamageReduction, criticalDamageReduction, pvpNeutralElementResistPercent, pvpEarthElementResistPercent, pvpWaterElementResistPercent, pvpAirElementResistPercent, pvpFireElementResistPercent, pvpNeutralElementReduction, pvpEarthElementReduction, pvpWaterElementReduction, pvpAirElementReduction, pvpFireElementReduction, meleeDamageDonePercent, meleeDamageReceivedPercent, rangedDamageDonePercent, rangedDamageReceivedPercent, weaponDamageDonePercent, weaponDamageReceivedPercent, spellDamageDonePercent, spellDamageReceivedPercent, spellModifications, probationTime)

      def encode(value: CharacterCharacteristicsInformations): ByteVector =
        varLong.encode(value.experience) ++
        varLong.encode(value.experienceLevelFloor) ++
        varLong.encode(value.experienceNextLevelFloor) ++
        varLong.encode(value.experienceBonusLimit) ++
        varLong.encode(value.kamas) ++
        varShort.encode(value.statsPoints) ++
        varShort.encode(value.additionnalPoints) ++
        varShort.encode(value.spellsPoints) ++
        Codec[ActorExtendedAlignmentInformations].encode(value.alignmentInfos) ++
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints) ++
        varShort.encode(value.energyPoints) ++
        varShort.encode(value.maxEnergyPoints) ++
        varShort.encode(value.actionPointsCurrent) ++
        varShort.encode(value.movementPointsCurrent) ++
        Codec[CharacterBaseCharacteristic].encode(value.initiative) ++
        Codec[CharacterBaseCharacteristic].encode(value.prospecting) ++
        Codec[CharacterBaseCharacteristic].encode(value.actionPoints) ++
        Codec[CharacterBaseCharacteristic].encode(value.movementPoints) ++
        Codec[CharacterBaseCharacteristic].encode(value.strength) ++
        Codec[CharacterBaseCharacteristic].encode(value.vitality) ++
        Codec[CharacterBaseCharacteristic].encode(value.wisdom) ++
        Codec[CharacterBaseCharacteristic].encode(value.chance) ++
        Codec[CharacterBaseCharacteristic].encode(value.agility) ++
        Codec[CharacterBaseCharacteristic].encode(value.intelligence) ++
        Codec[CharacterBaseCharacteristic].encode(value.range) ++
        Codec[CharacterBaseCharacteristic].encode(value.summonableCreaturesBoost) ++
        Codec[CharacterBaseCharacteristic].encode(value.reflect) ++
        Codec[CharacterBaseCharacteristic].encode(value.criticalHit) ++
        varShort.encode(value.criticalHitWeapon) ++
        Codec[CharacterBaseCharacteristic].encode(value.criticalMiss) ++
        Codec[CharacterBaseCharacteristic].encode(value.healBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.allDamagesBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.weaponDamagesBonusPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.damagesBonusPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.trapBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.trapBonusPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.glyphBonusPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.runeBonusPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.permanentDamagePercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.tackleBlock) ++
        Codec[CharacterBaseCharacteristic].encode(value.tackleEvade) ++
        Codec[CharacterBaseCharacteristic].encode(value.PAAttack) ++
        Codec[CharacterBaseCharacteristic].encode(value.PMAttack) ++
        Codec[CharacterBaseCharacteristic].encode(value.pushDamageBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.criticalDamageBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.neutralDamageBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.earthDamageBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.waterDamageBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.airDamageBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.fireDamageBonus) ++
        Codec[CharacterBaseCharacteristic].encode(value.dodgePALostProbability) ++
        Codec[CharacterBaseCharacteristic].encode(value.dodgePMLostProbability) ++
        Codec[CharacterBaseCharacteristic].encode(value.neutralElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.earthElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.waterElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.airElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.fireElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.neutralElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.earthElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.waterElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.airElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.fireElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.pushDamageReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.criticalDamageReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpNeutralElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpEarthElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpWaterElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpAirElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpFireElementResistPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpNeutralElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpEarthElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpWaterElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpAirElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.pvpFireElementReduction) ++
        Codec[CharacterBaseCharacteristic].encode(value.meleeDamageDonePercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.meleeDamageReceivedPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.rangedDamageDonePercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.rangedDamageReceivedPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.weaponDamageDonePercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.weaponDamageReceivedPercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.spellDamageDonePercent) ++
        Codec[CharacterBaseCharacteristic].encode(value.spellDamageReceivedPercent) ++
        list(ushort, Codec[CharacterSpellModification]).encode(value.spellModifications) ++
        int.encode(value.probationTime)
    }
}
