package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlaySpellAnimMessage(
  casterId: Long,
  targetCellId: Short,
  spellId: Short,
  spellLevel: Short
) extends Message(6114)

object GameRolePlaySpellAnimMessage {
  implicit val codec: Codec[GameRolePlaySpellAnimMessage] =
    new Codec[GameRolePlaySpellAnimMessage] {
      def decode: Get[GameRolePlaySpellAnimMessage] =
        for {
          casterId <- varLong.decode
          targetCellId <- varShort.decode
          spellId <- varShort.decode
          spellLevel <- short.decode
        } yield GameRolePlaySpellAnimMessage(casterId, targetCellId, spellId, spellLevel)

      def encode(value: GameRolePlaySpellAnimMessage): ByteVector =
        varLong.encode(value.casterId) ++
        varShort.encode(value.targetCellId) ++
        varShort.encode(value.spellId) ++
        short.encode(value.spellLevel)
    }
}
