package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectMovePricedMessage(
  objectUID: Int,
  quantity: Int,
  price: Long
) extends Message(5514)

object ExchangeObjectMovePricedMessage {
  implicit val codec: Codec[ExchangeObjectMovePricedMessage] =
    new Codec[ExchangeObjectMovePricedMessage] {
      def decode: Get[ExchangeObjectMovePricedMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
          price <- varLong.decode
        } yield ExchangeObjectMovePricedMessage(objectUID, quantity, price)

      def encode(value: ExchangeObjectMovePricedMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity) ++
        varLong.encode(value.price)
    }
}
