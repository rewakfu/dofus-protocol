package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutObjectPreset(
  slot: Byte,
  presetId: Short
) extends ShortcutObject {
  override val protocolId = 370
}

object ShortcutObjectPreset {
  implicit val codec: Codec[ShortcutObjectPreset] =
    new Codec[ShortcutObjectPreset] {
      def decode: Get[ShortcutObjectPreset] =
        for {
          slot <- byte.decode
          presetId <- short.decode
        } yield ShortcutObjectPreset(slot, presetId)

      def encode(value: ShortcutObjectPreset): ByteVector =
        byte.encode(value.slot) ++
        short.encode(value.presetId)
    }
}
