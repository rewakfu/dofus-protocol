package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightReduceDamagesMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  amount: Int
) extends Message(5526)

object GameActionFightReduceDamagesMessage {
  implicit val codec: Codec[GameActionFightReduceDamagesMessage] =
    new Codec[GameActionFightReduceDamagesMessage] {
      def decode: Get[GameActionFightReduceDamagesMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          amount <- varInt.decode
        } yield GameActionFightReduceDamagesMessage(actionId, sourceId, targetId, amount)

      def encode(value: GameActionFightReduceDamagesMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varInt.encode(value.amount)
    }
}
