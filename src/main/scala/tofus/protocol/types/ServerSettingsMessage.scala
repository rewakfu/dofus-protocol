package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerSettingsMessage(
  flags0: Byte,
  lang: String,
  community: Byte,
  gameType: Byte,
  arenaLeaveBanTime: Short,
  itemMaxLevel: Int
) extends Message(6340)

object ServerSettingsMessage {
  implicit val codec: Codec[ServerSettingsMessage] =
    new Codec[ServerSettingsMessage] {
      def decode: Get[ServerSettingsMessage] =
        for {
          flags0 <- byte.decode
          lang <- utf8(ushort).decode
          community <- byte.decode
          gameType <- byte.decode
          arenaLeaveBanTime <- varShort.decode
          itemMaxLevel <- int.decode
        } yield ServerSettingsMessage(flags0, lang, community, gameType, arenaLeaveBanTime, itemMaxLevel)

      def encode(value: ServerSettingsMessage): ByteVector =
        byte.encode(value.flags0) ++
        utf8(ushort).encode(value.lang) ++
        byte.encode(value.community) ++
        byte.encode(value.gameType) ++
        varShort.encode(value.arenaLeaveBanTime) ++
        int.encode(value.itemMaxLevel)
    }
}
