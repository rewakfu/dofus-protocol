package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AchievementObjective extends ProtocolType

final case class ConcreteAchievementObjective(
  id: Int,
  maxValue: Short
) extends AchievementObjective {
  override val protocolId = 404
}

object ConcreteAchievementObjective {
  implicit val codec: Codec[ConcreteAchievementObjective] =  
    new Codec[ConcreteAchievementObjective] {
      def decode: Get[ConcreteAchievementObjective] =
        for {
          id <- varInt.decode
          maxValue <- varShort.decode
        } yield ConcreteAchievementObjective(id, maxValue)

      def encode(value: ConcreteAchievementObjective): ByteVector =
        varInt.encode(value.id) ++
        varShort.encode(value.maxValue)
    }
}

object AchievementObjective {
  implicit val codec: Codec[AchievementObjective] =
    new Codec[AchievementObjective] {
      def decode: Get[AchievementObjective] =
        ushort.decode.flatMap {
          case 404 => Codec[ConcreteAchievementObjective].decode
          case 402 => Codec[AchievementStartedObjective].decode
        }

      def encode(value: AchievementObjective): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAchievementObjective => Codec[ConcreteAchievementObjective].encode(i)
          case i: AchievementStartedObjective => Codec[AchievementStartedObjective].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
