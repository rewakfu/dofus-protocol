package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachGameFightEndMessage(
  duration: Int,
  rewardRate: Short,
  lootShareLimitMalus: Short,
  results: List[FightResultListEntry],
  namedPartyTeamsOutcomes: List[NamedPartyTeamWithOutcome],
  budget: Int
) extends Message(6809)

object BreachGameFightEndMessage {
  implicit val codec: Codec[BreachGameFightEndMessage] =
    new Codec[BreachGameFightEndMessage] {
      def decode: Get[BreachGameFightEndMessage] =
        for {
          duration <- int.decode
          rewardRate <- varShort.decode
          lootShareLimitMalus <- short.decode
          results <- list(ushort, Codec[FightResultListEntry]).decode
          namedPartyTeamsOutcomes <- list(ushort, Codec[NamedPartyTeamWithOutcome]).decode
          budget <- int.decode
        } yield BreachGameFightEndMessage(duration, rewardRate, lootShareLimitMalus, results, namedPartyTeamsOutcomes, budget)

      def encode(value: BreachGameFightEndMessage): ByteVector =
        int.encode(value.duration) ++
        varShort.encode(value.rewardRate) ++
        short.encode(value.lootShareLimitMalus) ++
        list(ushort, Codec[FightResultListEntry]).encode(value.results) ++
        list(ushort, Codec[NamedPartyTeamWithOutcome]).encode(value.namedPartyTeamsOutcomes) ++
        int.encode(value.budget)
    }
}
