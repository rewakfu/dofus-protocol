package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTemporarySpellImmunityEffect(
  uid: Int,
  targetId: Double,
  turnDuration: Short,
  dispelable: Byte,
  spellId: Short,
  effectId: Int,
  parentBoostUid: Int,
  immuneSpellId: Int
) extends AbstractFightDispellableEffect {
  override val protocolId = 366
}

object FightTemporarySpellImmunityEffect {
  implicit val codec: Codec[FightTemporarySpellImmunityEffect] =
    new Codec[FightTemporarySpellImmunityEffect] {
      def decode: Get[FightTemporarySpellImmunityEffect] =
        for {
          uid <- varInt.decode
          targetId <- double.decode
          turnDuration <- short.decode
          dispelable <- byte.decode
          spellId <- varShort.decode
          effectId <- varInt.decode
          parentBoostUid <- varInt.decode
          immuneSpellId <- int.decode
        } yield FightTemporarySpellImmunityEffect(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, immuneSpellId)

      def encode(value: FightTemporarySpellImmunityEffect): ByteVector =
        varInt.encode(value.uid) ++
        double.encode(value.targetId) ++
        short.encode(value.turnDuration) ++
        byte.encode(value.dispelable) ++
        varShort.encode(value.spellId) ++
        varInt.encode(value.effectId) ++
        varInt.encode(value.parentBoostUid) ++
        int.encode(value.immuneSpellId)
    }
}
