package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareInformationsMessage(
  dareFixedInfos: DareInformations,
  dareVersatilesInfos: DareVersatileInformations
) extends Message(6656)

object DareInformationsMessage {
  implicit val codec: Codec[DareInformationsMessage] =
    new Codec[DareInformationsMessage] {
      def decode: Get[DareInformationsMessage] =
        for {
          dareFixedInfos <- Codec[DareInformations].decode
          dareVersatilesInfos <- Codec[DareVersatileInformations].decode
        } yield DareInformationsMessage(dareFixedInfos, dareVersatilesInfos)

      def encode(value: DareInformationsMessage): ByteVector =
        Codec[DareInformations].encode(value.dareFixedInfos) ++
        Codec[DareVersatileInformations].encode(value.dareVersatilesInfos)
    }
}
