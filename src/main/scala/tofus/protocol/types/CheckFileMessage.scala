package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CheckFileMessage(
  filenameHash: String,
  `type`: Byte,
  value: String
) extends Message(6156)

object CheckFileMessage {
  implicit val codec: Codec[CheckFileMessage] =
    new Codec[CheckFileMessage] {
      def decode: Get[CheckFileMessage] =
        for {
          filenameHash <- utf8(ushort).decode
          `type` <- byte.decode
          value <- utf8(ushort).decode
        } yield CheckFileMessage(filenameHash, `type`, value)

      def encode(value: CheckFileMessage): ByteVector =
        utf8(ushort).encode(value.filenameHash) ++
        byte.encode(value.`type`) ++
        utf8(ushort).encode(value.value)
    }
}
