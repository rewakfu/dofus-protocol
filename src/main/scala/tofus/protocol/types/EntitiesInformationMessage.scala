package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EntitiesInformationMessage(
  entities: List[EntityInformation]
) extends Message(6775)

object EntitiesInformationMessage {
  implicit val codec: Codec[EntitiesInformationMessage] =
    new Codec[EntitiesInformationMessage] {
      def decode: Get[EntitiesInformationMessage] =
        for {
          entities <- list(ushort, Codec[EntityInformation]).decode
        } yield EntitiesInformationMessage(entities)

      def encode(value: EntitiesInformationMessage): ByteVector =
        list(ushort, Codec[EntityInformation]).encode(value.entities)
    }
}
