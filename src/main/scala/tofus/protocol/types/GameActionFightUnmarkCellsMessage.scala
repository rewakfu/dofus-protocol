package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightUnmarkCellsMessage(
  actionId: Short,
  sourceId: Double,
  markId: Short
) extends Message(5570)

object GameActionFightUnmarkCellsMessage {
  implicit val codec: Codec[GameActionFightUnmarkCellsMessage] =
    new Codec[GameActionFightUnmarkCellsMessage] {
      def decode: Get[GameActionFightUnmarkCellsMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          markId <- short.decode
        } yield GameActionFightUnmarkCellsMessage(actionId, sourceId, markId)

      def encode(value: GameActionFightUnmarkCellsMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        short.encode(value.markId)
    }
}
