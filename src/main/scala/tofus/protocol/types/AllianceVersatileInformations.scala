package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceVersatileInformations(
  allianceId: Int,
  nbGuilds: Short,
  nbMembers: Short,
  nbSubarea: Short
) extends ProtocolType {
  override val protocolId = 432
}

object AllianceVersatileInformations {
  implicit val codec: Codec[AllianceVersatileInformations] =
    new Codec[AllianceVersatileInformations] {
      def decode: Get[AllianceVersatileInformations] =
        for {
          allianceId <- varInt.decode
          nbGuilds <- varShort.decode
          nbMembers <- varShort.decode
          nbSubarea <- varShort.decode
        } yield AllianceVersatileInformations(allianceId, nbGuilds, nbMembers, nbSubarea)

      def encode(value: AllianceVersatileInformations): ByteVector =
        varInt.encode(value.allianceId) ++
        varShort.encode(value.nbGuilds) ++
        varShort.encode(value.nbMembers) ++
        varShort.encode(value.nbSubarea)
    }
}
