package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GoldAddedMessage(
  gold: GoldItem
) extends Message(6030)

object GoldAddedMessage {
  implicit val codec: Codec[GoldAddedMessage] =
    new Codec[GoldAddedMessage] {
      def decode: Get[GoldAddedMessage] =
        for {
          gold <- Codec[GoldItem].decode
        } yield GoldAddedMessage(gold)

      def encode(value: GoldAddedMessage): ByteVector =
        Codec[GoldItem].encode(value.gold)
    }
}
