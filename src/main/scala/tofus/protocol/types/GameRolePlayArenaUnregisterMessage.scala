package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaUnregisterMessage(

) extends Message(6282)

object GameRolePlayArenaUnregisterMessage {
  implicit val codec: Codec[GameRolePlayArenaUnregisterMessage] =
    Codec.const(GameRolePlayArenaUnregisterMessage())
}
