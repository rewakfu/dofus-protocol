package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StartupActionAddMessage(
  newAction: StartupActionAddObject
) extends Message(6538)

object StartupActionAddMessage {
  implicit val codec: Codec[StartupActionAddMessage] =
    new Codec[StartupActionAddMessage] {
      def decode: Get[StartupActionAddMessage] =
        for {
          newAction <- Codec[StartupActionAddObject].decode
        } yield StartupActionAddMessage(newAction)

      def encode(value: StartupActionAddMessage): ByteVector =
        Codec[StartupActionAddObject].encode(value.newAction)
    }
}
