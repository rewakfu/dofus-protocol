package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFightTakePlaceRequestMessage(
  taxCollectorId: Double,
  replacedCharacterId: Int
) extends Message(6235)

object GuildFightTakePlaceRequestMessage {
  implicit val codec: Codec[GuildFightTakePlaceRequestMessage] =
    new Codec[GuildFightTakePlaceRequestMessage] {
      def decode: Get[GuildFightTakePlaceRequestMessage] =
        for {
          taxCollectorId <- double.decode
          replacedCharacterId <- int.decode
        } yield GuildFightTakePlaceRequestMessage(taxCollectorId, replacedCharacterId)

      def encode(value: GuildFightTakePlaceRequestMessage): ByteVector =
        double.encode(value.taxCollectorId) ++
        int.encode(value.replacedCharacterId)
    }
}
