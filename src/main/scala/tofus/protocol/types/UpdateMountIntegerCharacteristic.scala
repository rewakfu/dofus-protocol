package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class UpdateMountIntegerCharacteristic(
  `type`: Byte,
  value: Int
) extends UpdateMountCharacteristic {
  override val protocolId = 537
}

object UpdateMountIntegerCharacteristic {
  implicit val codec: Codec[UpdateMountIntegerCharacteristic] =
    new Codec[UpdateMountIntegerCharacteristic] {
      def decode: Get[UpdateMountIntegerCharacteristic] =
        for {
          `type` <- byte.decode
          value <- int.decode
        } yield UpdateMountIntegerCharacteristic(`type`, value)

      def encode(value: UpdateMountIntegerCharacteristic): ByteVector =
        byte.encode(value.`type`) ++
        int.encode(value.value)
    }
}
