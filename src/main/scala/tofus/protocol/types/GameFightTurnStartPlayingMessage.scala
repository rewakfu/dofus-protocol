package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnStartPlayingMessage(

) extends Message(6465)

object GameFightTurnStartPlayingMessage {
  implicit val codec: Codec[GameFightTurnStartPlayingMessage] =
    Codec.const(GameFightTurnStartPlayingMessage())
}
