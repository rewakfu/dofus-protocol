package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SetEnableAVARequestMessage(
  enable: Boolean
) extends Message(6443)

object SetEnableAVARequestMessage {
  implicit val codec: Codec[SetEnableAVARequestMessage] =
    new Codec[SetEnableAVARequestMessage] {
      def decode: Get[SetEnableAVARequestMessage] =
        for {
          enable <- bool.decode
        } yield SetEnableAVARequestMessage(enable)

      def encode(value: SetEnableAVARequestMessage): ByteVector =
        bool.encode(value.enable)
    }
}
