package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LockableCodeResultMessage(
  result: Byte
) extends Message(5672)

object LockableCodeResultMessage {
  implicit val codec: Codec[LockableCodeResultMessage] =
    new Codec[LockableCodeResultMessage] {
      def decode: Get[LockableCodeResultMessage] =
        for {
          result <- byte.decode
        } yield LockableCodeResultMessage(result)

      def encode(value: LockableCodeResultMessage): ByteVector =
        byte.encode(value.result)
    }
}
