package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterExperienceGainMessage(
  experienceCharacter: Long,
  experienceMount: Long,
  experienceGuild: Long,
  experienceIncarnation: Long
) extends Message(6321)

object CharacterExperienceGainMessage {
  implicit val codec: Codec[CharacterExperienceGainMessage] =
    new Codec[CharacterExperienceGainMessage] {
      def decode: Get[CharacterExperienceGainMessage] =
        for {
          experienceCharacter <- varLong.decode
          experienceMount <- varLong.decode
          experienceGuild <- varLong.decode
          experienceIncarnation <- varLong.decode
        } yield CharacterExperienceGainMessage(experienceCharacter, experienceMount, experienceGuild, experienceIncarnation)

      def encode(value: CharacterExperienceGainMessage): ByteVector =
        varLong.encode(value.experienceCharacter) ++
        varLong.encode(value.experienceMount) ++
        varLong.encode(value.experienceGuild) ++
        varLong.encode(value.experienceIncarnation)
    }
}
