package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayShowMultipleActorsMessage(
  informationsList: List[GameRolePlayActorInformations]
) extends Message(6712)

object GameRolePlayShowMultipleActorsMessage {
  implicit val codec: Codec[GameRolePlayShowMultipleActorsMessage] =
    new Codec[GameRolePlayShowMultipleActorsMessage] {
      def decode: Get[GameRolePlayShowMultipleActorsMessage] =
        for {
          informationsList <- list(ushort, Codec[GameRolePlayActorInformations]).decode
        } yield GameRolePlayShowMultipleActorsMessage(informationsList)

      def encode(value: GameRolePlayShowMultipleActorsMessage): ByteVector =
        list(ushort, Codec[GameRolePlayActorInformations]).encode(value.informationsList)
    }
}
