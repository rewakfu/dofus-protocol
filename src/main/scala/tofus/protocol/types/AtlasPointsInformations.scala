package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AtlasPointsInformations(
  `type`: Byte,
  coords: List[MapCoordinatesExtended]
) extends ProtocolType {
  override val protocolId = 175
}

object AtlasPointsInformations {
  implicit val codec: Codec[AtlasPointsInformations] =
    new Codec[AtlasPointsInformations] {
      def decode: Get[AtlasPointsInformations] =
        for {
          `type` <- byte.decode
          coords <- list(ushort, Codec[MapCoordinatesExtended]).decode
        } yield AtlasPointsInformations(`type`, coords)

      def encode(value: AtlasPointsInformations): ByteVector =
        byte.encode(value.`type`) ++
        list(ushort, Codec[MapCoordinatesExtended]).encode(value.coords)
    }
}
