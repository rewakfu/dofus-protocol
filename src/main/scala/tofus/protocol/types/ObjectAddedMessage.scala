package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectAddedMessage(
  `object`: ObjectItem,
  origin: Byte
) extends Message(3025)

object ObjectAddedMessage {
  implicit val codec: Codec[ObjectAddedMessage] =
    new Codec[ObjectAddedMessage] {
      def decode: Get[ObjectAddedMessage] =
        for {
          `object` <- Codec[ObjectItem].decode
          origin <- byte.decode
        } yield ObjectAddedMessage(`object`, origin)

      def encode(value: ObjectAddedMessage): ByteVector =
        Codec[ObjectItem].encode(value.`object`) ++
        byte.encode(value.origin)
    }
}
