package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicDateMessage(
  day: Byte,
  month: Byte,
  year: Short
) extends Message(177)

object BasicDateMessage {
  implicit val codec: Codec[BasicDateMessage] =
    new Codec[BasicDateMessage] {
      def decode: Get[BasicDateMessage] =
        for {
          day <- byte.decode
          month <- byte.decode
          year <- short.decode
        } yield BasicDateMessage(day, month, year)

      def encode(value: BasicDateMessage): ByteVector =
        byte.encode(value.day) ++
        byte.encode(value.month) ++
        short.encode(value.year)
    }
}
