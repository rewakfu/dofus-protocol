package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage(
  allow: Boolean
) extends Message(6021)

object ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage {
  implicit val codec: Codec[ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage] =
    new Codec[ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage] {
      def decode: Get[ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage] =
        for {
          allow <- bool.decode
        } yield ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage(allow)

      def encode(value: ExchangeMultiCraftSetCrafterCanUseHisRessourcesMessage): ByteVector =
        bool.encode(value.allow)
    }
}
