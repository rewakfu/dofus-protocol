package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachRewardsMessage(
  rewards: List[BreachReward]
) extends Message(6813)

object BreachRewardsMessage {
  implicit val codec: Codec[BreachRewardsMessage] =
    new Codec[BreachRewardsMessage] {
      def decode: Get[BreachRewardsMessage] =
        for {
          rewards <- list(ushort, Codec[BreachReward]).decode
        } yield BreachRewardsMessage(rewards)

      def encode(value: BreachRewardsMessage): ByteVector =
        list(ushort, Codec[BreachReward]).encode(value.rewards)
    }
}
