package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetUseResultWithMissingIdsMessage(
  presetId: Short,
  code: Byte,
  missingIds: List[Short]
) extends Message(6757)

object PresetUseResultWithMissingIdsMessage {
  implicit val codec: Codec[PresetUseResultWithMissingIdsMessage] =
    new Codec[PresetUseResultWithMissingIdsMessage] {
      def decode: Get[PresetUseResultWithMissingIdsMessage] =
        for {
          presetId <- short.decode
          code <- byte.decode
          missingIds <- list(ushort, varShort).decode
        } yield PresetUseResultWithMissingIdsMessage(presetId, code, missingIds)

      def encode(value: PresetUseResultWithMissingIdsMessage): ByteVector =
        short.encode(value.presetId) ++
        byte.encode(value.code) ++
        list(ushort, varShort).encode(value.missingIds)
    }
}
