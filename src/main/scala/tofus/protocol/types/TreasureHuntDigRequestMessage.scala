package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntDigRequestMessage(
  questType: Byte
) extends Message(6485)

object TreasureHuntDigRequestMessage {
  implicit val codec: Codec[TreasureHuntDigRequestMessage] =
    new Codec[TreasureHuntDigRequestMessage] {
      def decode: Get[TreasureHuntDigRequestMessage] =
        for {
          questType <- byte.decode
        } yield TreasureHuntDigRequestMessage(questType)

      def encode(value: TreasureHuntDigRequestMessage): ByteVector =
        byte.encode(value.questType)
    }
}
