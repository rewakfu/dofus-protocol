package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpellForPreset(
  spellId: Short,
  shortcuts: List[Short]
) extends ProtocolType {
  override val protocolId = 557
}

object SpellForPreset {
  implicit val codec: Codec[SpellForPreset] =
    new Codec[SpellForPreset] {
      def decode: Get[SpellForPreset] =
        for {
          spellId <- varShort.decode
          shortcuts <- list(ushort, short).decode
        } yield SpellForPreset(spellId, shortcuts)

      def encode(value: SpellForPreset): ByteVector =
        varShort.encode(value.spellId) ++
        list(ushort, short).encode(value.shortcuts)
    }
}
