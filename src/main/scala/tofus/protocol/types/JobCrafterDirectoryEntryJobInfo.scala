package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryEntryJobInfo(
  jobId: Byte,
  jobLevel: Short,
  free: Boolean,
  minLevel: Short
) extends ProtocolType {
  override val protocolId = 195
}

object JobCrafterDirectoryEntryJobInfo {
  implicit val codec: Codec[JobCrafterDirectoryEntryJobInfo] =
    new Codec[JobCrafterDirectoryEntryJobInfo] {
      def decode: Get[JobCrafterDirectoryEntryJobInfo] =
        for {
          jobId <- byte.decode
          jobLevel <- ubyte.decode
          free <- bool.decode
          minLevel <- ubyte.decode
        } yield JobCrafterDirectoryEntryJobInfo(jobId, jobLevel, free, minLevel)

      def encode(value: JobCrafterDirectoryEntryJobInfo): ByteVector =
        byte.encode(value.jobId) ++
        ubyte.encode(value.jobLevel) ++
        bool.encode(value.free) ++
        ubyte.encode(value.minLevel)
    }
}
