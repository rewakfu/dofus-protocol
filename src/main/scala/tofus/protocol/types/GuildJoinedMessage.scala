package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildJoinedMessage(
  guildInfo: ConcreteGuildInformations,
  memberRights: Int
) extends Message(5564)

object GuildJoinedMessage {
  implicit val codec: Codec[GuildJoinedMessage] =
    new Codec[GuildJoinedMessage] {
      def decode: Get[GuildJoinedMessage] =
        for {
          guildInfo <- Codec[ConcreteGuildInformations].decode
          memberRights <- varInt.decode
        } yield GuildJoinedMessage(guildInfo, memberRights)

      def encode(value: GuildJoinedMessage): ByteVector =
        Codec[ConcreteGuildInformations].encode(value.guildInfo) ++
        varInt.encode(value.memberRights)
    }
}
