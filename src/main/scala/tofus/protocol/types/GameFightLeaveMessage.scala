package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightLeaveMessage(
  charId: Double
) extends Message(721)

object GameFightLeaveMessage {
  implicit val codec: Codec[GameFightLeaveMessage] =
    new Codec[GameFightLeaveMessage] {
      def decode: Get[GameFightLeaveMessage] =
        for {
          charId <- double.decode
        } yield GameFightLeaveMessage(charId)

      def encode(value: GameFightLeaveMessage): ByteVector =
        double.encode(value.charId)
    }
}
