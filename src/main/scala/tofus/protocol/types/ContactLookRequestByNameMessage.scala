package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ContactLookRequestByNameMessage(
  requestId: Short,
  contactType: Byte,
  playerName: String
) extends Message(5933)

object ContactLookRequestByNameMessage {
  implicit val codec: Codec[ContactLookRequestByNameMessage] =
    new Codec[ContactLookRequestByNameMessage] {
      def decode: Get[ContactLookRequestByNameMessage] =
        for {
          requestId <- ubyte.decode
          contactType <- byte.decode
          playerName <- utf8(ushort).decode
        } yield ContactLookRequestByNameMessage(requestId, contactType, playerName)

      def encode(value: ContactLookRequestByNameMessage): ByteVector =
        ubyte.encode(value.requestId) ++
        byte.encode(value.contactType) ++
        utf8(ushort).encode(value.playerName)
    }
}
