package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage(
  solo: ArenaRankInfos,
  team: ArenaRankInfos,
  duel: ArenaRankInfos
) extends Message(6728)

object GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage {
  implicit val codec: Codec[GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage] =
    new Codec[GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage] {
      def decode: Get[GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage] =
        for {
          solo <- Codec[ArenaRankInfos].decode
          team <- Codec[ArenaRankInfos].decode
          duel <- Codec[ArenaRankInfos].decode
        } yield GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage(solo, team, duel)

      def encode(value: GameRolePlayArenaUpdatePlayerInfosAllQueuesMessage): ByteVector =
        Codec[ArenaRankInfos].encode(value.solo) ++
        Codec[ArenaRankInfos].encode(value.team) ++
        Codec[ArenaRankInfos].encode(value.duel)
    }
}
