package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTriggeredEffect(
  uid: Int,
  targetId: Double,
  turnDuration: Short,
  dispelable: Byte,
  spellId: Short,
  effectId: Int,
  parentBoostUid: Int,
  param1: Int,
  param2: Int,
  param3: Int,
  delay: Short
) extends AbstractFightDispellableEffect {
  override val protocolId = 210
}

object FightTriggeredEffect {
  implicit val codec: Codec[FightTriggeredEffect] =
    new Codec[FightTriggeredEffect] {
      def decode: Get[FightTriggeredEffect] =
        for {
          uid <- varInt.decode
          targetId <- double.decode
          turnDuration <- short.decode
          dispelable <- byte.decode
          spellId <- varShort.decode
          effectId <- varInt.decode
          parentBoostUid <- varInt.decode
          param1 <- int.decode
          param2 <- int.decode
          param3 <- int.decode
          delay <- short.decode
        } yield FightTriggeredEffect(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, param1, param2, param3, delay)

      def encode(value: FightTriggeredEffect): ByteVector =
        varInt.encode(value.uid) ++
        double.encode(value.targetId) ++
        short.encode(value.turnDuration) ++
        byte.encode(value.dispelable) ++
        varShort.encode(value.spellId) ++
        varInt.encode(value.effectId) ++
        varInt.encode(value.parentBoostUid) ++
        int.encode(value.param1) ++
        int.encode(value.param2) ++
        int.encode(value.param3) ++
        short.encode(value.delay)
    }
}
