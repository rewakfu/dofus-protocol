package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectUseMessage(
  objectUID: Int
) extends Message(3019)

object ObjectUseMessage {
  implicit val codec: Codec[ObjectUseMessage] =
    new Codec[ObjectUseMessage] {
      def decode: Get[ObjectUseMessage] =
        for {
          objectUID <- varInt.decode
        } yield ObjectUseMessage(objectUID)

      def encode(value: ObjectUseMessage): ByteVector =
        varInt.encode(value.objectUID)
    }
}
