package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareVersatileListMessage(
  dares: List[DareVersatileInformations]
) extends Message(6657)

object DareVersatileListMessage {
  implicit val codec: Codec[DareVersatileListMessage] =
    new Codec[DareVersatileListMessage] {
      def decode: Get[DareVersatileListMessage] =
        for {
          dares <- list(ushort, Codec[DareVersatileInformations]).decode
        } yield DareVersatileListMessage(dares)

      def encode(value: DareVersatileListMessage): ByteVector =
        list(ushort, Codec[DareVersatileInformations]).encode(value.dares)
    }
}
