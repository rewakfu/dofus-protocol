package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolListMessage(
  chosenIdols: List[Short],
  partyChosenIdols: List[Short],
  partyIdols: List[PartyIdol]
) extends Message(6585)

object IdolListMessage {
  implicit val codec: Codec[IdolListMessage] =
    new Codec[IdolListMessage] {
      def decode: Get[IdolListMessage] =
        for {
          chosenIdols <- list(ushort, varShort).decode
          partyChosenIdols <- list(ushort, varShort).decode
          partyIdols <- list(ushort, Codec[PartyIdol]).decode
        } yield IdolListMessage(chosenIdols, partyChosenIdols, partyIdols)

      def encode(value: IdolListMessage): ByteVector =
        list(ushort, varShort).encode(value.chosenIdols) ++
        list(ushort, varShort).encode(value.partyChosenIdols) ++
        list(ushort, Codec[PartyIdol]).encode(value.partyIdols)
    }
}
