package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaSwitchToFightServerMessage(
  address: String,
  ports: List[Int],
  ticket: ByteVector
) extends Message(6575)

object GameRolePlayArenaSwitchToFightServerMessage {
  implicit val codec: Codec[GameRolePlayArenaSwitchToFightServerMessage] =
    new Codec[GameRolePlayArenaSwitchToFightServerMessage] {
      def decode: Get[GameRolePlayArenaSwitchToFightServerMessage] =
        for {
          address <- utf8(ushort).decode
          ports <- list(ushort, ushort).decode
          ticket <- bytes(varInt).decode
        } yield GameRolePlayArenaSwitchToFightServerMessage(address, ports, ticket)

      def encode(value: GameRolePlayArenaSwitchToFightServerMessage): ByteVector =
        utf8(ushort).encode(value.address) ++
        list(ushort, ushort).encode(value.ports) ++
        bytes(varInt).encode(value.ticket)
    }
}
