package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionMarkedCell(
  cellId: Short,
  zoneSize: Byte,
  cellColor: Int,
  cellsType: Byte
) extends ProtocolType {
  override val protocolId = 85
}

object GameActionMarkedCell {
  implicit val codec: Codec[GameActionMarkedCell] =
    new Codec[GameActionMarkedCell] {
      def decode: Get[GameActionMarkedCell] =
        for {
          cellId <- varShort.decode
          zoneSize <- byte.decode
          cellColor <- int.decode
          cellsType <- byte.decode
        } yield GameActionMarkedCell(cellId, zoneSize, cellColor, cellsType)

      def encode(value: GameActionMarkedCell): ByteVector =
        varShort.encode(value.cellId) ++
        byte.encode(value.zoneSize) ++
        int.encode(value.cellColor) ++
        byte.encode(value.cellsType)
    }
}
