package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatsUpgradeRequestMessage(
  useAdditionnal: Boolean,
  statId: Byte,
  boostPoint: Short
) extends Message(5610)

object StatsUpgradeRequestMessage {
  implicit val codec: Codec[StatsUpgradeRequestMessage] =
    new Codec[StatsUpgradeRequestMessage] {
      def decode: Get[StatsUpgradeRequestMessage] =
        for {
          useAdditionnal <- bool.decode
          statId <- byte.decode
          boostPoint <- varShort.decode
        } yield StatsUpgradeRequestMessage(useAdditionnal, statId, boostPoint)

      def encode(value: StatsUpgradeRequestMessage): ByteVector =
        bool.encode(value.useAdditionnal) ++
        byte.encode(value.statId) ++
        varShort.encode(value.boostPoint)
    }
}
