package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTeamMemberTaxCollectorInformations(
  id: Double,
  firstNameId: Short,
  lastNameId: Short,
  level: Short,
  guildId: Int,
  uid: Double
) extends FightTeamMemberInformations {
  override val protocolId = 177
}

object FightTeamMemberTaxCollectorInformations {
  implicit val codec: Codec[FightTeamMemberTaxCollectorInformations] =
    new Codec[FightTeamMemberTaxCollectorInformations] {
      def decode: Get[FightTeamMemberTaxCollectorInformations] =
        for {
          id <- double.decode
          firstNameId <- varShort.decode
          lastNameId <- varShort.decode
          level <- ubyte.decode
          guildId <- varInt.decode
          uid <- double.decode
        } yield FightTeamMemberTaxCollectorInformations(id, firstNameId, lastNameId, level, guildId, uid)

      def encode(value: FightTeamMemberTaxCollectorInformations): ByteVector =
        double.encode(value.id) ++
        varShort.encode(value.firstNameId) ++
        varShort.encode(value.lastNameId) ++
        ubyte.encode(value.level) ++
        varInt.encode(value.guildId) ++
        double.encode(value.uid)
    }
}
