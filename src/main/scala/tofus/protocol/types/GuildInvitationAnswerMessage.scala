package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInvitationAnswerMessage(
  accept: Boolean
) extends Message(5556)

object GuildInvitationAnswerMessage {
  implicit val codec: Codec[GuildInvitationAnswerMessage] =
    new Codec[GuildInvitationAnswerMessage] {
      def decode: Get[GuildInvitationAnswerMessage] =
        for {
          accept <- bool.decode
        } yield GuildInvitationAnswerMessage(accept)

      def encode(value: GuildInvitationAnswerMessage): ByteVector =
        bool.encode(value.accept)
    }
}
