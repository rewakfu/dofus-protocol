package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeShopStockMultiMovementRemovedMessage(
  objectIdList: List[Int]
) extends Message(6037)

object ExchangeShopStockMultiMovementRemovedMessage {
  implicit val codec: Codec[ExchangeShopStockMultiMovementRemovedMessage] =
    new Codec[ExchangeShopStockMultiMovementRemovedMessage] {
      def decode: Get[ExchangeShopStockMultiMovementRemovedMessage] =
        for {
          objectIdList <- list(ushort, varInt).decode
        } yield ExchangeShopStockMultiMovementRemovedMessage(objectIdList)

      def encode(value: ExchangeShopStockMultiMovementRemovedMessage): ByteVector =
        list(ushort, varInt).encode(value.objectIdList)
    }
}
