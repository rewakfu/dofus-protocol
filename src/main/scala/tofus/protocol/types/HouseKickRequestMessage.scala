package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseKickRequestMessage(
  id: Long
) extends Message(5698)

object HouseKickRequestMessage {
  implicit val codec: Codec[HouseKickRequestMessage] =
    new Codec[HouseKickRequestMessage] {
      def decode: Get[HouseKickRequestMessage] =
        for {
          id <- varLong.decode
        } yield HouseKickRequestMessage(id)

      def encode(value: HouseKickRequestMessage): ByteVector =
        varLong.encode(value.id)
    }
}
