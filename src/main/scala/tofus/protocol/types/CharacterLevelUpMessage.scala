package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterLevelUpMessage(
  newLevel: Short
) extends Message(5670)

object CharacterLevelUpMessage {
  implicit val codec: Codec[CharacterLevelUpMessage] =
    new Codec[CharacterLevelUpMessage] {
      def decode: Get[CharacterLevelUpMessage] =
        for {
          newLevel <- varShort.decode
        } yield CharacterLevelUpMessage(newLevel)

      def encode(value: CharacterLevelUpMessage): ByteVector =
        varShort.encode(value.newLevel)
    }
}
