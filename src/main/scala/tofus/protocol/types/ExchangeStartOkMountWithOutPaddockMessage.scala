package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkMountWithOutPaddockMessage(
  stabledMountsDescription: List[MountClientData]
) extends Message(5991)

object ExchangeStartOkMountWithOutPaddockMessage {
  implicit val codec: Codec[ExchangeStartOkMountWithOutPaddockMessage] =
    new Codec[ExchangeStartOkMountWithOutPaddockMessage] {
      def decode: Get[ExchangeStartOkMountWithOutPaddockMessage] =
        for {
          stabledMountsDescription <- list(ushort, Codec[MountClientData]).decode
        } yield ExchangeStartOkMountWithOutPaddockMessage(stabledMountsDescription)

      def encode(value: ExchangeStartOkMountWithOutPaddockMessage): ByteVector =
        list(ushort, Codec[MountClientData]).encode(value.stabledMountsDescription)
    }
}
