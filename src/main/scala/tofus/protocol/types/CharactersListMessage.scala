package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharactersListMessage(
  characters: List[CharacterBaseInformations],
  hasStartupActions: Boolean
) extends Message(151)

object CharactersListMessage {
  implicit val codec: Codec[CharactersListMessage] =
    new Codec[CharactersListMessage] {
      def decode: Get[CharactersListMessage] =
        for {
          characters <- list(ushort, Codec[CharacterBaseInformations]).decode
          hasStartupActions <- bool.decode
        } yield CharactersListMessage(characters, hasStartupActions)

      def encode(value: CharactersListMessage): ByteVector =
        list(ushort, Codec[CharacterBaseInformations]).encode(value.characters) ++
        bool.encode(value.hasStartupActions)
    }
}
