package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionMark(
  markAuthorId: Double,
  markTeamId: Byte,
  markSpellId: Int,
  markSpellLevel: Short,
  markId: Short,
  markType: Byte,
  markimpactCell: Short,
  cells: List[GameActionMarkedCell],
  active: Boolean
) extends ProtocolType {
  override val protocolId = 351
}

object GameActionMark {
  implicit val codec: Codec[GameActionMark] =
    new Codec[GameActionMark] {
      def decode: Get[GameActionMark] =
        for {
          markAuthorId <- double.decode
          markTeamId <- byte.decode
          markSpellId <- int.decode
          markSpellLevel <- short.decode
          markId <- short.decode
          markType <- byte.decode
          markimpactCell <- short.decode
          cells <- list(ushort, Codec[GameActionMarkedCell]).decode
          active <- bool.decode
        } yield GameActionMark(markAuthorId, markTeamId, markSpellId, markSpellLevel, markId, markType, markimpactCell, cells, active)

      def encode(value: GameActionMark): ByteVector =
        double.encode(value.markAuthorId) ++
        byte.encode(value.markTeamId) ++
        int.encode(value.markSpellId) ++
        short.encode(value.markSpellLevel) ++
        short.encode(value.markId) ++
        byte.encode(value.markType) ++
        short.encode(value.markimpactCell) ++
        list(ushort, Codec[GameActionMarkedCell]).encode(value.cells) ++
        bool.encode(value.active)
    }
}
