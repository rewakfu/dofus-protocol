package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectModifiedMessage(
  remote: Boolean,
  `object`: ObjectItem
) extends Message(5519)

object ExchangeObjectModifiedMessage {
  implicit val codec: Codec[ExchangeObjectModifiedMessage] =
    new Codec[ExchangeObjectModifiedMessage] {
      def decode: Get[ExchangeObjectModifiedMessage] =
        for {
          remote <- bool.decode
          `object` <- Codec[ObjectItem].decode
        } yield ExchangeObjectModifiedMessage(remote, `object`)

      def encode(value: ExchangeObjectModifiedMessage): ByteVector =
        bool.encode(value.remote) ++
        Codec[ObjectItem].encode(value.`object`)
    }
}
