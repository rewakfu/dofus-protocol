package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendGuildSetWarnOnAchievementCompleteMessage(
  enable: Boolean
) extends Message(6382)

object FriendGuildSetWarnOnAchievementCompleteMessage {
  implicit val codec: Codec[FriendGuildSetWarnOnAchievementCompleteMessage] =
    new Codec[FriendGuildSetWarnOnAchievementCompleteMessage] {
      def decode: Get[FriendGuildSetWarnOnAchievementCompleteMessage] =
        for {
          enable <- bool.decode
        } yield FriendGuildSetWarnOnAchievementCompleteMessage(enable)

      def encode(value: FriendGuildSetWarnOnAchievementCompleteMessage): ByteVector =
        bool.encode(value.enable)
    }
}
