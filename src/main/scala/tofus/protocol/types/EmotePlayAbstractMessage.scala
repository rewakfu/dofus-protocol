package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmotePlayAbstractMessage(
  emoteId: Short,
  emoteStartTime: Double
) extends Message(5690)

object EmotePlayAbstractMessage {
  implicit val codec: Codec[EmotePlayAbstractMessage] =
    new Codec[EmotePlayAbstractMessage] {
      def decode: Get[EmotePlayAbstractMessage] =
        for {
          emoteId <- ubyte.decode
          emoteStartTime <- double.decode
        } yield EmotePlayAbstractMessage(emoteId, emoteStartTime)

      def encode(value: EmotePlayAbstractMessage): ByteVector =
        ubyte.encode(value.emoteId) ++
        double.encode(value.emoteStartTime)
    }
}
