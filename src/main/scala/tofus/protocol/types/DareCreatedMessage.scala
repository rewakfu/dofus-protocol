package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareCreatedMessage(
  dareInfos: DareInformations,
  needNotifications: Boolean
) extends Message(6668)

object DareCreatedMessage {
  implicit val codec: Codec[DareCreatedMessage] =
    new Codec[DareCreatedMessage] {
      def decode: Get[DareCreatedMessage] =
        for {
          dareInfos <- Codec[DareInformations].decode
          needNotifications <- bool.decode
        } yield DareCreatedMessage(dareInfos, needNotifications)

      def encode(value: DareCreatedMessage): ByteVector =
        Codec[DareInformations].encode(value.dareInfos) ++
        bool.encode(value.needNotifications)
    }
}
