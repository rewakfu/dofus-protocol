package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightMultipleSummonMessage(
  actionId: Short,
  sourceId: Double,
  summons: List[GameContextSummonsInformation]
) extends Message(6837)

object GameActionFightMultipleSummonMessage {
  implicit val codec: Codec[GameActionFightMultipleSummonMessage] =
    new Codec[GameActionFightMultipleSummonMessage] {
      def decode: Get[GameActionFightMultipleSummonMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          summons <- list(ushort, Codec[GameContextSummonsInformation]).decode
        } yield GameActionFightMultipleSummonMessage(actionId, sourceId, summons)

      def encode(value: GameActionFightMultipleSummonMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        list(ushort, Codec[GameContextSummonsInformation]).encode(value.summons)
    }
}
