package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EnabledChannelsMessage(
  channels: ByteVector,
  disallowed: ByteVector
) extends Message(892)

object EnabledChannelsMessage {
  implicit val codec: Codec[EnabledChannelsMessage] =
    new Codec[EnabledChannelsMessage] {
      def decode: Get[EnabledChannelsMessage] =
        for {
          channels <- bytes(ushort).decode
          disallowed <- bytes(ushort).decode
        } yield EnabledChannelsMessage(channels, disallowed)

      def encode(value: EnabledChannelsMessage): ByteVector =
        bytes(ushort).encode(value.channels) ++
        bytes(ushort).encode(value.disallowed)
    }
}
