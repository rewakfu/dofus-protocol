package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildLeftMessage(

) extends Message(5562)

object GuildLeftMessage {
  implicit val codec: Codec[GuildLeftMessage] =
    Codec.const(GuildLeftMessage())
}
