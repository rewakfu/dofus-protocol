package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerExperienceModificatorMessage(
  experiencePercent: Short
) extends Message(6237)

object ServerExperienceModificatorMessage {
  implicit val codec: Codec[ServerExperienceModificatorMessage] =
    new Codec[ServerExperienceModificatorMessage] {
      def decode: Get[ServerExperienceModificatorMessage] =
        for {
          experiencePercent <- varShort.decode
        } yield ServerExperienceModificatorMessage(experiencePercent)

      def encode(value: ServerExperienceModificatorMessage): ByteVector =
        varShort.encode(value.experiencePercent)
    }
}
