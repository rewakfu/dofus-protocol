package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementSwapPositionsRequestMessage(
  cellId: Short,
  requestedId: Double
) extends Message(6541)

object GameFightPlacementSwapPositionsRequestMessage {
  implicit val codec: Codec[GameFightPlacementSwapPositionsRequestMessage] =
    new Codec[GameFightPlacementSwapPositionsRequestMessage] {
      def decode: Get[GameFightPlacementSwapPositionsRequestMessage] =
        for {
          cellId <- varShort.decode
          requestedId <- double.decode
        } yield GameFightPlacementSwapPositionsRequestMessage(cellId, requestedId)

      def encode(value: GameFightPlacementSwapPositionsRequestMessage): ByteVector =
        varShort.encode(value.cellId) ++
        double.encode(value.requestedId)
    }
}
