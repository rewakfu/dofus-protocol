package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait QuestObjectiveInformations extends ProtocolType

final case class ConcreteQuestObjectiveInformations(
  objectiveId: Short,
  objectiveStatus: Boolean,
  dialogParams: List[String]
) extends QuestObjectiveInformations {
  override val protocolId = 385
}

object ConcreteQuestObjectiveInformations {
  implicit val codec: Codec[ConcreteQuestObjectiveInformations] =  
    new Codec[ConcreteQuestObjectiveInformations] {
      def decode: Get[ConcreteQuestObjectiveInformations] =
        for {
          objectiveId <- varShort.decode
          objectiveStatus <- bool.decode
          dialogParams <- list(ushort, utf8(ushort)).decode
        } yield ConcreteQuestObjectiveInformations(objectiveId, objectiveStatus, dialogParams)

      def encode(value: ConcreteQuestObjectiveInformations): ByteVector =
        varShort.encode(value.objectiveId) ++
        bool.encode(value.objectiveStatus) ++
        list(ushort, utf8(ushort)).encode(value.dialogParams)
    }
}

object QuestObjectiveInformations {
  implicit val codec: Codec[QuestObjectiveInformations] =
    new Codec[QuestObjectiveInformations] {
      def decode: Get[QuestObjectiveInformations] =
        ushort.decode.flatMap {
          case 385 => Codec[ConcreteQuestObjectiveInformations].decode
          case 386 => Codec[QuestObjectiveInformationsWithCompletion].decode
        }

      def encode(value: QuestObjectiveInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteQuestObjectiveInformations => Codec[ConcreteQuestObjectiveInformations].encode(i)
          case i: QuestObjectiveInformationsWithCompletion => Codec[QuestObjectiveInformationsWithCompletion].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
