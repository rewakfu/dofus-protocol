package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementSwapPositionsCancelledMessage(
  requestId: Int,
  cancellerId: Double
) extends Message(6546)

object GameFightPlacementSwapPositionsCancelledMessage {
  implicit val codec: Codec[GameFightPlacementSwapPositionsCancelledMessage] =
    new Codec[GameFightPlacementSwapPositionsCancelledMessage] {
      def decode: Get[GameFightPlacementSwapPositionsCancelledMessage] =
        for {
          requestId <- int.decode
          cancellerId <- double.decode
        } yield GameFightPlacementSwapPositionsCancelledMessage(requestId, cancellerId)

      def encode(value: GameFightPlacementSwapPositionsCancelledMessage): ByteVector =
        int.encode(value.requestId) ++
        double.encode(value.cancellerId)
    }
}
