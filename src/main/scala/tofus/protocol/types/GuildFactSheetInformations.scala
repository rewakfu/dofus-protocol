package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GuildFactSheetInformations extends GuildInformations

final case class ConcreteGuildFactSheetInformations(
  guildId: Int,
  guildName: String,
  guildLevel: Short,
  guildEmblem: GuildEmblem,
  leaderId: Long,
  nbMembers: Short
) extends GuildFactSheetInformations {
  override val protocolId = 424
}

object ConcreteGuildFactSheetInformations {
  implicit val codec: Codec[ConcreteGuildFactSheetInformations] =  
    new Codec[ConcreteGuildFactSheetInformations] {
      def decode: Get[ConcreteGuildFactSheetInformations] =
        for {
          guildId <- varInt.decode
          guildName <- utf8(ushort).decode
          guildLevel <- ubyte.decode
          guildEmblem <- Codec[GuildEmblem].decode
          leaderId <- varLong.decode
          nbMembers <- varShort.decode
        } yield ConcreteGuildFactSheetInformations(guildId, guildName, guildLevel, guildEmblem, leaderId, nbMembers)

      def encode(value: ConcreteGuildFactSheetInformations): ByteVector =
        varInt.encode(value.guildId) ++
        utf8(ushort).encode(value.guildName) ++
        ubyte.encode(value.guildLevel) ++
        Codec[GuildEmblem].encode(value.guildEmblem) ++
        varLong.encode(value.leaderId) ++
        varShort.encode(value.nbMembers)
    }
}

object GuildFactSheetInformations {
  implicit val codec: Codec[GuildFactSheetInformations] =
    new Codec[GuildFactSheetInformations] {
      def decode: Get[GuildFactSheetInformations] =
        ushort.decode.flatMap {
          case 424 => Codec[ConcreteGuildFactSheetInformations].decode
          case 423 => Codec[GuildInsiderFactSheetInformations].decode
        }

      def encode(value: GuildFactSheetInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGuildFactSheetInformations => Codec[ConcreteGuildFactSheetInformations].encode(i)
          case i: GuildInsiderFactSheetInformations => Codec[GuildInsiderFactSheetInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
