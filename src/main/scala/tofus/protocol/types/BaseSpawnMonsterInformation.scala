package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait BaseSpawnMonsterInformation extends SpawnInformation

final case class ConcreteBaseSpawnMonsterInformation(
  creatureGenericId: Short
) extends BaseSpawnMonsterInformation {
  override val protocolId = 582
}

object ConcreteBaseSpawnMonsterInformation {
  implicit val codec: Codec[ConcreteBaseSpawnMonsterInformation] =  
    new Codec[ConcreteBaseSpawnMonsterInformation] {
      def decode: Get[ConcreteBaseSpawnMonsterInformation] =
        for {
          creatureGenericId <- varShort.decode
        } yield ConcreteBaseSpawnMonsterInformation(creatureGenericId)

      def encode(value: ConcreteBaseSpawnMonsterInformation): ByteVector =
        varShort.encode(value.creatureGenericId)
    }
}

object BaseSpawnMonsterInformation {
  implicit val codec: Codec[BaseSpawnMonsterInformation] =
    new Codec[BaseSpawnMonsterInformation] {
      def decode: Get[BaseSpawnMonsterInformation] =
        ushort.decode.flatMap {
          case 582 => Codec[ConcreteBaseSpawnMonsterInformation].decode
          case 581 => Codec[SpawnScaledMonsterInformation].decode
          case 572 => Codec[SpawnMonsterInformation].decode
        }

      def encode(value: BaseSpawnMonsterInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteBaseSpawnMonsterInformation => Codec[ConcreteBaseSpawnMonsterInformation].encode(i)
          case i: SpawnScaledMonsterInformation => Codec[SpawnScaledMonsterInformation].encode(i)
          case i: SpawnMonsterInformation => Codec[SpawnMonsterInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
