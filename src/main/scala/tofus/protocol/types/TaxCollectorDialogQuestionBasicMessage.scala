package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorDialogQuestionBasicMessage(
  guildInfo: ConcreteBasicGuildInformations
) extends Message(5619)

object TaxCollectorDialogQuestionBasicMessage {
  implicit val codec: Codec[TaxCollectorDialogQuestionBasicMessage] =
    new Codec[TaxCollectorDialogQuestionBasicMessage] {
      def decode: Get[TaxCollectorDialogQuestionBasicMessage] =
        for {
          guildInfo <- Codec[ConcreteBasicGuildInformations].decode
        } yield TaxCollectorDialogQuestionBasicMessage(guildInfo)

      def encode(value: TaxCollectorDialogQuestionBasicMessage): ByteVector =
        Codec[ConcreteBasicGuildInformations].encode(value.guildInfo)
    }
}
