package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait CharacterBaseInformations extends CharacterMinimalPlusLookInformations

final case class ConcreteCharacterBaseInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte,
  sex: Boolean
) extends CharacterBaseInformations {
  override val protocolId = 45
}

object ConcreteCharacterBaseInformations {
  implicit val codec: Codec[ConcreteCharacterBaseInformations] =  
    new Codec[ConcreteCharacterBaseInformations] {
      def decode: Get[ConcreteCharacterBaseInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
          sex <- bool.decode
        } yield ConcreteCharacterBaseInformations(id, name, level, entityLook, breed, sex)

      def encode(value: ConcreteCharacterBaseInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex)
    }
}

object CharacterBaseInformations {
  implicit val codec: Codec[CharacterBaseInformations] =
    new Codec[CharacterBaseInformations] {
      def decode: Get[CharacterBaseInformations] =
        ushort.decode.flatMap {
          case 45 => Codec[ConcreteCharacterBaseInformations].decode
          case 391 => Codec[PartyMemberArenaInformations].decode
          case 90 => Codec[ConcretePartyMemberInformations].decode
          case 376 => Codec[PartyInvitationMemberInformations].decode
          case 474 => Codec[CharacterHardcoreOrEpicInformations].decode
        }

      def encode(value: CharacterBaseInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteCharacterBaseInformations => Codec[ConcreteCharacterBaseInformations].encode(i)
          case i: PartyMemberArenaInformations => Codec[PartyMemberArenaInformations].encode(i)
          case i: ConcretePartyMemberInformations => Codec[ConcretePartyMemberInformations].encode(i)
          case i: PartyInvitationMemberInformations => Codec[PartyInvitationMemberInformations].encode(i)
          case i: CharacterHardcoreOrEpicInformations => Codec[CharacterHardcoreOrEpicInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
