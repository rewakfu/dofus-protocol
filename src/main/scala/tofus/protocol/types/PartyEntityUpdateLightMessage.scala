package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyEntityUpdateLightMessage(
  partyId: Int,
  id: Long,
  lifePoints: Int,
  maxLifePoints: Int,
  prospecting: Short,
  regenRate: Short,
  indexId: Byte
) extends Message(6781)

object PartyEntityUpdateLightMessage {
  implicit val codec: Codec[PartyEntityUpdateLightMessage] =
    new Codec[PartyEntityUpdateLightMessage] {
      def decode: Get[PartyEntityUpdateLightMessage] =
        for {
          partyId <- varInt.decode
          id <- varLong.decode
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
          prospecting <- varShort.decode
          regenRate <- ubyte.decode
          indexId <- byte.decode
        } yield PartyEntityUpdateLightMessage(partyId, id, lifePoints, maxLifePoints, prospecting, regenRate, indexId)

      def encode(value: PartyEntityUpdateLightMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.id) ++
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints) ++
        varShort.encode(value.prospecting) ++
        ubyte.encode(value.regenRate) ++
        byte.encode(value.indexId)
    }
}
