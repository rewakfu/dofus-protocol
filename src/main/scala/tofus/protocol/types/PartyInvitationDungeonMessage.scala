package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationDungeonMessage(
  partyId: Int,
  partyType: Byte,
  partyName: String,
  maxParticipants: Byte,
  fromId: Long,
  fromName: String,
  toId: Long,
  dungeonId: Short
) extends Message(6244)

object PartyInvitationDungeonMessage {
  implicit val codec: Codec[PartyInvitationDungeonMessage] =
    new Codec[PartyInvitationDungeonMessage] {
      def decode: Get[PartyInvitationDungeonMessage] =
        for {
          partyId <- varInt.decode
          partyType <- byte.decode
          partyName <- utf8(ushort).decode
          maxParticipants <- byte.decode
          fromId <- varLong.decode
          fromName <- utf8(ushort).decode
          toId <- varLong.decode
          dungeonId <- varShort.decode
        } yield PartyInvitationDungeonMessage(partyId, partyType, partyName, maxParticipants, fromId, fromName, toId, dungeonId)

      def encode(value: PartyInvitationDungeonMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.partyType) ++
        utf8(ushort).encode(value.partyName) ++
        byte.encode(value.maxParticipants) ++
        varLong.encode(value.fromId) ++
        utf8(ushort).encode(value.fromName) ++
        varLong.encode(value.toId) ++
        varShort.encode(value.dungeonId)
    }
}
