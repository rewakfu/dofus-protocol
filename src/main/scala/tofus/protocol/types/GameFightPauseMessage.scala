package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPauseMessage(
  isPaused: Boolean
) extends Message(6754)

object GameFightPauseMessage {
  implicit val codec: Codec[GameFightPauseMessage] =
    new Codec[GameFightPauseMessage] {
      def decode: Get[GameFightPauseMessage] =
        for {
          isPaused <- bool.decode
        } yield GameFightPauseMessage(isPaused)

      def encode(value: GameFightPauseMessage): ByteVector =
        bool.encode(value.isPaused)
    }
}
