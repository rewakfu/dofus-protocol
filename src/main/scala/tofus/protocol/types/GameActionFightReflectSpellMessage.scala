package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightReflectSpellMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double
) extends Message(5531)

object GameActionFightReflectSpellMessage {
  implicit val codec: Codec[GameActionFightReflectSpellMessage] =
    new Codec[GameActionFightReflectSpellMessage] {
      def decode: Get[GameActionFightReflectSpellMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
        } yield GameActionFightReflectSpellMessage(actionId, sourceId, targetId)

      def encode(value: GameActionFightReflectSpellMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId)
    }
}
