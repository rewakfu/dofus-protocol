package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceCreationStartedMessage(

) extends Message(6394)

object AllianceCreationStartedMessage {
  implicit val codec: Codec[AllianceCreationStartedMessage] =
    Codec.const(AllianceCreationStartedMessage())
}
