package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectTransfertListFromInvMessage(
  ids: List[Int]
) extends Message(6183)

object ExchangeObjectTransfertListFromInvMessage {
  implicit val codec: Codec[ExchangeObjectTransfertListFromInvMessage] =
    new Codec[ExchangeObjectTransfertListFromInvMessage] {
      def decode: Get[ExchangeObjectTransfertListFromInvMessage] =
        for {
          ids <- list(ushort, varInt).decode
        } yield ExchangeObjectTransfertListFromInvMessage(ids)

      def encode(value: ExchangeObjectTransfertListFromInvMessage): ByteVector =
        list(ushort, varInt).encode(value.ids)
    }
}
