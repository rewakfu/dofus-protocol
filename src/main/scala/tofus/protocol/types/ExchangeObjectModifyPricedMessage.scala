package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectModifyPricedMessage(
  objectUID: Int,
  quantity: Int,
  price: Long
) extends Message(6238)

object ExchangeObjectModifyPricedMessage {
  implicit val codec: Codec[ExchangeObjectModifyPricedMessage] =
    new Codec[ExchangeObjectModifyPricedMessage] {
      def decode: Get[ExchangeObjectModifyPricedMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
          price <- varLong.decode
        } yield ExchangeObjectModifyPricedMessage(objectUID, quantity, price)

      def encode(value: ExchangeObjectModifyPricedMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity) ++
        varLong.encode(value.price)
    }
}
