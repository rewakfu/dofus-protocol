package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapRunningFightDetailsRequestMessage(
  fightId: Short
) extends Message(5750)

object MapRunningFightDetailsRequestMessage {
  implicit val codec: Codec[MapRunningFightDetailsRequestMessage] =
    new Codec[MapRunningFightDetailsRequestMessage] {
      def decode: Get[MapRunningFightDetailsRequestMessage] =
        for {
          fightId <- varShort.decode
        } yield MapRunningFightDetailsRequestMessage(fightId)

      def encode(value: MapRunningFightDetailsRequestMessage): ByteVector =
        varShort.encode(value.fightId)
    }
}
