package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicCharactersListMessage(
  characters: List[CharacterBaseInformations]
) extends Message(6475)

object BasicCharactersListMessage {
  implicit val codec: Codec[BasicCharactersListMessage] =
    new Codec[BasicCharactersListMessage] {
      def decode: Get[BasicCharactersListMessage] =
        for {
          characters <- list(ushort, Codec[CharacterBaseInformations]).decode
        } yield BasicCharactersListMessage(characters)

      def encode(value: BasicCharactersListMessage): ByteVector =
        list(ushort, Codec[CharacterBaseInformations]).encode(value.characters)
    }
}
