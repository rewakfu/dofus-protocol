package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait Shortcut extends ProtocolType

final case class ConcreteShortcut(
  slot: Byte
) extends Shortcut {
  override val protocolId = 369
}

object ConcreteShortcut {
  implicit val codec: Codec[ConcreteShortcut] =  
    new Codec[ConcreteShortcut] {
      def decode: Get[ConcreteShortcut] =
        for {
          slot <- byte.decode
        } yield ConcreteShortcut(slot)

      def encode(value: ConcreteShortcut): ByteVector =
        byte.encode(value.slot)
    }
}

object Shortcut {
  implicit val codec: Codec[Shortcut] =
    new Codec[Shortcut] {
      def decode: Get[Shortcut] =
        ushort.decode.flatMap {
          case 369 => Codec[ConcreteShortcut].decode
          case 370 => Codec[ShortcutObjectPreset].decode
          case 371 => Codec[ShortcutObjectItem].decode
          case 492 => Codec[ShortcutObjectIdolsPreset].decode
          case 367 => Codec[ConcreteShortcutObject].decode
          case 544 => Codec[ShortcutEntitiesPreset].decode
          case 389 => Codec[ShortcutEmote].decode
          case 368 => Codec[ShortcutSpell].decode
          case 388 => Codec[ShortcutSmiley].decode
        }

      def encode(value: Shortcut): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteShortcut => Codec[ConcreteShortcut].encode(i)
          case i: ShortcutObjectPreset => Codec[ShortcutObjectPreset].encode(i)
          case i: ShortcutObjectItem => Codec[ShortcutObjectItem].encode(i)
          case i: ShortcutObjectIdolsPreset => Codec[ShortcutObjectIdolsPreset].encode(i)
          case i: ConcreteShortcutObject => Codec[ConcreteShortcutObject].encode(i)
          case i: ShortcutEntitiesPreset => Codec[ShortcutEntitiesPreset].encode(i)
          case i: ShortcutEmote => Codec[ShortcutEmote].encode(i)
          case i: ShortcutSpell => Codec[ShortcutSpell].encode(i)
          case i: ShortcutSmiley => Codec[ShortcutSmiley].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
