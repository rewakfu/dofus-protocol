package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightUpdateTeamMessage(
  fightId: Short,
  team: ConcreteFightTeamInformations
) extends Message(5572)

object GameFightUpdateTeamMessage {
  implicit val codec: Codec[GameFightUpdateTeamMessage] =
    new Codec[GameFightUpdateTeamMessage] {
      def decode: Get[GameFightUpdateTeamMessage] =
        for {
          fightId <- varShort.decode
          team <- Codec[ConcreteFightTeamInformations].decode
        } yield GameFightUpdateTeamMessage(fightId, team)

      def encode(value: GameFightUpdateTeamMessage): ByteVector =
        varShort.encode(value.fightId) ++
        Codec[ConcreteFightTeamInformations].encode(value.team)
    }
}
