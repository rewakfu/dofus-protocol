package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterSelectedForceMessage(
  id: Int
) extends Message(6068)

object CharacterSelectedForceMessage {
  implicit val codec: Codec[CharacterSelectedForceMessage] =
    new Codec[CharacterSelectedForceMessage] {
      def decode: Get[CharacterSelectedForceMessage] =
        for {
          id <- int.decode
        } yield CharacterSelectedForceMessage(id)

      def encode(value: CharacterSelectedForceMessage): ByteVector =
        int.encode(value.id)
    }
}
