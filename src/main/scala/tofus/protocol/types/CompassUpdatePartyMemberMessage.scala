package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CompassUpdatePartyMemberMessage(
  `type`: Byte,
  coords: MapCoordinates,
  memberId: Long,
  active: Boolean
) extends Message(5589)

object CompassUpdatePartyMemberMessage {
  implicit val codec: Codec[CompassUpdatePartyMemberMessage] =
    new Codec[CompassUpdatePartyMemberMessage] {
      def decode: Get[CompassUpdatePartyMemberMessage] =
        for {
          `type` <- byte.decode
          coords <- Codec[MapCoordinates].decode
          memberId <- varLong.decode
          active <- bool.decode
        } yield CompassUpdatePartyMemberMessage(`type`, coords, memberId, active)

      def encode(value: CompassUpdatePartyMemberMessage): ByteVector =
        byte.encode(value.`type`) ++
        Codec[MapCoordinates].encode(value.coords) ++
        varLong.encode(value.memberId) ++
        bool.encode(value.active)
    }
}
