package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendAddedMessage(
  friendAdded: FriendInformations
) extends Message(5599)

object FriendAddedMessage {
  implicit val codec: Codec[FriendAddedMessage] =
    new Codec[FriendAddedMessage] {
      def decode: Get[FriendAddedMessage] =
        for {
          friendAdded <- Codec[FriendInformations].decode
        } yield FriendAddedMessage(friendAdded)

      def encode(value: FriendAddedMessage): ByteVector =
        Codec[FriendInformations].encode(value.friendAdded)
    }
}
