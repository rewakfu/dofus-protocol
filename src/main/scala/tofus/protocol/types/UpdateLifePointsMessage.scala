package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class UpdateLifePointsMessage(
  lifePoints: Int,
  maxLifePoints: Int
) extends Message(5658)

object UpdateLifePointsMessage {
  implicit val codec: Codec[UpdateLifePointsMessage] =
    new Codec[UpdateLifePointsMessage] {
      def decode: Get[UpdateLifePointsMessage] =
        for {
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
        } yield UpdateLifePointsMessage(lifePoints, maxLifePoints)

      def encode(value: UpdateLifePointsMessage): ByteVector =
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints)
    }
}
