package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayDelayedActionMessage(
  delayedCharacterId: Double,
  delayTypeId: Byte,
  delayEndTime: Double
) extends Message(6153)

object GameRolePlayDelayedActionMessage {
  implicit val codec: Codec[GameRolePlayDelayedActionMessage] =
    new Codec[GameRolePlayDelayedActionMessage] {
      def decode: Get[GameRolePlayDelayedActionMessage] =
        for {
          delayedCharacterId <- double.decode
          delayTypeId <- byte.decode
          delayEndTime <- double.decode
        } yield GameRolePlayDelayedActionMessage(delayedCharacterId, delayTypeId, delayEndTime)

      def encode(value: GameRolePlayDelayedActionMessage): ByteVector =
        double.encode(value.delayedCharacterId) ++
        byte.encode(value.delayTypeId) ++
        double.encode(value.delayEndTime)
    }
}
