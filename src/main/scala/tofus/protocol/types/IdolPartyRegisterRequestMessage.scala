package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolPartyRegisterRequestMessage(
  register: Boolean
) extends Message(6582)

object IdolPartyRegisterRequestMessage {
  implicit val codec: Codec[IdolPartyRegisterRequestMessage] =
    new Codec[IdolPartyRegisterRequestMessage] {
      def decode: Get[IdolPartyRegisterRequestMessage] =
        for {
          register <- bool.decode
        } yield IdolPartyRegisterRequestMessage(register)

      def encode(value: IdolPartyRegisterRequestMessage): ByteVector =
        bool.encode(value.register)
    }
}
