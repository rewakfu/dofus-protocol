package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceMotdMessage(
  content: String,
  timestamp: Int,
  memberId: Long,
  memberName: String
) extends Message(6685)

object AllianceMotdMessage {
  implicit val codec: Codec[AllianceMotdMessage] =
    new Codec[AllianceMotdMessage] {
      def decode: Get[AllianceMotdMessage] =
        for {
          content <- utf8(ushort).decode
          timestamp <- int.decode
          memberId <- varLong.decode
          memberName <- utf8(ushort).decode
        } yield AllianceMotdMessage(content, timestamp, memberId, memberName)

      def encode(value: AllianceMotdMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        varLong.encode(value.memberId) ++
        utf8(ushort).encode(value.memberName)
    }
}
