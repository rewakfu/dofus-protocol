package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestObjectiveValidatedMessage(
  questId: Short,
  objectiveId: Short
) extends Message(6098)

object QuestObjectiveValidatedMessage {
  implicit val codec: Codec[QuestObjectiveValidatedMessage] =
    new Codec[QuestObjectiveValidatedMessage] {
      def decode: Get[QuestObjectiveValidatedMessage] =
        for {
          questId <- varShort.decode
          objectiveId <- varShort.decode
        } yield QuestObjectiveValidatedMessage(questId, objectiveId)

      def encode(value: QuestObjectiveValidatedMessage): ByteVector =
        varShort.encode(value.questId) ++
        varShort.encode(value.objectiveId)
    }
}
