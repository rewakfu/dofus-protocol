package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveUseRequestMessage(
  elemId: Int,
  skillInstanceUid: Int
) extends Message(5001)

object InteractiveUseRequestMessage {
  implicit val codec: Codec[InteractiveUseRequestMessage] =
    new Codec[InteractiveUseRequestMessage] {
      def decode: Get[InteractiveUseRequestMessage] =
        for {
          elemId <- varInt.decode
          skillInstanceUid <- varInt.decode
        } yield InteractiveUseRequestMessage(elemId, skillInstanceUid)

      def encode(value: InteractiveUseRequestMessage): ByteVector =
        varInt.encode(value.elemId) ++
        varInt.encode(value.skillInstanceUid)
    }
}
