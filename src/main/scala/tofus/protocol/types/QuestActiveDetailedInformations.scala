package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestActiveDetailedInformations(
  questId: Short,
  stepId: Short,
  objectives: List[QuestObjectiveInformations]
) extends QuestActiveInformations {
  override val protocolId = 382
}

object QuestActiveDetailedInformations {
  implicit val codec: Codec[QuestActiveDetailedInformations] =
    new Codec[QuestActiveDetailedInformations] {
      def decode: Get[QuestActiveDetailedInformations] =
        for {
          questId <- varShort.decode
          stepId <- varShort.decode
          objectives <- list(ushort, Codec[QuestObjectiveInformations]).decode
        } yield QuestActiveDetailedInformations(questId, stepId, objectives)

      def encode(value: QuestActiveDetailedInformations): ByteVector =
        varShort.encode(value.questId) ++
        varShort.encode(value.stepId) ++
        list(ushort, Codec[QuestObjectiveInformations]).encode(value.objectives)
    }
}
