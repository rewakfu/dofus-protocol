package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachTeleportResponseMessage(
  teleported: Boolean
) extends Message(6816)

object BreachTeleportResponseMessage {
  implicit val codec: Codec[BreachTeleportResponseMessage] =
    new Codec[BreachTeleportResponseMessage] {
      def decode: Get[BreachTeleportResponseMessage] =
        for {
          teleported <- bool.decode
        } yield BreachTeleportResponseMessage(teleported)

      def encode(value: BreachTeleportResponseMessage): ByteVector =
        bool.encode(value.teleported)
    }
}
