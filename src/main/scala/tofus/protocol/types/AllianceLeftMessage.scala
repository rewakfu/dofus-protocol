package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceLeftMessage(

) extends Message(6398)

object AllianceLeftMessage {
  implicit val codec: Codec[AllianceLeftMessage] =
    Codec.const(AllianceLeftMessage())
}
