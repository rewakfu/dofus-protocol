package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachEnterMessage(
  owner: Long
) extends Message(6810)

object BreachEnterMessage {
  implicit val codec: Codec[BreachEnterMessage] =
    new Codec[BreachEnterMessage] {
      def decode: Get[BreachEnterMessage] =
        for {
          owner <- varLong.decode
        } yield BreachEnterMessage(owner)

      def encode(value: BreachEnterMessage): ByteVector =
        varLong.encode(value.owner)
    }
}
