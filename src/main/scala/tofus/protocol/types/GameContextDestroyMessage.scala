package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextDestroyMessage(

) extends Message(201)

object GameContextDestroyMessage {
  implicit val codec: Codec[GameContextDestroyMessage] =
    Codec.const(GameContextDestroyMessage())
}
