package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryListRequestMessage(
  jobId: Byte
) extends Message(6047)

object JobCrafterDirectoryListRequestMessage {
  implicit val codec: Codec[JobCrafterDirectoryListRequestMessage] =
    new Codec[JobCrafterDirectoryListRequestMessage] {
      def decode: Get[JobCrafterDirectoryListRequestMessage] =
        for {
          jobId <- byte.decode
        } yield JobCrafterDirectoryListRequestMessage(jobId)

      def encode(value: JobCrafterDirectoryListRequestMessage): ByteVector =
        byte.encode(value.jobId)
    }
}
