package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInformationsMembersMessage(
  members: List[GuildMember]
) extends Message(5558)

object GuildInformationsMembersMessage {
  implicit val codec: Codec[GuildInformationsMembersMessage] =
    new Codec[GuildInformationsMembersMessage] {
      def decode: Get[GuildInformationsMembersMessage] =
        for {
          members <- list(ushort, Codec[GuildMember]).decode
        } yield GuildInformationsMembersMessage(members)

      def encode(value: GuildInformationsMembersMessage): ByteVector =
        list(ushort, Codec[GuildMember]).encode(value.members)
    }
}
