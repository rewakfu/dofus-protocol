package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInAllianceFactsMessage(
  infos: GuildFactSheetInformations,
  creationDate: Int,
  nbTaxCollectors: Short,
  members: List[CharacterMinimalGuildPublicInformations],
  allianceInfos: ConcreteBasicNamedAllianceInformations
) extends Message(6422)

object GuildInAllianceFactsMessage {
  implicit val codec: Codec[GuildInAllianceFactsMessage] =
    new Codec[GuildInAllianceFactsMessage] {
      def decode: Get[GuildInAllianceFactsMessage] =
        for {
          infos <- Codec[GuildFactSheetInformations].decode
          creationDate <- int.decode
          nbTaxCollectors <- varShort.decode
          members <- list(ushort, Codec[CharacterMinimalGuildPublicInformations]).decode
          allianceInfos <- Codec[ConcreteBasicNamedAllianceInformations].decode
        } yield GuildInAllianceFactsMessage(infos, creationDate, nbTaxCollectors, members, allianceInfos)

      def encode(value: GuildInAllianceFactsMessage): ByteVector =
        Codec[GuildFactSheetInformations].encode(value.infos) ++
        int.encode(value.creationDate) ++
        varShort.encode(value.nbTaxCollectors) ++
        list(ushort, Codec[CharacterMinimalGuildPublicInformations]).encode(value.members) ++
        Codec[ConcreteBasicNamedAllianceInformations].encode(value.allianceInfos)
    }
}
