package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorMovementRemoveMessage(
  collectorId: Double
) extends Message(5915)

object TaxCollectorMovementRemoveMessage {
  implicit val codec: Codec[TaxCollectorMovementRemoveMessage] =
    new Codec[TaxCollectorMovementRemoveMessage] {
      def decode: Get[TaxCollectorMovementRemoveMessage] =
        for {
          collectorId <- double.decode
        } yield TaxCollectorMovementRemoveMessage(collectorId)

      def encode(value: TaxCollectorMovementRemoveMessage): ByteVector =
        double.encode(value.collectorId)
    }
}
