package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerSessionConstantsMessage(
  variables: List[ServerSessionConstant]
) extends Message(6434)

object ServerSessionConstantsMessage {
  implicit val codec: Codec[ServerSessionConstantsMessage] =
    new Codec[ServerSessionConstantsMessage] {
      def decode: Get[ServerSessionConstantsMessage] =
        for {
          variables <- list(ushort, Codec[ServerSessionConstant]).decode
        } yield ServerSessionConstantsMessage(variables)

      def encode(value: ServerSessionConstantsMessage): ByteVector =
        list(ushort, Codec[ServerSessionConstant]).encode(value.variables)
    }
}
