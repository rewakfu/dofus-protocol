package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildPaddockRemovedMessage(
  paddockId: Double
) extends Message(5955)

object GuildPaddockRemovedMessage {
  implicit val codec: Codec[GuildPaddockRemovedMessage] =
    new Codec[GuildPaddockRemovedMessage] {
      def decode: Get[GuildPaddockRemovedMessage] =
        for {
          paddockId <- double.decode
        } yield GuildPaddockRemovedMessage(paddockId)

      def encode(value: GuildPaddockRemovedMessage): ByteVector =
        double.encode(value.paddockId)
    }
}
