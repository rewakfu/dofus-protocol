package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportBuddiesAnswerMessage(
  accept: Boolean
) extends Message(6294)

object TeleportBuddiesAnswerMessage {
  implicit val codec: Codec[TeleportBuddiesAnswerMessage] =
    new Codec[TeleportBuddiesAnswerMessage] {
      def decode: Get[TeleportBuddiesAnswerMessage] =
        for {
          accept <- bool.decode
        } yield TeleportBuddiesAnswerMessage(accept)

      def encode(value: TeleportBuddiesAnswerMessage): ByteVector =
        bool.encode(value.accept)
    }
}
