package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicStatMessage(
  timeSpent: Double,
  statId: Short
) extends Message(6530)

object BasicStatMessage {
  implicit val codec: Codec[BasicStatMessage] =
    new Codec[BasicStatMessage] {
      def decode: Get[BasicStatMessage] =
        for {
          timeSpent <- double.decode
          statId <- varShort.decode
        } yield BasicStatMessage(timeSpent, statId)

      def encode(value: BasicStatMessage): ByteVector =
        double.encode(value.timeSpent) ++
        varShort.encode(value.statId)
    }
}
