package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountsTakenFromPaddockMessage(
  name: String,
  worldX: Short,
  worldY: Short,
  ownername: String
) extends Message(6554)

object ExchangeMountsTakenFromPaddockMessage {
  implicit val codec: Codec[ExchangeMountsTakenFromPaddockMessage] =
    new Codec[ExchangeMountsTakenFromPaddockMessage] {
      def decode: Get[ExchangeMountsTakenFromPaddockMessage] =
        for {
          name <- utf8(ushort).decode
          worldX <- short.decode
          worldY <- short.decode
          ownername <- utf8(ushort).decode
        } yield ExchangeMountsTakenFromPaddockMessage(name, worldX, worldY, ownername)

      def encode(value: ExchangeMountsTakenFromPaddockMessage): ByteVector =
        utf8(ushort).encode(value.name) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        utf8(ushort).encode(value.ownername)
    }
}
