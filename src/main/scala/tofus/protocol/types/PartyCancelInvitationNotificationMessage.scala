package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyCancelInvitationNotificationMessage(
  partyId: Int,
  cancelerId: Long,
  guestId: Long
) extends Message(6251)

object PartyCancelInvitationNotificationMessage {
  implicit val codec: Codec[PartyCancelInvitationNotificationMessage] =
    new Codec[PartyCancelInvitationNotificationMessage] {
      def decode: Get[PartyCancelInvitationNotificationMessage] =
        for {
          partyId <- varInt.decode
          cancelerId <- varLong.decode
          guestId <- varLong.decode
        } yield PartyCancelInvitationNotificationMessage(partyId, cancelerId, guestId)

      def encode(value: PartyCancelInvitationNotificationMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.cancelerId) ++
        varLong.encode(value.guestId)
    }
}
