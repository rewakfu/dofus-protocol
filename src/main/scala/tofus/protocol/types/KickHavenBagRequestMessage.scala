package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class KickHavenBagRequestMessage(
  guestId: Long
) extends Message(6652)

object KickHavenBagRequestMessage {
  implicit val codec: Codec[KickHavenBagRequestMessage] =
    new Codec[KickHavenBagRequestMessage] {
      def decode: Get[KickHavenBagRequestMessage] =
        for {
          guestId <- varLong.decode
        } yield KickHavenBagRequestMessage(guestId)

      def encode(value: KickHavenBagRequestMessage): ByteVector =
        varLong.encode(value.guestId)
    }
}
