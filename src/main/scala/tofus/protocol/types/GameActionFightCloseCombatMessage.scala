package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightCloseCombatMessage(
  actionId: Short,
  sourceId: Double,
  flags0: Byte,
  targetId: Double,
  destinationCellId: Short,
  critical: Byte,
  weaponGenericId: Short
) extends Message(6116)

object GameActionFightCloseCombatMessage {
  implicit val codec: Codec[GameActionFightCloseCombatMessage] =
    new Codec[GameActionFightCloseCombatMessage] {
      def decode: Get[GameActionFightCloseCombatMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          flags0 <- byte.decode
          targetId <- double.decode
          destinationCellId <- short.decode
          critical <- byte.decode
          weaponGenericId <- varShort.decode
        } yield GameActionFightCloseCombatMessage(actionId, sourceId, flags0, targetId, destinationCellId, critical, weaponGenericId)

      def encode(value: GameActionFightCloseCombatMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        byte.encode(value.flags0) ++
        double.encode(value.targetId) ++
        short.encode(value.destinationCellId) ++
        byte.encode(value.critical) ++
        varShort.encode(value.weaponGenericId)
    }
}
