package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatCommunityChannelCommunityMessage(
  communityId: Short
) extends Message(6730)

object ChatCommunityChannelCommunityMessage {
  implicit val codec: Codec[ChatCommunityChannelCommunityMessage] =
    new Codec[ChatCommunityChannelCommunityMessage] {
      def decode: Get[ChatCommunityChannelCommunityMessage] =
        for {
          communityId <- short.decode
        } yield ChatCommunityChannelCommunityMessage(communityId)

      def encode(value: ChatCommunityChannelCommunityMessage): ByteVector =
        short.encode(value.communityId)
    }
}
