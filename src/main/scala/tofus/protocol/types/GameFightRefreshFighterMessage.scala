package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightRefreshFighterMessage(
  informations: GameContextActorInformations
) extends Message(6309)

object GameFightRefreshFighterMessage {
  implicit val codec: Codec[GameFightRefreshFighterMessage] =
    new Codec[GameFightRefreshFighterMessage] {
      def decode: Get[GameFightRefreshFighterMessage] =
        for {
          informations <- Codec[GameContextActorInformations].decode
        } yield GameFightRefreshFighterMessage(informations)

      def encode(value: GameFightRefreshFighterMessage): ByteVector =
        Codec[GameContextActorInformations].encode(value.informations)
    }
}
