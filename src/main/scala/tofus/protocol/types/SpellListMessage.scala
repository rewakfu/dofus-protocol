package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpellListMessage(
  spellPrevisualization: Boolean,
  spells: List[SpellItem]
) extends Message(1200)

object SpellListMessage {
  implicit val codec: Codec[SpellListMessage] =
    new Codec[SpellListMessage] {
      def decode: Get[SpellListMessage] =
        for {
          spellPrevisualization <- bool.decode
          spells <- list(ushort, Codec[SpellItem]).decode
        } yield SpellListMessage(spellPrevisualization, spells)

      def encode(value: SpellListMessage): ByteVector =
        bool.encode(value.spellPrevisualization) ++
        list(ushort, Codec[SpellItem]).encode(value.spells)
    }
}
