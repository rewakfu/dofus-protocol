package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterHardcoreOrEpicInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte,
  sex: Boolean,
  deathState: Byte,
  deathCount: Short,
  deathMaxLevel: Short
) extends CharacterBaseInformations {
  override val protocolId = 474
}

object CharacterHardcoreOrEpicInformations {
  implicit val codec: Codec[CharacterHardcoreOrEpicInformations] =
    new Codec[CharacterHardcoreOrEpicInformations] {
      def decode: Get[CharacterHardcoreOrEpicInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
          sex <- bool.decode
          deathState <- byte.decode
          deathCount <- varShort.decode
          deathMaxLevel <- varShort.decode
        } yield CharacterHardcoreOrEpicInformations(id, name, level, entityLook, breed, sex, deathState, deathCount, deathMaxLevel)

      def encode(value: CharacterHardcoreOrEpicInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        byte.encode(value.deathState) ++
        varShort.encode(value.deathCount) ++
        varShort.encode(value.deathMaxLevel)
    }
}
