package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightFighterEntityLightInformation(
  flags0: Byte,
  id: Double,
  wave: Byte,
  level: Short,
  breed: Byte,
  entityModelId: Byte,
  masterId: Double
) extends GameFightFighterLightInformations {
  override val protocolId = 548
}

object GameFightFighterEntityLightInformation {
  implicit val codec: Codec[GameFightFighterEntityLightInformation] =
    new Codec[GameFightFighterEntityLightInformation] {
      def decode: Get[GameFightFighterEntityLightInformation] =
        for {
          flags0 <- byte.decode
          id <- double.decode
          wave <- byte.decode
          level <- varShort.decode
          breed <- byte.decode
          entityModelId <- byte.decode
          masterId <- double.decode
        } yield GameFightFighterEntityLightInformation(flags0, id, wave, level, breed, entityModelId, masterId)

      def encode(value: GameFightFighterEntityLightInformation): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.id) ++
        byte.encode(value.wave) ++
        varShort.encode(value.level) ++
        byte.encode(value.breed) ++
        byte.encode(value.entityModelId) ++
        double.encode(value.masterId)
    }
}
