package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutObjectIdolsPreset(
  slot: Byte,
  presetId: Short
) extends ShortcutObject {
  override val protocolId = 492
}

object ShortcutObjectIdolsPreset {
  implicit val codec: Codec[ShortcutObjectIdolsPreset] =
    new Codec[ShortcutObjectIdolsPreset] {
      def decode: Get[ShortcutObjectIdolsPreset] =
        for {
          slot <- byte.decode
          presetId <- short.decode
        } yield ShortcutObjectIdolsPreset(slot, presetId)

      def encode(value: ShortcutObjectIdolsPreset): ByteVector =
        byte.encode(value.slot) ++
        short.encode(value.presetId)
    }
}
