package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HousePropertiesMessage(
  houseId: Int,
  doorsOnMap: List[Int],
  properties: HouseInstanceInformations
) extends Message(5734)

object HousePropertiesMessage {
  implicit val codec: Codec[HousePropertiesMessage] =
    new Codec[HousePropertiesMessage] {
      def decode: Get[HousePropertiesMessage] =
        for {
          houseId <- varInt.decode
          doorsOnMap <- list(ushort, int).decode
          properties <- Codec[HouseInstanceInformations].decode
        } yield HousePropertiesMessage(houseId, doorsOnMap, properties)

      def encode(value: HousePropertiesMessage): ByteVector =
        varInt.encode(value.houseId) ++
        list(ushort, int).encode(value.doorsOnMap) ++
        Codec[HouseInstanceInformations].encode(value.properties)
    }
}
