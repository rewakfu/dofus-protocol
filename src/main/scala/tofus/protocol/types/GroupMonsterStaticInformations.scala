package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GroupMonsterStaticInformations extends ProtocolType

final case class ConcreteGroupMonsterStaticInformations(
  mainCreatureLightInfos: ConcreteMonsterInGroupLightInformations,
  underlings: List[MonsterInGroupInformations]
) extends GroupMonsterStaticInformations {
  override val protocolId = 140
}

object ConcreteGroupMonsterStaticInformations {
  implicit val codec: Codec[ConcreteGroupMonsterStaticInformations] =  
    new Codec[ConcreteGroupMonsterStaticInformations] {
      def decode: Get[ConcreteGroupMonsterStaticInformations] =
        for {
          mainCreatureLightInfos <- Codec[ConcreteMonsterInGroupLightInformations].decode
          underlings <- list(ushort, Codec[MonsterInGroupInformations]).decode
        } yield ConcreteGroupMonsterStaticInformations(mainCreatureLightInfos, underlings)

      def encode(value: ConcreteGroupMonsterStaticInformations): ByteVector =
        Codec[ConcreteMonsterInGroupLightInformations].encode(value.mainCreatureLightInfos) ++
        list(ushort, Codec[MonsterInGroupInformations]).encode(value.underlings)
    }
}

object GroupMonsterStaticInformations {
  implicit val codec: Codec[GroupMonsterStaticInformations] =
    new Codec[GroupMonsterStaticInformations] {
      def decode: Get[GroupMonsterStaticInformations] =
        ushort.decode.flatMap {
          case 140 => Codec[ConcreteGroupMonsterStaticInformations].decode
          case 396 => Codec[GroupMonsterStaticInformationsWithAlternatives].decode
        }

      def encode(value: GroupMonsterStaticInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGroupMonsterStaticInformations => Codec[ConcreteGroupMonsterStaticInformations].encode(i)
          case i: GroupMonsterStaticInformationsWithAlternatives => Codec[GroupMonsterStaticInformationsWithAlternatives].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
