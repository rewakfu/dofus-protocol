package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ArenaFighterLeaveMessage(
  leaver: ConcreteCharacterBasicMinimalInformations
) extends Message(6700)

object ArenaFighterLeaveMessage {
  implicit val codec: Codec[ArenaFighterLeaveMessage] =
    new Codec[ArenaFighterLeaveMessage] {
      def decode: Get[ArenaFighterLeaveMessage] =
        for {
          leaver <- Codec[ConcreteCharacterBasicMinimalInformations].decode
        } yield ArenaFighterLeaveMessage(leaver)

      def encode(value: ArenaFighterLeaveMessage): ByteVector =
        Codec[ConcreteCharacterBasicMinimalInformations].encode(value.leaver)
    }
}
