package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightSwapRequestMessage(
  subAreaId: Short,
  targetId: Long
) extends Message(5901)

object PrismFightSwapRequestMessage {
  implicit val codec: Codec[PrismFightSwapRequestMessage] =
    new Codec[PrismFightSwapRequestMessage] {
      def decode: Get[PrismFightSwapRequestMessage] =
        for {
          subAreaId <- varShort.decode
          targetId <- varLong.decode
        } yield PrismFightSwapRequestMessage(subAreaId, targetId)

      def encode(value: PrismFightSwapRequestMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        varLong.encode(value.targetId)
    }
}
