package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntMessage(
  questType: Byte,
  startMapId: Double,
  knownStepsList: List[TreasureHuntStep],
  totalStepCount: Byte,
  checkPointCurrent: Int,
  checkPointTotal: Int,
  availableRetryCount: Int,
  flags: List[TreasureHuntFlag]
) extends Message(6486)

object TreasureHuntMessage {
  implicit val codec: Codec[TreasureHuntMessage] =
    new Codec[TreasureHuntMessage] {
      def decode: Get[TreasureHuntMessage] =
        for {
          questType <- byte.decode
          startMapId <- double.decode
          knownStepsList <- list(ushort, Codec[TreasureHuntStep]).decode
          totalStepCount <- byte.decode
          checkPointCurrent <- varInt.decode
          checkPointTotal <- varInt.decode
          availableRetryCount <- int.decode
          flags <- list(ushort, Codec[TreasureHuntFlag]).decode
        } yield TreasureHuntMessage(questType, startMapId, knownStepsList, totalStepCount, checkPointCurrent, checkPointTotal, availableRetryCount, flags)

      def encode(value: TreasureHuntMessage): ByteVector =
        byte.encode(value.questType) ++
        double.encode(value.startMapId) ++
        list(ushort, Codec[TreasureHuntStep]).encode(value.knownStepsList) ++
        byte.encode(value.totalStepCount) ++
        varInt.encode(value.checkPointCurrent) ++
        varInt.encode(value.checkPointTotal) ++
        int.encode(value.availableRetryCount) ++
        list(ushort, Codec[TreasureHuntFlag]).encode(value.flags)
    }
}
