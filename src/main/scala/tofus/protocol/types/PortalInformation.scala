package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PortalInformation extends ProtocolType

final case class ConcretePortalInformation(
  portalId: Int,
  areaId: Short
) extends PortalInformation {
  override val protocolId = 466
}

object ConcretePortalInformation {
  implicit val codec: Codec[ConcretePortalInformation] =  
    new Codec[ConcretePortalInformation] {
      def decode: Get[ConcretePortalInformation] =
        for {
          portalId <- int.decode
          areaId <- short.decode
        } yield ConcretePortalInformation(portalId, areaId)

      def encode(value: ConcretePortalInformation): ByteVector =
        int.encode(value.portalId) ++
        short.encode(value.areaId)
    }
}

object PortalInformation {
  implicit val codec: Codec[PortalInformation] =
    new Codec[PortalInformation] {
      def decode: Get[PortalInformation] =
        ushort.decode.flatMap {
          case 466 => Codec[ConcretePortalInformation].decode

        }

      def encode(value: PortalInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePortalInformation => Codec[ConcretePortalInformation].encode(i)

        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
