package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceModificationStartedMessage(
  flags0: Byte
) extends Message(6444)

object AllianceModificationStartedMessage {
  implicit val codec: Codec[AllianceModificationStartedMessage] =
    new Codec[AllianceModificationStartedMessage] {
      def decode: Get[AllianceModificationStartedMessage] =
        for {
          flags0 <- byte.decode
        } yield AllianceModificationStartedMessage(flags0)

      def encode(value: AllianceModificationStartedMessage): ByteVector =
        byte.encode(value.flags0)
    }
}
