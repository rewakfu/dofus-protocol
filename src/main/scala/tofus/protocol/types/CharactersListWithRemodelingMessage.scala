package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharactersListWithRemodelingMessage(
  characters: List[CharacterBaseInformations],
  hasStartupActions: Boolean,
  charactersToRemodel: List[CharacterToRemodelInformations]
) extends Message(6550)

object CharactersListWithRemodelingMessage {
  implicit val codec: Codec[CharactersListWithRemodelingMessage] =
    new Codec[CharactersListWithRemodelingMessage] {
      def decode: Get[CharactersListWithRemodelingMessage] =
        for {
          characters <- list(ushort, Codec[CharacterBaseInformations]).decode
          hasStartupActions <- bool.decode
          charactersToRemodel <- list(ushort, Codec[CharacterToRemodelInformations]).decode
        } yield CharactersListWithRemodelingMessage(characters, hasStartupActions, charactersToRemodel)

      def encode(value: CharactersListWithRemodelingMessage): ByteVector =
        list(ushort, Codec[CharacterBaseInformations]).encode(value.characters) ++
        bool.encode(value.hasStartupActions) ++
        list(ushort, Codec[CharacterToRemodelInformations]).encode(value.charactersToRemodel)
    }
}
