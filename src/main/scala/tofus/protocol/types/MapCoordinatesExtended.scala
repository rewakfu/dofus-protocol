package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapCoordinatesExtended(
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short
) extends MapCoordinatesAndId {
  override val protocolId = 176
}

object MapCoordinatesExtended {
  implicit val codec: Codec[MapCoordinatesExtended] =
    new Codec[MapCoordinatesExtended] {
      def decode: Get[MapCoordinatesExtended] =
        for {
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
        } yield MapCoordinatesExtended(worldX, worldY, mapId, subAreaId)

      def encode(value: MapCoordinatesExtended): ByteVector =
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId)
    }
}
