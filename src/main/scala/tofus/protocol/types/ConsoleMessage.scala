package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ConsoleMessage(
  `type`: Byte,
  content: String
) extends Message(75)

object ConsoleMessage {
  implicit val codec: Codec[ConsoleMessage] =
    new Codec[ConsoleMessage] {
      def decode: Get[ConsoleMessage] =
        for {
          `type` <- byte.decode
          content <- utf8(ushort).decode
        } yield ConsoleMessage(`type`, content)

      def encode(value: ConsoleMessage): ByteVector =
        byte.encode(value.`type`) ++
        utf8(ushort).encode(value.content)
    }
}
