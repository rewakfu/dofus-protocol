package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismAttackRequestMessage(

) extends Message(6042)

object PrismAttackRequestMessage {
  implicit val codec: Codec[PrismAttackRequestMessage] =
    Codec.const(PrismAttackRequestMessage())
}
