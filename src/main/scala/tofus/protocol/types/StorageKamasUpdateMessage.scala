package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StorageKamasUpdateMessage(
  kamasTotal: Long
) extends Message(5645)

object StorageKamasUpdateMessage {
  implicit val codec: Codec[StorageKamasUpdateMessage] =
    new Codec[StorageKamasUpdateMessage] {
      def decode: Get[StorageKamasUpdateMessage] =
        for {
          kamasTotal <- varLong.decode
        } yield StorageKamasUpdateMessage(kamasTotal)

      def encode(value: StorageKamasUpdateMessage): ByteVector =
        varLong.encode(value.kamasTotal)
    }
}
