package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementRewardRequestMessage(
  achievementId: Short
) extends Message(6377)

object AchievementRewardRequestMessage {
  implicit val codec: Codec[AchievementRewardRequestMessage] =
    new Codec[AchievementRewardRequestMessage] {
      def decode: Get[AchievementRewardRequestMessage] =
        for {
          achievementId <- short.decode
        } yield AchievementRewardRequestMessage(achievementId)

      def encode(value: AchievementRewardRequestMessage): ByteVector =
        short.encode(value.achievementId)
    }
}
