package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ZaapRespawnSaveRequestMessage(

) extends Message(6572)

object ZaapRespawnSaveRequestMessage {
  implicit val codec: Codec[ZaapRespawnSaveRequestMessage] =
    Codec.const(ZaapRespawnSaveRequestMessage())
}
