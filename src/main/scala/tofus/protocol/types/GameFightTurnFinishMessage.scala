package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnFinishMessage(
  isAfk: Boolean
) extends Message(718)

object GameFightTurnFinishMessage {
  implicit val codec: Codec[GameFightTurnFinishMessage] =
    new Codec[GameFightTurnFinishMessage] {
      def decode: Get[GameFightTurnFinishMessage] =
        for {
          isAfk <- bool.decode
        } yield GameFightTurnFinishMessage(isAfk)

      def encode(value: GameFightTurnFinishMessage): ByteVector =
        bool.encode(value.isAfk)
    }
}
