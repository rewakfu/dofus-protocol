package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnListMessage(
  ids: List[Double],
  deadsIds: List[Double]
) extends Message(713)

object GameFightTurnListMessage {
  implicit val codec: Codec[GameFightTurnListMessage] =
    new Codec[GameFightTurnListMessage] {
      def decode: Get[GameFightTurnListMessage] =
        for {
          ids <- list(ushort, double).decode
          deadsIds <- list(ushort, double).decode
        } yield GameFightTurnListMessage(ids, deadsIds)

      def encode(value: GameFightTurnListMessage): ByteVector =
        list(ushort, double).encode(value.ids) ++
        list(ushort, double).encode(value.deadsIds)
    }
}
