package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait CharacterMinimalGuildInformations extends CharacterMinimalPlusLookInformations

final case class ConcreteCharacterMinimalGuildInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte,
  guild: ConcreteBasicGuildInformations
) extends CharacterMinimalGuildInformations {
  override val protocolId = 445
}

object ConcreteCharacterMinimalGuildInformations {
  implicit val codec: Codec[ConcreteCharacterMinimalGuildInformations] =  
    new Codec[ConcreteCharacterMinimalGuildInformations] {
      def decode: Get[ConcreteCharacterMinimalGuildInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
          guild <- Codec[ConcreteBasicGuildInformations].decode
        } yield ConcreteCharacterMinimalGuildInformations(id, name, level, entityLook, breed, guild)

      def encode(value: ConcreteCharacterMinimalGuildInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed) ++
        Codec[ConcreteBasicGuildInformations].encode(value.guild)
    }
}

object CharacterMinimalGuildInformations {
  implicit val codec: Codec[CharacterMinimalGuildInformations] =
    new Codec[CharacterMinimalGuildInformations] {
      def decode: Get[CharacterMinimalGuildInformations] =
        ushort.decode.flatMap {
          case 445 => Codec[ConcreteCharacterMinimalGuildInformations].decode
          case 444 => Codec[CharacterMinimalAllianceInformations].decode
        }

      def encode(value: CharacterMinimalGuildInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteCharacterMinimalGuildInformations => Codec[ConcreteCharacterMinimalGuildInformations].encode(i)
          case i: CharacterMinimalAllianceInformations => Codec[CharacterMinimalAllianceInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
