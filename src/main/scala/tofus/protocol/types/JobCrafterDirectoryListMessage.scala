package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryListMessage(
  listEntries: List[JobCrafterDirectoryListEntry]
) extends Message(6046)

object JobCrafterDirectoryListMessage {
  implicit val codec: Codec[JobCrafterDirectoryListMessage] =
    new Codec[JobCrafterDirectoryListMessage] {
      def decode: Get[JobCrafterDirectoryListMessage] =
        for {
          listEntries <- list(ushort, Codec[JobCrafterDirectoryListEntry]).decode
        } yield JobCrafterDirectoryListMessage(listEntries)

      def encode(value: JobCrafterDirectoryListMessage): ByteVector =
        list(ushort, Codec[JobCrafterDirectoryListEntry]).encode(value.listEntries)
    }
}
