package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameFightFighterNamedInformations extends GameFightFighterInformations

final case class ConcreteGameFightFighterNamedInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short],
  name: String,
  status: ConcretePlayerStatus,
  leagueId: Short,
  ladderPosition: Int,
  hiddenInPrefight: Boolean
) extends GameFightFighterNamedInformations {
  override val protocolId = 158
}

object ConcreteGameFightFighterNamedInformations {
  implicit val codec: Codec[ConcreteGameFightFighterNamedInformations] =  
    new Codec[ConcreteGameFightFighterNamedInformations] {
      def decode: Get[ConcreteGameFightFighterNamedInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
          name <- utf8(ushort).decode
          status <- Codec[ConcretePlayerStatus].decode
          leagueId <- varShort.decode
          ladderPosition <- int.decode
          hiddenInPrefight <- bool.decode
        } yield ConcreteGameFightFighterNamedInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions, name, status, leagueId, ladderPosition, hiddenInPrefight)

      def encode(value: ConcreteGameFightFighterNamedInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions) ++
        utf8(ushort).encode(value.name) ++
        Codec[ConcretePlayerStatus].encode(value.status) ++
        varShort.encode(value.leagueId) ++
        int.encode(value.ladderPosition) ++
        bool.encode(value.hiddenInPrefight)
    }
}

object GameFightFighterNamedInformations {
  implicit val codec: Codec[GameFightFighterNamedInformations] =
    new Codec[GameFightFighterNamedInformations] {
      def decode: Get[GameFightFighterNamedInformations] =
        ushort.decode.flatMap {
          case 158 => Codec[ConcreteGameFightFighterNamedInformations].decode
          case 46 => Codec[GameFightCharacterInformations].decode
          case 50 => Codec[GameFightMutantInformations].decode
        }

      def encode(value: GameFightFighterNamedInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameFightFighterNamedInformations => Codec[ConcreteGameFightFighterNamedInformations].encode(i)
          case i: GameFightCharacterInformations => Codec[GameFightCharacterInformations].encode(i)
          case i: GameFightMutantInformations => Codec[GameFightMutantInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
