package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseUnsoldItemsMessage(
  items: List[ConcreteObjectItemGenericQuantity]
) extends Message(6612)

object ExchangeBidHouseUnsoldItemsMessage {
  implicit val codec: Codec[ExchangeBidHouseUnsoldItemsMessage] =
    new Codec[ExchangeBidHouseUnsoldItemsMessage] {
      def decode: Get[ExchangeBidHouseUnsoldItemsMessage] =
        for {
          items <- list(ushort, Codec[ConcreteObjectItemGenericQuantity]).decode
        } yield ExchangeBidHouseUnsoldItemsMessage(items)

      def encode(value: ExchangeBidHouseUnsoldItemsMessage): ByteVector =
        list(ushort, Codec[ConcreteObjectItemGenericQuantity]).encode(value.items)
    }
}
