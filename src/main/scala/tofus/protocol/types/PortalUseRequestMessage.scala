package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PortalUseRequestMessage(
  portalId: Int
) extends Message(6492)

object PortalUseRequestMessage {
  implicit val codec: Codec[PortalUseRequestMessage] =
    new Codec[PortalUseRequestMessage] {
      def decode: Get[PortalUseRequestMessage] =
        for {
          portalId <- varInt.decode
        } yield PortalUseRequestMessage(portalId)

      def encode(value: PortalUseRequestMessage): ByteVector =
        varInt.encode(value.portalId)
    }
}
