package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnResumeMessage(
  id: Double,
  waitTime: Int,
  remainingTime: Int
) extends Message(6307)

object GameFightTurnResumeMessage {
  implicit val codec: Codec[GameFightTurnResumeMessage] =
    new Codec[GameFightTurnResumeMessage] {
      def decode: Get[GameFightTurnResumeMessage] =
        for {
          id <- double.decode
          waitTime <- varInt.decode
          remainingTime <- varInt.decode
        } yield GameFightTurnResumeMessage(id, waitTime, remainingTime)

      def encode(value: GameFightTurnResumeMessage): ByteVector =
        double.encode(value.id) ++
        varInt.encode(value.waitTime) ++
        varInt.encode(value.remainingTime)
    }
}
