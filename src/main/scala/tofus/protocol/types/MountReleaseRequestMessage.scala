package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountReleaseRequestMessage(

) extends Message(5980)

object MountReleaseRequestMessage {
  implicit val codec: Codec[MountReleaseRequestMessage] =
    Codec.const(MountReleaseRequestMessage())
}
