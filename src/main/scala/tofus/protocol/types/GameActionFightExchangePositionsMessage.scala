package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightExchangePositionsMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  casterCellId: Short,
  targetCellId: Short
) extends Message(5527)

object GameActionFightExchangePositionsMessage {
  implicit val codec: Codec[GameActionFightExchangePositionsMessage] =
    new Codec[GameActionFightExchangePositionsMessage] {
      def decode: Get[GameActionFightExchangePositionsMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          casterCellId <- short.decode
          targetCellId <- short.decode
        } yield GameActionFightExchangePositionsMessage(actionId, sourceId, targetId, casterCellId, targetCellId)

      def encode(value: GameActionFightExchangePositionsMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.casterCellId) ++
        short.encode(value.targetCellId)
    }
}
