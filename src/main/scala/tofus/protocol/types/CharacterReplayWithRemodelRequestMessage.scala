package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterReplayWithRemodelRequestMessage(
  characterId: Long,
  remodel: RemodelingInformation
) extends Message(6551)

object CharacterReplayWithRemodelRequestMessage {
  implicit val codec: Codec[CharacterReplayWithRemodelRequestMessage] =
    new Codec[CharacterReplayWithRemodelRequestMessage] {
      def decode: Get[CharacterReplayWithRemodelRequestMessage] =
        for {
          characterId <- varLong.decode
          remodel <- Codec[RemodelingInformation].decode
        } yield CharacterReplayWithRemodelRequestMessage(characterId, remodel)

      def encode(value: CharacterReplayWithRemodelRequestMessage): ByteVector =
        varLong.encode(value.characterId) ++
        Codec[RemodelingInformation].encode(value.remodel)
    }
}
