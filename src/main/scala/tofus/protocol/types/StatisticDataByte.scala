package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatisticDataByte(
  value: Byte
) extends StatisticData {
  override val protocolId = 486
}

object StatisticDataByte {
  implicit val codec: Codec[StatisticDataByte] =
    new Codec[StatisticDataByte] {
      def decode: Get[StatisticDataByte] =
        for {
          value <- byte.decode
        } yield StatisticDataByte(value)

      def encode(value: StatisticDataByte): ByteVector =
        byte.encode(value.value)
    }
}
