package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StartupActionsExecuteMessage(

) extends Message(1302)

object StartupActionsExecuteMessage {
  implicit val codec: Codec[StartupActionsExecuteMessage] =
    Codec.const(StartupActionsExecuteMessage())
}
