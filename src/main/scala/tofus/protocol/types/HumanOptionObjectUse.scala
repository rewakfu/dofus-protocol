package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionObjectUse(
  delayTypeId: Byte,
  delayEndTime: Double,
  objectGID: Short
) extends HumanOption {
  override val protocolId = 449
}

object HumanOptionObjectUse {
  implicit val codec: Codec[HumanOptionObjectUse] =
    new Codec[HumanOptionObjectUse] {
      def decode: Get[HumanOptionObjectUse] =
        for {
          delayTypeId <- byte.decode
          delayEndTime <- double.decode
          objectGID <- varShort.decode
        } yield HumanOptionObjectUse(delayTypeId, delayEndTime, objectGID)

      def encode(value: HumanOptionObjectUse): ByteVector =
        byte.encode(value.delayTypeId) ++
        double.encode(value.delayEndTime) ++
        varShort.encode(value.objectGID)
    }
}
