package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangePlayerMultiCraftRequestMessage(
  exchangeType: Byte,
  target: Long,
  skillId: Int
) extends Message(5784)

object ExchangePlayerMultiCraftRequestMessage {
  implicit val codec: Codec[ExchangePlayerMultiCraftRequestMessage] =
    new Codec[ExchangePlayerMultiCraftRequestMessage] {
      def decode: Get[ExchangePlayerMultiCraftRequestMessage] =
        for {
          exchangeType <- byte.decode
          target <- varLong.decode
          skillId <- varInt.decode
        } yield ExchangePlayerMultiCraftRequestMessage(exchangeType, target, skillId)

      def encode(value: ExchangePlayerMultiCraftRequestMessage): ByteVector =
        byte.encode(value.exchangeType) ++
        varLong.encode(value.target) ++
        varInt.encode(value.skillId)
    }
}
