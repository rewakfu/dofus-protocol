package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ObjectItemInRolePlay extends ProtocolType

final case class ConcreteObjectItemInRolePlay(
  cellId: Short,
  objectGID: Short
) extends ObjectItemInRolePlay {
  override val protocolId = 198
}

object ConcreteObjectItemInRolePlay {
  implicit val codec: Codec[ConcreteObjectItemInRolePlay] =  
    new Codec[ConcreteObjectItemInRolePlay] {
      def decode: Get[ConcreteObjectItemInRolePlay] =
        for {
          cellId <- varShort.decode
          objectGID <- varShort.decode
        } yield ConcreteObjectItemInRolePlay(cellId, objectGID)

      def encode(value: ConcreteObjectItemInRolePlay): ByteVector =
        varShort.encode(value.cellId) ++
        varShort.encode(value.objectGID)
    }
}

object ObjectItemInRolePlay {
  implicit val codec: Codec[ObjectItemInRolePlay] =
    new Codec[ObjectItemInRolePlay] {
      def decode: Get[ObjectItemInRolePlay] =
        ushort.decode.flatMap {
          case 198 => Codec[ConcreteObjectItemInRolePlay].decode
          case 185 => Codec[PaddockItem].decode
        }

      def encode(value: ObjectItemInRolePlay): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteObjectItemInRolePlay => Codec[ConcreteObjectItemInRolePlay].encode(i)
          case i: PaddockItem => Codec[PaddockItem].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
