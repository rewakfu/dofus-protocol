package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayNpcWithQuestInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  npcId: Short,
  sex: Boolean,
  specialArtworkId: Short,
  questFlag: GameRolePlayNpcQuestFlag
) extends GameRolePlayNpcInformations {
  override val protocolId = 383
}

object GameRolePlayNpcWithQuestInformations {
  implicit val codec: Codec[GameRolePlayNpcWithQuestInformations] =
    new Codec[GameRolePlayNpcWithQuestInformations] {
      def decode: Get[GameRolePlayNpcWithQuestInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          npcId <- varShort.decode
          sex <- bool.decode
          specialArtworkId <- varShort.decode
          questFlag <- Codec[GameRolePlayNpcQuestFlag].decode
        } yield GameRolePlayNpcWithQuestInformations(contextualId, disposition, look, npcId, sex, specialArtworkId, questFlag)

      def encode(value: GameRolePlayNpcWithQuestInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        varShort.encode(value.npcId) ++
        bool.encode(value.sex) ++
        varShort.encode(value.specialArtworkId) ++
        Codec[GameRolePlayNpcQuestFlag].encode(value.questFlag)
    }
}
