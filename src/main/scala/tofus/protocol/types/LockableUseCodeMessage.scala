package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LockableUseCodeMessage(
  code: String
) extends Message(5667)

object LockableUseCodeMessage {
  implicit val codec: Codec[LockableUseCodeMessage] =
    new Codec[LockableUseCodeMessage] {
      def decode: Get[LockableUseCodeMessage] =
        for {
          code <- utf8(ushort).decode
        } yield LockableUseCodeMessage(code)

      def encode(value: LockableUseCodeMessage): ByteVector =
        utf8(ushort).encode(value.code)
    }
}
