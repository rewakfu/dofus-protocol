package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EditHavenBagFinishedMessage(

) extends Message(6628)

object EditHavenBagFinishedMessage {
  implicit val codec: Codec[EditHavenBagFinishedMessage] =
    Codec.const(EditHavenBagFinishedMessage())
}
