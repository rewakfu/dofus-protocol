package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatClientPrivateWithObjectMessage(
  content: String,
  receiver: String,
  objects: List[ObjectItem]
) extends Message(852)

object ChatClientPrivateWithObjectMessage {
  implicit val codec: Codec[ChatClientPrivateWithObjectMessage] =
    new Codec[ChatClientPrivateWithObjectMessage] {
      def decode: Get[ChatClientPrivateWithObjectMessage] =
        for {
          content <- utf8(ushort).decode
          receiver <- utf8(ushort).decode
          objects <- list(ushort, Codec[ObjectItem]).decode
        } yield ChatClientPrivateWithObjectMessage(content, receiver, objects)

      def encode(value: ChatClientPrivateWithObjectMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        utf8(ushort).encode(value.receiver) ++
        list(ushort, Codec[ObjectItem]).encode(value.objects)
    }
}
