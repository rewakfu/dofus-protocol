package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntGiveUpRequestMessage(
  questType: Byte
) extends Message(6487)

object TreasureHuntGiveUpRequestMessage {
  implicit val codec: Codec[TreasureHuntGiveUpRequestMessage] =
    new Codec[TreasureHuntGiveUpRequestMessage] {
      def decode: Get[TreasureHuntGiveUpRequestMessage] =
        for {
          questType <- byte.decode
        } yield TreasureHuntGiveUpRequestMessage(questType)

      def encode(value: TreasureHuntGiveUpRequestMessage): ByteVector =
        byte.encode(value.questType)
    }
}
