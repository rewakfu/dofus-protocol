package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyIdol(
  id: Short,
  xpBonusPercent: Short,
  dropBonusPercent: Short,
  ownersIds: List[Long]
) extends Idol {
  override val protocolId = 490
}

object PartyIdol {
  implicit val codec: Codec[PartyIdol] =
    new Codec[PartyIdol] {
      def decode: Get[PartyIdol] =
        for {
          id <- varShort.decode
          xpBonusPercent <- varShort.decode
          dropBonusPercent <- varShort.decode
          ownersIds <- list(ushort, varLong).decode
        } yield PartyIdol(id, xpBonusPercent, dropBonusPercent, ownersIds)

      def encode(value: PartyIdol): ByteVector =
        varShort.encode(value.id) ++
        varShort.encode(value.xpBonusPercent) ++
        varShort.encode(value.dropBonusPercent) ++
        list(ushort, varLong).encode(value.ownersIds)
    }
}
