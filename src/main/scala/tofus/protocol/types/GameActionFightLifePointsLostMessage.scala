package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightLifePointsLostMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  loss: Int,
  permanentDamages: Int,
  elementId: Int
) extends Message(6312)

object GameActionFightLifePointsLostMessage {
  implicit val codec: Codec[GameActionFightLifePointsLostMessage] =
    new Codec[GameActionFightLifePointsLostMessage] {
      def decode: Get[GameActionFightLifePointsLostMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          loss <- varInt.decode
          permanentDamages <- varInt.decode
          elementId <- varInt.decode
        } yield GameActionFightLifePointsLostMessage(actionId, sourceId, targetId, loss, permanentDamages, elementId)

      def encode(value: GameActionFightLifePointsLostMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varInt.encode(value.loss) ++
        varInt.encode(value.permanentDamages) ++
        varInt.encode(value.elementId)
    }
}
