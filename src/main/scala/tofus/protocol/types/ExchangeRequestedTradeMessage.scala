package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeRequestedTradeMessage(
  exchangeType: Byte,
  source: Long,
  target: Long
) extends Message(5523)

object ExchangeRequestedTradeMessage {
  implicit val codec: Codec[ExchangeRequestedTradeMessage] =
    new Codec[ExchangeRequestedTradeMessage] {
      def decode: Get[ExchangeRequestedTradeMessage] =
        for {
          exchangeType <- byte.decode
          source <- varLong.decode
          target <- varLong.decode
        } yield ExchangeRequestedTradeMessage(exchangeType, source, target)

      def encode(value: ExchangeRequestedTradeMessage): ByteVector =
        byte.encode(value.exchangeType) ++
        varLong.encode(value.source) ++
        varLong.encode(value.target)
    }
}
