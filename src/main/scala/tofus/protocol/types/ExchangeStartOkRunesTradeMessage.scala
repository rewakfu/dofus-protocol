package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkRunesTradeMessage(

) extends Message(6567)

object ExchangeStartOkRunesTradeMessage {
  implicit val codec: Codec[ExchangeStartOkRunesTradeMessage] =
    Codec.const(ExchangeStartOkRunesTradeMessage())
}
