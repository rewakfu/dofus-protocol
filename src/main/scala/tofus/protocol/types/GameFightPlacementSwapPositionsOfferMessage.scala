package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementSwapPositionsOfferMessage(
  requestId: Int,
  requesterId: Double,
  requesterCellId: Short,
  requestedId: Double,
  requestedCellId: Short
) extends Message(6542)

object GameFightPlacementSwapPositionsOfferMessage {
  implicit val codec: Codec[GameFightPlacementSwapPositionsOfferMessage] =
    new Codec[GameFightPlacementSwapPositionsOfferMessage] {
      def decode: Get[GameFightPlacementSwapPositionsOfferMessage] =
        for {
          requestId <- int.decode
          requesterId <- double.decode
          requesterCellId <- varShort.decode
          requestedId <- double.decode
          requestedCellId <- varShort.decode
        } yield GameFightPlacementSwapPositionsOfferMessage(requestId, requesterId, requesterCellId, requestedId, requestedCellId)

      def encode(value: GameFightPlacementSwapPositionsOfferMessage): ByteVector =
        int.encode(value.requestId) ++
        double.encode(value.requesterId) ++
        varShort.encode(value.requesterCellId) ++
        double.encode(value.requestedId) ++
        varShort.encode(value.requestedCellId)
    }
}
