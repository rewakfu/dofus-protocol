package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightDispellableEffectMessage(
  actionId: Short,
  sourceId: Double,
  effect: AbstractFightDispellableEffect
) extends Message(6070)

object GameActionFightDispellableEffectMessage {
  implicit val codec: Codec[GameActionFightDispellableEffectMessage] =
    new Codec[GameActionFightDispellableEffectMessage] {
      def decode: Get[GameActionFightDispellableEffectMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          effect <- Codec[AbstractFightDispellableEffect].decode
        } yield GameActionFightDispellableEffectMessage(actionId, sourceId, effect)

      def encode(value: GameActionFightDispellableEffectMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        Codec[AbstractFightDispellableEffect].encode(value.effect)
    }
}
