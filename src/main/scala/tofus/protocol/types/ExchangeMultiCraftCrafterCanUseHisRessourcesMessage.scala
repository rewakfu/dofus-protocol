package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMultiCraftCrafterCanUseHisRessourcesMessage(
  allowed: Boolean
) extends Message(6020)

object ExchangeMultiCraftCrafterCanUseHisRessourcesMessage {
  implicit val codec: Codec[ExchangeMultiCraftCrafterCanUseHisRessourcesMessage] =
    new Codec[ExchangeMultiCraftCrafterCanUseHisRessourcesMessage] {
      def decode: Get[ExchangeMultiCraftCrafterCanUseHisRessourcesMessage] =
        for {
          allowed <- bool.decode
        } yield ExchangeMultiCraftCrafterCanUseHisRessourcesMessage(allowed)

      def encode(value: ExchangeMultiCraftCrafterCanUseHisRessourcesMessage): ByteVector =
        bool.encode(value.allowed)
    }
}
