package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeOkMultiCraftMessage(
  initiatorId: Long,
  otherId: Long,
  role: Byte
) extends Message(5768)

object ExchangeOkMultiCraftMessage {
  implicit val codec: Codec[ExchangeOkMultiCraftMessage] =
    new Codec[ExchangeOkMultiCraftMessage] {
      def decode: Get[ExchangeOkMultiCraftMessage] =
        for {
          initiatorId <- varLong.decode
          otherId <- varLong.decode
          role <- byte.decode
        } yield ExchangeOkMultiCraftMessage(initiatorId, otherId, role)

      def encode(value: ExchangeOkMultiCraftMessage): ByteVector =
        varLong.encode(value.initiatorId) ++
        varLong.encode(value.otherId) ++
        byte.encode(value.role)
    }
}
