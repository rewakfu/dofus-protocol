package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeGuildTaxCollectorGetMessage(
  collectorName: String,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short,
  userName: String,
  callerId: Long,
  callerName: String,
  experience: Double,
  pods: Short,
  objectsInfos: List[ConcreteObjectItemGenericQuantity]
) extends Message(5762)

object ExchangeGuildTaxCollectorGetMessage {
  implicit val codec: Codec[ExchangeGuildTaxCollectorGetMessage] =
    new Codec[ExchangeGuildTaxCollectorGetMessage] {
      def decode: Get[ExchangeGuildTaxCollectorGetMessage] =
        for {
          collectorName <- utf8(ushort).decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          userName <- utf8(ushort).decode
          callerId <- varLong.decode
          callerName <- utf8(ushort).decode
          experience <- double.decode
          pods <- varShort.decode
          objectsInfos <- list(ushort, Codec[ConcreteObjectItemGenericQuantity]).decode
        } yield ExchangeGuildTaxCollectorGetMessage(collectorName, worldX, worldY, mapId, subAreaId, userName, callerId, callerName, experience, pods, objectsInfos)

      def encode(value: ExchangeGuildTaxCollectorGetMessage): ByteVector =
        utf8(ushort).encode(value.collectorName) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        utf8(ushort).encode(value.userName) ++
        varLong.encode(value.callerId) ++
        utf8(ushort).encode(value.callerName) ++
        double.encode(value.experience) ++
        varShort.encode(value.pods) ++
        list(ushort, Codec[ConcreteObjectItemGenericQuantity]).encode(value.objectsInfos)
    }
}
