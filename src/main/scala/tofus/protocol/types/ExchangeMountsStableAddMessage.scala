package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountsStableAddMessage(
  mountDescription: List[MountClientData]
) extends Message(6555)

object ExchangeMountsStableAddMessage {
  implicit val codec: Codec[ExchangeMountsStableAddMessage] =
    new Codec[ExchangeMountsStableAddMessage] {
      def decode: Get[ExchangeMountsStableAddMessage] =
        for {
          mountDescription <- list(ushort, Codec[MountClientData]).decode
        } yield ExchangeMountsStableAddMessage(mountDescription)

      def encode(value: ExchangeMountsStableAddMessage): ByteVector =
        list(ushort, Codec[MountClientData]).encode(value.mountDescription)
    }
}
