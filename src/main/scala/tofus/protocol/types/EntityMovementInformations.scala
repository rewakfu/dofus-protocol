package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EntityMovementInformations(
  id: Int,
  steps: ByteVector
) extends ProtocolType {
  override val protocolId = 63
}

object EntityMovementInformations {
  implicit val codec: Codec[EntityMovementInformations] =
    new Codec[EntityMovementInformations] {
      def decode: Get[EntityMovementInformations] =
        for {
          id <- int.decode
          steps <- bytes(ushort).decode
        } yield EntityMovementInformations(id, steps)

      def encode(value: EntityMovementInformations): ByteVector =
        int.encode(value.id) ++
        bytes(ushort).encode(value.steps)
    }
}
