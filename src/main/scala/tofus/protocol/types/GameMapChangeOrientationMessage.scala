package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapChangeOrientationMessage(
  orientation: ActorOrientation
) extends Message(946)

object GameMapChangeOrientationMessage {
  implicit val codec: Codec[GameMapChangeOrientationMessage] =
    new Codec[GameMapChangeOrientationMessage] {
      def decode: Get[GameMapChangeOrientationMessage] =
        for {
          orientation <- Codec[ActorOrientation].decode
        } yield GameMapChangeOrientationMessage(orientation)

      def encode(value: GameMapChangeOrientationMessage): ByteVector =
        Codec[ActorOrientation].encode(value.orientation)
    }
}
