package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapMovementRequestMessage(
  keyMovements: List[Short],
  mapId: Double
) extends Message(950)

object GameMapMovementRequestMessage {
  implicit val codec: Codec[GameMapMovementRequestMessage] =
    new Codec[GameMapMovementRequestMessage] {
      def decode: Get[GameMapMovementRequestMessage] =
        for {
          keyMovements <- list(ushort, short).decode
          mapId <- double.decode
        } yield GameMapMovementRequestMessage(keyMovements, mapId)

      def encode(value: GameMapMovementRequestMessage): ByteVector =
        list(ushort, short).encode(value.keyMovements) ++
        double.encode(value.mapId)
    }
}
