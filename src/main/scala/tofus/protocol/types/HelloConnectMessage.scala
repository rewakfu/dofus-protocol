package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HelloConnectMessage(
  salt: String,
  key: ByteVector
) extends Message(3)

object HelloConnectMessage {
  implicit val codec: Codec[HelloConnectMessage] =
    new Codec[HelloConnectMessage] {
      def decode: Get[HelloConnectMessage] =
        for {
          salt <- utf8(ushort).decode
          key <- bytes(varInt).decode
        } yield HelloConnectMessage(salt, key)

      def encode(value: HelloConnectMessage): ByteVector =
        utf8(ushort).encode(value.salt) ++
        bytes(varInt).encode(value.key)
    }
}
