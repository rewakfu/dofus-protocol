package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyLeaveRequestMessage(
  partyId: Int
) extends Message(5593)

object PartyLeaveRequestMessage {
  implicit val codec: Codec[PartyLeaveRequestMessage] =
    new Codec[PartyLeaveRequestMessage] {
      def decode: Get[PartyLeaveRequestMessage] =
        for {
          partyId <- varInt.decode
        } yield PartyLeaveRequestMessage(partyId)

      def encode(value: PartyLeaveRequestMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
