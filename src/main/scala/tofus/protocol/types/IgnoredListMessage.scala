package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredListMessage(
  ignoredList: List[IgnoredInformations]
) extends Message(5674)

object IgnoredListMessage {
  implicit val codec: Codec[IgnoredListMessage] =
    new Codec[IgnoredListMessage] {
      def decode: Get[IgnoredListMessage] =
        for {
          ignoredList <- list(ushort, Codec[IgnoredInformations]).decode
        } yield IgnoredListMessage(ignoredList)

      def encode(value: IgnoredListMessage): ByteVector =
        list(ushort, Codec[IgnoredInformations]).encode(value.ignoredList)
    }
}
