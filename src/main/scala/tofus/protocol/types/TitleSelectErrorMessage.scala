package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TitleSelectErrorMessage(
  reason: Byte
) extends Message(6373)

object TitleSelectErrorMessage {
  implicit val codec: Codec[TitleSelectErrorMessage] =
    new Codec[TitleSelectErrorMessage] {
      def decode: Get[TitleSelectErrorMessage] =
        for {
          reason <- byte.decode
        } yield TitleSelectErrorMessage(reason)

      def encode(value: TitleSelectErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
