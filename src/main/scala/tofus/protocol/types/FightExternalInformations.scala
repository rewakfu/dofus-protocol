package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightExternalInformations(
  fightId: Short,
  fightType: Byte,
  fightStart: Int,
  fightSpectatorLocked: Boolean,
  fightTeams: List[FightTeamLightInformations],
  fightTeamsOptions: List[FightOptionsInformations]
) extends ProtocolType {
  override val protocolId = 117
}

object FightExternalInformations {
  implicit val codec: Codec[FightExternalInformations] =
    new Codec[FightExternalInformations] {
      def decode: Get[FightExternalInformations] =
        for {
          fightId <- varShort.decode
          fightType <- byte.decode
          fightStart <- int.decode
          fightSpectatorLocked <- bool.decode
          fightTeams <- listC(2, Codec[FightTeamLightInformations]).decode
          fightTeamsOptions <- listC(2, Codec[FightOptionsInformations]).decode
        } yield FightExternalInformations(fightId, fightType, fightStart, fightSpectatorLocked, fightTeams, fightTeamsOptions)

      def encode(value: FightExternalInformations): ByteVector =
        varShort.encode(value.fightId) ++
        byte.encode(value.fightType) ++
        int.encode(value.fightStart) ++
        bool.encode(value.fightSpectatorLocked) ++
        listC(2, Codec[FightTeamLightInformations]).encode(value.fightTeams) ++
        listC(2, Codec[FightOptionsInformations]).encode(value.fightTeamsOptions)
    }
}
