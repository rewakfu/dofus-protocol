package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeLeaveMessage(
  dialogType: Byte,
  success: Boolean
) extends Message(5628)

object ExchangeLeaveMessage {
  implicit val codec: Codec[ExchangeLeaveMessage] =
    new Codec[ExchangeLeaveMessage] {
      def decode: Get[ExchangeLeaveMessage] =
        for {
          dialogType <- byte.decode
          success <- bool.decode
        } yield ExchangeLeaveMessage(dialogType, success)

      def encode(value: ExchangeLeaveMessage): ByteVector =
        byte.encode(value.dialogType) ++
        bool.encode(value.success)
    }
}
