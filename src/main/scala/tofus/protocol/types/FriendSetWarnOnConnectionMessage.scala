package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendSetWarnOnConnectionMessage(
  enable: Boolean
) extends Message(5602)

object FriendSetWarnOnConnectionMessage {
  implicit val codec: Codec[FriendSetWarnOnConnectionMessage] =
    new Codec[FriendSetWarnOnConnectionMessage] {
      def decode: Get[FriendSetWarnOnConnectionMessage] =
        for {
          enable <- bool.decode
        } yield FriendSetWarnOnConnectionMessage(enable)

      def encode(value: FriendSetWarnOnConnectionMessage): ByteVector =
        bool.encode(value.enable)
    }
}
