package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeRequestOnTaxCollectorMessage(

) extends Message(5779)

object ExchangeRequestOnTaxCollectorMessage {
  implicit val codec: Codec[ExchangeRequestOnTaxCollectorMessage] =
    Codec.const(ExchangeRequestOnTaxCollectorMessage())
}
