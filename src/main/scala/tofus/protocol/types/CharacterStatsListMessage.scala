package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterStatsListMessage(
  stats: CharacterCharacteristicsInformations
) extends Message(500)

object CharacterStatsListMessage {
  implicit val codec: Codec[CharacterStatsListMessage] =
    new Codec[CharacterStatsListMessage] {
      def decode: Get[CharacterStatsListMessage] =
        for {
          stats <- Codec[CharacterCharacteristicsInformations].decode
        } yield CharacterStatsListMessage(stats)

      def encode(value: CharacterStatsListMessage): ByteVector =
        Codec[CharacterCharacteristicsInformations].encode(value.stats)
    }
}
