package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PaddockBuyableInformations extends ProtocolType

final case class ConcretePaddockBuyableInformations(
  price: Long,
  locked: Boolean
) extends PaddockBuyableInformations {
  override val protocolId = 130
}

object ConcretePaddockBuyableInformations {
  implicit val codec: Codec[ConcretePaddockBuyableInformations] =  
    new Codec[ConcretePaddockBuyableInformations] {
      def decode: Get[ConcretePaddockBuyableInformations] =
        for {
          price <- varLong.decode
          locked <- bool.decode
        } yield ConcretePaddockBuyableInformations(price, locked)

      def encode(value: ConcretePaddockBuyableInformations): ByteVector =
        varLong.encode(value.price) ++
        bool.encode(value.locked)
    }
}

object PaddockBuyableInformations {
  implicit val codec: Codec[PaddockBuyableInformations] =
    new Codec[PaddockBuyableInformations] {
      def decode: Get[PaddockBuyableInformations] =
        ushort.decode.flatMap {
          case 130 => Codec[ConcretePaddockBuyableInformations].decode
          case 508 => Codec[PaddockGuildedInformations].decode
        }

      def encode(value: PaddockBuyableInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePaddockBuyableInformations => Codec[ConcretePaddockBuyableInformations].encode(i)
          case i: PaddockGuildedInformations => Codec[PaddockGuildedInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
