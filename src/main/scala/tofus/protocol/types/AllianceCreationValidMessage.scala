package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceCreationValidMessage(
  allianceName: String,
  allianceTag: String,
  allianceEmblem: GuildEmblem
) extends Message(6393)

object AllianceCreationValidMessage {
  implicit val codec: Codec[AllianceCreationValidMessage] =
    new Codec[AllianceCreationValidMessage] {
      def decode: Get[AllianceCreationValidMessage] =
        for {
          allianceName <- utf8(ushort).decode
          allianceTag <- utf8(ushort).decode
          allianceEmblem <- Codec[GuildEmblem].decode
        } yield AllianceCreationValidMessage(allianceName, allianceTag, allianceEmblem)

      def encode(value: AllianceCreationValidMessage): ByteVector =
        utf8(ushort).encode(value.allianceName) ++
        utf8(ushort).encode(value.allianceTag) ++
        Codec[GuildEmblem].encode(value.allianceEmblem)
    }
}
