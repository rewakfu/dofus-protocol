package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightSpectateMessage(
  effects: List[FightDispellableEffectExtendedInformations],
  marks: List[GameActionMark],
  gameTurn: Short,
  fightStart: Int,
  idols: List[ConcreteIdol],
  fxTriggerCounts: List[GameFightEffectTriggerCount]
) extends Message(6069)

object GameFightSpectateMessage {
  implicit val codec: Codec[GameFightSpectateMessage] =
    new Codec[GameFightSpectateMessage] {
      def decode: Get[GameFightSpectateMessage] =
        for {
          effects <- list(ushort, Codec[FightDispellableEffectExtendedInformations]).decode
          marks <- list(ushort, Codec[GameActionMark]).decode
          gameTurn <- varShort.decode
          fightStart <- int.decode
          idols <- list(ushort, Codec[ConcreteIdol]).decode
          fxTriggerCounts <- list(ushort, Codec[GameFightEffectTriggerCount]).decode
        } yield GameFightSpectateMessage(effects, marks, gameTurn, fightStart, idols, fxTriggerCounts)

      def encode(value: GameFightSpectateMessage): ByteVector =
        list(ushort, Codec[FightDispellableEffectExtendedInformations]).encode(value.effects) ++
        list(ushort, Codec[GameActionMark]).encode(value.marks) ++
        varShort.encode(value.gameTurn) ++
        int.encode(value.fightStart) ++
        list(ushort, Codec[ConcreteIdol]).encode(value.idols) ++
        list(ushort, Codec[GameFightEffectTriggerCount]).encode(value.fxTriggerCounts)
    }
}
