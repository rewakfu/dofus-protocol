package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SetCharacterRestrictionsMessage(
  actorId: Double,
  restrictions: ActorRestrictionsInformations
) extends Message(170)

object SetCharacterRestrictionsMessage {
  implicit val codec: Codec[SetCharacterRestrictionsMessage] =
    new Codec[SetCharacterRestrictionsMessage] {
      def decode: Get[SetCharacterRestrictionsMessage] =
        for {
          actorId <- double.decode
          restrictions <- Codec[ActorRestrictionsInformations].decode
        } yield SetCharacterRestrictionsMessage(actorId, restrictions)

      def encode(value: SetCharacterRestrictionsMessage): ByteVector =
        double.encode(value.actorId) ++
        Codec[ActorRestrictionsInformations].encode(value.restrictions)
    }
}
