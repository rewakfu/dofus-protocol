package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyNewGuestMessage(
  partyId: Int,
  guest: PartyGuestInformations
) extends Message(6260)

object PartyNewGuestMessage {
  implicit val codec: Codec[PartyNewGuestMessage] =
    new Codec[PartyNewGuestMessage] {
      def decode: Get[PartyNewGuestMessage] =
        for {
          partyId <- varInt.decode
          guest <- Codec[PartyGuestInformations].decode
        } yield PartyNewGuestMessage(partyId, guest)

      def encode(value: PartyNewGuestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        Codec[PartyGuestInformations].encode(value.guest)
    }
}
