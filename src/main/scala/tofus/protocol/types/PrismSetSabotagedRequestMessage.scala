package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismSetSabotagedRequestMessage(
  subAreaId: Short
) extends Message(6468)

object PrismSetSabotagedRequestMessage {
  implicit val codec: Codec[PrismSetSabotagedRequestMessage] =
    new Codec[PrismSetSabotagedRequestMessage] {
      def decode: Get[PrismSetSabotagedRequestMessage] =
        for {
          subAreaId <- varShort.decode
        } yield PrismSetSabotagedRequestMessage(subAreaId)

      def encode(value: PrismSetSabotagedRequestMessage): ByteVector =
        varShort.encode(value.subAreaId)
    }
}
