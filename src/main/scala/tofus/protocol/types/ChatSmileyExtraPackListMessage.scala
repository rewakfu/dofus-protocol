package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatSmileyExtraPackListMessage(
  packIds: ByteVector
) extends Message(6596)

object ChatSmileyExtraPackListMessage {
  implicit val codec: Codec[ChatSmileyExtraPackListMessage] =
    new Codec[ChatSmileyExtraPackListMessage] {
      def decode: Get[ChatSmileyExtraPackListMessage] =
        for {
          packIds <- bytes(ushort).decode
        } yield ChatSmileyExtraPackListMessage(packIds)

      def encode(value: ChatSmileyExtraPackListMessage): ByteVector =
        bytes(ushort).encode(value.packIds)
    }
}
