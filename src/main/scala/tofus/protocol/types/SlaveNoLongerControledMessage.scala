package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SlaveNoLongerControledMessage(
  masterId: Double,
  slaveId: Double
) extends Message(6807)

object SlaveNoLongerControledMessage {
  implicit val codec: Codec[SlaveNoLongerControledMessage] =
    new Codec[SlaveNoLongerControledMessage] {
      def decode: Get[SlaveNoLongerControledMessage] =
        for {
          masterId <- double.decode
          slaveId <- double.decode
        } yield SlaveNoLongerControledMessage(masterId, slaveId)

      def encode(value: SlaveNoLongerControledMessage): ByteVector =
        double.encode(value.masterId) ++
        double.encode(value.slaveId)
    }
}
