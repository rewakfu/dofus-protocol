package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait Idol extends ProtocolType

final case class ConcreteIdol(
  id: Short,
  xpBonusPercent: Short,
  dropBonusPercent: Short
) extends Idol {
  override val protocolId = 489
}

object ConcreteIdol {
  implicit val codec: Codec[ConcreteIdol] =  
    new Codec[ConcreteIdol] {
      def decode: Get[ConcreteIdol] =
        for {
          id <- varShort.decode
          xpBonusPercent <- varShort.decode
          dropBonusPercent <- varShort.decode
        } yield ConcreteIdol(id, xpBonusPercent, dropBonusPercent)

      def encode(value: ConcreteIdol): ByteVector =
        varShort.encode(value.id) ++
        varShort.encode(value.xpBonusPercent) ++
        varShort.encode(value.dropBonusPercent)
    }
}

object Idol {
  implicit val codec: Codec[Idol] =
    new Codec[Idol] {
      def decode: Get[Idol] =
        ushort.decode.flatMap {
          case 489 => Codec[ConcreteIdol].decode
          case 490 => Codec[PartyIdol].decode
        }

      def encode(value: Idol): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteIdol => Codec[ConcreteIdol].encode(i)
          case i: PartyIdol => Codec[PartyIdol].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
