package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseItemAddOkMessage(
  itemInfo: ObjectItemToSellInBid
) extends Message(5945)

object ExchangeBidHouseItemAddOkMessage {
  implicit val codec: Codec[ExchangeBidHouseItemAddOkMessage] =
    new Codec[ExchangeBidHouseItemAddOkMessage] {
      def decode: Get[ExchangeBidHouseItemAddOkMessage] =
        for {
          itemInfo <- Codec[ObjectItemToSellInBid].decode
        } yield ExchangeBidHouseItemAddOkMessage(itemInfo)

      def encode(value: ExchangeBidHouseItemAddOkMessage): ByteVector =
        Codec[ObjectItemToSellInBid].encode(value.itemInfo)
    }
}
