package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeSellMessage(
  objectToSellId: Int,
  quantity: Int
) extends Message(5778)

object ExchangeSellMessage {
  implicit val codec: Codec[ExchangeSellMessage] =
    new Codec[ExchangeSellMessage] {
      def decode: Get[ExchangeSellMessage] =
        for {
          objectToSellId <- varInt.decode
          quantity <- varInt.decode
        } yield ExchangeSellMessage(objectToSellId, quantity)

      def encode(value: ExchangeSellMessage): ByteVector =
        varInt.encode(value.objectToSellId) ++
        varInt.encode(value.quantity)
    }
}
