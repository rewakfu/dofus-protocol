package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GuildVersatileInformations extends ProtocolType

final case class ConcreteGuildVersatileInformations(
  guildId: Int,
  leaderId: Long,
  guildLevel: Short,
  nbMembers: Short
) extends GuildVersatileInformations {
  override val protocolId = 435
}

object ConcreteGuildVersatileInformations {
  implicit val codec: Codec[ConcreteGuildVersatileInformations] =  
    new Codec[ConcreteGuildVersatileInformations] {
      def decode: Get[ConcreteGuildVersatileInformations] =
        for {
          guildId <- varInt.decode
          leaderId <- varLong.decode
          guildLevel <- ubyte.decode
          nbMembers <- ubyte.decode
        } yield ConcreteGuildVersatileInformations(guildId, leaderId, guildLevel, nbMembers)

      def encode(value: ConcreteGuildVersatileInformations): ByteVector =
        varInt.encode(value.guildId) ++
        varLong.encode(value.leaderId) ++
        ubyte.encode(value.guildLevel) ++
        ubyte.encode(value.nbMembers)
    }
}

object GuildVersatileInformations {
  implicit val codec: Codec[GuildVersatileInformations] =
    new Codec[GuildVersatileInformations] {
      def decode: Get[GuildVersatileInformations] =
        ushort.decode.flatMap {
          case 435 => Codec[ConcreteGuildVersatileInformations].decode
          case 437 => Codec[GuildInAllianceVersatileInformations].decode
        }

      def encode(value: GuildVersatileInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGuildVersatileInformations => Codec[ConcreteGuildVersatileInformations].encode(i)
          case i: GuildInAllianceVersatileInformations => Codec[GuildInAllianceVersatileInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
