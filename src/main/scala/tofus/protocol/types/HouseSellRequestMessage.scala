package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseSellRequestMessage(
  instanceId: Int,
  amount: Long,
  forSale: Boolean
) extends Message(5697)

object HouseSellRequestMessage {
  implicit val codec: Codec[HouseSellRequestMessage] =
    new Codec[HouseSellRequestMessage] {
      def decode: Get[HouseSellRequestMessage] =
        for {
          instanceId <- int.decode
          amount <- varLong.decode
          forSale <- bool.decode
        } yield HouseSellRequestMessage(instanceId, amount, forSale)

      def encode(value: HouseSellRequestMessage): ByteVector =
        int.encode(value.instanceId) ++
        varLong.encode(value.amount) ++
        bool.encode(value.forSale)
    }
}
