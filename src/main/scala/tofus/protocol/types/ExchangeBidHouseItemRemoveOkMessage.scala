package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseItemRemoveOkMessage(
  sellerId: Int
) extends Message(5946)

object ExchangeBidHouseItemRemoveOkMessage {
  implicit val codec: Codec[ExchangeBidHouseItemRemoveOkMessage] =
    new Codec[ExchangeBidHouseItemRemoveOkMessage] {
      def decode: Get[ExchangeBidHouseItemRemoveOkMessage] =
        for {
          sellerId <- int.decode
        } yield ExchangeBidHouseItemRemoveOkMessage(sellerId)

      def encode(value: ExchangeBidHouseItemRemoveOkMessage): ByteVector =
        int.encode(value.sellerId)
    }
}
