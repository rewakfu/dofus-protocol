package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceChangeGuildRightsMessage(
  guildId: Int,
  rights: Byte
) extends Message(6426)

object AllianceChangeGuildRightsMessage {
  implicit val codec: Codec[AllianceChangeGuildRightsMessage] =
    new Codec[AllianceChangeGuildRightsMessage] {
      def decode: Get[AllianceChangeGuildRightsMessage] =
        for {
          guildId <- varInt.decode
          rights <- byte.decode
        } yield AllianceChangeGuildRightsMessage(guildId, rights)

      def encode(value: AllianceChangeGuildRightsMessage): ByteVector =
        varInt.encode(value.guildId) ++
        byte.encode(value.rights)
    }
}
