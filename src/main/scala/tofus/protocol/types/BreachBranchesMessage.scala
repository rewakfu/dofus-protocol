package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachBranchesMessage(
  branches: List[ExtendedBreachBranch]
) extends Message(6812)

object BreachBranchesMessage {
  implicit val codec: Codec[BreachBranchesMessage] =
    new Codec[BreachBranchesMessage] {
      def decode: Get[BreachBranchesMessage] =
        for {
          branches <- list(ushort, Codec[ExtendedBreachBranch]).decode
        } yield BreachBranchesMessage(branches)

      def encode(value: BreachBranchesMessage): ByteVector =
        list(ushort, Codec[ExtendedBreachBranch]).encode(value.branches)
    }
}
