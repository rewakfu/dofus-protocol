package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuestLimitationMessage(
  reason: Byte
) extends Message(6506)

object GuestLimitationMessage {
  implicit val codec: Codec[GuestLimitationMessage] =
    new Codec[GuestLimitationMessage] {
      def decode: Get[GuestLimitationMessage] =
        for {
          reason <- byte.decode
        } yield GuestLimitationMessage(reason)

      def encode(value: GuestLimitationMessage): ByteVector =
        byte.encode(value.reason)
    }
}
