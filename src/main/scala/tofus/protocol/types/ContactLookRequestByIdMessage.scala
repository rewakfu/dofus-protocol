package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ContactLookRequestByIdMessage(
  requestId: Short,
  contactType: Byte,
  playerId: Long
) extends Message(5935)

object ContactLookRequestByIdMessage {
  implicit val codec: Codec[ContactLookRequestByIdMessage] =
    new Codec[ContactLookRequestByIdMessage] {
      def decode: Get[ContactLookRequestByIdMessage] =
        for {
          requestId <- ubyte.decode
          contactType <- byte.decode
          playerId <- varLong.decode
        } yield ContactLookRequestByIdMessage(requestId, contactType, playerId)

      def encode(value: ContactLookRequestByIdMessage): ByteVector =
        ubyte.encode(value.requestId) ++
        byte.encode(value.contactType) ++
        varLong.encode(value.playerId)
    }
}
