package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationDungeonDetailsMessage(
  partyId: Int,
  partyType: Byte,
  partyName: String,
  fromId: Long,
  fromName: String,
  leaderId: Long,
  members: List[PartyInvitationMemberInformations],
  guests: List[PartyGuestInformations],
  dungeonId: Short,
  playersDungeonReady: List[Boolean]
) extends Message(6262)

object PartyInvitationDungeonDetailsMessage {
  implicit val codec: Codec[PartyInvitationDungeonDetailsMessage] =
    new Codec[PartyInvitationDungeonDetailsMessage] {
      def decode: Get[PartyInvitationDungeonDetailsMessage] =
        for {
          partyId <- varInt.decode
          partyType <- byte.decode
          partyName <- utf8(ushort).decode
          fromId <- varLong.decode
          fromName <- utf8(ushort).decode
          leaderId <- varLong.decode
          members <- list(ushort, Codec[PartyInvitationMemberInformations]).decode
          guests <- list(ushort, Codec[PartyGuestInformations]).decode
          dungeonId <- varShort.decode
          playersDungeonReady <- list(ushort, bool).decode
        } yield PartyInvitationDungeonDetailsMessage(partyId, partyType, partyName, fromId, fromName, leaderId, members, guests, dungeonId, playersDungeonReady)

      def encode(value: PartyInvitationDungeonDetailsMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.partyType) ++
        utf8(ushort).encode(value.partyName) ++
        varLong.encode(value.fromId) ++
        utf8(ushort).encode(value.fromName) ++
        varLong.encode(value.leaderId) ++
        list(ushort, Codec[PartyInvitationMemberInformations]).encode(value.members) ++
        list(ushort, Codec[PartyGuestInformations]).encode(value.guests) ++
        varShort.encode(value.dungeonId) ++
        list(ushort, bool).encode(value.playersDungeonReady)
    }
}
