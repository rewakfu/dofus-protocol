package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NamedPartyTeam(
  teamId: Byte,
  partyName: String
) extends ProtocolType {
  override val protocolId = 469
}

object NamedPartyTeam {
  implicit val codec: Codec[NamedPartyTeam] =
    new Codec[NamedPartyTeam] {
      def decode: Get[NamedPartyTeam] =
        for {
          teamId <- byte.decode
          partyName <- utf8(ushort).decode
        } yield NamedPartyTeam(teamId, partyName)

      def encode(value: NamedPartyTeam): ByteVector =
        byte.encode(value.teamId) ++
        utf8(ushort).encode(value.partyName)
    }
}
