package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightJoinLeaveRequestMessage(
  subAreaId: Short,
  join: Boolean
) extends Message(5843)

object PrismFightJoinLeaveRequestMessage {
  implicit val codec: Codec[PrismFightJoinLeaveRequestMessage] =
    new Codec[PrismFightJoinLeaveRequestMessage] {
      def decode: Get[PrismFightJoinLeaveRequestMessage] =
        for {
          subAreaId <- varShort.decode
          join <- bool.decode
        } yield PrismFightJoinLeaveRequestMessage(subAreaId, join)

      def encode(value: PrismFightJoinLeaveRequestMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        bool.encode(value.join)
    }
}
