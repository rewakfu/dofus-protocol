package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayShowChallengeMessage(
  commonsInfos: FightCommonInformations
) extends Message(301)

object GameRolePlayShowChallengeMessage {
  implicit val codec: Codec[GameRolePlayShowChallengeMessage] =
    new Codec[GameRolePlayShowChallengeMessage] {
      def decode: Get[GameRolePlayShowChallengeMessage] =
        for {
          commonsInfos <- Codec[FightCommonInformations].decode
        } yield GameRolePlayShowChallengeMessage(commonsInfos)

      def encode(value: GameRolePlayShowChallengeMessage): ByteVector =
        Codec[FightCommonInformations].encode(value.commonsInfos)
    }
}
