package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaInvitationCandidatesAnswer(
  candidates: List[LeagueFriendInformations]
) extends Message(6783)

object GameRolePlayArenaInvitationCandidatesAnswer {
  implicit val codec: Codec[GameRolePlayArenaInvitationCandidatesAnswer] =
    new Codec[GameRolePlayArenaInvitationCandidatesAnswer] {
      def decode: Get[GameRolePlayArenaInvitationCandidatesAnswer] =
        for {
          candidates <- list(ushort, Codec[LeagueFriendInformations]).decode
        } yield GameRolePlayArenaInvitationCandidatesAnswer(candidates)

      def encode(value: GameRolePlayArenaInvitationCandidatesAnswer): ByteVector =
        list(ushort, Codec[LeagueFriendInformations]).encode(value.candidates)
    }
}
