package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightCastOnTargetRequestMessage(
  spellId: Short,
  targetId: Double
) extends Message(6330)

object GameActionFightCastOnTargetRequestMessage {
  implicit val codec: Codec[GameActionFightCastOnTargetRequestMessage] =
    new Codec[GameActionFightCastOnTargetRequestMessage] {
      def decode: Get[GameActionFightCastOnTargetRequestMessage] =
        for {
          spellId <- varShort.decode
          targetId <- double.decode
        } yield GameActionFightCastOnTargetRequestMessage(spellId, targetId)

      def encode(value: GameActionFightCastOnTargetRequestMessage): ByteVector =
        varShort.encode(value.spellId) ++
        double.encode(value.targetId)
    }
}
