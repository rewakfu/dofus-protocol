package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BufferInformation(
  id: Long,
  amount: Long
) extends ProtocolType {
  override val protocolId = 570
}

object BufferInformation {
  implicit val codec: Codec[BufferInformation] =
    new Codec[BufferInformation] {
      def decode: Get[BufferInformation] =
        for {
          id <- varLong.decode
          amount <- varLong.decode
        } yield BufferInformation(id, amount)

      def encode(value: BufferInformation): ByteVector =
        varLong.encode(value.id) ++
        varLong.encode(value.amount)
    }
}
