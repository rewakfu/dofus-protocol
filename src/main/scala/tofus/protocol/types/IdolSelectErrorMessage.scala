package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolSelectErrorMessage(
  flags0: Byte,
  reason: Byte,
  idolId: Short
) extends Message(6584)

object IdolSelectErrorMessage {
  implicit val codec: Codec[IdolSelectErrorMessage] =
    new Codec[IdolSelectErrorMessage] {
      def decode: Get[IdolSelectErrorMessage] =
        for {
          flags0 <- byte.decode
          reason <- byte.decode
          idolId <- varShort.decode
        } yield IdolSelectErrorMessage(flags0, reason, idolId)

      def encode(value: IdolSelectErrorMessage): ByteVector =
        byte.encode(value.flags0) ++
        byte.encode(value.reason) ++
        varShort.encode(value.idolId)
    }
}
