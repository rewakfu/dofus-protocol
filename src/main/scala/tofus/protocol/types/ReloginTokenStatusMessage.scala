package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ReloginTokenStatusMessage(
  validToken: Boolean,
  ticket: ByteVector
) extends Message(6539)

object ReloginTokenStatusMessage {
  implicit val codec: Codec[ReloginTokenStatusMessage] =
    new Codec[ReloginTokenStatusMessage] {
      def decode: Get[ReloginTokenStatusMessage] =
        for {
          validToken <- bool.decode
          ticket <- bytes(varInt).decode
        } yield ReloginTokenStatusMessage(validToken, ticket)

      def encode(value: ReloginTokenStatusMessage): ByteVector =
        bool.encode(value.validToken) ++
        bytes(varInt).encode(value.ticket)
    }
}
