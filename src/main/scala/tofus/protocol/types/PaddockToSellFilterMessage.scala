package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockToSellFilterMessage(
  areaId: Int,
  atLeastNbMount: Byte,
  atLeastNbMachine: Byte,
  maxPrice: Long,
  orderBy: Byte
) extends Message(6161)

object PaddockToSellFilterMessage {
  implicit val codec: Codec[PaddockToSellFilterMessage] =
    new Codec[PaddockToSellFilterMessage] {
      def decode: Get[PaddockToSellFilterMessage] =
        for {
          areaId <- int.decode
          atLeastNbMount <- byte.decode
          atLeastNbMachine <- byte.decode
          maxPrice <- varLong.decode
          orderBy <- byte.decode
        } yield PaddockToSellFilterMessage(areaId, atLeastNbMount, atLeastNbMachine, maxPrice, orderBy)

      def encode(value: PaddockToSellFilterMessage): ByteVector =
        int.encode(value.areaId) ++
        byte.encode(value.atLeastNbMount) ++
        byte.encode(value.atLeastNbMachine) ++
        varLong.encode(value.maxPrice) ++
        byte.encode(value.orderBy)
    }
}
