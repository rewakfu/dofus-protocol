package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeSetCraftRecipeMessage(
  objectGID: Short
) extends Message(6389)

object ExchangeSetCraftRecipeMessage {
  implicit val codec: Codec[ExchangeSetCraftRecipeMessage] =
    new Codec[ExchangeSetCraftRecipeMessage] {
      def decode: Get[ExchangeSetCraftRecipeMessage] =
        for {
          objectGID <- varShort.decode
        } yield ExchangeSetCraftRecipeMessage(objectGID)

      def encode(value: ExchangeSetCraftRecipeMessage): ByteVector =
        varShort.encode(value.objectGID)
    }
}
