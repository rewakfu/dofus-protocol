package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LivingObjectChangeSkinRequestMessage(
  livingUID: Int,
  livingPosition: Short,
  skinId: Int
) extends Message(5725)

object LivingObjectChangeSkinRequestMessage {
  implicit val codec: Codec[LivingObjectChangeSkinRequestMessage] =
    new Codec[LivingObjectChangeSkinRequestMessage] {
      def decode: Get[LivingObjectChangeSkinRequestMessage] =
        for {
          livingUID <- varInt.decode
          livingPosition <- ubyte.decode
          skinId <- varInt.decode
        } yield LivingObjectChangeSkinRequestMessage(livingUID, livingPosition, skinId)

      def encode(value: LivingObjectChangeSkinRequestMessage): ByteVector =
        varInt.encode(value.livingUID) ++
        ubyte.encode(value.livingPosition) ++
        varInt.encode(value.skinId)
    }
}
