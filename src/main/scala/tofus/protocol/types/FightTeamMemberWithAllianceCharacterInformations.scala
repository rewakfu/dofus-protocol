package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTeamMemberWithAllianceCharacterInformations(
  id: Double,
  name: String,
  level: Short,
  allianceInfos: ConcreteBasicAllianceInformations
) extends FightTeamMemberCharacterInformations {
  override val protocolId = 426
}

object FightTeamMemberWithAllianceCharacterInformations {
  implicit val codec: Codec[FightTeamMemberWithAllianceCharacterInformations] =
    new Codec[FightTeamMemberWithAllianceCharacterInformations] {
      def decode: Get[FightTeamMemberWithAllianceCharacterInformations] =
        for {
          id <- double.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          allianceInfos <- Codec[ConcreteBasicAllianceInformations].decode
        } yield FightTeamMemberWithAllianceCharacterInformations(id, name, level, allianceInfos)

      def encode(value: FightTeamMemberWithAllianceCharacterInformations): ByteVector =
        double.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[ConcreteBasicAllianceInformations].encode(value.allianceInfos)
    }
}
