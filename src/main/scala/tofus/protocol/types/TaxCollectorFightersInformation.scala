package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorFightersInformation(
  collectorId: Double,
  allyCharactersInformations: List[CharacterMinimalPlusLookInformations],
  enemyCharactersInformations: List[CharacterMinimalPlusLookInformations]
) extends ProtocolType {
  override val protocolId = 169
}

object TaxCollectorFightersInformation {
  implicit val codec: Codec[TaxCollectorFightersInformation] =
    new Codec[TaxCollectorFightersInformation] {
      def decode: Get[TaxCollectorFightersInformation] =
        for {
          collectorId <- double.decode
          allyCharactersInformations <- list(ushort, Codec[CharacterMinimalPlusLookInformations]).decode
          enemyCharactersInformations <- list(ushort, Codec[CharacterMinimalPlusLookInformations]).decode
        } yield TaxCollectorFightersInformation(collectorId, allyCharactersInformations, enemyCharactersInformations)

      def encode(value: TaxCollectorFightersInformation): ByteVector =
        double.encode(value.collectorId) ++
        list(ushort, Codec[CharacterMinimalPlusLookInformations]).encode(value.allyCharactersInformations) ++
        list(ushort, Codec[CharacterMinimalPlusLookInformations]).encode(value.enemyCharactersInformations)
    }
}
