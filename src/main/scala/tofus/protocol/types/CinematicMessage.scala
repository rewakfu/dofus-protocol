package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CinematicMessage(
  cinematicId: Short
) extends Message(6053)

object CinematicMessage {
  implicit val codec: Codec[CinematicMessage] =
    new Codec[CinematicMessage] {
      def decode: Get[CinematicMessage] =
        for {
          cinematicId <- varShort.decode
        } yield CinematicMessage(cinematicId)

      def encode(value: CinematicMessage): ByteVector =
        varShort.encode(value.cinematicId)
    }
}
