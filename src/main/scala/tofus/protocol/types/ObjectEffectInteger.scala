package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectInteger(
  actionId: Short,
  value: Int
) extends ObjectEffect {
  override val protocolId = 70
}

object ObjectEffectInteger {
  implicit val codec: Codec[ObjectEffectInteger] =
    new Codec[ObjectEffectInteger] {
      def decode: Get[ObjectEffectInteger] =
        for {
          actionId <- varShort.decode
          value <- varInt.decode
        } yield ObjectEffectInteger(actionId, value)

      def encode(value: ObjectEffectInteger): ByteVector =
        varShort.encode(value.actionId) ++
        varInt.encode(value.value)
    }
}
