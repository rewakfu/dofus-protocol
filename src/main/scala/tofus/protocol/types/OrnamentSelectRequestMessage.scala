package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class OrnamentSelectRequestMessage(
  ornamentId: Short
) extends Message(6374)

object OrnamentSelectRequestMessage {
  implicit val codec: Codec[OrnamentSelectRequestMessage] =
    new Codec[OrnamentSelectRequestMessage] {
      def decode: Get[OrnamentSelectRequestMessage] =
        for {
          ornamentId <- varShort.decode
        } yield OrnamentSelectRequestMessage(ornamentId)

      def encode(value: OrnamentSelectRequestMessage): ByteVector =
        varShort.encode(value.ornamentId)
    }
}
