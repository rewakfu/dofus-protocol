package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyMemberEjectedMessage(
  partyId: Int,
  leavingPlayerId: Long,
  kickerId: Long
) extends Message(6252)

object PartyMemberEjectedMessage {
  implicit val codec: Codec[PartyMemberEjectedMessage] =
    new Codec[PartyMemberEjectedMessage] {
      def decode: Get[PartyMemberEjectedMessage] =
        for {
          partyId <- varInt.decode
          leavingPlayerId <- varLong.decode
          kickerId <- varLong.decode
        } yield PartyMemberEjectedMessage(partyId, leavingPlayerId, kickerId)

      def encode(value: PartyMemberEjectedMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.leavingPlayerId) ++
        varLong.encode(value.kickerId)
    }
}
