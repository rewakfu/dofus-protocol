package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LeaveDialogMessage(
  dialogType: Byte
) extends Message(5502)

object LeaveDialogMessage {
  implicit val codec: Codec[LeaveDialogMessage] =
    new Codec[LeaveDialogMessage] {
      def decode: Get[LeaveDialogMessage] =
        for {
          dialogType <- byte.decode
        } yield LeaveDialogMessage(dialogType)

      def encode(value: LeaveDialogMessage): ByteVector =
        byte.encode(value.dialogType)
    }
}
