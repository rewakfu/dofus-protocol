package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightChangeLookMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  entityLook: EntityLook
) extends Message(5532)

object GameActionFightChangeLookMessage {
  implicit val codec: Codec[GameActionFightChangeLookMessage] =
    new Codec[GameActionFightChangeLookMessage] {
      def decode: Get[GameActionFightChangeLookMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          entityLook <- Codec[EntityLook].decode
        } yield GameActionFightChangeLookMessage(actionId, sourceId, targetId, entityLook)

      def encode(value: GameActionFightChangeLookMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        Codec[EntityLook].encode(value.entityLook)
    }
}
