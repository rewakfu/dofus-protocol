package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AreaFightModificatorUpdateMessage(
  spellPairId: Int
) extends Message(6493)

object AreaFightModificatorUpdateMessage {
  implicit val codec: Codec[AreaFightModificatorUpdateMessage] =
    new Codec[AreaFightModificatorUpdateMessage] {
      def decode: Get[AreaFightModificatorUpdateMessage] =
        for {
          spellPairId <- int.decode
        } yield AreaFightModificatorUpdateMessage(spellPairId)

      def encode(value: AreaFightModificatorUpdateMessage): ByteVector =
        int.encode(value.spellPairId)
    }
}
