package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftResultMagicWithObjectDescMessage(
  craftResult: Byte,
  objectInfo: ObjectItemNotInContainer,
  magicPoolStatus: Byte
) extends Message(6188)

object ExchangeCraftResultMagicWithObjectDescMessage {
  implicit val codec: Codec[ExchangeCraftResultMagicWithObjectDescMessage] =
    new Codec[ExchangeCraftResultMagicWithObjectDescMessage] {
      def decode: Get[ExchangeCraftResultMagicWithObjectDescMessage] =
        for {
          craftResult <- byte.decode
          objectInfo <- Codec[ObjectItemNotInContainer].decode
          magicPoolStatus <- byte.decode
        } yield ExchangeCraftResultMagicWithObjectDescMessage(craftResult, objectInfo, magicPoolStatus)

      def encode(value: ExchangeCraftResultMagicWithObjectDescMessage): ByteVector =
        byte.encode(value.craftResult) ++
        Codec[ObjectItemNotInContainer].encode(value.objectInfo) ++
        byte.encode(value.magicPoolStatus)
    }
}
