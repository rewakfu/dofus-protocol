package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceMotdSetRequestMessage(
  content: String
) extends Message(6687)

object AllianceMotdSetRequestMessage {
  implicit val codec: Codec[AllianceMotdSetRequestMessage] =
    new Codec[AllianceMotdSetRequestMessage] {
      def decode: Get[AllianceMotdSetRequestMessage] =
        for {
          content <- utf8(ushort).decode
        } yield AllianceMotdSetRequestMessage(content)

      def encode(value: AllianceMotdSetRequestMessage): ByteVector =
        utf8(ushort).encode(value.content)
    }
}
