package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DocumentReadingBeginMessage(
  documentId: Short
) extends Message(5675)

object DocumentReadingBeginMessage {
  implicit val codec: Codec[DocumentReadingBeginMessage] =
    new Codec[DocumentReadingBeginMessage] {
      def decode: Get[DocumentReadingBeginMessage] =
        for {
          documentId <- varShort.decode
        } yield DocumentReadingBeginMessage(documentId)

      def encode(value: DocumentReadingBeginMessage): ByteVector =
        varShort.encode(value.documentId)
    }
}
