package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayRemoveChallengeMessage(
  fightId: Short
) extends Message(300)

object GameRolePlayRemoveChallengeMessage {
  implicit val codec: Codec[GameRolePlayRemoveChallengeMessage] =
    new Codec[GameRolePlayRemoveChallengeMessage] {
      def decode: Get[GameRolePlayRemoveChallengeMessage] =
        for {
          fightId <- varShort.decode
        } yield GameRolePlayRemoveChallengeMessage(fightId)

      def encode(value: GameRolePlayRemoveChallengeMessage): ByteVector =
        varShort.encode(value.fightId)
    }
}
