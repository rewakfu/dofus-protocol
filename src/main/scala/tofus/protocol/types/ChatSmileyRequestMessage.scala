package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatSmileyRequestMessage(
  smileyId: Short
) extends Message(800)

object ChatSmileyRequestMessage {
  implicit val codec: Codec[ChatSmileyRequestMessage] =
    new Codec[ChatSmileyRequestMessage] {
      def decode: Get[ChatSmileyRequestMessage] =
        for {
          smileyId <- varShort.decode
        } yield ChatSmileyRequestMessage(smileyId)

      def encode(value: ChatSmileyRequestMessage): ByteVector =
        varShort.encode(value.smileyId)
    }
}
