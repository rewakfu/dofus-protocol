package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapNpcsQuestStatusUpdateMessage(
  mapId: Double,
  npcsIdsWithQuest: List[Int],
  questFlags: List[GameRolePlayNpcQuestFlag],
  npcsIdsWithoutQuest: List[Int]
) extends Message(5642)

object MapNpcsQuestStatusUpdateMessage {
  implicit val codec: Codec[MapNpcsQuestStatusUpdateMessage] =
    new Codec[MapNpcsQuestStatusUpdateMessage] {
      def decode: Get[MapNpcsQuestStatusUpdateMessage] =
        for {
          mapId <- double.decode
          npcsIdsWithQuest <- list(ushort, int).decode
          questFlags <- list(ushort, Codec[GameRolePlayNpcQuestFlag]).decode
          npcsIdsWithoutQuest <- list(ushort, int).decode
        } yield MapNpcsQuestStatusUpdateMessage(mapId, npcsIdsWithQuest, questFlags, npcsIdsWithoutQuest)

      def encode(value: MapNpcsQuestStatusUpdateMessage): ByteVector =
        double.encode(value.mapId) ++
        list(ushort, int).encode(value.npcsIdsWithQuest) ++
        list(ushort, Codec[GameRolePlayNpcQuestFlag]).encode(value.questFlags) ++
        list(ushort, int).encode(value.npcsIdsWithoutQuest)
    }
}
