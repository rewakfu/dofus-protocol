package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PlayerStatusUpdateMessage(
  accountId: Int,
  playerId: Long,
  status: PlayerStatus
) extends Message(6386)

object PlayerStatusUpdateMessage {
  implicit val codec: Codec[PlayerStatusUpdateMessage] =
    new Codec[PlayerStatusUpdateMessage] {
      def decode: Get[PlayerStatusUpdateMessage] =
        for {
          accountId <- int.decode
          playerId <- varLong.decode
          status <- Codec[PlayerStatus].decode
        } yield PlayerStatusUpdateMessage(accountId, playerId, status)

      def encode(value: PlayerStatusUpdateMessage): ByteVector =
        int.encode(value.accountId) ++
        varLong.encode(value.playerId) ++
        Codec[PlayerStatus].encode(value.status)
    }
}
