package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait Preset extends ProtocolType

final case class ConcretePreset(
  id: Short
) extends Preset {
  override val protocolId = 355
}

object ConcretePreset {
  implicit val codec: Codec[ConcretePreset] =  
    new Codec[ConcretePreset] {
      def decode: Get[ConcretePreset] =
        for {
          id <- short.decode
        } yield ConcretePreset(id)

      def encode(value: ConcretePreset): ByteVector =
        short.encode(value.id)
    }
}

object Preset {
  implicit val codec: Codec[Preset] =
    new Codec[Preset] {
      def decode: Get[Preset] =
        ushort.decode.flatMap {
          case 355 => Codec[ConcretePreset].decode
          case 519 => Codec[SpellsPreset].decode
          case 517 => Codec[ItemsPreset].decode
          case 534 => Codec[CharacterBuildPreset].decode
          case 520 => Codec[ConcretePresetsContainerPreset].decode
          case 521 => Codec[StatsPreset].decode
          case 532 => Codec[FullStatsPreset].decode
          case 491 => Codec[IdolsPreset].decode
          case 545 => Codec[EntitiesPreset].decode
        }

      def encode(value: Preset): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePreset => Codec[ConcretePreset].encode(i)
          case i: SpellsPreset => Codec[SpellsPreset].encode(i)
          case i: ItemsPreset => Codec[ItemsPreset].encode(i)
          case i: CharacterBuildPreset => Codec[CharacterBuildPreset].encode(i)
          case i: ConcretePresetsContainerPreset => Codec[ConcretePresetsContainerPreset].encode(i)
          case i: StatsPreset => Codec[StatsPreset].encode(i)
          case i: FullStatsPreset => Codec[FullStatsPreset].encode(i)
          case i: IdolsPreset => Codec[IdolsPreset].encode(i)
          case i: EntitiesPreset => Codec[EntitiesPreset].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
