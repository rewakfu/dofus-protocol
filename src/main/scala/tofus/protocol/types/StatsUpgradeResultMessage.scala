package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatsUpgradeResultMessage(
  result: Byte,
  nbCharacBoost: Short
) extends Message(5609)

object StatsUpgradeResultMessage {
  implicit val codec: Codec[StatsUpgradeResultMessage] =
    new Codec[StatsUpgradeResultMessage] {
      def decode: Get[StatsUpgradeResultMessage] =
        for {
          result <- byte.decode
          nbCharacBoost <- varShort.decode
        } yield StatsUpgradeResultMessage(result, nbCharacBoost)

      def encode(value: StatsUpgradeResultMessage): ByteVector =
        byte.encode(value.result) ++
        varShort.encode(value.nbCharacBoost)
    }
}
