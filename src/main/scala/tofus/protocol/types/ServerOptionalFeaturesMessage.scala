package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerOptionalFeaturesMessage(
  features: ByteVector
) extends Message(6305)

object ServerOptionalFeaturesMessage {
  implicit val codec: Codec[ServerOptionalFeaturesMessage] =
    new Codec[ServerOptionalFeaturesMessage] {
      def decode: Get[ServerOptionalFeaturesMessage] =
        for {
          features <- bytes(ushort).decode
        } yield ServerOptionalFeaturesMessage(features)

      def encode(value: ServerOptionalFeaturesMessage): ByteVector =
        bytes(ushort).encode(value.features)
    }
}
