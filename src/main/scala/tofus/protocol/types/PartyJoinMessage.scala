package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyJoinMessage(
  partyId: Int,
  partyType: Byte,
  partyLeaderId: Long,
  maxParticipants: Byte,
  members: List[PartyMemberInformations],
  guests: List[PartyGuestInformations],
  restricted: Boolean,
  partyName: String
) extends Message(5576)

object PartyJoinMessage {
  implicit val codec: Codec[PartyJoinMessage] =
    new Codec[PartyJoinMessage] {
      def decode: Get[PartyJoinMessage] =
        for {
          partyId <- varInt.decode
          partyType <- byte.decode
          partyLeaderId <- varLong.decode
          maxParticipants <- byte.decode
          members <- list(ushort, Codec[PartyMemberInformations]).decode
          guests <- list(ushort, Codec[PartyGuestInformations]).decode
          restricted <- bool.decode
          partyName <- utf8(ushort).decode
        } yield PartyJoinMessage(partyId, partyType, partyLeaderId, maxParticipants, members, guests, restricted, partyName)

      def encode(value: PartyJoinMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.partyType) ++
        varLong.encode(value.partyLeaderId) ++
        byte.encode(value.maxParticipants) ++
        list(ushort, Codec[PartyMemberInformations]).encode(value.members) ++
        list(ushort, Codec[PartyGuestInformations]).encode(value.guests) ++
        bool.encode(value.restricted) ++
        utf8(ushort).encode(value.partyName)
    }
}
