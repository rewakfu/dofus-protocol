package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameDataPaddockObjectListAddMessage(
  paddockItemDescription: List[PaddockItem]
) extends Message(5992)

object GameDataPaddockObjectListAddMessage {
  implicit val codec: Codec[GameDataPaddockObjectListAddMessage] =
    new Codec[GameDataPaddockObjectListAddMessage] {
      def decode: Get[GameDataPaddockObjectListAddMessage] =
        for {
          paddockItemDescription <- list(ushort, Codec[PaddockItem]).decode
        } yield GameDataPaddockObjectListAddMessage(paddockItemDescription)

      def encode(value: GameDataPaddockObjectListAddMessage): ByteVector =
        list(ushort, Codec[PaddockItem]).encode(value.paddockItemDescription)
    }
}
