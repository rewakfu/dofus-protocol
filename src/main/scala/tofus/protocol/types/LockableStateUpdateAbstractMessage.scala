package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LockableStateUpdateAbstractMessage(
  locked: Boolean
) extends Message(5671)

object LockableStateUpdateAbstractMessage {
  implicit val codec: Codec[LockableStateUpdateAbstractMessage] =
    new Codec[LockableStateUpdateAbstractMessage] {
      def decode: Get[LockableStateUpdateAbstractMessage] =
        for {
          locked <- bool.decode
        } yield LockableStateUpdateAbstractMessage(locked)

      def encode(value: LockableStateUpdateAbstractMessage): ByteVector =
        bool.encode(value.locked)
    }
}
