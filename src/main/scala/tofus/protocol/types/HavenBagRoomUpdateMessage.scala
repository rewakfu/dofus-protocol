package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagRoomUpdateMessage(
  action: Byte,
  roomsPreview: List[HavenBagRoomPreviewInformation]
) extends Message(6860)

object HavenBagRoomUpdateMessage {
  implicit val codec: Codec[HavenBagRoomUpdateMessage] =
    new Codec[HavenBagRoomUpdateMessage] {
      def decode: Get[HavenBagRoomUpdateMessage] =
        for {
          action <- byte.decode
          roomsPreview <- list(ushort, Codec[HavenBagRoomPreviewInformation]).decode
        } yield HavenBagRoomUpdateMessage(action, roomsPreview)

      def encode(value: HavenBagRoomUpdateMessage): ByteVector =
        byte.encode(value.action) ++
        list(ushort, Codec[HavenBagRoomPreviewInformation]).encode(value.roomsPreview)
    }
}
