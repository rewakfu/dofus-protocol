package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolPartyRefreshMessage(
  partyIdol: PartyIdol
) extends Message(6583)

object IdolPartyRefreshMessage {
  implicit val codec: Codec[IdolPartyRefreshMessage] =
    new Codec[IdolPartyRefreshMessage] {
      def decode: Get[IdolPartyRefreshMessage] =
        for {
          partyIdol <- Codec[PartyIdol].decode
        } yield IdolPartyRefreshMessage(partyIdol)

      def encode(value: IdolPartyRefreshMessage): ByteVector =
        Codec[PartyIdol].encode(value.partyIdol)
    }
}
