package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredGetListMessage(

) extends Message(5676)

object IgnoredGetListMessage {
  implicit val codec: Codec[IgnoredGetListMessage] =
    Codec.const(IgnoredGetListMessage())
}
