package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildBulletinMessage(
  content: String,
  timestamp: Int,
  memberId: Long,
  memberName: String,
  lastNotifiedTimestamp: Int
) extends Message(6689)

object GuildBulletinMessage {
  implicit val codec: Codec[GuildBulletinMessage] =
    new Codec[GuildBulletinMessage] {
      def decode: Get[GuildBulletinMessage] =
        for {
          content <- utf8(ushort).decode
          timestamp <- int.decode
          memberId <- varLong.decode
          memberName <- utf8(ushort).decode
          lastNotifiedTimestamp <- int.decode
        } yield GuildBulletinMessage(content, timestamp, memberId, memberName, lastNotifiedTimestamp)

      def encode(value: GuildBulletinMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        varLong.encode(value.memberId) ++
        utf8(ushort).encode(value.memberName) ++
        int.encode(value.lastNotifiedTimestamp)
    }
}
