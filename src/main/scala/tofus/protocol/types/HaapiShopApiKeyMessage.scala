package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiShopApiKeyMessage(
  token: String
) extends Message(6858)

object HaapiShopApiKeyMessage {
  implicit val codec: Codec[HaapiShopApiKeyMessage] =
    new Codec[HaapiShopApiKeyMessage] {
      def decode: Get[HaapiShopApiKeyMessage] =
        for {
          token <- utf8(ushort).decode
        } yield HaapiShopApiKeyMessage(token)

      def encode(value: HaapiShopApiKeyMessage): ByteVector =
        utf8(ushort).encode(value.token)
    }
}
