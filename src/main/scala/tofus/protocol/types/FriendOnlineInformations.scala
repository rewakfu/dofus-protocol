package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendOnlineInformations(
  accountId: Int,
  accountName: String,
  playerState: Byte,
  lastConnection: Short,
  achievementPoints: Int,
  leagueId: Short,
  ladderPosition: Int,
  flags0: Byte,
  playerId: Long,
  playerName: String,
  level: Short,
  alignmentSide: Byte,
  breed: Byte,
  guildInfo: ConcreteGuildInformations,
  moodSmileyId: Short,
  status: PlayerStatus
) extends FriendInformations {
  override val protocolId = 92
}

object FriendOnlineInformations {
  implicit val codec: Codec[FriendOnlineInformations] =
    new Codec[FriendOnlineInformations] {
      def decode: Get[FriendOnlineInformations] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
          playerState <- byte.decode
          lastConnection <- varShort.decode
          achievementPoints <- int.decode
          leagueId <- varShort.decode
          ladderPosition <- int.decode
          flags0 <- byte.decode
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
          level <- varShort.decode
          alignmentSide <- byte.decode
          breed <- byte.decode
          guildInfo <- Codec[ConcreteGuildInformations].decode
          moodSmileyId <- varShort.decode
          status <- Codec[PlayerStatus].decode
        } yield FriendOnlineInformations(accountId, accountName, playerState, lastConnection, achievementPoints, leagueId, ladderPosition, flags0, playerId, playerName, level, alignmentSide, breed, guildInfo, moodSmileyId, status)

      def encode(value: FriendOnlineInformations): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName) ++
        byte.encode(value.playerState) ++
        varShort.encode(value.lastConnection) ++
        int.encode(value.achievementPoints) ++
        varShort.encode(value.leagueId) ++
        int.encode(value.ladderPosition) ++
        byte.encode(value.flags0) ++
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName) ++
        varShort.encode(value.level) ++
        byte.encode(value.alignmentSide) ++
        byte.encode(value.breed) ++
        Codec[ConcreteGuildInformations].encode(value.guildInfo) ++
        varShort.encode(value.moodSmileyId) ++
        Codec[PlayerStatus].encode(value.status)
    }
}
