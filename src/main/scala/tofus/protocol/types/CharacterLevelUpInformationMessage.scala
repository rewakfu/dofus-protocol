package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterLevelUpInformationMessage(
  newLevel: Short,
  name: String,
  id: Long
) extends Message(6076)

object CharacterLevelUpInformationMessage {
  implicit val codec: Codec[CharacterLevelUpInformationMessage] =
    new Codec[CharacterLevelUpInformationMessage] {
      def decode: Get[CharacterLevelUpInformationMessage] =
        for {
          newLevel <- varShort.decode
          name <- utf8(ushort).decode
          id <- varLong.decode
        } yield CharacterLevelUpInformationMessage(newLevel, name, id)

      def encode(value: CharacterLevelUpInformationMessage): ByteVector =
        varShort.encode(value.newLevel) ++
        utf8(ushort).encode(value.name) ++
        varLong.encode(value.id)
    }
}
