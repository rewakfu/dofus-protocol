package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockSellRequestMessage(
  price: Long,
  forSale: Boolean
) extends Message(5953)

object PaddockSellRequestMessage {
  implicit val codec: Codec[PaddockSellRequestMessage] =
    new Codec[PaddockSellRequestMessage] {
      def decode: Get[PaddockSellRequestMessage] =
        for {
          price <- varLong.decode
          forSale <- bool.decode
        } yield PaddockSellRequestMessage(price, forSale)

      def encode(value: PaddockSellRequestMessage): ByteVector =
        varLong.encode(value.price) ++
        bool.encode(value.forSale)
    }
}
