package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildPaddockTeleportRequestMessage(
  paddockId: Double
) extends Message(5957)

object GuildPaddockTeleportRequestMessage {
  implicit val codec: Codec[GuildPaddockTeleportRequestMessage] =
    new Codec[GuildPaddockTeleportRequestMessage] {
      def decode: Get[GuildPaddockTeleportRequestMessage] =
        for {
          paddockId <- double.decode
        } yield GuildPaddockTeleportRequestMessage(paddockId)

      def encode(value: GuildPaddockTeleportRequestMessage): ByteVector =
        double.encode(value.paddockId)
    }
}
