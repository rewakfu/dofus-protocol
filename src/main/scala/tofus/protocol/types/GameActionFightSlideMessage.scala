package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightSlideMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  startCellId: Short,
  endCellId: Short
) extends Message(5525)

object GameActionFightSlideMessage {
  implicit val codec: Codec[GameActionFightSlideMessage] =
    new Codec[GameActionFightSlideMessage] {
      def decode: Get[GameActionFightSlideMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          startCellId <- short.decode
          endCellId <- short.decode
        } yield GameActionFightSlideMessage(actionId, sourceId, targetId, startCellId, endCellId)

      def encode(value: GameActionFightSlideMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.startCellId) ++
        short.encode(value.endCellId)
    }
}
