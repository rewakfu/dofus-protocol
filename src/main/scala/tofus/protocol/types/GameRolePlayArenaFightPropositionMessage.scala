package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaFightPropositionMessage(
  fightId: Short,
  alliesId: List[Double],
  duration: Short
) extends Message(6276)

object GameRolePlayArenaFightPropositionMessage {
  implicit val codec: Codec[GameRolePlayArenaFightPropositionMessage] =
    new Codec[GameRolePlayArenaFightPropositionMessage] {
      def decode: Get[GameRolePlayArenaFightPropositionMessage] =
        for {
          fightId <- varShort.decode
          alliesId <- list(ushort, double).decode
          duration <- varShort.decode
        } yield GameRolePlayArenaFightPropositionMessage(fightId, alliesId, duration)

      def encode(value: GameRolePlayArenaFightPropositionMessage): ByteVector =
        varShort.encode(value.fightId) ++
        list(ushort, double).encode(value.alliesId) ++
        varShort.encode(value.duration)
    }
}
