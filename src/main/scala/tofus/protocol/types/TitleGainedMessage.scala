package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TitleGainedMessage(
  titleId: Short
) extends Message(6364)

object TitleGainedMessage {
  implicit val codec: Codec[TitleGainedMessage] =
    new Codec[TitleGainedMessage] {
      def decode: Get[TitleGainedMessage] =
        for {
          titleId <- varShort.decode
        } yield TitleGainedMessage(titleId)

      def encode(value: TitleGainedMessage): ByteVector =
        varShort.encode(value.titleId)
    }
}
