package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyNameSetRequestMessage(
  partyId: Int,
  partyName: String
) extends Message(6503)

object PartyNameSetRequestMessage {
  implicit val codec: Codec[PartyNameSetRequestMessage] =
    new Codec[PartyNameSetRequestMessage] {
      def decode: Get[PartyNameSetRequestMessage] =
        for {
          partyId <- varInt.decode
          partyName <- utf8(ushort).decode
        } yield PartyNameSetRequestMessage(partyId, partyName)

      def encode(value: PartyNameSetRequestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        utf8(ushort).encode(value.partyName)
    }
}
