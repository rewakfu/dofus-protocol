package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountToggleRidingRequestMessage(

) extends Message(5976)

object MountToggleRidingRequestMessage {
  implicit val codec: Codec[MountToggleRidingRequestMessage] =
    Codec.const(MountToggleRidingRequestMessage())
}
