package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementDetailsRequestMessage(
  achievementId: Short
) extends Message(6380)

object AchievementDetailsRequestMessage {
  implicit val codec: Codec[AchievementDetailsRequestMessage] =
    new Codec[AchievementDetailsRequestMessage] {
      def decode: Get[AchievementDetailsRequestMessage] =
        for {
          achievementId <- varShort.decode
        } yield AchievementDetailsRequestMessage(achievementId)

      def encode(value: AchievementDetailsRequestMessage): ByteVector =
        varShort.encode(value.achievementId)
    }
}
