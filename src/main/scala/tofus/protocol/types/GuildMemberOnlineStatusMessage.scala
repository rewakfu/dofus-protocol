package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMemberOnlineStatusMessage(
  memberId: Long,
  online: Boolean
) extends Message(6061)

object GuildMemberOnlineStatusMessage {
  implicit val codec: Codec[GuildMemberOnlineStatusMessage] =
    new Codec[GuildMemberOnlineStatusMessage] {
      def decode: Get[GuildMemberOnlineStatusMessage] =
        for {
          memberId <- varLong.decode
          online <- bool.decode
        } yield GuildMemberOnlineStatusMessage(memberId, online)

      def encode(value: GuildMemberOnlineStatusMessage): ByteVector =
        varLong.encode(value.memberId) ++
        bool.encode(value.online)
    }
}
