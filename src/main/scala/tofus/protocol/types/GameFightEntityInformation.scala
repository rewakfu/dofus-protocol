package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightEntityInformation(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short],
  entityModelId: Byte,
  level: Short,
  masterId: Double
) extends GameFightFighterInformations {
  override val protocolId = 551
}

object GameFightEntityInformation {
  implicit val codec: Codec[GameFightEntityInformation] =
    new Codec[GameFightEntityInformation] {
      def decode: Get[GameFightEntityInformation] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
          entityModelId <- byte.decode
          level <- varShort.decode
          masterId <- double.decode
        } yield GameFightEntityInformation(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions, entityModelId, level, masterId)

      def encode(value: GameFightEntityInformation): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions) ++
        byte.encode(value.entityModelId) ++
        varShort.encode(value.level) ++
        double.encode(value.masterId)
    }
}
