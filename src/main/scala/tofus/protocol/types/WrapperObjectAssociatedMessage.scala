package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class WrapperObjectAssociatedMessage(
  hostUID: Int
) extends Message(6523)

object WrapperObjectAssociatedMessage {
  implicit val codec: Codec[WrapperObjectAssociatedMessage] =
    new Codec[WrapperObjectAssociatedMessage] {
      def decode: Get[WrapperObjectAssociatedMessage] =
        for {
          hostUID <- varInt.decode
        } yield WrapperObjectAssociatedMessage(hostUID)

      def encode(value: WrapperObjectAssociatedMessage): ByteVector =
        varInt.encode(value.hostUID)
    }
}
