package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectUseOnCellMessage(
  objectUID: Int,
  cells: Short
) extends Message(3013)

object ObjectUseOnCellMessage {
  implicit val codec: Codec[ObjectUseOnCellMessage] =
    new Codec[ObjectUseOnCellMessage] {
      def decode: Get[ObjectUseOnCellMessage] =
        for {
          objectUID <- varInt.decode
          cells <- varShort.decode
        } yield ObjectUseOnCellMessage(objectUID, cells)

      def encode(value: ObjectUseOnCellMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varShort.encode(value.cells)
    }
}
