package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicNoOperationMessage(

) extends Message(176)

object BasicNoOperationMessage {
  implicit val codec: Codec[BasicNoOperationMessage] =
    Codec.const(BasicNoOperationMessage())
}
