package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachInvitationRequestMessage(
  guests: List[Long]
) extends Message(6794)

object BreachInvitationRequestMessage {
  implicit val codec: Codec[BreachInvitationRequestMessage] =
    new Codec[BreachInvitationRequestMessage] {
      def decode: Get[BreachInvitationRequestMessage] =
        for {
          guests <- list(ushort, varLong).decode
        } yield BreachInvitationRequestMessage(guests)

      def encode(value: BreachInvitationRequestMessage): ByteVector =
        list(ushort, varLong).encode(value.guests)
    }
}
