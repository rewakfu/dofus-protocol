package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class RefreshCharacterStatsMessage(
  fighterId: Double,
  stats: ConcreteGameFightMinimalStats
) extends Message(6699)

object RefreshCharacterStatsMessage {
  implicit val codec: Codec[RefreshCharacterStatsMessage] =
    new Codec[RefreshCharacterStatsMessage] {
      def decode: Get[RefreshCharacterStatsMessage] =
        for {
          fighterId <- double.decode
          stats <- Codec[ConcreteGameFightMinimalStats].decode
        } yield RefreshCharacterStatsMessage(fighterId, stats)

      def encode(value: RefreshCharacterStatsMessage): ByteVector =
        double.encode(value.fighterId) ++
        Codec[ConcreteGameFightMinimalStats].encode(value.stats)
    }
}
