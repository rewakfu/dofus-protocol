package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiValidationMessage(
  action: Byte,
  code: Short
) extends Message(6844)

object HaapiValidationMessage {
  implicit val codec: Codec[HaapiValidationMessage] =
    new Codec[HaapiValidationMessage] {
      def decode: Get[HaapiValidationMessage] =
        for {
          action <- byte.decode
          code <- varShort.decode
        } yield HaapiValidationMessage(action, code)

      def encode(value: HaapiValidationMessage): ByteVector =
        byte.encode(value.action) ++
        varShort.encode(value.code)
    }
}
