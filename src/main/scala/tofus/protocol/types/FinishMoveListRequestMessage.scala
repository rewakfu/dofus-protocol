package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FinishMoveListRequestMessage(

) extends Message(6702)

object FinishMoveListRequestMessage {
  implicit val codec: Codec[FinishMoveListRequestMessage] =
    Codec.const(FinishMoveListRequestMessage())
}
