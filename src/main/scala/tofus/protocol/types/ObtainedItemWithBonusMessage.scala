package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObtainedItemWithBonusMessage(
  genericId: Short,
  baseQuantity: Int,
  bonusQuantity: Int
) extends Message(6520)

object ObtainedItemWithBonusMessage {
  implicit val codec: Codec[ObtainedItemWithBonusMessage] =
    new Codec[ObtainedItemWithBonusMessage] {
      def decode: Get[ObtainedItemWithBonusMessage] =
        for {
          genericId <- varShort.decode
          baseQuantity <- varInt.decode
          bonusQuantity <- varInt.decode
        } yield ObtainedItemWithBonusMessage(genericId, baseQuantity, bonusQuantity)

      def encode(value: ObtainedItemWithBonusMessage): ByteVector =
        varShort.encode(value.genericId) ++
        varInt.encode(value.baseQuantity) ++
        varInt.encode(value.bonusQuantity)
    }
}
