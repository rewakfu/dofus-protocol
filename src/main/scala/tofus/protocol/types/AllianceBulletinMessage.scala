package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceBulletinMessage(
  content: String,
  timestamp: Int,
  memberId: Long,
  memberName: String,
  lastNotifiedTimestamp: Int
) extends Message(6690)

object AllianceBulletinMessage {
  implicit val codec: Codec[AllianceBulletinMessage] =
    new Codec[AllianceBulletinMessage] {
      def decode: Get[AllianceBulletinMessage] =
        for {
          content <- utf8(ushort).decode
          timestamp <- int.decode
          memberId <- varLong.decode
          memberName <- utf8(ushort).decode
          lastNotifiedTimestamp <- int.decode
        } yield AllianceBulletinMessage(content, timestamp, memberId, memberName, lastNotifiedTimestamp)

      def encode(value: AllianceBulletinMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        varLong.encode(value.memberId) ++
        utf8(ushort).encode(value.memberName) ++
        int.encode(value.lastNotifiedTimestamp)
    }
}
