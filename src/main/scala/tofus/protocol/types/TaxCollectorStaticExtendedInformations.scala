package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorStaticExtendedInformations(
  firstNameId: Short,
  lastNameId: Short,
  guildIdentity: ConcreteGuildInformations,
  allianceIdentity: ConcreteAllianceInformations
) extends TaxCollectorStaticInformations {
  override val protocolId = 440
}

object TaxCollectorStaticExtendedInformations {
  implicit val codec: Codec[TaxCollectorStaticExtendedInformations] =
    new Codec[TaxCollectorStaticExtendedInformations] {
      def decode: Get[TaxCollectorStaticExtendedInformations] =
        for {
          firstNameId <- varShort.decode
          lastNameId <- varShort.decode
          guildIdentity <- Codec[ConcreteGuildInformations].decode
          allianceIdentity <- Codec[ConcreteAllianceInformations].decode
        } yield TaxCollectorStaticExtendedInformations(firstNameId, lastNameId, guildIdentity, allianceIdentity)

      def encode(value: TaxCollectorStaticExtendedInformations): ByteVector =
        varShort.encode(value.firstNameId) ++
        varShort.encode(value.lastNameId) ++
        Codec[ConcreteGuildInformations].encode(value.guildIdentity) ++
        Codec[ConcreteAllianceInformations].encode(value.allianceIdentity)
    }
}
