package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInvitationMessage(
  targetId: Long
) extends Message(6395)

object AllianceInvitationMessage {
  implicit val codec: Codec[AllianceInvitationMessage] =
    new Codec[AllianceInvitationMessage] {
      def decode: Get[AllianceInvitationMessage] =
        for {
          targetId <- varLong.decode
        } yield AllianceInvitationMessage(targetId)

      def encode(value: AllianceInvitationMessage): ByteVector =
        varLong.encode(value.targetId)
    }
}
