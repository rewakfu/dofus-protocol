package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobExperience(
  jobId: Byte,
  jobLevel: Short,
  jobXP: Long,
  jobXpLevelFloor: Long,
  jobXpNextLevelFloor: Long
) extends ProtocolType {
  override val protocolId = 98
}

object JobExperience {
  implicit val codec: Codec[JobExperience] =
    new Codec[JobExperience] {
      def decode: Get[JobExperience] =
        for {
          jobId <- byte.decode
          jobLevel <- ubyte.decode
          jobXP <- varLong.decode
          jobXpLevelFloor <- varLong.decode
          jobXpNextLevelFloor <- varLong.decode
        } yield JobExperience(jobId, jobLevel, jobXP, jobXpLevelFloor, jobXpNextLevelFloor)

      def encode(value: JobExperience): ByteVector =
        byte.encode(value.jobId) ++
        ubyte.encode(value.jobLevel) ++
        varLong.encode(value.jobXP) ++
        varLong.encode(value.jobXpLevelFloor) ++
        varLong.encode(value.jobXpNextLevelFloor)
    }
}
