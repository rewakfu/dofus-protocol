package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StartupActionsListMessage(
  actions: List[StartupActionAddObject]
) extends Message(1301)

object StartupActionsListMessage {
  implicit val codec: Codec[StartupActionsListMessage] =
    new Codec[StartupActionsListMessage] {
      def decode: Get[StartupActionsListMessage] =
        for {
          actions <- list(ushort, Codec[StartupActionAddObject]).decode
        } yield StartupActionsListMessage(actions)

      def encode(value: StartupActionsListMessage): ByteVector =
        list(ushort, Codec[StartupActionAddObject]).encode(value.actions)
    }
}
