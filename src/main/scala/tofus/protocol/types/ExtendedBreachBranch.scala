package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ExtendedBreachBranch extends BreachBranch

final case class ConcreteExtendedBreachBranch(
  room: Byte,
  element: Int,
  bosses: List[ConcreteMonsterInGroupLightInformations],
  map: Double,
  score: Short,
  relativeScore: Short,
  monsters: List[ConcreteMonsterInGroupLightInformations],
  rewards: List[BreachReward],
  modifier: Int,
  prize: Int
) extends ExtendedBreachBranch {
  override val protocolId = 560
}

object ConcreteExtendedBreachBranch {
  implicit val codec: Codec[ConcreteExtendedBreachBranch] =  
    new Codec[ConcreteExtendedBreachBranch] {
      def decode: Get[ConcreteExtendedBreachBranch] =
        for {
          room <- byte.decode
          element <- int.decode
          bosses <- list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).decode
          map <- double.decode
          score <- short.decode
          relativeScore <- short.decode
          monsters <- list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).decode
          rewards <- list(ushort, Codec[BreachReward]).decode
          modifier <- varInt.decode
          prize <- varInt.decode
        } yield ConcreteExtendedBreachBranch(room, element, bosses, map, score, relativeScore, monsters, rewards, modifier, prize)

      def encode(value: ConcreteExtendedBreachBranch): ByteVector =
        byte.encode(value.room) ++
        int.encode(value.element) ++
        list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).encode(value.bosses) ++
        double.encode(value.map) ++
        short.encode(value.score) ++
        short.encode(value.relativeScore) ++
        list(ushort, Codec[ConcreteMonsterInGroupLightInformations]).encode(value.monsters) ++
        list(ushort, Codec[BreachReward]).encode(value.rewards) ++
        varInt.encode(value.modifier) ++
        varInt.encode(value.prize)
    }
}

object ExtendedBreachBranch {
  implicit val codec: Codec[ExtendedBreachBranch] =
    new Codec[ExtendedBreachBranch] {
      def decode: Get[ExtendedBreachBranch] =
        ushort.decode.flatMap {
          case 560 => Codec[ConcreteExtendedBreachBranch].decode
          case 578 => Codec[ExtendedLockedBreachBranch].decode
        }

      def encode(value: ExtendedBreachBranch): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteExtendedBreachBranch => Codec[ConcreteExtendedBreachBranch].encode(i)
          case i: ExtendedLockedBreachBranch => Codec[ExtendedLockedBreachBranch].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
