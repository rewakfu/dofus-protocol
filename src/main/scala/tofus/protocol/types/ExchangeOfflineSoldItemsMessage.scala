package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeOfflineSoldItemsMessage(
  bidHouseItems: List[ObjectItemQuantityPriceDateEffects],
  merchantItems: List[ObjectItemQuantityPriceDateEffects]
) extends Message(6613)

object ExchangeOfflineSoldItemsMessage {
  implicit val codec: Codec[ExchangeOfflineSoldItemsMessage] =
    new Codec[ExchangeOfflineSoldItemsMessage] {
      def decode: Get[ExchangeOfflineSoldItemsMessage] =
        for {
          bidHouseItems <- list(ushort, Codec[ObjectItemQuantityPriceDateEffects]).decode
          merchantItems <- list(ushort, Codec[ObjectItemQuantityPriceDateEffects]).decode
        } yield ExchangeOfflineSoldItemsMessage(bidHouseItems, merchantItems)

      def encode(value: ExchangeOfflineSoldItemsMessage): ByteVector =
        list(ushort, Codec[ObjectItemQuantityPriceDateEffects]).encode(value.bidHouseItems) ++
        list(ushort, Codec[ObjectItemQuantityPriceDateEffects]).encode(value.merchantItems)
    }
}
