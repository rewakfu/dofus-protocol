package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterLoadingCompleteMessage(

) extends Message(6471)

object CharacterLoadingCompleteMessage {
  implicit val codec: Codec[CharacterLoadingCompleteMessage] =
    Codec.const(CharacterLoadingCompleteMessage())
}
