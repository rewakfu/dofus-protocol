package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeKamaModifiedMessage(
  remote: Boolean,
  quantity: Long
) extends Message(5521)

object ExchangeKamaModifiedMessage {
  implicit val codec: Codec[ExchangeKamaModifiedMessage] =
    new Codec[ExchangeKamaModifiedMessage] {
      def decode: Get[ExchangeKamaModifiedMessage] =
        for {
          remote <- bool.decode
          quantity <- varLong.decode
        } yield ExchangeKamaModifiedMessage(remote, quantity)

      def encode(value: ExchangeKamaModifiedMessage): ByteVector =
        bool.encode(value.remote) ++
        varLong.encode(value.quantity)
    }
}
