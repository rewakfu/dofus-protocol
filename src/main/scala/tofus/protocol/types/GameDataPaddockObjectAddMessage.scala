package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameDataPaddockObjectAddMessage(
  paddockItemDescription: PaddockItem
) extends Message(5990)

object GameDataPaddockObjectAddMessage {
  implicit val codec: Codec[GameDataPaddockObjectAddMessage] =
    new Codec[GameDataPaddockObjectAddMessage] {
      def decode: Get[GameDataPaddockObjectAddMessage] =
        for {
          paddockItemDescription <- Codec[PaddockItem].decode
        } yield GameDataPaddockObjectAddMessage(paddockItemDescription)

      def encode(value: GameDataPaddockObjectAddMessage): ByteVector =
        Codec[PaddockItem].encode(value.paddockItemDescription)
    }
}
