package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementDetailedListRequestMessage(
  categoryId: Short
) extends Message(6357)

object AchievementDetailedListRequestMessage {
  implicit val codec: Codec[AchievementDetailedListRequestMessage] =
    new Codec[AchievementDetailedListRequestMessage] {
      def decode: Get[AchievementDetailedListRequestMessage] =
        for {
          categoryId <- varShort.decode
        } yield AchievementDetailedListRequestMessage(categoryId)

      def encode(value: AchievementDetailedListRequestMessage): ByteVector =
        varShort.encode(value.categoryId)
    }
}
