package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CurrentServerStatusUpdateMessage(
  status: Byte
) extends Message(6525)

object CurrentServerStatusUpdateMessage {
  implicit val codec: Codec[CurrentServerStatusUpdateMessage] =
    new Codec[CurrentServerStatusUpdateMessage] {
      def decode: Get[CurrentServerStatusUpdateMessage] =
        for {
          status <- byte.decode
        } yield CurrentServerStatusUpdateMessage(status)

      def encode(value: CurrentServerStatusUpdateMessage): ByteVector =
        byte.encode(value.status)
    }
}
