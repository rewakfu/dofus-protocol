package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FinishMoveSetRequestMessage(
  finishMoveId: Int,
  finishMoveState: Boolean
) extends Message(6703)

object FinishMoveSetRequestMessage {
  implicit val codec: Codec[FinishMoveSetRequestMessage] =
    new Codec[FinishMoveSetRequestMessage] {
      def decode: Get[FinishMoveSetRequestMessage] =
        for {
          finishMoveId <- int.decode
          finishMoveState <- bool.decode
        } yield FinishMoveSetRequestMessage(finishMoveId, finishMoveState)

      def encode(value: FinishMoveSetRequestMessage): ByteVector =
        int.encode(value.finishMoveId) ++
        bool.encode(value.finishMoveState)
    }
}
