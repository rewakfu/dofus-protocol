package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftCountModifiedMessage(
  count: Int
) extends Message(6595)

object ExchangeCraftCountModifiedMessage {
  implicit val codec: Codec[ExchangeCraftCountModifiedMessage] =
    new Codec[ExchangeCraftCountModifiedMessage] {
      def decode: Get[ExchangeCraftCountModifiedMessage] =
        for {
          count <- varInt.decode
        } yield ExchangeCraftCountModifiedMessage(count)

      def encode(value: ExchangeCraftCountModifiedMessage): ByteVector =
        varInt.encode(value.count)
    }
}
