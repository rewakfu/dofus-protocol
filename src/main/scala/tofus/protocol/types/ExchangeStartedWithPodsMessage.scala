package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartedWithPodsMessage(
  exchangeType: Byte,
  firstCharacterId: Double,
  firstCharacterCurrentWeight: Int,
  firstCharacterMaxWeight: Int,
  secondCharacterId: Double,
  secondCharacterCurrentWeight: Int,
  secondCharacterMaxWeight: Int
) extends Message(6129)

object ExchangeStartedWithPodsMessage {
  implicit val codec: Codec[ExchangeStartedWithPodsMessage] =
    new Codec[ExchangeStartedWithPodsMessage] {
      def decode: Get[ExchangeStartedWithPodsMessage] =
        for {
          exchangeType <- byte.decode
          firstCharacterId <- double.decode
          firstCharacterCurrentWeight <- varInt.decode
          firstCharacterMaxWeight <- varInt.decode
          secondCharacterId <- double.decode
          secondCharacterCurrentWeight <- varInt.decode
          secondCharacterMaxWeight <- varInt.decode
        } yield ExchangeStartedWithPodsMessage(exchangeType, firstCharacterId, firstCharacterCurrentWeight, firstCharacterMaxWeight, secondCharacterId, secondCharacterCurrentWeight, secondCharacterMaxWeight)

      def encode(value: ExchangeStartedWithPodsMessage): ByteVector =
        byte.encode(value.exchangeType) ++
        double.encode(value.firstCharacterId) ++
        varInt.encode(value.firstCharacterCurrentWeight) ++
        varInt.encode(value.firstCharacterMaxWeight) ++
        double.encode(value.secondCharacterId) ++
        varInt.encode(value.secondCharacterCurrentWeight) ++
        varInt.encode(value.secondCharacterMaxWeight)
    }
}
