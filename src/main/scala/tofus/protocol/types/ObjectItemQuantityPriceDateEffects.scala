package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItemQuantityPriceDateEffects(
  objectGID: Short,
  quantity: Int,
  price: Long,
  effects: ObjectEffects,
  date: Int
) extends ObjectItemGenericQuantity {
  override val protocolId = 577
}

object ObjectItemQuantityPriceDateEffects {
  implicit val codec: Codec[ObjectItemQuantityPriceDateEffects] =
    new Codec[ObjectItemQuantityPriceDateEffects] {
      def decode: Get[ObjectItemQuantityPriceDateEffects] =
        for {
          objectGID <- varShort.decode
          quantity <- varInt.decode
          price <- varLong.decode
          effects <- Codec[ObjectEffects].decode
          date <- int.decode
        } yield ObjectItemQuantityPriceDateEffects(objectGID, quantity, price, effects, date)

      def encode(value: ObjectItemQuantityPriceDateEffects): ByteVector =
        varShort.encode(value.objectGID) ++
        varInt.encode(value.quantity) ++
        varLong.encode(value.price) ++
        Codec[ObjectEffects].encode(value.effects) ++
        int.encode(value.date)
    }
}
