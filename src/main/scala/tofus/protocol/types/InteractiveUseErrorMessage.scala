package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveUseErrorMessage(
  elemId: Int,
  skillInstanceUid: Int
) extends Message(6384)

object InteractiveUseErrorMessage {
  implicit val codec: Codec[InteractiveUseErrorMessage] =
    new Codec[InteractiveUseErrorMessage] {
      def decode: Get[InteractiveUseErrorMessage] =
        for {
          elemId <- varInt.decode
          skillInstanceUid <- varInt.decode
        } yield InteractiveUseErrorMessage(elemId, skillInstanceUid)

      def encode(value: InteractiveUseErrorMessage): ByteVector =
        varInt.encode(value.elemId) ++
        varInt.encode(value.skillInstanceUid)
    }
}
