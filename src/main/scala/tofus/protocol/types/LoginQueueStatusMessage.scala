package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LoginQueueStatusMessage(
  position: Int,
  total: Int
) extends Message(10)

object LoginQueueStatusMessage {
  implicit val codec: Codec[LoginQueueStatusMessage] =
    new Codec[LoginQueueStatusMessage] {
      def decode: Get[LoginQueueStatusMessage] =
        for {
          position <- ushort.decode
          total <- ushort.decode
        } yield LoginQueueStatusMessage(position, total)

      def encode(value: LoginQueueStatusMessage): ByteVector =
        ushort.encode(value.position) ++
        ushort.encode(value.total)
    }
}
