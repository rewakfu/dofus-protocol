package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmotePlayErrorMessage(
  emoteId: Short
) extends Message(5688)

object EmotePlayErrorMessage {
  implicit val codec: Codec[EmotePlayErrorMessage] =
    new Codec[EmotePlayErrorMessage] {
      def decode: Get[EmotePlayErrorMessage] =
        for {
          emoteId <- ubyte.decode
        } yield EmotePlayErrorMessage(emoteId)

      def encode(value: EmotePlayErrorMessage): ByteVector =
        ubyte.encode(value.emoteId)
    }
}
