package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EntityLook(
  bonesId: Short,
  skins: List[Short],
  indexedColors: List[Int],
  scales: List[Short],
  subentities: List[SubEntity]
) extends ProtocolType {
  override val protocolId = 55
}

object EntityLook {
  implicit val codec: Codec[EntityLook] =
    new Codec[EntityLook] {
      def decode: Get[EntityLook] =
        for {
          bonesId <- varShort.decode
          skins <- list(ushort, varShort).decode
          indexedColors <- list(ushort, int).decode
          scales <- list(ushort, varShort).decode
          subentities <- list(ushort, Codec[SubEntity]).decode
        } yield EntityLook(bonesId, skins, indexedColors, scales, subentities)

      def encode(value: EntityLook): ByteVector =
        varShort.encode(value.bonesId) ++
        list(ushort, varShort).encode(value.skins) ++
        list(ushort, int).encode(value.indexedColors) ++
        list(ushort, varShort).encode(value.scales) ++
        list(ushort, Codec[SubEntity]).encode(value.subentities)
    }
}
