package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectGroundAddedMessage(
  cellId: Short,
  objectGID: Short
) extends Message(3017)

object ObjectGroundAddedMessage {
  implicit val codec: Codec[ObjectGroundAddedMessage] =
    new Codec[ObjectGroundAddedMessage] {
      def decode: Get[ObjectGroundAddedMessage] =
        for {
          cellId <- varShort.decode
          objectGID <- varShort.decode
        } yield ObjectGroundAddedMessage(cellId, objectGID)

      def encode(value: ObjectGroundAddedMessage): ByteVector =
        varShort.encode(value.cellId) ++
        varShort.encode(value.objectGID)
    }
}
