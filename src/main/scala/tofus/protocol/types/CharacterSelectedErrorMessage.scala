package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterSelectedErrorMessage(

) extends Message(5836)

object CharacterSelectedErrorMessage {
  implicit val codec: Codec[CharacterSelectedErrorMessage] =
    Codec.const(CharacterSelectedErrorMessage())
}
