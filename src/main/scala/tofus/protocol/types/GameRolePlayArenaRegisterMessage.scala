package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaRegisterMessage(
  battleMode: Int
) extends Message(6280)

object GameRolePlayArenaRegisterMessage {
  implicit val codec: Codec[GameRolePlayArenaRegisterMessage] =
    new Codec[GameRolePlayArenaRegisterMessage] {
      def decode: Get[GameRolePlayArenaRegisterMessage] =
        for {
          battleMode <- int.decode
        } yield GameRolePlayArenaRegisterMessage(battleMode)

      def encode(value: GameRolePlayArenaRegisterMessage): ByteVector =
        int.encode(value.battleMode)
    }
}
