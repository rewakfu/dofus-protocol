package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftPaymentModifiedMessage(
  goldSum: Long
) extends Message(6578)

object ExchangeCraftPaymentModifiedMessage {
  implicit val codec: Codec[ExchangeCraftPaymentModifiedMessage] =
    new Codec[ExchangeCraftPaymentModifiedMessage] {
      def decode: Get[ExchangeCraftPaymentModifiedMessage] =
        for {
          goldSum <- varLong.decode
        } yield ExchangeCraftPaymentModifiedMessage(goldSum)

      def encode(value: ExchangeCraftPaymentModifiedMessage): ByteVector =
        varLong.encode(value.goldSum)
    }
}
