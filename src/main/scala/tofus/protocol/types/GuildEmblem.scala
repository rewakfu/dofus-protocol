package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildEmblem(
  symbolShape: Short,
  symbolColor: Int,
  backgroundShape: Byte,
  backgroundColor: Int
) extends ProtocolType {
  override val protocolId = 87
}

object GuildEmblem {
  implicit val codec: Codec[GuildEmblem] =
    new Codec[GuildEmblem] {
      def decode: Get[GuildEmblem] =
        for {
          symbolShape <- varShort.decode
          symbolColor <- int.decode
          backgroundShape <- byte.decode
          backgroundColor <- int.decode
        } yield GuildEmblem(symbolShape, symbolColor, backgroundShape, backgroundColor)

      def encode(value: GuildEmblem): ByteVector =
        varShort.encode(value.symbolShape) ++
        int.encode(value.symbolColor) ++
        byte.encode(value.backgroundShape) ++
        int.encode(value.backgroundColor)
    }
}
