package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait HouseInformations extends ProtocolType

final case class ConcreteHouseInformations(
  houseId: Int,
  modelId: Short
) extends HouseInformations {
  override val protocolId = 111
}

object ConcreteHouseInformations {
  implicit val codec: Codec[ConcreteHouseInformations] =  
    new Codec[ConcreteHouseInformations] {
      def decode: Get[ConcreteHouseInformations] =
        for {
          houseId <- varInt.decode
          modelId <- varShort.decode
        } yield ConcreteHouseInformations(houseId, modelId)

      def encode(value: ConcreteHouseInformations): ByteVector =
        varInt.encode(value.houseId) ++
        varShort.encode(value.modelId)
    }
}

object HouseInformations {
  implicit val codec: Codec[HouseInformations] =
    new Codec[HouseInformations] {
      def decode: Get[HouseInformations] =
        ushort.decode.flatMap {
          case 111 => Codec[ConcreteHouseInformations].decode
          case 510 => Codec[HouseOnMapInformations].decode
          case 390 => Codec[AccountHouseInformations].decode
          case 170 => Codec[HouseInformationsForGuild].decode
          case 218 => Codec[HouseInformationsInside].decode
        }

      def encode(value: HouseInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteHouseInformations => Codec[ConcreteHouseInformations].encode(i)
          case i: HouseOnMapInformations => Codec[HouseOnMapInformations].encode(i)
          case i: AccountHouseInformations => Codec[AccountHouseInformations].encode(i)
          case i: HouseInformationsForGuild => Codec[HouseInformationsForGuild].encode(i)
          case i: HouseInformationsInside => Codec[HouseInformationsInside].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
