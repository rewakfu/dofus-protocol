package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightDispellEffectMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  verboseCast: Boolean,
  boostUID: Int
) extends Message(6113)

object GameActionFightDispellEffectMessage {
  implicit val codec: Codec[GameActionFightDispellEffectMessage] =
    new Codec[GameActionFightDispellEffectMessage] {
      def decode: Get[GameActionFightDispellEffectMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          verboseCast <- bool.decode
          boostUID <- int.decode
        } yield GameActionFightDispellEffectMessage(actionId, sourceId, targetId, verboseCast, boostUID)

      def encode(value: GameActionFightDispellEffectMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        bool.encode(value.verboseCast) ++
        int.encode(value.boostUID)
    }
}
