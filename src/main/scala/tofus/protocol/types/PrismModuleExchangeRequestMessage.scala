package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismModuleExchangeRequestMessage(

) extends Message(6531)

object PrismModuleExchangeRequestMessage {
  implicit val codec: Codec[PrismModuleExchangeRequestMessage] =
    Codec.const(PrismModuleExchangeRequestMessage())
}
