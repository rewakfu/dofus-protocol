package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatServerWithObjectMessage(
  channel: Byte,
  content: String,
  timestamp: Int,
  fingerprint: String,
  senderId: Double,
  senderName: String,
  prefix: String,
  senderAccountId: Int,
  objects: List[ObjectItem]
) extends Message(883)

object ChatServerWithObjectMessage {
  implicit val codec: Codec[ChatServerWithObjectMessage] =
    new Codec[ChatServerWithObjectMessage] {
      def decode: Get[ChatServerWithObjectMessage] =
        for {
          channel <- byte.decode
          content <- utf8(ushort).decode
          timestamp <- int.decode
          fingerprint <- utf8(ushort).decode
          senderId <- double.decode
          senderName <- utf8(ushort).decode
          prefix <- utf8(ushort).decode
          senderAccountId <- int.decode
          objects <- list(ushort, Codec[ObjectItem]).decode
        } yield ChatServerWithObjectMessage(channel, content, timestamp, fingerprint, senderId, senderName, prefix, senderAccountId, objects)

      def encode(value: ChatServerWithObjectMessage): ByteVector =
        byte.encode(value.channel) ++
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        utf8(ushort).encode(value.fingerprint) ++
        double.encode(value.senderId) ++
        utf8(ushort).encode(value.senderName) ++
        utf8(ushort).encode(value.prefix) ++
        int.encode(value.senderAccountId) ++
        list(ushort, Codec[ObjectItem]).encode(value.objects)
    }
}
