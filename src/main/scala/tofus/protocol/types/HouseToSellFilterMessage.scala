package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseToSellFilterMessage(
  areaId: Int,
  atLeastNbRoom: Byte,
  atLeastNbChest: Byte,
  skillRequested: Short,
  maxPrice: Long,
  orderBy: Byte
) extends Message(6137)

object HouseToSellFilterMessage {
  implicit val codec: Codec[HouseToSellFilterMessage] =
    new Codec[HouseToSellFilterMessage] {
      def decode: Get[HouseToSellFilterMessage] =
        for {
          areaId <- int.decode
          atLeastNbRoom <- byte.decode
          atLeastNbChest <- byte.decode
          skillRequested <- varShort.decode
          maxPrice <- varLong.decode
          orderBy <- byte.decode
        } yield HouseToSellFilterMessage(areaId, atLeastNbRoom, atLeastNbChest, skillRequested, maxPrice, orderBy)

      def encode(value: HouseToSellFilterMessage): ByteVector =
        int.encode(value.areaId) ++
        byte.encode(value.atLeastNbRoom) ++
        byte.encode(value.atLeastNbChest) ++
        varShort.encode(value.skillRequested) ++
        varLong.encode(value.maxPrice) ++
        byte.encode(value.orderBy)
    }
}
