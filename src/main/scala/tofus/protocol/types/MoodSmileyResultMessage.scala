package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MoodSmileyResultMessage(
  resultCode: Byte,
  smileyId: Short
) extends Message(6196)

object MoodSmileyResultMessage {
  implicit val codec: Codec[MoodSmileyResultMessage] =
    new Codec[MoodSmileyResultMessage] {
      def decode: Get[MoodSmileyResultMessage] =
        for {
          resultCode <- byte.decode
          smileyId <- varShort.decode
        } yield MoodSmileyResultMessage(resultCode, smileyId)

      def encode(value: MoodSmileyResultMessage): ByteVector =
        byte.encode(value.resultCode) ++
        varShort.encode(value.smileyId)
    }
}
