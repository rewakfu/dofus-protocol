package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendsGetListMessage(

) extends Message(4001)

object FriendsGetListMessage {
  implicit val codec: Codec[FriendsGetListMessage] =
    Codec.const(FriendsGetListMessage())
}
