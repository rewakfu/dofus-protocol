package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObtainedItemMessage(
  genericId: Short,
  baseQuantity: Int
) extends Message(6519)

object ObtainedItemMessage {
  implicit val codec: Codec[ObtainedItemMessage] =
    new Codec[ObtainedItemMessage] {
      def decode: Get[ObtainedItemMessage] =
        for {
          genericId <- varShort.decode
          baseQuantity <- varInt.decode
        } yield ObtainedItemMessage(genericId, baseQuantity)

      def encode(value: ObtainedItemMessage): ByteVector =
        varShort.encode(value.genericId) ++
        varInt.encode(value.baseQuantity)
    }
}
