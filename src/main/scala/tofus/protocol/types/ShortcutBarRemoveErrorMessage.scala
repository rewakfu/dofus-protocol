package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarRemoveErrorMessage(
  error: Byte
) extends Message(6222)

object ShortcutBarRemoveErrorMessage {
  implicit val codec: Codec[ShortcutBarRemoveErrorMessage] =
    new Codec[ShortcutBarRemoveErrorMessage] {
      def decode: Get[ShortcutBarRemoveErrorMessage] =
        for {
          error <- byte.decode
        } yield ShortcutBarRemoveErrorMessage(error)

      def encode(value: ShortcutBarRemoveErrorMessage): ByteVector =
        byte.encode(value.error)
    }
}
