package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ServerSessionConstantLong(
  id: Short,
  value: Double
) extends ServerSessionConstant {
  override val protocolId = 429
}

object ServerSessionConstantLong {
  implicit val codec: Codec[ServerSessionConstantLong] =
    new Codec[ServerSessionConstantLong] {
      def decode: Get[ServerSessionConstantLong] =
        for {
          id <- varShort.decode
          value <- double.decode
        } yield ServerSessionConstantLong(id, value)

      def encode(value: ServerSessionConstantLong): ByteVector =
        varShort.encode(value.id) ++
        double.encode(value.value)
    }
}
