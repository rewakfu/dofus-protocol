package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountsPaddockRemoveMessage(
  mountsId: List[Int]
) extends Message(6559)

object ExchangeMountsPaddockRemoveMessage {
  implicit val codec: Codec[ExchangeMountsPaddockRemoveMessage] =
    new Codec[ExchangeMountsPaddockRemoveMessage] {
      def decode: Get[ExchangeMountsPaddockRemoveMessage] =
        for {
          mountsId <- list(ushort, varInt).decode
        } yield ExchangeMountsPaddockRemoveMessage(mountsId)

      def encode(value: ExchangeMountsPaddockRemoveMessage): ByteVector =
        list(ushort, varInt).encode(value.mountsId)
    }
}
