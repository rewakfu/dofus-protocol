package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolSelectRequestMessage(
  flags0: Byte,
  idolId: Short
) extends Message(6587)

object IdolSelectRequestMessage {
  implicit val codec: Codec[IdolSelectRequestMessage] =
    new Codec[IdolSelectRequestMessage] {
      def decode: Get[IdolSelectRequestMessage] =
        for {
          flags0 <- byte.decode
          idolId <- varShort.decode
        } yield IdolSelectRequestMessage(flags0, idolId)

      def encode(value: IdolSelectRequestMessage): ByteVector =
        byte.encode(value.flags0) ++
        varShort.encode(value.idolId)
    }
}
