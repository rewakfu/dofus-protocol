package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightResumeWithSlavesMessage(
  effects: List[FightDispellableEffectExtendedInformations],
  marks: List[GameActionMark],
  gameTurn: Short,
  fightStart: Int,
  idols: List[ConcreteIdol],
  fxTriggerCounts: List[GameFightEffectTriggerCount],
  spellCooldowns: List[GameFightSpellCooldown],
  summonCount: Byte,
  bombCount: Byte,
  slavesInfo: List[GameFightResumeSlaveInfo]
) extends Message(6215)

object GameFightResumeWithSlavesMessage {
  implicit val codec: Codec[GameFightResumeWithSlavesMessage] =
    new Codec[GameFightResumeWithSlavesMessage] {
      def decode: Get[GameFightResumeWithSlavesMessage] =
        for {
          effects <- list(ushort, Codec[FightDispellableEffectExtendedInformations]).decode
          marks <- list(ushort, Codec[GameActionMark]).decode
          gameTurn <- varShort.decode
          fightStart <- int.decode
          idols <- list(ushort, Codec[ConcreteIdol]).decode
          fxTriggerCounts <- list(ushort, Codec[GameFightEffectTriggerCount]).decode
          spellCooldowns <- list(ushort, Codec[GameFightSpellCooldown]).decode
          summonCount <- byte.decode
          bombCount <- byte.decode
          slavesInfo <- list(ushort, Codec[GameFightResumeSlaveInfo]).decode
        } yield GameFightResumeWithSlavesMessage(effects, marks, gameTurn, fightStart, idols, fxTriggerCounts, spellCooldowns, summonCount, bombCount, slavesInfo)

      def encode(value: GameFightResumeWithSlavesMessage): ByteVector =
        list(ushort, Codec[FightDispellableEffectExtendedInformations]).encode(value.effects) ++
        list(ushort, Codec[GameActionMark]).encode(value.marks) ++
        varShort.encode(value.gameTurn) ++
        int.encode(value.fightStart) ++
        list(ushort, Codec[ConcreteIdol]).encode(value.idols) ++
        list(ushort, Codec[GameFightEffectTriggerCount]).encode(value.fxTriggerCounts) ++
        list(ushort, Codec[GameFightSpellCooldown]).encode(value.spellCooldowns) ++
        byte.encode(value.summonCount) ++
        byte.encode(value.bombCount) ++
        list(ushort, Codec[GameFightResumeSlaveInfo]).encode(value.slavesInfo)
    }
}
