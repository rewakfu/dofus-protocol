package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildLevelUpMessage(
  newLevel: Short
) extends Message(6062)

object GuildLevelUpMessage {
  implicit val codec: Codec[GuildLevelUpMessage] =
    new Codec[GuildLevelUpMessage] {
      def decode: Get[GuildLevelUpMessage] =
        for {
          newLevel <- ubyte.decode
        } yield GuildLevelUpMessage(newLevel)

      def encode(value: GuildLevelUpMessage): ByteVector =
        ubyte.encode(value.newLevel)
    }
}
