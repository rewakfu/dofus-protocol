package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntStepDig(

) extends ProtocolType {
  override val protocolId = 465
}

object TreasureHuntStepDig {
  implicit val codec: Codec[TreasureHuntStepDig] =
    Codec.const(TreasureHuntStepDig())
}
