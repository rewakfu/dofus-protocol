package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ObjectItemMinimalInformation extends Item

final case class ConcreteObjectItemMinimalInformation(
  objectGID: Short,
  effects: List[ObjectEffect]
) extends ObjectItemMinimalInformation {
  override val protocolId = 124
}

object ConcreteObjectItemMinimalInformation {
  implicit val codec: Codec[ConcreteObjectItemMinimalInformation] =  
    new Codec[ConcreteObjectItemMinimalInformation] {
      def decode: Get[ConcreteObjectItemMinimalInformation] =
        for {
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
        } yield ConcreteObjectItemMinimalInformation(objectGID, effects)

      def encode(value: ConcreteObjectItemMinimalInformation): ByteVector =
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects)
    }
}

object ObjectItemMinimalInformation {
  implicit val codec: Codec[ObjectItemMinimalInformation] =
    new Codec[ObjectItemMinimalInformation] {
      def decode: Get[ObjectItemMinimalInformation] =
        ushort.decode.flatMap {
          case 124 => Codec[ConcreteObjectItemMinimalInformation].decode
          case 352 => Codec[ObjectItemToSellInNpcShop].decode
          case 387 => Codec[ObjectItemInformationWithQuantity].decode
        }

      def encode(value: ObjectItemMinimalInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteObjectItemMinimalInformation => Codec[ConcreteObjectItemMinimalInformation].encode(i)
          case i: ObjectItemToSellInNpcShop => Codec[ObjectItemToSellInNpcShop].encode(i)
          case i: ObjectItemInformationWithQuantity => Codec[ObjectItemInformationWithQuantity].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
