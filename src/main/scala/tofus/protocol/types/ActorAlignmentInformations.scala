package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ActorAlignmentInformations extends ProtocolType

final case class ConcreteActorAlignmentInformations(
  alignmentSide: Byte,
  alignmentValue: Byte,
  alignmentGrade: Byte,
  characterPower: Double
) extends ActorAlignmentInformations {
  override val protocolId = 201
}

object ConcreteActorAlignmentInformations {
  implicit val codec: Codec[ConcreteActorAlignmentInformations] =  
    new Codec[ConcreteActorAlignmentInformations] {
      def decode: Get[ConcreteActorAlignmentInformations] =
        for {
          alignmentSide <- byte.decode
          alignmentValue <- byte.decode
          alignmentGrade <- byte.decode
          characterPower <- double.decode
        } yield ConcreteActorAlignmentInformations(alignmentSide, alignmentValue, alignmentGrade, characterPower)

      def encode(value: ConcreteActorAlignmentInformations): ByteVector =
        byte.encode(value.alignmentSide) ++
        byte.encode(value.alignmentValue) ++
        byte.encode(value.alignmentGrade) ++
        double.encode(value.characterPower)
    }
}

object ActorAlignmentInformations {
  implicit val codec: Codec[ActorAlignmentInformations] =
    new Codec[ActorAlignmentInformations] {
      def decode: Get[ActorAlignmentInformations] =
        ushort.decode.flatMap {
          case 201 => Codec[ConcreteActorAlignmentInformations].decode
          case 202 => Codec[ActorExtendedAlignmentInformations].decode
        }

      def encode(value: ActorAlignmentInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteActorAlignmentInformations => Codec[ConcreteActorAlignmentInformations].encode(i)
          case i: ActorExtendedAlignmentInformations => Codec[ActorExtendedAlignmentInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
