package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntLegendaryRequestMessage(
  legendaryId: Short
) extends Message(6499)

object TreasureHuntLegendaryRequestMessage {
  implicit val codec: Codec[TreasureHuntLegendaryRequestMessage] =
    new Codec[TreasureHuntLegendaryRequestMessage] {
      def decode: Get[TreasureHuntLegendaryRequestMessage] =
        for {
          legendaryId <- varShort.decode
        } yield TreasureHuntLegendaryRequestMessage(legendaryId)

      def encode(value: TreasureHuntLegendaryRequestMessage): ByteVector =
        varShort.encode(value.legendaryId)
    }
}
