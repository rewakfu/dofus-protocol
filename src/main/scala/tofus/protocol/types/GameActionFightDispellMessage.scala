package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightDispellMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  verboseCast: Boolean
) extends Message(5533)

object GameActionFightDispellMessage {
  implicit val codec: Codec[GameActionFightDispellMessage] =
    new Codec[GameActionFightDispellMessage] {
      def decode: Get[GameActionFightDispellMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          verboseCast <- bool.decode
        } yield GameActionFightDispellMessage(actionId, sourceId, targetId, verboseCast)

      def encode(value: GameActionFightDispellMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        bool.encode(value.verboseCast)
    }
}
