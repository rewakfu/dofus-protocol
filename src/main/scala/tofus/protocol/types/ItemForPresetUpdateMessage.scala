package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ItemForPresetUpdateMessage(
  presetId: Short,
  presetItem: ItemForPreset
) extends Message(6760)

object ItemForPresetUpdateMessage {
  implicit val codec: Codec[ItemForPresetUpdateMessage] =
    new Codec[ItemForPresetUpdateMessage] {
      def decode: Get[ItemForPresetUpdateMessage] =
        for {
          presetId <- short.decode
          presetItem <- Codec[ItemForPreset].decode
        } yield ItemForPresetUpdateMessage(presetId, presetItem)

      def encode(value: ItemForPresetUpdateMessage): ByteVector =
        short.encode(value.presetId) ++
        Codec[ItemForPreset].encode(value.presetItem)
    }
}
