package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItem(
  position: Short,
  objectGID: Short,
  effects: List[ObjectEffect],
  objectUID: Int,
  quantity: Int
) extends Item {
  override val protocolId = 37
}

object ObjectItem {
  implicit val codec: Codec[ObjectItem] =
    new Codec[ObjectItem] {
      def decode: Get[ObjectItem] =
        for {
          position <- short.decode
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ObjectItem(position, objectGID, effects, objectUID, quantity)

      def encode(value: ObjectItem): ByteVector =
        short.encode(value.position) ++
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
