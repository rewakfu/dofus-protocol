package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachRoomLockedMessage(

) extends Message(6862)

object BreachRoomLockedMessage {
  implicit val codec: Codec[BreachRoomLockedMessage] =
    Codec.const(BreachRoomLockedMessage())
}
