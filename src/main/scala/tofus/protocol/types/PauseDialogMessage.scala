package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PauseDialogMessage(
  dialogType: Byte
) extends Message(6012)

object PauseDialogMessage {
  implicit val codec: Codec[PauseDialogMessage] =
    new Codec[PauseDialogMessage] {
      def decode: Get[PauseDialogMessage] =
        for {
          dialogType <- byte.decode
        } yield PauseDialogMessage(dialogType)

      def encode(value: PauseDialogMessage): ByteVector =
        byte.encode(value.dialogType)
    }
}
