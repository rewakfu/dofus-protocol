package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InventoryContentMessage(
  objects: List[ObjectItem],
  kamas: Long
) extends Message(3016)

object InventoryContentMessage {
  implicit val codec: Codec[InventoryContentMessage] =
    new Codec[InventoryContentMessage] {
      def decode: Get[InventoryContentMessage] =
        for {
          objects <- list(ushort, Codec[ObjectItem]).decode
          kamas <- varLong.decode
        } yield InventoryContentMessage(objects, kamas)

      def encode(value: InventoryContentMessage): ByteVector =
        list(ushort, Codec[ObjectItem]).encode(value.objects) ++
        varLong.encode(value.kamas)
    }
}
