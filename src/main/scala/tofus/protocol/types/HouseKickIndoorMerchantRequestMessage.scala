package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseKickIndoorMerchantRequestMessage(
  cellId: Short
) extends Message(5661)

object HouseKickIndoorMerchantRequestMessage {
  implicit val codec: Codec[HouseKickIndoorMerchantRequestMessage] =
    new Codec[HouseKickIndoorMerchantRequestMessage] {
      def decode: Get[HouseKickIndoorMerchantRequestMessage] =
        for {
          cellId <- varShort.decode
        } yield HouseKickIndoorMerchantRequestMessage(cellId)

      def encode(value: HouseKickIndoorMerchantRequestMessage): ByteVector =
        varShort.encode(value.cellId)
    }
}
