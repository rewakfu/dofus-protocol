package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareCriteria(
  `type`: Byte,
  params: List[Int]
) extends ProtocolType {
  override val protocolId = 501
}

object DareCriteria {
  implicit val codec: Codec[DareCriteria] =
    new Codec[DareCriteria] {
      def decode: Get[DareCriteria] =
        for {
          `type` <- byte.decode
          params <- list(ushort, int).decode
        } yield DareCriteria(`type`, params)

      def encode(value: DareCriteria): ByteVector =
        byte.encode(value.`type`) ++
        list(ushort, int).encode(value.params)
    }
}
