package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SequenceEndMessage(
  actionId: Short,
  authorId: Double,
  sequenceType: Byte
) extends Message(956)

object SequenceEndMessage {
  implicit val codec: Codec[SequenceEndMessage] =
    new Codec[SequenceEndMessage] {
      def decode: Get[SequenceEndMessage] =
        for {
          actionId <- varShort.decode
          authorId <- double.decode
          sequenceType <- byte.decode
        } yield SequenceEndMessage(actionId, authorId, sequenceType)

      def encode(value: SequenceEndMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.authorId) ++
        byte.encode(value.sequenceType)
    }
}
