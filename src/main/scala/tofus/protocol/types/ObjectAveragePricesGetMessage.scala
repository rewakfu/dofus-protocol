package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectAveragePricesGetMessage(

) extends Message(6334)

object ObjectAveragePricesGetMessage {
  implicit val codec: Codec[ObjectAveragePricesGetMessage] =
    Codec.const(ObjectAveragePricesGetMessage())
}
