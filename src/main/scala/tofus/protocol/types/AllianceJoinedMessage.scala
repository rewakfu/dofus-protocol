package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceJoinedMessage(
  allianceInfo: ConcreteAllianceInformations,
  enabled: Boolean,
  leadingGuildId: Int
) extends Message(6402)

object AllianceJoinedMessage {
  implicit val codec: Codec[AllianceJoinedMessage] =
    new Codec[AllianceJoinedMessage] {
      def decode: Get[AllianceJoinedMessage] =
        for {
          allianceInfo <- Codec[ConcreteAllianceInformations].decode
          enabled <- bool.decode
          leadingGuildId <- varInt.decode
        } yield AllianceJoinedMessage(allianceInfo, enabled, leadingGuildId)

      def encode(value: AllianceJoinedMessage): ByteVector =
        Codec[ConcreteAllianceInformations].encode(value.allianceInfo) ++
        bool.encode(value.enabled) ++
        varInt.encode(value.leadingGuildId)
    }
}
