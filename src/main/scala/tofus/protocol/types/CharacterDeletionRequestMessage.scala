package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterDeletionRequestMessage(
  characterId: Long,
  secretAnswerHash: String
) extends Message(165)

object CharacterDeletionRequestMessage {
  implicit val codec: Codec[CharacterDeletionRequestMessage] =
    new Codec[CharacterDeletionRequestMessage] {
      def decode: Get[CharacterDeletionRequestMessage] =
        for {
          characterId <- varLong.decode
          secretAnswerHash <- utf8(ushort).decode
        } yield CharacterDeletionRequestMessage(characterId, secretAnswerHash)

      def encode(value: CharacterDeletionRequestMessage): ByteVector =
        varLong.encode(value.characterId) ++
        utf8(ushort).encode(value.secretAnswerHash)
    }
}
