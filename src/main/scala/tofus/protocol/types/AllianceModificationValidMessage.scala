package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceModificationValidMessage(
  allianceName: String,
  allianceTag: String,
  Alliancemblem: GuildEmblem
) extends Message(6450)

object AllianceModificationValidMessage {
  implicit val codec: Codec[AllianceModificationValidMessage] =
    new Codec[AllianceModificationValidMessage] {
      def decode: Get[AllianceModificationValidMessage] =
        for {
          allianceName <- utf8(ushort).decode
          allianceTag <- utf8(ushort).decode
          Alliancemblem <- Codec[GuildEmblem].decode
        } yield AllianceModificationValidMessage(allianceName, allianceTag, Alliancemblem)

      def encode(value: AllianceModificationValidMessage): ByteVector =
        utf8(ushort).encode(value.allianceName) ++
        utf8(ushort).encode(value.allianceTag) ++
        Codec[GuildEmblem].encode(value.Alliancemblem)
    }
}
