name := "tofus-protocol"

organization := "rewakfu"

version := "0.0.6"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core"   % "2.1.1",
  "co.fs2"        %% "fs2-core"    % "2.2.1",
  "org.scodec"    %% "scodec-cats" % "1.0.0",
  "org.scalatest" %% "scalatest"   % "3.1.1" % "test"
)

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))
bintrayVcsUrl := Some("git@gitlab.com:rewakfu/dofus-protocol.git")
bintrayRepository := "maven"

sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false
